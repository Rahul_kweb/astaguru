﻿using Astaguru_webapi.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Repository.Lib;
namespace Astaguru_webapi.Services.UserService
{
    public class CurrentAuctionService
    {
        Utility util = new Utility();
        Log log = new Log();
        //public List<Auction> GetCurrentAuctionList(string Subquery, int Page)
        //{
        //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
        //    {
        //        var para = new DynamicParameters();
        //        if (Subquery.Length > 0)
        //        {
        //            var Listscheme = con.Query<Auction>("select acution.*, artist.firstname,artist.lastname,(case when len(artist.firstname + ' ' + artist.lastname) > 13 then left(ltrim(artist.firstname + ' ' + artist.lastname),13) + '...' else (artist.firstname + ' ' + artist.lastname) end) as name, medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId from acution, artist, medium, category, style,AuctionList where " + Subquery + " acution.artistid = artist.artistid and acution.mediumid = medium.mediumid and acution.categoryid=category.categoryid and AuctionList.AuctionId= acution.Online and acution.styleid=style.styleid and acution.online = 43 ORDER BY acution.productid OFFSET " + Page + " ROWS FETCH NEXT 20 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
        //            return Listscheme;
        //        }
        //        else
        //        {
        //            var Listscheme = con.Query<Auction>("select acution.*, artist.firstname,artist.lastname,(case when len(artist.firstname + ' ' + artist.lastname) > 13 then left(ltrim(artist.firstname + ' ' + artist.lastname),13) + '...' else (artist.firstname + ' ' + artist.lastname) end) as name, medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId from acution, artist, medium, category, style,AuctionList where acution.artistid = artist.artistid and acution.mediumid = medium.mediumid and acution.categoryid=category.categoryid and AuctionList.AuctionId= acution.Online and acution.styleid=style.styleid and acution.online = 43 ORDER BY acution.productid OFFSET " + Page + " ROWS FETCH NEXT 20 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
        //            return Listscheme;
        //        }
        //    }
        //}

        public List<Auction> GetCurrentAuctionList(string Subquery, int Page)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    if (Subquery.Length > 0 && Subquery != null)
                    {
                        var Listscheme = con.Query<Auction>("select * from v2_defaultlots WHERE " + Subquery + " ORDER BY cast(v2_defaultlots.reference as int) ASC OFFSET " + Page + " ROWS FETCH NEXT 20 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
                        con.Close();
                        return Listscheme;
                    }
                    else
                    {
                        var Listscheme = con.Query<Auction>("select * from v2_defaultlots ORDER BY cast(v2_defaultlots.reference as int) ASC OFFSET " + Page + " ROWS FETCH NEXT 20 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
                        con.Close();
                        return Listscheme;
                    }
                }

                    catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }

        public List<Auction> GetLatestBid()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    //var Listscheme = con.Query<Auction>("select * from v2_lotslatest", null, null, true, 0, CommandType.Text).ToList();

                    //created on 31_1_2020
                    #region
                    var Listscheme = con.Query<Auction>("select top 20 * from v2_lotslatest order by  currentDate desc", null, null, true, 0, CommandType.Text).ToList();
                    #endregion
                    con.Close();
                    return Listscheme;
                }

                    catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }


        //public double GetCurrentAuctionCount(string Subquery)
        //{
        //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
        //    {
        //        var para = new DynamicParameters();
        //        if (Subquery.Length > 0)
        //        {
        //            var Listscheme = con.Query<double>("select COUNT(acution.productid) from acution, artist, medium, category, style,AuctionList where " + Subquery + " acution.artistid = artist.artistid and acution.mediumid = medium.mediumid and acution.categoryid=category.categoryid and AuctionList.AuctionId= acution.Online and acution.styleid=style.styleid and acution.online = 43", null, null, true, 0, CommandType.Text).SingleOrDefault();
        //            return Listscheme;
        //        }
        //        else
        //        {
        //            var Listscheme = con.Query<double>("select COUNT(acution.productid) from acution, artist, medium, category, style,AuctionList where acution.artistid = artist.artistid and acution.mediumid = medium.mediumid and acution.categoryid=category.categoryid and AuctionList.AuctionId= acution.Online and acution.styleid=style.styleid and acution.online = 43", null, null, true, 0, CommandType.Text).SingleOrDefault();
        //            return Listscheme;
        //        }

        //        //para.Add("@Mode", "GetCurrentAuctionList");
        //        //return con.Query<Auction>("CRUDCurrentAuction", para, null, true, 0, CommandType.StoredProcedure).ToList();
        //    }
        //}

        public double GetCurrentAuctionCount(string Subquery)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    if (Subquery.Length > 0)
                    {
                        var Listscheme = con.Query<double>("select COUNT(v2_defaultlots.productid) from v2_defaultlots WHERE " + Subquery + "", null, null, true, 0, CommandType.Text).SingleOrDefault();
                        con.Close();
                        return Listscheme;
                    }
                    else
                    {
                        var Listscheme = con.Query<double>("select COUNT(v2_defaultlots.productid) from v2_defaultlots", null, null, true, 0, CommandType.Text).SingleOrDefault();
                        con.Close();
                        return Listscheme;
                    }
                }
             

                 catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
            }
        }


        public Auction GetCurrentAuctionDetail(int productid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetCurrentAuctionDetail");
                    para.Add("@productid", productid);
                   var val=con.Query<Auction>("CRUDCurrentAuction", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                        return val;
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }

        public Auction GetAuctionDetail(int productid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetAuctionDetail");
                    para.Add("@productid", productid);
                    var val= con.Query<Auction>("CRUDCurrentAuction", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                    return val;
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }

        public int CheckArtistAssginedbidUserlist(Auction AUC)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "CheckArtistAssginedbidUserlist");
                    para.Add("@userid", AUC.userid);
                    para.Add("@artistid", AUC.productid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
            }
            return value;
        }

        public int AddArtisttoBidUserList(Auction AUC)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "AddArtisttoBidUserList");
                para.Add("@userid", AUC.userid);
                para.Add("@artistid", AUC.productid);
                value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }
            return value;
        }

        public int InsertBidRecord(Auction AUC)
        {
            //created on 13_02_2020
            //var value = (dynamic)null;
            var value = 0;
            //var checkduplicatebid = 0;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString());
            DataTable dt = new DataTable();
            SqlDataAdapter ad = new SqlDataAdapter("select Bidpricers from bidrecord where productid=" + AUC.productid + " and UserId=" + AUC.userid + " and Bidpricers=" + AUC.nextValidBidRs + "", con);
            ad.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                using (SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
                {
                    try
                    {
                        var para = new DynamicParameters();
                        para.Add("@Mode", "InsertBidRecordMob");
                        para.Add("@Firstname", AUC.firstname);
                        para.Add("@Lastname", AUC.lastname);
                        para.Add("@Thumbnail", AUC.thumbnail);
                        para.Add("@Bidpricers", AUC.nextValidBidRs);
                        para.Add("@Bidpriceus", AUC.nextValidBidUs);
                        para.Add("@Reference", AUC.reference);
                        para.Add("@anoname", AUC.nickname);
                        para.Add("@validbidpricers", AUC.pricers);
                        para.Add("@validbidpriceus", AUC.priceus);
                        para.Add("@currentbid", AUC.currentbid);
                        para.Add("@recentbid", AUC.recentbid);
                        para.Add("@UserId", AUC.userid);
                        para.Add("@Username", AUC.username);
                        para.Add("@productid", AUC.productid);
                        para.Add("@Auctionid", AUC.Online);
                        para.Add("@proxy", AUC.proxy);

                        para.Add("@browserName", AUC.browserName);
                        para.Add("@latitude", AUC.latitude);
                        para.Add("@longitude", AUC.longitude);
                        para.Add("@ipAddress", AUC.ipAddress);

                        value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                        con.Close();
                        return value;
                    }


                    catch (Exception ex)
                    {
                        con.Close();
                        log.logErrorMessage(ex.Message);
                        log.logErrorMessage(ex.StackTrace);
                        return 0;
                    }
                }

            }

            return value;


        }

        public int InsertProxyBidRecord(Auction AUC)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "InsertProxyBidRecord");
                    para.Add("@UserId", AUC.userid);
                    para.Add("@productid", AUC.productid);
                    para.Add("@ProxyAmt", AUC.ProxyAmt);
                    para.Add("@ProxyAmtus", AUC.ProxyAmtus);
                    para.Add("@Createdby", AUC.username);
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@Createdby", AUC.username);
                    para.Add("@BidBy", 0);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
            }
            return value;
        }
        public int UpdateAcutionPrice(Auction AUC)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "UpdateAcutionPrice");
                para.Add("@pricers", AUC.nextValidBidRs);
                para.Add("@priceus", AUC.nextValidBidUs);
                para.Add("@productid", AUC.productid);
                value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();

            }
            return value;
        }

        public int UpdateBidClosingTime(int productid)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "UpdateBidClosingTime");
                para.Add("@productid", productid);
                value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }
            return value;
        }

        public int GetUserBiderOwner(string reference)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "GetUserBiderOwner");
                para.Add("@Reference", reference);
                value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }
            return value;
        }

        public List<Auction> GetProxyInfo(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetProxyInfo");
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@productid", AUC.productid);
                    para.Add("@price", AUC.pricers);
                    para.Add("@UserId", AUC.userid);
                    var val= con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                    return val;
                }
                

                    catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }

        public List<Auction> GetUpdatedProxyInfo(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetUpdatedProxyInfo");
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@productid", AUC.productid);
                    para.Add("@price", AUC.curprice);
                    var val=  con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                    return val;
                }
               

                 catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }


        public List<Auction> GetAcutionData(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "GetAcutionData");
                para.Add("@Auctionid", AUC.Online);
                para.Add("@productid", AUC.productid);
                return con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
            }
        }

        public int UpdateRecentCurrentBid(Auction AUC)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "UpdateRecentCurrentBid");
                    para.Add("@currentbid", AUC.currentbid);
                    para.Add("@recentbid", AUC.recentbid);
                    para.Add("@Bidrecordid", AUC.Bidrecordid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }

            }
            return value;
        }

        public List<Auction> getBidUserList(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "getBidUserList");
                    para.Add("@productid", AUC.productid);
                    para.Add("@userid", AUC.userid);
                   var val=  con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                    return val;
                }
             

                      catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }

        public List<Auction> getBidRecordList(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "getBidRecordList");
                    para.Add("@productid", AUC.productid);
                    return con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }
                
                    catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }

        public int updateBid(Auction AUC)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "updateBid");
                    para.Add("@currentbid", AUC.currentbid);
                    para.Add("@recentbid", AUC.recentbid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }
                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }

            }
            return value;
        }

        public int updateRecentBid(Auction AUC)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "updateRecentBid");
                para.Add("@recentbid", AUC.recentbid);
                para.Add("@Bidrecordid", AUC.Bidrecordid);
                value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }
            return value;
        }


        public List<Auction> GetProxyDetails(int productid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetProxyDetails");
                    para.Add("@productid", productid);
                    var val= con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                    return val;
                }
               

                    catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }

        public List<Auction> GetBidHistory(int productid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetBidHistory");
                    para.Add("@productid", productid);
                   var val=  con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                    return val;
                }
                

                  catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
            }
        }

        public Auction GetAdditionalCharges(int productid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "GetAdditionalCharges");
                para.Add("@productid", productid);
                return con.Query<Auction>("CRUDCurrentAuction", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
            }
        }


        //public List<Auction> GetLatestBid(int auctionstyle, int auctionId)
        //{
        //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
        //    {
        //        var para = new DynamicParameters();
        //        if (auctionstyle == 1)
        //        {
        //            var Listscheme = con.Query<Auction>("select Top 20 acution.*,artist.firstname,artist.lastname,(case when len(artist.firstname + ' ' + artist.lastname) > 13 then left(ltrim(artist.firstname + ' ' + artist.lastname),13) + '...' else (artist.firstname + ' ' + artist.lastname) end) as name,  medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId,bidrecord.bidpricers,bidrecord.daterec  from bidrecord join (((((acution join medium on acution.mediumid = medium.mediumid) join category on acution.categoryid = category.categoryid) join style on acution.styleid = style.styleid) join AuctionList on acution.Online=AuctionList.AuctionId) join artist on acution.artistid = artist.artistid) on bidrecord.Reference = acution.reference and acution.Online = " + auctionId + " where bidrecord.recentbid=1 order by daterec desc", null, null, true, 0, CommandType.Text).ToList();
        //            return Listscheme;
        //        }
        //        else if (auctionstyle == 2)
        //        {
        //            var Listscheme = con.Query<Auction>("select Top 20 acution.*,'' as firstname,'' as lastname,(case when len(title) > 13 then left(title,13) + '...' else title end) as name, medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId,bidrecord.bidpricers,bidrecord.daterec  from bidrecord join ((((acution join medium on acution.mediumid = medium.mediumid) join category on acution.categoryid = category.categoryid) join style on acution.styleid = style.styleid) join AuctionList on acution.Online=AuctionList.AuctionId) on bidrecord.Reference = acution.reference and acution.Online = " +auctionId+ " where bidrecord.recentbid=1 order by daterec desc", null, null, true, 0, CommandType.Text).ToList();
        //            return Listscheme;
        //        }
        //        else
        //        {
        //            var Listscheme = con.Query<Auction>("select Top 20 acution.*,'' as firstname,'' as lastname,(case when len(title) > 13 then left(title,13) + '...' else title end) as name, medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId,bidrecord.bidpricers,bidrecord.daterec  from bidrecord join ((((acution join medium on acution.mediumid = medium.mediumid) join category on acution.categoryid = category.categoryid) join style on acution.styleid = style.styleid) join AuctionList on acution.Online=AuctionList.AuctionId) on bidrecord.Reference = acution.reference and acution.Online = " + auctionId + " where bidrecord.recentbid=1 order by daterec desc", null, null, true, 0, CommandType.Text).ToList();
        //            return Listscheme;
        //        }

        //        //para.Add("@Mode", "GetCurrentAuctionList");
        //        //return con.Query<Auction>("CRUDCurrentAuction", para, null, true, 0, CommandType.StoredProcedure).ToList();
        //    }
        //}


        //public List<Auction> GetHighValueBid(int auctionstyle, int auctionId)
        //{
        //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
        //    {
        //        var para = new DynamicParameters();
        //        if (auctionstyle == 1)
        //        {
        //            var Listscheme = con.Query<Auction>("select Top 20 acution.*,artist.firstname,artist.lastname,(case when len(artist.firstname + ' ' + artist.lastname) > 13 then left(ltrim(artist.firstname + ' ' + artist.lastname),13) + '...' else (artist.firstname + ' ' + artist.lastname) end) as name,  medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId,bidrecord.bidpricers,bidrecord.daterec,(select count(bidpriceus)num from bidrecord where productid=acution.productid)Number  from bidrecord join (((((acution join medium on acution.mediumid = medium.mediumid) join category on acution.categoryid = category.categoryid) join style on acution.styleid = style.styleid) join AuctionList on acution.Online=AuctionList.AuctionId) join artist on acution.artistid = artist.artistid) on bidrecord.Reference = acution.reference and acution.Online = " + auctionId + " where bidrecord.recentbid=1 order by acution.pricers desc", null, null, true, 0, CommandType.Text).ToList();
        //            return Listscheme;
        //        }
        //        else if (auctionstyle == 2)
        //        {
        //            var Listscheme = con.Query<Auction>("select Top 20 acution.*,'' as firstname,'' as lastname,(case when len(title) > 13 then left(title,13) + '...' else title end) as name,  medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId,bidrecord.bidpricers,bidrecord.daterec,(select count(bidpriceus)num from bidrecord where productid=acution.productid)Number  from bidrecord join ((((acution join medium on acution.mediumid = medium.mediumid) join category on acution.categoryid = category.categoryid) join style on acution.styleid = style.styleid) join AuctionList on acution.Online=AuctionList.AuctionId) on bidrecord.Reference = acution.reference and acution.Online = " + auctionId+ " where bidrecord.recentbid=1 order by acution.pricers desc", null, null, true, 0, CommandType.Text).ToList();
        //            return Listscheme;
        //        }
        //        else
        //        {
        //            var Listscheme = con.Query<Auction>("select Top 20 acution.*,'' as firstname,'' as lastname,(case when len(title) > 13 then left(title,13) + '...' else title end) as name,  medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId,bidrecord.bidpricers,bidrecord.daterec,(select count(bidpriceus)num from bidrecord where productid=acution.productid)Number  from bidrecord join ((((acution join medium on acution.mediumid = medium.mediumid) join category on acution.categoryid = category.categoryid) join style on acution.styleid = style.styleid) join AuctionList on acution.Online=AuctionList.AuctionId) on bidrecord.Reference = acution.reference and acution.Online = " + auctionId + " where bidrecord.recentbid=1 order by acution.pricers desc", null, null, true, 0, CommandType.Text).ToList();
        //            return Listscheme;
        //        }

        //        //para.Add("@Mode", "GetCurrentAuctionList");
        //        //return con.Query<Auction>("CRUDCurrentAuction", para, null, true, 0, CommandType.StoredProcedure).ToList();
        //    }
        //}

        public List<Auction> GetHighValueBid()

        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                //var Listscheme = con.Query<Auction>("Select * FROM v2_lotssignificant", null, null, true, 0, CommandType.Text).ToList();
                //created on 31_1_2020
                #region
                var Listscheme = con.Query<Auction>("Select top 20 * FROM v2_lotssignificant order by pricers desc,priceus desc", null, null, true, 0, CommandType.Text).ToList();
                #endregion
                return Listscheme;
            }
        }


        //public List<Auction> GetMostPopularLots(int auctionstyle, int auctionId)
        //{
        //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
        //    {
        //        var para = new DynamicParameters();
        //        if (auctionstyle == 1)
        //        {
        //            var Listscheme = con.Query<Auction>("select Top 20 acution.*,artist.firstname,artist.lastname,(case when len(artist.firstname + ' ' + artist.lastname) > 13 then left(ltrim(artist.firstname + ' ' + artist.lastname),13) + '...' else (artist.firstname + ' ' + artist.lastname) end) as name,  medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId,bidrecord.bidpricers,bidrecord.daterec,(select count(bidpriceus)num from bidrecord where productid=acution.productid)Number  from bidrecord join (((((acution join medium on acution.mediumid = medium.mediumid) join category on acution.categoryid = category.categoryid) join style on acution.styleid = style.styleid) join AuctionList on acution.Online=AuctionList.AuctionId) join artist on acution.artistid = artist.artistid) on bidrecord.Reference = acution.reference and acution.Online = " + auctionId + " where bidrecord.recentbid=1 order by number desc", null, null, true, 0, CommandType.Text).ToList();
        //            return Listscheme;
        //        }
        //        else if (auctionstyle == 2)
        //        {
        //            var Listscheme = con.Query<Auction>("select Top 20 acution.*,'' as firstname,'' as lastname,(case when len(title) > 13 then left(title,13) + '...' else title end) as name,  medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId,bidrecord.bidpricers,bidrecord.daterec,(select count(bidpriceus)num from bidrecord where productid=acution.productid)Number  from bidrecord join ((((acution join medium on acution.mediumid = medium.mediumid) join category on acution.categoryid = category.categoryid) join style on acution.styleid = style.styleid) join AuctionList on acution.Online=AuctionList.AuctionId) on bidrecord.Reference = acution.reference and acution.Online = " + auctionId + " where bidrecord.recentbid=1 order by number desc", null, null, true, 0, CommandType.Text).ToList();
        //            return Listscheme;
        //        }
        //        else
        //        {
        //            var Listscheme = con.Query<Auction>("select Top 20 acution.*,'' as firstname,'' as lastname,(case when len(title) > 13 then left(title,13) + '...' else title end) as name,  medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId,bidrecord.bidpricers,bidrecord.daterec,(select count(bidpriceus)num from bidrecord where productid=acution.productid)Number  from bidrecord join ((((acution join medium on acution.mediumid = medium.mediumid) join category on acution.categoryid = category.categoryid) join style on acution.styleid = style.styleid) join AuctionList on acution.Online=AuctionList.AuctionId) on bidrecord.Reference = acution.reference and acution.Online = " + auctionId + " where bidrecord.recentbid=1 order by number desc", null, null, true, 0, CommandType.Text).ToList();
        //            return Listscheme;
        //        }

        //        //para.Add("@Mode", "GetCurrentAuctionList");
        //        //return con.Query<Auction>("CRUDCurrentAuction", para, null, true, 0, CommandType.StoredProcedure).ToList();
        //    }
        //}

        public List<Auction> GetMostPopularLots()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                //var Listscheme = con.Query<Auction>("select * FROM v2_lotspopular", null, null, true, 0, CommandType.Text).ToList();
                //created on 31_1_2020
                #region
                var Listscheme = con.Query<Auction>("select top 20 * FROM v2_lotspopular", null, null, true, 0, CommandType.Text).ToList();
                #endregion

                return Listscheme;
            }
        }


        //public List<Auction> GetClosingLots(int auctionstyle, int auctionId)
        //{
        //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
        //    {
        //        var para = new DynamicParameters();
        //        if (auctionstyle == 1)
        //        {
        //            var Listscheme = con.Query<Auction>("select acution.*,artist.firstname,artist.lastname,(case when len(artist.firstname + ' ' + artist.lastname) > 13 then left(ltrim(artist.firstname + ' ' + artist.lastname),13) + '...' else (artist.firstname + ' ' + artist.lastname) end) as name,  medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId from (((((acution join medium on acution.mediumid = medium.mediumid) join category on acution.categoryid = category.categoryid) join style on acution.styleid = style.styleid) join AuctionList on acution.Online=AuctionList.AuctionId) join artist on acution.artistid = artist.artistid) where acution.Online = " + auctionId+ " and DATEDIFF(minute,getdate(),bidclosingtime) > 0 and DATEDIFF(minute,getdate(),bidclosingtime) <= 30  order by bidclosingtime", null, null, true, 0, CommandType.Text).ToList();
        //            return Listscheme;
        //        }
        //        else if (auctionstyle == 2)
        //        {
        //            var Listscheme = con.Query<Auction>("select acution.*,'' as firstname,'' as lastname,(case when len(title) > 13 then left(title,13) + '...' else title end) as name, medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId  from ((((acution join medium on acution.mediumid = medium.mediumid) join category on acution.categoryid = category.categoryid) join style on acution.styleid = style.styleid) join AuctionList on acution.Online=AuctionList.AuctionId) where acution.Online = " + auctionId+ " and DATEDIFF(minute,getdate(),bidclosingtime) > 0 and DATEDIFF(minute,getdate(),bidclosingtime) <= 30  order by bidclosingtime ", null, null, true, 0, CommandType.Text).ToList();
        //            return Listscheme;
        //        }
        //        else
        //        {
        //            var Listscheme = con.Query<Auction>("select acution.*,'' as firstname,'' as lastname,(case when len(title) > 13 then left(title,13) + '...' else title end) as name, medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId  from ((((acution join medium on acution.mediumid = medium.mediumid) join category on acution.categoryid = category.categoryid) join style on acution.styleid = style.styleid) join AuctionList on acution.Online=AuctionList.AuctionId) where acution.Online = " + auctionId + " and DATEDIFF(minute,getdate(),bidclosingtime) > 0 and DATEDIFF(minute,getdate(),bidclosingtime) <= 30  order by bidclosingtime ", null, null, true, 0, CommandType.Text).ToList();
        //            return Listscheme;
        //        }

        //        //para.Add("@Mode", "GetCurrentAuctionList");
        //        //return con.Query<Auction>("CRUDCurrentAuction", para, null, true, 0, CommandType.StoredProcedure).ToList();
        //    }
        //}

        public List<Auction> GetClosingLots()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();

                var Listscheme = con.Query<Auction>("select * FROM v2_lotsclosingtime", null, null, true, 0, CommandType.Text).ToList();
                return Listscheme;
            }
        }

        public int DeleteArtisttoBidUserList(Auction AUC)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "DeleteArtisttoBidUserList");
                para.Add("@userid", AUC.userid);
                para.Add("@artistid", AUC.productid);
                value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }
            return value;
        }

        public List<Auction> GetMyPurchase(int userid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "GetMyPurchase");
                para.Add("@UserId", userid);
                return con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
            }
        }

        public Auction Getproxyinfosame(int AuctionId)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "Getproxyinfosame");
                para.Add("@AuctionId", AuctionId);
                return con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).FirstOrDefault();

            }
        }

        //new created on 20-1-2020
        #region
        public int Getcountbidproduct(int prooductid)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetcountMostPopularCount");
                    para.Add("@productid", prooductid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();

                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }

            }

            return value;
        }

        #endregion


        //created on 26-1-20
        public int Gethighestproxybytime(int productid)
        {
            int value = 0;
            DataTable dt = new DataTable();
            dt = util.Display("exec CRUDBid 'Gethighestproxybytime',0,0,'','','',0,0,'','',0,0,0,0,''," + productid + "");
            if (dt.Rows.Count > 0)
            {
                int bidpricevalue = int.Parse(dt.Rows[0]["Bidpricers"].ToString());
                DataTable dt1 = new DataTable();
                dt1 = util.Display("exec CRUDBid 'CheckmultipleGethighestproxybytime',0, 0,'','',''," + bidpricevalue + ", 0,'','',0,0,0,0,''," + productid + "");
                if (dt1.Rows.Count == 2)
                {
                    value = int.Parse(dt.Rows[0]["UserId"].ToString());

                }
            }

            return value;
        }

        //created on 28-1-2020
        #region

        public Auction Getcurrentleadinguser(int productid)                             //get leading user Bidpricers
        {

            Auction objAuction = new Auction();
            Utility util = new Utility();

            DataTable dt = new DataTable();
            dt = util.Display("exec CRUDBid 'Getcurrentleadinguser',0,0,'','','',0,0,'','',0,0,0,0,''," + productid + "");

            if (dt.Rows.Count > 0)
            {
                objAuction.userid = int.Parse(dt.Rows[0]["UserId"].ToString());
                if (dt.Rows[0]["amountlimt"].ToString() == "")
                {
                    objAuction.amountlimt = 0;
                }

                else
                {
                    objAuction.amountlimt = int.Parse(dt.Rows[0]["amountlimt"].ToString());
                }
                objAuction.Bidpricers = int.Parse(dt.Rows[0]["bidpricers"].ToString());
            }
            return objAuction;
        }



        public Auction Getcurrentpriceuser(int productid, int userid)                               //get outbid user Bidpricers
        {
            Auction objAuction = new Auction();
            DataTable dt = new DataTable();
            dt = util.Display("exec CRUDBid 'Getcurrentpriceuser'," + userid + ",0,'','','',0,0,'','',0,0,0,0,''," + productid + "");
            if (dt.Rows.Count > 0)
            {

                objAuction.Bidpricers = int.Parse(dt.Rows[0]["Bidpricers"].ToString());
                objAuction.userid = int.Parse(dt.Rows[0]["userid"].ToString());

                if (dt.Rows[0]["amountlimt"].ToString() == "")
                {
                    objAuction.amountlimt = 0;
                }

                else
                {
                    objAuction.amountlimt = int.Parse(dt.Rows[0]["amountlimt"].ToString());
                }
            }
            return objAuction;
        }

        public Auction Getcurrentleadinguserforbidclose(int productid)                                //get leadinguser after bid close  
        {

            Auction objAuction = new Auction();
            DataTable dt = new DataTable();
            dt = util.Display("exec CRUDBid 'Getcurrentleadinguserforbidclose',0,0,'','','',0,0,'','',0,0,0,0,''," + productid + "");
            if (dt.Rows.Count > 0)
            {
                objAuction.amountlimt = int.Parse(dt.Rows[0]["amountlimt"].ToString());
                objAuction.Bidpricers = int.Parse(dt.Rows[0]["Bidpricers"].ToString());
                objAuction.userid = int.Parse(dt.Rows[0]["userid"].ToString());
                objAuction.checkbidlimit = int.Parse(dt.Rows[0]["checkbidlimit"].ToString());
                objAuction.Bidrecordid = int.Parse(dt.Rows[0]["Bidrecordid"].ToString());
            }
            return objAuction;
        }


        //created on 29-1-20

        public int updatechecklimit(int userid, int Bidrecordid)                   //set flag for leading uuser after bid closed
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "Updatechecklimit");
                    para.Add("@userid", userid);
                    para.Add("@Bidrecordid", Bidrecordid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }

                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }

            }
            return value;
        }
        #endregion


        //created on 5_2_20
        public Auction GetcurrentpriceuserAfterproxycheck(int productid, int userid)                         //get proxyuser data
        {

            Auction objAuction = new Auction();
            DataTable dt = new DataTable();
            dt = util.Display("exec CRUDBid 'GetcurrentpriceuserAfterproxycheck'," + userid + ",0,'','','',0,0,'','',0,0,0,0,''," + productid + "");
            if (dt.Rows.Count > 0)
            {
                objAuction.ProxyAmt = int.Parse(dt.Rows[0]["ProxyAmt"].ToString());
                objAuction.Bidpricers = int.Parse(dt.Rows[0]["Bidpricers"].ToString());
                objAuction.userid = int.Parse(dt.Rows[0]["userid"].ToString());
                objAuction.amountlimt = int.Parse(dt.Rows[0]["amountlimt"].ToString());
                objAuction.islessproxy = int.Parse(dt.Rows[0]["islessproxy"].ToString());
            }
            return objAuction;
        }

        public int Getuserlimit(int userid)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "Getuserlimit");
                    para.Add("@userid", userid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                    return value;
                }
                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }


            }
      
        }


        //created on 6_2_2020
        public void Updateislesssproxy(int userid, int productid, int islessproxy)                 //set flag for proxy user
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "Updateislesssproxy");
                    para.Add("@userid", userid);
                    para.Add("@productid", productid);
                    para.Add("@islessproxy", islessproxy);
                    con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }
               

                    catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                
                }
            }
        }



        //Created on 12_02_20

        #region


        public void Isleading(int productid, int userid, int bidpricers, int islead)                        //Update leading user
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {

                    var para = new DynamicParameters();
                    para.Add("@Mode", "Isleading");
                    para.Add("@userid", userid);
                    para.Add("@productid", productid);
                    para.Add("@Bidpricers", bidpricers);
                    para.Add("@islead", islead);
                    con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }
                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    
                }
            }
        }


        public int getoutbidamount(int productid, int userid)                      //get outbid user
        {

            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "getoutbidamount");
                    para.Add("@userid", userid);
                    para.Add("@productid", productid);
                    value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                    return value;
                }


                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    con.Close();
                    return 0;
                }
            }

        }

        #endregion


        //created on 17_2_2020
        public List<Auction> Getsameproxy(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "Getsameproxy");
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@productid", AUC.productid);
                    para.Add("@price", AUC.nextValidBidRs);
                    var val =   con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                    return val;
                }
                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }


            }
        }


        public List<Auction> GetproxyUser(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetproxyUser");
                    para.Add("@Auctionid", AUC.Online);
                    para.Add("@productid", AUC.productid);
                    para.Add("@price", AUC.nextValidBidRs);

                    //para.Add("@price", AUC.pricers);          
                    var val = con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                    con.Close();
                        return val;
                }
                catch (Exception ex)
                {
                  
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
          
                
            }
        }

        public List<Auction> GetUpdatedProxyuser(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "GetUpdatedProxyuser");
                para.Add("@Auctionid", AUC.Online);
                para.Add("@productid", AUC.productid);
                para.Add("@price", AUC.curprice);
                para.Add("@nextvalid", AUC.nextValidBidRs);
                var val = con.Query<Auction>("CRUDBid", para, null, true, 0, CommandType.StoredProcedure).ToList();
                con.Close();
                return val;
            }
        }


        public void Isleadingproxy(int productid, int userid, int ProxyAmt, int islead)                        //Update leading user
        {

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "Isleadingproxy");
                para.Add("@userid", userid);
                para.Add("@productid", productid);
                para.Add("@islead", islead);
                para.Add("@ProxyAmt", ProxyAmt);
                con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                con.Close();
            }

        }

        public int checkforproxyuser(int productid, int userid)
        {

            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "checkforproxyuser");
                para.Add("@userid", userid);
                para.Add("@productid", productid);
                value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                con.Close();
            }
            return value;

        }

        public int getoutbidproxyamount(int productid, int userid)                      //get outbid user
        {

            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "getoutbidproxyamount");
                para.Add("@userid", userid);
                para.Add("@productid", productid);
                value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                con.Close();
            }
            return value;
        }


        public Auction GetCurrentLeadingProxyuser(int productid)                             //get leading user proxy
        {
            Auction objAuction = new Auction();
            DataTable dt = new DataTable();
            dt = util.Display("exec CRUDBid 'GetCurrentLeadingProxyuser',0,0,'','','',0,0,'','',0,0,0,0,''," + productid + "");

            if (dt.Rows.Count > 0)
            {
                objAuction.userid = int.Parse(dt.Rows[0]["UserId"].ToString());
                objAuction.amountlimt = int.Parse(dt.Rows[0]["amountlimt"].ToString());
                objAuction.ProxyAmt = int.Parse(dt.Rows[0]["ProxyAmt"].ToString());
            }
            return objAuction;


        }


        // created on 2_2_2020

        public Auction Getcurrentoutbiduser(int productid, int userid)                               //get outbid user Bidpricers
        {

            Auction objAuction = new Auction();

            DataTable dt = new DataTable();
            dt = util.Display("exec CRUDBid 'Getcurrentoutbiduser'," + userid + ",0,'','','',0,0,'','',0,0,0,0,''," + productid + "");

            if (dt.Rows.Count > 0)
            {

                objAuction.Bidpricers = int.Parse(dt.Rows[0]["Bidpricers"].ToString());
                objAuction.userid = int.Parse(dt.Rows[0]["userid"].ToString());
                if (dt.Rows[0]["amountlimt"].ToString() == "")
                {
                    objAuction.amountlimt = 0;
                }

                else
                {

                    objAuction.email = dt.Rows[0]["email"].ToString();
                }
                objAuction.username = dt.Rows[0]["username"].ToString();

                objAuction.mobile = dt.Rows[0]["mobile"].ToString();
                objAuction.name = dt.Rows[0]["name"].ToString();

            }
            return objAuction;
        }


        public int checkforproxyuserfroutbidmessage(int productid, int userid)
        {

            int proxyuser = 0;
            DataTable dt = new DataTable();
            dt = util.Display("exec CRUDBid 'checkforproxyuserfroutbidmessage'," + userid + ",0,'','','',0,0,'','',0,0,0,0,''," + productid + "");
            if (dt.Rows.Count > 0)
            {
                proxyuser = dt.Rows.Count;
            }
            return proxyuser;
        }

        public int InsertBidRecordMob(Auction AUC, string bidByVal, string deviceTocken, string OSversion, string modelName, string ipAddress, string latitude, string longitude, string fullAddress, string userLocation)
        {
            var value = (dynamic)null;
            //var checkduplicatebid = 0;

            //var checkproxyuser = AUC.proxy;
            //if (AUC.proxy != 1)
            //{
            //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            //    {
            //        var para = new DynamicParameters();
            //        para.Add("@Mode", "checkduplicateBidprice");
            //        para.Add("@productid", AUC.productid);
            //        para.Add("@Bidpricers", AUC.nextValidBidRs);
            //        checkduplicatebid = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
            //    }
            //}

            //else
            //{
            //    checkduplicatebid = 0;
            //}

            //if (checkduplicatebid == 0)
            //{
                CurrentAuctionService CAS = new CurrentAuctionService();
                Auction objproductdetail = CAS.GetCurrentAuctionDetail(AUC.productid);

                objproductdetail.nextValidBidRs = Convert.ToInt32(Astaguru_webapi.Services.Common.getNextValidBidAmount(objproductdetail.pricers, 1).ToString());

                if (AUC.nextValidBidRs > objproductdetail.pricers)
                {
                    using (SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
                {
                    try
                    {
                        var para = new DynamicParameters();
                        para.Add("@Mode", "InsertBidRecordMob");
                        para.Add("@Firstname", AUC.firstname);
                        para.Add("@Lastname", AUC.lastname);
                        para.Add("@Thumbnail", AUC.thumbnail);
                        para.Add("@Bidpricers", AUC.nextValidBidRs);
                        para.Add("@Bidpriceus", AUC.nextValidBidUs);
                        para.Add("@Reference", AUC.reference);
                        para.Add("@anoname", AUC.nickname);
                        para.Add("@validbidpricers", AUC.pricers);
                        para.Add("@validbidpriceus", AUC.priceus);
                        para.Add("@currentbid", AUC.currentbid);
                        para.Add("@recentbid", AUC.recentbid);
                        para.Add("@UserId", AUC.userid);
                        para.Add("@Username", AUC.username);
                        para.Add("@productid", AUC.productid);
                        para.Add("@Auctionid", AUC.Online);
                        para.Add("@proxy", AUC.proxy);

                        para.Add("@browserName", AUC.browserName);
                        para.Add("@latitude", latitude);
                        para.Add("@longitude", longitude);
                        para.Add("@ipAddress", ipAddress);

                        para.Add("@BidBy", bidByVal);
                        para.Add("@deviceTocken", deviceTocken);
                        para.Add("@OSversion", OSversion);
                        para.Add("@modelName", modelName);
                        para.Add("@userLocation", userLocation);
                        para.Add("@fullAddress", fullAddress);


                        value = con1.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                        con1.Close();
                    }
                    catch (Exception ex)
                    {
                        con1.Close();
                        log.logErrorMessage(ex.Message);
                        log.logErrorMessage(ex.StackTrace);
                    }

                    
                    }
                }

            //}

            return value;


        }



        public int InsertProxyBidRecordMob(Auction AUC, string bidByVal, string deviceTocken, string OSversion, string modelName, string ipAddress, string latitude, string longitude, string fullAddress, string userLocation)
        {
            var value = (dynamic)null;
            var checkduplicatebid = 0;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "checkduplicateProxyAmountUser");
                    para.Add("@productid", AUC.productid);
                    para.Add("@ProxyAmt", AUC.ProxyAmt);
                    para.Add("@userid", AUC.userid);
                    checkduplicatebid = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                    con.Close();
                }
                catch (Exception ex)
                {
                    con.Close();
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                }

            }

            if (checkduplicatebid == 0)
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                    try {
                        var para = new DynamicParameters();
                        para.Add("@Mode", "InsertProxyBidRecordMob");
                        para.Add("@UserId", AUC.userid);
                        para.Add("@productid", AUC.productid);
                        para.Add("@ProxyAmt", AUC.ProxyAmt);
                        para.Add("@ProxyAmtus", AUC.ProxyAmtus);
                        para.Add("@Createdby", AUC.username);
                        para.Add("@Auctionid", AUC.Online);
                        para.Add("@Createdby", AUC.username);
                        para.Add("@latitude", latitude);
                        para.Add("@longitude", longitude);
                        para.Add("@ipAddress", ipAddress);

                        para.Add("@BidBy", bidByVal);
                        para.Add("@deviceTocken", deviceTocken);
                        para.Add("@OSversion", OSversion);
                        para.Add("@modelName", modelName);
                        para.Add("@userLocation", userLocation);
                        para.Add("@fullAddress", fullAddress);
                        value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        con.Close();
                        log.logErrorMessage(ex.Message);
                        log.logErrorMessage(ex.StackTrace);
                    }

                }
            }
         

            return value;
        }

        public int GetCurrentAuctionId()
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "GetCurrentAuctionId");
                value = con.Query<int>("CRUDBid", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                con.Close();
            }
            return value;
            
        }

    }
}