﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class CityController : ApiController
    {

        Utility util = new Utility();
        public resourceCity Get(int stateid)
        {

            List<City> objAuctionlist = new List<City>();

            DataTable dt = new DataTable();
            dt = util.Display("select * from city where stateid=" + stateid + "");

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    City objAuction = new City();
                    objAuction.cityid = int.Parse(dr["cityid"].ToString());
                    objAuction.cityname = dr["cityname"].ToString();
                    objAuction.stateid = int.Parse(dr["stateid"].ToString());
                    


                    objAuctionlist.Add(objAuction);

                }

            }

            resourceCity objresource = new resourceCity();
            objresource.resource = objAuctionlist;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, objresource.resource);
            return objresource;

        }
    }
}
