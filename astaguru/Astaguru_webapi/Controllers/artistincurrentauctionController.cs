﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class artistincurrentauctionController : ApiController
    {
        Utility util = new Utility();
        HttpResponseMessage response;
        public resourceArtist Get()
        {
            List<Artist> objaucList = new List<Artist>();

            DataTable dt = new DataTable();
            dt = util.Display("select * from artistincurrentauction");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Artist objAuction = new Artist();

                    objAuction.FirstName = dr["FirstName"].ToString();
                    objAuction.LastName = dr["LastName"].ToString();
                    objAuction.artistid = int.Parse(dr["artistid"].ToString());


                    objaucList.Add(objAuction);

                }
            }
            resourceArtist objresource = new resourceArtist();
            objresource.resource = objaucList;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, objresource.resource);
            return objresource;

        }
    }
}
