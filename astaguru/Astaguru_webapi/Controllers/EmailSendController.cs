﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class EmailSendController : ApiController
    {
        HttpResponseMessage response;
        Responsemessage objmsg = new Responsemessage();
        Utility util = new Utility();
        public HttpResponseMessage Post(EmailSend obj)
        {

         
            foreach (var data in obj.to)
            {

                string ToEmailid = data.email;
                String[] emailid = new String[] { ToEmailid };
               
                if (util.SendEmail(obj.body_text, emailid, obj.subject, "", null))
                {
                    objmsg.msg = "Email sent successfully";
                    response = Request.CreateResponse(HttpStatusCode.OK, objmsg.msg);
                }
                else
                {
                    objmsg.msg = "Email not sent successfully";
                    response = Request.CreateResponse(HttpStatusCode.OK, objmsg.msg);
                }
            }
        
     
            return response;
        }
        }
}
