﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class CountryController : ApiController
    {
        Utility util = new Utility();
        public resourceCountry Get()
        {

            List<Country> objAuctionlist = new List<Country>();

            DataTable dt = new DataTable();
            dt = util.Display("select * from country");
         
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Country objAuction = new Country();
                    objAuction.countryid = int.Parse(dr["countryid"].ToString());
                    objAuction.countryname = dr["countryname"].ToString(); 
                    objAuction.countrycode= dr["countrycode"].ToString();


                    objAuctionlist.Add(objAuction);

                }

            }

            resourceCountry objresource = new resourceCountry();
            objresource.resource = objAuctionlist;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, objresource.resource);
            return objresource;

        }
    }
}
