﻿using Astaguru_webapi.Models;
using Astaguru_webapi.Services;
using Astaguru_webapi.Services.UserService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;



namespace Astaguru_webapi.Controllers
{
    //[EnableCors(origins: "http://localhost:56311/", headers: "*", methods: "*")]
    [EnableCors(origins: "http://asta.kwebmakerdigital.com/", headers: "*", methods: "*")]
    public class SaveproxybidController : ApiController
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ConnectionString);
        CurrentAuctionService CAS = new CurrentAuctionService();
        CommonController COMC = new CommonController();
        UserServices US = new UserServices();
        Auction auc = new Auction();
        List<Auction> listAuction = new List<Auction>();

        HttpResponseMessage response;

        public HttpResponseMessage Post(Auction AUC)
        {
            Responsemessage responsemessage = new Responsemessage();
            try
            {
                int isOldUser = AUC.isOldUser;


                string country = AUC.country;
                string username = AUC.username;
                string nickname = AUC.nickname;

                int userid = AUC.userid;   //COMC.checkUserSessionValue();

                int amountlimt = CAS.Getuserlimit(userid);

                string bidByVal = AUC.bidByVal;
                string deviceTocken = AUC.deviceTocken;
                string OSversion = AUC.OSversion;
                string modelName = AUC.modelName;
                string ipAddress = AUC.ipAddress;
                string latitude = AUC.latitude;
                string longitude = AUC.longitude;
                string fullAddress = AUC.fullAddress;
                string userLocation = AUC.userLocation;


                User UR = US.GetBillingAddress(AUC.userid); //get email address and mobile

                if (userid > 0)
                {
                    auc = CAS.GetCurrentAuctionDetail(AUC.productid);



                    auc.nextValidBidRs = Convert.ToInt32(Astaguru_webapi.Services.Common.getNextValidBidAmount(auc.pricers, 1).ToString()); // Next Valid Bid
                    auc.nextValidBidUs = auc.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); ;

                    //Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(auc.priceus, 2).ToString()); // Next Valid Bid

                    auc.userid = userid;
                    auc.ProxyAmt = AUC.ProxyAmt;
                    auc.ProxyAmtus = AUC.ProxyAmt / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); ;

                    int lastbidamountRs = auc.nextValidBidRs;
                    int lastbidamountUs = auc.nextValidBidUs;

                    int BidAmountRs = AUC.nextValidBidRs;
                    int BidAmountUs = AUC.nextValidBidUs;

                    if (isOldUser == 0)
                    {
                        if (country == "India")
                        {
                            if (AUC.ProxyAmt > amountlimt)
                            {
                                responsemessage.currentStatus = 1;
                                responsemessage.msg = "Your Bid Limit Exceeded. Kindly Contact with Your Administrator";


                                responsemessage.mobileNum = UR.Mobile;
                                responsemessage.emailID = UR.email;
                                responsemessage.Username = auc.username;
                                responsemessage.outBidAmountRs = 0;
                                responsemessage.outBidAmountUs = 0;
                                responsemessage.lastBidpriceRs = 0;
                                responsemessage.lastBidpriceUs = 0;

                          

                                response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                                return response;
                            }
                        }
                        else
                        {
                            if (auc.ProxyAmtus > amountlimt)
                            {
                                responsemessage.currentStatus = 1;
                                responsemessage.msg = "Your Bid Limit Exceeded. Kindly Contact with Your Administrator";

                                responsemessage.mobileNum = UR.Mobile;
                                responsemessage.emailID = UR.email;
                                responsemessage.Username = auc.username;
                                responsemessage.outBidAmountRs = 0;
                                responsemessage.outBidAmountUs = 0;
                                responsemessage.lastBidpriceRs = 0;
                                responsemessage.lastBidpriceUs = 0;

                               
                                response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                                return response;
                            }
                        }
                    }

                    if (auc.Ownerid == userid)
                    {
                        responsemessage.currentStatus = 7;
                        responsemessage.msg = "Sorry, you're not permitted to bid on this lot. Please contact our office for support.";
                        responsemessage.mobileNum = UR.Mobile;
                        responsemessage.emailID = UR.email; ;
                        responsemessage.Username = AUC.username;
                        responsemessage.BidAmountRs = BidAmountRs;
                        responsemessage.BidAmountUs = BidAmountUs;
                        response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                        return response;
                    }

                    // Add 3 Mins if bid closing time less than equal to 3 min

                    if (auc.timeRemains <= 180)
                    {
                        CAS.UpdateBidClosingTime(AUC.productid);
                    }


                    auc.username = username;
                    string Result = COMC.CurrentProxyMail(auc);       //created on 12_11_2020
                    int Proxyid = CAS.InsertProxyBidRecordMob(auc, bidByVal, deviceTocken, OSversion, modelName, ipAddress, latitude, longitude, fullAddress, userLocation);
                    auc.currentbid = 1;
                    auc.recentbid = 0;
                    auc.proxy = 1;
                    auc.nickname = nickname;
                    AUC.Bidrecordid = CAS.InsertBidRecord(auc);

                    // Update Bid Limit Function
                    //if (isOldUser == 0)
                    //{
                    //    int updateLimit = amountlimt - AUC.ProxyAmt;
                    //    US.UpdateBidLimit(userid, updateLimit);
                    //    Session["BidLimit"] = updateLimit;
                    //}

                    if (AUC.Bidrecordid > 0)
                    {
                        CAS.UpdateAcutionPrice(auc);
                    }
                    else
                    {
                        //Failed
                    }
                    auc.pricers = auc.nextValidBidRs; // Current bid value 
                    auc.priceus = auc.nextValidBidUs;
                    auc.curprice = auc.nextValidBidRs;

                    auc.nextValidBidRs = Convert.ToInt32(Astaguru_webapi.Services.Common.getNextValidBidAmount(auc.nextValidBidRs, 1).ToString()); // Next Valid Bid
                    auc.nextValidBidUs = auc.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);; // Next Valid Bid

                    Auction returnAuc = COMC.AddProxybidRecords(auc);

                    if (returnAuc != null)
                    {

                        Auction proxyinfosame = CAS.Getproxyinfosame(auc.Online);

                        if (proxyinfosame != null)
                        {
                            if (returnAuc.userid != proxyinfosame.userid)
                            {
                                Auction bidrecord = new Auction();
                                bidrecord.firstname = proxyinfosame.firstname;
                                bidrecord.lastname = proxyinfosame.lastname;
                                bidrecord.thumbnail = proxyinfosame.thumbnail;
                                bidrecord.productid = returnAuc.productid;
                                bidrecord.nextValidBidRs = returnAuc.nextValidBidRs;
                                bidrecord.nextValidBidUs = returnAuc.nextValidBidUs; // Convert.ToInt32(returnAuc.Bidpricers) / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                                bidrecord.pricers = returnAuc.nextValidBidRs;
                                bidrecord.priceus = returnAuc.nextValidBidUs; //Convert.ToInt32(proxyinfosame.ProxyAmt) / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                                //bidrecord.daterec = DateTime.Now;
                                bidrecord.reference = proxyinfosame.reference;
                                bidrecord.nickname = proxyinfosame.nickname;
                                bidrecord.username = proxyinfosame.username;
                                bidrecord.currentbid = 1;
                                bidrecord.recentbid = 1;
                                bidrecord.userid = proxyinfosame.userid;
                                bidrecord.Auctionid = auc.Online;
                                bidrecord.proxy = 1;
                                // bidrecord.earlyproxy = 1;

                                //Auction bidRecord = new Auction();
                                CAS.InsertBidRecord(bidrecord);

                            }

                        }

                        // Send Email
                        if (AUC.LastBidId > 0)
                        {
                            if (returnAuc.userid != AUC.LastBidId)
                            {
                                returnAuc.LastBidId = AUC.LastBidId;
                                returnAuc.productid = auc.productid;
                                returnAuc.curpriceUs = returnAuc.curprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); ;// Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(auc.nextValidBidUs, 2).ToString());
                                COMC.sendMailOutbid(returnAuc);
                            }
                        }

                    }
                    else
                    {
                        AUC.recentbid = 1;
                        CAS.updateRecentBid(AUC);
                    }

                    // Update Bid Limit Function
                    //created on 28-1-20
                    //#region

                    if (isOldUser == 0)
                    {
                        var currentLeadinguser = CAS.GetCurrentLeadingProxyuser(AUC.productid);


                        if (AUC.userid == currentLeadinguser.userid)
                        {

                            //int updateLimit = currentLeadinguser.amountlimt - currentLeadinguser.ProxyAmt;

                            //int updateLimit = currentLeadinguser.amountlimt - currentLeadinguser.Bidpricers;

                            int updateLimit = currentLeadinguser.amountlimt - currentLeadinguser.ProxyAmt;
                            US.UpdateBidLimit(currentLeadinguser.userid, updateLimit);
                            //Session["BidLimit"] = updateLimit;
                            int isleadcurrent = 1;
                            CAS.Isleadingproxy(AUC.productid, currentLeadinguser.userid, currentLeadinguser.ProxyAmt, isleadcurrent);

                            Auction objAction = CAS.Getcurrentpriceuser(AUC.productid, AUC.userid);

                            if (AUC.userid != objAction.userid)
                            {
                                int checkforprocyuser = CAS.checkforproxyuser(AUC.productid, objAction.userid);
                                if (checkforprocyuser > 0)
                                {
                                    int outbidproxyamount = CAS.getoutbidproxyamount(AUC.productid, objAction.userid);

                                    int updateLimit1 = objAction.amountlimt + outbidproxyamount;
                                    US.UpdateBidLimit(objAction.userid, updateLimit1);
                                    int isleadoutbid = 0;
                                    CAS.Isleadingproxy(AUC.productid, objAction.userid, outbidproxyamount, isleadoutbid);
                                }
                                else
                                {
                                    int outbidpricers = CAS.getoutbidamount(AUC.productid, objAction.userid);

                                    int updateLimit1 = objAction.amountlimt + outbidpricers;
                                    US.UpdateBidLimit(objAction.userid, updateLimit1);
                                    int isleadoutbid = 0;
                                    CAS.Isleading(AUC.productid, objAction.userid, outbidpricers, isleadoutbid);
                                }

                            }
                        }
                    }
                    else
                    {
                        var currentLeadinguser = CAS.Getcurrentleadinguser(AUC.productid);
                        Auction objAction = CAS.Getcurrentpriceuser(AUC.productid, AUC.userid);

                        if (currentLeadinguser.userid != objAction.userid)
                        {
                            int checkforprocyuser = CAS.checkforproxyuser(AUC.productid, objAction.userid);
                            if (checkforprocyuser > 0)
                            {
                                int outbidproxyamount = CAS.getoutbidproxyamount(AUC.productid, objAction.userid);

                                int updateLimit1 = objAction.amountlimt + outbidproxyamount;
                                US.UpdateBidLimit(objAction.userid, updateLimit1);
                                int isleadoutbid = 0;
                                CAS.Isleadingproxy(AUC.productid, objAction.userid, outbidproxyamount, isleadoutbid);
                            }
                            else
                            {
                                int outbidpricers = CAS.getoutbidamount(AUC.productid, objAction.userid);

                                int updateLimit1 = objAction.amountlimt + outbidpricers;
                                US.UpdateBidLimit(objAction.userid, updateLimit1);
                                int isleadoutbid = 0;
                                CAS.Isleading(AUC.productid, objAction.userid, outbidpricers, isleadoutbid);
                            }

                        }
                    }


                    //#endregion


                    var currentLeadinguseroutbid = CAS.Getcurrentleadinguser(AUC.productid);          // get current leading user for outbid  another
                    Auction objActionoutbid = CAS.Getcurrentoutbiduser(AUC.productid, currentLeadinguseroutbid.userid);    // get out bid user
                    if (currentLeadinguseroutbid.userid != objActionoutbid.userid)
                    {
                        //Session["OutProxyErr"] = "have been out bid due to proxy. Kindly bid again.";
                        // CAll Mail Function                      /// uncomment on 2/03/2020
                        if (objActionoutbid.userid > 0)
                        {
                            AUC.mailPreprice = objActionoutbid.Bidpricers;
                            AUC.curprice = currentLeadinguseroutbid.Bidpricers;
                            AUC.reference = auc.reference;
                            AUC.title = auc.title;
                            AUC.thumbnail = auc.thumbnail;
                            AUC.nextValidBidRs = Convert.ToInt32(Astaguru_webapi.Services.Common.getNextValidBidAmount(AUC.curprice, 1).ToString());
                            AUC.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                            //int page= Convert.ToInt32(Request["page"].ToString());
                            //COMC.sendMailOutbiduser(AUC, objActionoutbid.userid, objActionoutbid.username);

                            COMC.sendoutbidmsg(AUC.reference, objActionoutbid.name, objActionoutbid.mobile);
                        }
                
                    }

                    // Add to My Auction Gallary 
                    int isPresentArtist = CAS.CheckArtistAssginedbidUserlist(AUC);
                    if (isPresentArtist > 0)
                    {

                    }
                    else
                    {
                        int addArtist = CAS.AddArtisttoBidUserList(AUC);
                    }


                    var currentLeadinguserproxy = CAS.Getcurrentleadinguser(AUC.productid);          // get current leading user for outbid  another
                    Auction objActionproxy = CAS.Getcurrentoutbiduser(AUC.productid, currentLeadinguserproxy.userid);    // get out bid user
                    if (userid != currentLeadinguserproxy.userid && userid == objActionproxy.userid)
                    {
                        int checkforprocyuser = CAS.checkforproxyuserfroutbidmessage(AUC.productid, currentLeadinguserproxy.userid);
                        if (checkforprocyuser > 0)
                        {
                            responsemessage.currentStatus = 2;
                            responsemessage.msg = "You have been out bid due to proxy. Kindly bid again.";
                            responsemessage.mobileNum = UR.Mobile;
                            responsemessage.emailID = UR.email;
                            responsemessage.Username = auc.username;
                            responsemessage.outBidAmountRs = objActionproxy.Bidpricers;
                            objActionproxy.Bidpriceus = objActionproxy.Bidpricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                            responsemessage.outBidAmountUs = objActionproxy.Bidpriceus;
                            responsemessage.lastBidpriceRs = 0;
                            responsemessage.lastBidpriceUs = 0;

                            response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                            return response;
                        }
                    }


                    //addToGallary(AUC);
                    responsemessage.currentStatus = 3;
                    responsemessage.msg = "CURRENTLY LEADING";
                    responsemessage.mobileNum = UR.Mobile;
                    responsemessage.emailID = UR.email;
                    responsemessage.Username = AUC.username;
                    //responsemessage.outBidAmountRs = objActionproxy.Bidpricers;
                    //int outBidAmountUs = objActionproxy.Bidpricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                    //responsemessage.outBidAmountUs = outBidAmountUs;

                    responsemessage.outBidAmountRs = lastbidamountRs;
                    responsemessage.outBidAmountUs = lastbidamountUs;
                    responsemessage.lastBidpriceRs = 0;
                    responsemessage.lastBidpriceUs = 0;



                    response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                  return response;
                }
                else
                {
                    responsemessage.currentStatus = 4;
                    responsemessage.msg = "User not exist please login again.";
                    responsemessage.mobileNum = "";
                    responsemessage.emailID = "";
                    responsemessage.Username = "";
                    responsemessage.outBidAmountRs = 0;
                    responsemessage.outBidAmountUs = 0;
                    responsemessage.lastBidpriceRs = 0;
                    responsemessage.lastBidpriceUs = 0;
                    response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                    return response;
                }
            }
            catch (Exception Ex)
            {
                //User UR = US.GetBillingAddress(auc.userid);
                responsemessage.currentStatus = 5;
                responsemessage.msg = "Something went wrong. Please try after sometime";
                responsemessage.mobileNum = "";
                responsemessage.emailID ="";
                responsemessage.Username = "";
                responsemessage.outBidAmountRs = 0;
                responsemessage.outBidAmountUs = 0;
                responsemessage.lastBidpriceRs = 0;
                responsemessage.lastBidpriceUs = 0;
                response = Request.CreateResponse(HttpStatusCode.BadRequest, responsemessage);
                return response;
            }

        }

        //public HttpResponseMessage addToGallary(Auction AUC)
        //{
        //    try
        //    {
        //        int isPresentArtist = CAS.CheckArtistAssginedbidUserlist(AUC);

        //        if (isPresentArtist > 0)
        //        {
        //            response = Request.CreateResponse(HttpStatusCode.BadRequest, "Add gallary Error");
        //            return response;
        //            //return Json(new { status = "Error" }, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            int addArtist = CAS.AddArtisttoBidUserList(AUC);
        //            response = Request.CreateResponse(HttpStatusCode.OK, "Added in gallary");
        //            return response;
        //            //return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
        //        return response;
        //        //return Json(new { status = "Exception" }, JsonRequestBehavior.AllowGet);
        //    }

        //}

    }
}
