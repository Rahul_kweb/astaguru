﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class updateProfileController : ApiController
    {
        HttpResponseMessage response;
        Utility util = new Utility();

        public HttpResponseMessage Post(User objuser)
        {
            responseUpdateprofile responsemessage = new responseUpdateprofile();

            DataTable dt = new DataTable();


            using (SqlCommand cmd = new SqlCommand("Proc_user_Register_Mob"))
            {


                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "updateProfile");
                cmd.Parameters.AddWithValue("@userid", objuser.userid);
                cmd.Parameters.AddWithValue("@genderid", objuser.genderid);
                cmd.Parameters.AddWithValue("@bday", objuser.bday);
                cmd.Parameters.AddWithValue("@bmonth", objuser.bmonth);
                cmd.Parameters.AddWithValue("@byear", objuser.byear);
                cmd.Parameters.AddWithValue("@interestedIds", objuser.interestedIds);
                cmd.Parameters.AddWithValue("@country", objuser.country);
                cmd.Parameters.AddWithValue("@state", objuser.state);
                cmd.Parameters.AddWithValue("@address1", objuser.address1);
                cmd.Parameters.AddWithValue("@address2", objuser.address2);
                cmd.Parameters.AddWithValue("@zip", objuser.zip);
                cmd.Parameters.AddWithValue("@city", objuser.city);
                cmd.Parameters.AddWithValue("@companyName", objuser.companyName);
                cmd.Parameters.AddWithValue("@BillingCountry", objuser.BillingCountry);
                cmd.Parameters.AddWithValue("@BillingAddress", objuser.BillingAddress);
                cmd.Parameters.AddWithValue("@billingAddress2", objuser.billingAddress2);
                cmd.Parameters.AddWithValue("@BillingCity", objuser.BillingCity);
                cmd.Parameters.AddWithValue("@BillingState", objuser.BillingState);
                cmd.Parameters.AddWithValue("@BillingZip", objuser.BillingZip);
                cmd.Parameters.AddWithValue("@bCountryid", objuser.bCountryid);
                cmd.Parameters.AddWithValue("@bCityid", objuser.bCityid);
                cmd.Parameters.AddWithValue("@cityid", objuser.cityid);
                cmd.Parameters.AddWithValue("@MobileVerified", objuser.MobileVerified);
                cmd.Parameters.AddWithValue("@bStateid", objuser.bStateid);
                cmd.Parameters.AddWithValue("@nickname", objuser.nickname);
                cmd.Parameters.AddWithValue("@countryid", objuser.countryid);
                cmd.Parameters.AddWithValue("@buy", objuser.buy);

                cmd.Parameters.AddWithValue("@GSTIN", objuser.GSTIN);
                cmd.Parameters.AddWithValue("@panCard", objuser.panCard);

                cmd.Parameters.AddWithValue("@acNumber", objuser.acNumber);
                cmd.Parameters.AddWithValue("@holderName", objuser.holderName);
                cmd.Parameters.AddWithValue("@ifscCode", objuser.ifscCode);
                cmd.Parameters.AddWithValue("@branchName", objuser.branchName);
                cmd.Parameters.AddWithValue("@swiftCode", objuser.swiftCode);
                cmd.Parameters.AddWithValue("@aadharCard", objuser.aadharCard);

                cmd.Parameters.AddWithValue("@imagePanCard", objuser.imagePanCard);
                cmd.Parameters.AddWithValue("@imageAadharCard", objuser.imageAadharCard);



                //byte[] imageBytes = Convert.FromBase64String(objuser.PanImage);
                //MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
                //ms.Write(imageBytes, 0, imageBytes.Length);
                //System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                //image.Save(HttpContext.Current.Server.MapPath("~/content/panCardImage/" + objuser.imagePanCard + ""));

              

                if (objuser.PanImage != null)
                {

                    /// save panCardImage 
                    String path = HttpContext.Current.Server.MapPath("~/Content/panCardImage"); //Path

                    //Check if directory exist
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
                    }

                    string imageName = objuser.imagePanCard;

                    //set the image path
                    string imgPath = Path.Combine(path, imageName);

                    byte[] imageBytes = Convert.FromBase64String(objuser.PanImage);

                    File.WriteAllBytes(imgPath, imageBytes);
                }



                if (objuser.AadharImage != null)
                {
                    /// save aadharCardImage 

                    String pathAdhar = HttpContext.Current.Server.MapPath("~/Content/aadharCardImage"); //Path

                    //Check if directory exist
                    if (!System.IO.Directory.Exists(pathAdhar))
                    {
                        System.IO.Directory.CreateDirectory(pathAdhar); //Create directory if it doesn't exist
                    }

                    string imageNameAdhar = objuser.imageAadharCard;

                    //set the image path
                    string imgPathAdhar = Path.Combine(pathAdhar, imageNameAdhar);

                    byte[] imageBytesAdhar = Convert.FromBase64String(objuser.AadharImage);

                    File.WriteAllBytes(imgPathAdhar, imageBytesAdhar);
                }





                if (util.Execute(cmd))
                {


                    responsemessage.status = "Success";
                    responsemessage.msg = "profile Updated Successfully";
                    response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);

                }
                else
                {
                    responsemessage.status = "Fail";
                    responsemessage.msg = "profile not Updated Successfully";
                    response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                }
            }
            return response;
        }







    }
}
