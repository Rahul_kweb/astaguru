﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Astaguru_webapi.Controllers
{
    [EnableCors(origins: "http://asta.kwebmakerdigital.com/", headers: "*", methods: "*")]
    public class UserRegistrationController : ApiController
    {
        HttpResponseMessage response;
        Utility util = new Utility();

        public HttpResponseMessage Post(userRequest objRequest)
        {
            ResponseregisterNEw responsemessage = new ResponseregisterNEw();

            DataTable dt = new DataTable();

            foreach (var objuser in objRequest.resource)
            {
                using (SqlCommand cmd = new SqlCommand("Proc_user_Register_Mob"))
                {
                    if (objuser.Mobile.Contains("+"))
                    {
                        objuser.Mobile = objuser.Mobile.Replace("+", string.Empty);
                    }
                    if (objuser.t_mobile.Contains("+"))
                    {
                        objuser.t_mobile = objuser.t_mobile.Replace("+", string.Empty);
                    }


                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "RegisterUser");
                    cmd.Parameters.AddWithValue("@name", objuser.name);
                    cmd.Parameters.AddWithValue("@Mobile", objuser.Mobile);
                    cmd.Parameters.AddWithValue("@email", objuser.email);
                    cmd.Parameters.AddWithValue("@lastname", objuser.lastname);
                    cmd.Parameters.AddWithValue("@address1", objuser.address1);
                    cmd.Parameters.AddWithValue("@city", objuser.city);
                    cmd.Parameters.AddWithValue("@zip", objuser.zip);
                    cmd.Parameters.AddWithValue("@state", objuser.state);
                    cmd.Parameters.AddWithValue("@country", objuser.country);
                    cmd.Parameters.AddWithValue("@telephone", objuser.telephone);
                    cmd.Parameters.AddWithValue("@password", objuser.password);
                    cmd.Parameters.AddWithValue("@fax", objuser.fax);
                    cmd.Parameters.AddWithValue("@Billingname", objuser.BillingName);
                    cmd.Parameters.AddWithValue("@BillingAddress", objuser.BillingAddress);
                    cmd.Parameters.AddWithValue("@BillingCity", objuser.BillingCity);
                    cmd.Parameters.AddWithValue("@BillingState", objuser.BillingState);
                    cmd.Parameters.AddWithValue("@BillingCountry", objuser.BillingCountry);
                    cmd.Parameters.AddWithValue("@BillingZip", objuser.BillingZip);
                    cmd.Parameters.AddWithValue("@BillingTelephone", objuser.BillingTelephone);
                    cmd.Parameters.AddWithValue("@BillingEmail", objuser.BillingEmail);


                    cmd.Parameters.AddWithValue("@bCountryid", objuser.bCountryid);
                    cmd.Parameters.AddWithValue("@bCityid", objuser.bCityid);
                    cmd.Parameters.AddWithValue("@t_username", objuser.t_username);
                    cmd.Parameters.AddWithValue("@androidDeviceTocken", objuser.androidDeviceTocken);
                    cmd.Parameters.AddWithValue("@t_fax", objuser.t_fax);
                    cmd.Parameters.AddWithValue("@t_billingname", objuser.t_billingname);
                    cmd.Parameters.AddWithValue("@t_mobile", objuser.t_mobile);
                    cmd.Parameters.AddWithValue("@t_address1", objuser.t_address1);
                    cmd.Parameters.AddWithValue("@t_billingemail", objuser.t_billingemail);
                    cmd.Parameters.AddWithValue("@applyforbid", objuser.applyforbid);
                    cmd.Parameters.AddWithValue("@t_billingaddress", objuser.t_billingaddress);
                    cmd.Parameters.AddWithValue("@genderid", objuser.genderid);
                    cmd.Parameters.AddWithValue("@cityid", objuser.cityid);
                    cmd.Parameters.AddWithValue("@chatdept", objuser.chatdept);
                    cmd.Parameters.AddWithValue("@bmonth", objuser.bmonth);
                    cmd.Parameters.AddWithValue("@applyforchange", objuser.applyforchange);
                    cmd.Parameters.AddWithValue("@MobileVerified", objuser.MobileVerified);
                    cmd.Parameters.AddWithValue("@t_firstname", objuser.t_firstname);
                    cmd.Parameters.AddWithValue("@t_billingcity", objuser.t_billingcity);
                    cmd.Parameters.AddWithValue("@t_billingcountry", objuser.t_billingcountry);
                    cmd.Parameters.AddWithValue("@t_email", objuser.t_email);
                    cmd.Parameters.AddWithValue("@t_State", objuser.t_State);
                    cmd.Parameters.AddWithValue("@bStateid", objuser.bStateid);
                    cmd.Parameters.AddWithValue("@t_telephone", objuser.t_telephone);
                    cmd.Parameters.AddWithValue("@t_billingzip", objuser.t_billingzip);
                    cmd.Parameters.AddWithValue("@EmailVerified", objuser.EmailVerified);
                    cmd.Parameters.AddWithValue("@address2", objuser.address2);
                    cmd.Parameters.AddWithValue("@nickname", objuser.nickname);
                    cmd.Parameters.AddWithValue("@admin", objuser.admin);
                    cmd.Parameters.AddWithValue("@t_City", objuser.t_City);
                    cmd.Parameters.AddWithValue("@byear", objuser.byear);
                    cmd.Parameters.AddWithValue("@t_lastname", objuser.t_lastname);
                    cmd.Parameters.AddWithValue("@Visits", objuser.Visits);
                    cmd.Parameters.AddWithValue("@t_password", objuser.t_password);
                    cmd.Parameters.AddWithValue("@countryid", objuser.countryid);
                    cmd.Parameters.AddWithValue("@RegistrationDate", objuser.RegistrationDate);
                    cmd.Parameters.AddWithValue("@t_Country", objuser.t_Country);
                    cmd.Parameters.AddWithValue("@confirmbid", objuser.confirmbid);
                    cmd.Parameters.AddWithValue("@aboutId", objuser.aboutId);
                    cmd.Parameters.AddWithValue("@t_billingtelephone", objuser.t_billingtelephone);
                    cmd.Parameters.AddWithValue("@bday", objuser.bday);
                    cmd.Parameters.AddWithValue("@SmsCode", objuser.SmsCode);
                    cmd.Parameters.AddWithValue("@buy", objuser.buy);
                    cmd.Parameters.AddWithValue("@username", objuser.username);
                    cmd.Parameters.AddWithValue("@t_nickname ", objuser.t_nickname);
                    cmd.Parameters.AddWithValue("@t_billingstate", objuser.t_billingstate);
                    cmd.Parameters.AddWithValue("@deviceTocken", objuser.deviceTocken);
                    cmd.Parameters.AddWithValue("@interestedIds", objuser.interestedIds);




                    if (util.Execute(cmd))
                    {
                        int userid = 0;
                        dt = util.Display("select userid from users where password='" + objuser.password + "' and email='" + objuser.email + "' and name='" + objuser.name + "'");

                        if (dt.Rows.Count > 0)
                        {
                            userid = int.Parse(dt.Rows[0]["userid"].ToString());
                        }



                        responsemessage.userid = userid;
                        response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);

                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK, responsemessage);
                    }
                }
            }



            return response;


        }
    }
}
