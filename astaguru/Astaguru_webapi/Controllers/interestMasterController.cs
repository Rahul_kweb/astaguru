﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class interestMasterController : ApiController
    {
        Utility util = new Utility();
        public resourceinterestMaster Get()
        {

            List<interestMaster> objAuctionlist = new List<interestMaster>();

            DataTable dt = new DataTable();
            dt = util.Display("select * from interestMaster");

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    interestMaster objAuction = new interestMaster();
                    objAuction.interestedId = int.Parse(dr["interestedId"].ToString());
                    objAuction.interest = dr["interest"].ToString();
                    objAuction.status = int.Parse(dr["status"].ToString());



                    objAuctionlist.Add(objAuction);

                }

            }

            resourceinterestMaster objresource = new resourceinterestMaster();
            objresource.resource = objAuctionlist;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, objresource.resource);
            return objresource;

        }
    }
}
