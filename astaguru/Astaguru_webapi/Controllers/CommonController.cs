﻿using Astaguru_webapi.Models;
using Astaguru_webapi.Services;
using Astaguru_webapi.Services.UserService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Astaguru_webapi.Controllers
{
    public class CommonController : Controller
    {
        CurrentAuctionService CAS = new CurrentAuctionService();    
        List<Auction> listProxyAuc = new List<Auction>();
        List<Auction> listAuc = new List<Auction>();
        List<Auction> aucBidRecord = new List<Auction>();
        UserService US = new UserService();

        int curprice = 0;


        [HttpPost]
        public Auction AddProxybidRecords(Auction AUC)
        {
            Auction bidrecordfinal = new Auction();

            int currentuser = 0;
            List<int> userlist = new List<int>();
            listProxyAuc = CAS.GetProxyInfo(AUC);
            listAuc = CAS.GetAcutionData(AUC);
            int pricers = 0;
            //curprice = AUC.curprice;
            curprice = AUC.pricers;
            while (listProxyAuc.Count > 0)
            {
                if (listProxyAuc.Count == 1 && AUC.userid == listProxyAuc[0].userid && currentuser == AUC.userid)
                {
                    AUC.recentbid = 1;
                    AUC.currentbid = 1;
                    CAS.UpdateRecentCurrentBid(AUC);
                    return AUC;

                }
                else if (listProxyAuc.Count == 1 && currentuser == listProxyAuc[0].userid)
                {
                    AUC.recentbid = 1;
                    AUC.currentbid = 1;
                    AUC.userid = listProxyAuc[0].userid;
                    CAS.UpdateRecentCurrentBid(AUC);
                    return AUC;
                }

                foreach (var proxy in listProxyAuc)
                {
                    // Added to skip record if same user come again
                    if (currentuser == proxy.userid)
                    {
                        continue;
                    }


                    // Need To Check This
                    if (!userlist.Contains(proxy.userid))
                    {
                        userlist.Add(proxy.userid);
                        aucBidRecord = CAS.getBidUserList(AUC);

                        foreach (var userBid in aucBidRecord)
                        {
                            userBid.currentbid = 0;
                        }
                    }

                    pricers = proxy.ProxyAmt;

                    if (AUC.nextValidBidRs == 0)
                    {
                        AUC.nextValidBidRs = proxy.pricers;
                    }

                    // Need To Do This
                    // Dim val_increase As Decimal = 1.1
                    //If pricelow >= 10000000 Then
                    //    val_increase = 1.05
                    //End If

                    //Changed as last proxy record was missing...........If proxy.bid.ProxyAmt > pricelow * val_increase Then

                    //if (proxy.ProxyAmt >= AUC.nextValidBidRs)
                    //created on 26-01-2020
                    #region
                    if (proxy.ProxyAmt > AUC.nextValidBidRs)
                    #endregion
                    {

                        Auction bidRecord = new Auction();

                        List<Auction> listProxyBid = new List<Auction>();
                        listProxyBid = CAS.GetproxyUser(AUC);
                        if (listProxyBid.Count > 0)
                        {
                            foreach (var proxyuser in listProxyBid)
                            {
                                bidRecord.firstname = proxy.firstname;
                                bidRecord.lastname = proxy.lastname;
                                bidRecord.thumbnail = proxy.thumbnail;
                                bidRecord.productid = proxy.productid;
                                bidRecord.pricers = AUC.pricers;

                                AUC.mailPreprice = AUC.pricers;
                                AUC.mailPrepriceUs = AUC.mailPreprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);

                                bidRecord.nickname = proxy.nickname;
                                bidRecord.priceus = bidRecord.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;

                                if (proxyuser.ProxyAmt == AUC.nextValidBidRs)
                                {
                                    AUC.nextValidBidRs = Convert.ToInt32(Astaguru_webapi.Services.Common.getNextValidBidAmount(AUC.nextValidBidRs, 1).ToString());
                                    bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                                    bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                                    curprice = bidRecord.nextValidBidRs;
                                }
                                else
                                {
                                    bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                                    bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                                    curprice = bidRecord.nextValidBidRs;
                                }

                                // bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                                // bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                                // curprice = bidRecord.nextValidBidRs;
                                //bidRecord.daterec = Now : Already taken in query

                                bidRecord.reference = proxy.reference;
                                bidRecord.anoname = proxy.nickname;
                                bidRecord.username = proxy.username;
                                bidRecord.currentbid = 0;
                                bidRecord.recentbid = 0;
                                bidRecord.userid = proxy.userid;
                                bidRecord.proxy = 1;
                                bidRecord.Online = proxy.Auctionid;
                                AUC.pricers = AUC.nextValidBidRs;
                                bidRecord.nickname = proxy.nickname;
                                bidRecord.isOldUser = proxy.isOldUser;

                                // New 
                                bidrecordfinal = bidRecord;

                                AUC.Bidrecordid = CAS.InsertBidRecord(bidRecord);
                            }
                        }
                        else
                        {
                            bidRecord.firstname = proxy.firstname;
                            bidRecord.lastname = proxy.lastname;
                            bidRecord.thumbnail = proxy.thumbnail;
                            bidRecord.productid = proxy.productid;
                            bidRecord.pricers = AUC.pricers;

                            AUC.mailPreprice = AUC.pricers;
                            AUC.mailPrepriceUs = AUC.mailPreprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);

                            bidRecord.nickname = proxy.nickname;
                            bidRecord.priceus = bidRecord.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;

                            bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                            bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                            curprice = bidRecord.nextValidBidRs;


                            // bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                            // bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                            // curprice = bidRecord.nextValidBidRs;
                            //bidRecord.daterec = Now : Already taken in query

                            bidRecord.reference = proxy.reference;
                            bidRecord.anoname = proxy.nickname;
                            bidRecord.username = proxy.username;
                            bidRecord.currentbid = 0;
                            bidRecord.recentbid = 0;
                            bidRecord.userid = proxy.userid;
                            bidRecord.proxy = 1;
                            bidRecord.Online = proxy.Auctionid;
                            AUC.pricers = AUC.nextValidBidRs;
                            bidRecord.nickname = proxy.nickname;
                            bidRecord.isOldUser = proxy.isOldUser;

                            // New 
                            bidrecordfinal = bidRecord;

                            AUC.Bidrecordid = CAS.InsertBidRecord(bidRecord);
                        }
                        if (AUC.Bidrecordid > 0)
                        {
                            AUC.nextValidBidRs = bidRecord.nextValidBidRs;
                            AUC.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); //bidRecord.nextValidBidUs;
                            CAS.UpdateAcutionPrice(AUC);
                        }
                        else
                        {
                            //Failed
                        }

                        AUC.nextValidBidRs = Convert.ToInt32(Astaguru_webapi.Services.Common.getNextValidBidAmount(AUC.nextValidBidRs, 1).ToString());
                        currentuser = bidRecord.userid;

                    }
                    //created on 26-01-2020
                    #region
                    else if (proxy.ProxyAmt == AUC.nextValidBidRs)
                    {
                        Auction bidRecord = new Auction();

                        List<Auction> listProxyBid = new List<Auction>();
                        listProxyBid = CAS.Getsameproxy(AUC);
                        foreach (var proxynew in listProxyBid)
                        {
                            bidRecord.firstname = proxynew.firstname;
                            bidRecord.lastname = proxynew.lastname;
                            bidRecord.thumbnail = proxynew.thumbnail;
                            bidRecord.productid = proxynew.productid;
                            bidRecord.pricers = AUC.pricers;
                            bidRecord.nickname = proxynew.nickname;
                            bidRecord.priceus = AUC.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                            // AUC.priceus;
                            //bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                            //bidRecord.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                            //curprice = bidRecord.nextValidBidRs; // bidRecord.nextValidBidRs;
                            //bidRecord.daterec = Now : Already taken in query
                            //created on 12_02_2020
                            bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                            bidRecord.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                            curprice = AUC.nextValidBidRs;


                            bidRecord.reference = proxynew.reference;
                            bidRecord.anoname = proxynew.nickname;
                            bidRecord.username = proxynew.username;
                            bidRecord.currentbid = 0;
                            bidRecord.recentbid = 0;
                            bidRecord.userid = proxynew.userid;
                            bidRecord.proxy = 1;
                            bidRecord.Online = AUC.Auctionid;
                            AUC.pricers = AUC.nextValidBidRs;
                            // AUC.pricers = proxy.ProxyAmt;
                            bidRecord.nickname = proxynew.nickname;
                            bidRecord.isOldUser = proxynew.isOldUser;

                            bidrecordfinal = bidRecord;
                            AUC.Bidrecordid = CAS.InsertBidRecord(bidRecord);

                        }

                        if (AUC.Bidrecordid > 0)
                        {
                            AUC.nextValidBidRs = bidRecord.nextValidBidRs;
                            AUC.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); //bidRecord.nextValidBidUs;
                            CAS.UpdateAcutionPrice(AUC);
                        }
                        else
                        {
                            //Failed
                        }



                        AUC.nextValidBidRs = Convert.ToInt32(Astaguru_webapi.Services.Common.getNextValidBidAmount(AUC.nextValidBidRs, 1).ToString());
                        currentuser = bidRecord.userid;
                    }
                    #endregion
                    else if (proxy.ProxyAmt > curprice || proxy.ProxyAmt == curprice)
                    {
                        //Auction bidRecord = new Auction();
                        //bidRecord.firstname = proxy.firstname;
                        //bidRecord.lastname = proxy.lastname;
                        //bidRecord.thumbnail = proxy.thumbnail;
                        //bidRecord.productid = proxy.productid;
                        //bidRecord.pricers = AUC.pricers;
                        //bidRecord.nickname = proxy.nickname;
                        //bidRecord.priceus = bidRecord.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;
                        //bidRecord.nextValidBidRs = proxy.ProxyAmt; // Next valid bid
                        //bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                        //curprice = proxy.ProxyAmt; // bidRecord.nextValidBidRs;
                        ////bidRecord.daterec = Now : Already taken in query

                        //bidRecord.reference = proxy.reference;
                        //bidRecord.anoname = proxy.nickname;
                        //bidRecord.username = proxy.username;
                        //bidRecord.currentbid = 0;
                        //bidRecord.recentbid = 0;
                        //bidRecord.userid = proxy.userid;
                        //bidRecord.proxy = 1;
                        //bidRecord.Online = proxy.Auctionid;
                        //AUC.pricers = AUC.nextValidBidRs;
                        //bidRecord.nickname = proxy.nickname;
                        //// New 
                        //bidrecordfinal = bidRecord;
                        //AUC.Bidrecordid = CAS.InsertBidRecord(bidRecord);

                        //created on 25-01-2020
                        #region
                        Auction bidRecord = new Auction();

                        List<Auction> listProxyBid = new List<Auction>();
                        listProxyBid = CAS.GetUpdatedProxyuser(AUC);
                        if (listProxyBid.Count > 0)
                        {
                            foreach (var proxynew in listProxyBid)
                            {
                                if (proxynew.ProxyAmt > curprice)
                                {
                                    bidRecord.firstname = proxynew.firstname;
                                    bidRecord.lastname = proxynew.lastname;
                                    bidRecord.thumbnail = proxynew.thumbnail;
                                    bidRecord.productid = proxynew.productid;
                                    bidRecord.pricers = AUC.pricers;
                                    bidRecord.nickname = proxynew.nickname;
                                    bidRecord.priceus = bidRecord.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;
                                    bidRecord.nextValidBidRs = proxynew.ProxyAmt; // Next valid bid
                                    bidRecord.nextValidBidUs = proxynew.ProxyAmt / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                                    curprice = proxynew.ProxyAmt; // bidRecord.nextValidBidRs;
                                                                  //bidRecord.daterec = Now : Already taken in query

                                    bidRecord.reference = proxynew.reference;
                                    bidRecord.anoname = proxynew.nickname;
                                    bidRecord.username = proxynew.username;
                                    bidRecord.currentbid = 0;
                                    bidRecord.recentbid = 0;
                                    bidRecord.userid = proxynew.userid;
                                    bidRecord.proxy = 1;
                                    bidRecord.Online = proxynew.Auctionid;
                                    //comment on 04_07_2020
                                    AUC.pricers = bidRecord.nextValidBidRs;
                                    bidRecord.nickname = proxynew.nickname;
                                    bidRecord.isOldUser = proxynew.isOldUser;

                                    bidrecordfinal = bidRecord;
                                    AUC.Bidrecordid = CAS.InsertBidRecord(bidRecord);
                                    proxy.ProxyAmt = proxynew.ProxyAmt;
                                }
                                else if (proxynew.ProxyAmt == curprice)
                                {
                                    bidRecord.firstname = proxynew.firstname;
                                    bidRecord.lastname = proxynew.lastname;
                                    bidRecord.thumbnail = proxynew.thumbnail;
                                    bidRecord.productid = proxynew.productid;
                                    bidRecord.pricers = AUC.pricers;
                                    bidRecord.nickname = proxynew.nickname;
                                    bidRecord.priceus = AUC.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;
                                    bidRecord.nextValidBidRs = proxynew.ProxyAmt; // Next valid bid
                                    bidRecord.nextValidBidUs = proxynew.ProxyAmt / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                                    curprice = proxynew.ProxyAmt; // bidRecord.nextValidBidRs;
                                                                  //bidRecord.daterec = Now : Already taken in query

                                    bidRecord.reference = proxynew.reference;
                                    bidRecord.anoname = proxynew.nickname;
                                    bidRecord.username = proxynew.username;
                                    bidRecord.currentbid = 0;
                                    bidRecord.recentbid = 0;
                                    bidRecord.userid = proxynew.userid;
                                    bidRecord.proxy = 1;
                                    bidRecord.Online = proxynew.Auctionid;
                                    AUC.pricers = AUC.nextValidBidRs;
                                    bidRecord.nickname = proxynew.nickname;
                                    bidRecord.isOldUser = proxynew.isOldUser;

                                    bidrecordfinal = bidRecord;
                                    AUC.Bidrecordid = CAS.InsertBidRecord(bidRecord);
                                    proxy.ProxyAmt = proxynew.ProxyAmt;
                                }
                            }
                        }
                        #endregion


                        if (AUC.Bidrecordid > 0)
                        {
                            AUC.nextValidBidRs = proxy.ProxyAmt;

                            AUC.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); //proxy.ProxyAmtus;
                            CAS.UpdateAcutionPrice(AUC);
                        }
                        else
                        {
                            //Failed
                        }

                        AUC.nextValidBidRs = Convert.ToInt32(Astaguru_webapi.Services.Common.getNextValidBidAmount(proxy.ProxyAmt, 1).ToString());
                        currentuser = bidRecord.userid;
                    }

                }
                AUC.curprice = curprice;
                listProxyAuc = CAS.GetUpdatedProxyInfo(AUC);

            }


            if (listProxyAuc.Count == 0)
            {

                if (bidrecordfinal.userid > 0)
                {
                    return bidrecordfinal;
                }
                else
                {
                    return null;
                }



            }
            else
            {
                AUC.nextValidBidRs = 0;
                AUC.recentbid = 1;
                AUC.currentbid = 1;
            }



            return AUC;
        }

        public string sendMailOutbid(Auction AUC)
        {
            User UR = US.GetBillingAddress(AUC.LastBidId);

            string basePathUrl = Common.baseUrlPath;
            string FromEmail = ConfigurationManager.AppSettings["fromId"];
            string Password = ConfigurationManager.AppSettings["smtpPassword"];
            string ToEmail = UR.email;
            try
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(FromEmail, "contact@astaguru.com");
                msg.To.Add(ToEmail);
                msg.Subject = "AstaGuru - You have been Outbid on Lot# " + AUC.reference + "";
                string str = string.Empty;
                str = "Dear  " + AUC.firstname;
                str += "</b><br><br>We would like to bring it to your notice that you have been outbid on Lot# " + AUC.reference + ",in the ongoing AstaGuru Online Auction. Your highest bid was on Rs." + Common.rupeeFormat(AUC.mailPreprice.ToString()) + "($" + AUC.mailPrepriceUs + ")" + " The current highest bid stands at  " + Common.rupeeFormat(AUC.curprice.ToString()) + "($" + AUC.curpriceUs + "). Continue to contest for Lot# " + AUC.reference + ", please place your updated bid here <a href=\"" + basePathUrl + "/Home/LotDetails?productid=" + AUC.productid + "\">Click here.</a>";
                str += "<br><br><br>";
                //str += "<br><br><br>In case you have any queries with regards to the Lots that are part of the auction or the bidding process, please feel free to contact us on 91-22 2204 8138/39 or write to us at contact@astaguru.com. Our team will be glad to assist you with the same.<br><br><br>";

                str += "Lot No : " + AUC.reference + " <br>Title :" + AUC.title + "<br>";
                str += "Current Highest Bid : Rs." + Common.rupeeFormat(AUC.curprice.ToString()) + " ($" + AUC.curpriceUs + ")<br>Next Incremental Bid Amount : Rs." + Common.rupeeFormat(AUC.nextValidBidRs.ToString()) + " ($" + AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + ")<br><br><br><img src='http://www.astaguru.com/" + AUC.thumbnail + "'><br><br><br>";
                str += "In case you have any queries with regards to the Lots that are part of the auction or the bidding process, please feel free to contact us on 91-22 2204 8138/39 or write to us at contact@astaguru.com. Our team will be glad to assist you with the same.<br><br><br>";
                str += "Warmest Regards,<br>Team AstaGuru.";
                //msg.Body = str;
                msg.IsBodyHtml = true;
                string html = str;
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, new ContentType("text/html"));
                msg.AlternateViews.Add(htmlView);
                //SmtpClient sc = new SmtpClient("sarpstechnologies.com", 25);
                SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["smtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]));
                sc.Host = "smtp.gmail.com";
                sc.Port = 587;
                sc.UseDefaultCredentials = true;
                // sc.Host = "sarpstechnologies.com";
                //  sc.Port = 25;
                sc.Credentials = new NetworkCredential(FromEmail, Password);
                sc.EnableSsl = true; // Hide this If u use instead of gmail
                sc.Send(msg);

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        //created on 02_03_2020
        public string sendMailOutbiduser(Auction AUC, int userid, string username)
        {
            User UR = US.GetBillingAddress(userid);

            string basePathUrl = Common.baseUrlPath;
            string FromEmail = ConfigurationManager.AppSettings["fromId"];
            string Password = ConfigurationManager.AppSettings["smtpPassword"];
            string ToEmail = UR.email;
            try
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(FromEmail, "contact@astaguru.com");
                msg.To.Add(ToEmail);
                msg.Subject = "AstaGuru - You have been Outbid on Lot# " + AUC.reference + "";
                string str = string.Empty;
                str = "Dear  " + username;
                str += "</b><br><br>We would like to bring it to your notice that you have been outbid on Lot# " + AUC.reference + ",in the ongoing AstaGuru Online Auction. Your highest bid was on Rs." + Common.rupeeFormat(AUC.mailPreprice.ToString()) + "($" + AUC.mailPreprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + ")" + " The current highest bid stands at  " + Common.rupeeFormat(AUC.curprice.ToString()) + "($" + AUC.curprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + "). Continue to contest for Lot# " + AUC.reference + ", please place your updated bid here <a href=\"" + basePathUrl + "/Home/LotDetails?productid=" + AUC.productid + "&page=" + AUC.page + "\">Click here.</a>";
                str += "<br><br><br>";
                str += "<br><br><br>In case you have any queries with regards to the Lots that are part of the auction or the bidding process, please feel free to contact us on 91-22 2204 8138/39 or write to us at contact@astaguru.com. Our team will be glad to assist you with the same.<br><br><br>";

                str += "Lot No : " + AUC.reference + " <br>Title :" + AUC.title + "<br>";
                str += "Current Highest Bid : Rs." + Common.rupeeFormat(AUC.curprice.ToString()) + " ($" + AUC.curprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + ")<br>Next Incremental Bid Amount : Rs." + Common.rupeeFormat(AUC.nextValidBidRs.ToString()) + " ($" + AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + ")<br><br><br><img src='http://www.astaguru.com/" + AUC.thumbnail + "'><br><br><br>";
                str += "In case you have any queries with regards to the Lots that are part of the auction or the bidding process, please feel free to contact us on 91-22 2204 8138/39 or write to us at contact@astaguru.com. Our team will be glad to assist you with the same.<br><br><br>";
                str += "Warmest Regards,<br>Team AstaGuru.";
                //msg.Body = str;
                msg.IsBodyHtml = true;
                string html = str;
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, new ContentType("text/html"));
                msg.AlternateViews.Add(htmlView);
                //SmtpClient sc = new SmtpClient("sarpstechnologies.com", 25);
                SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["smtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]));
                sc.Host = "smtp.gmail.com";
                sc.Port = 587;
                sc.UseDefaultCredentials = true;
                // sc.Host = "sarpstechnologies.com";
                //  sc.Port = 25;
                sc.Credentials = new NetworkCredential(FromEmail, Password);
                sc.EnableSsl = true; // Hide this If u use instead of gmail
                sc.Send(msg);

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// created on 12_08_2020

        public void sendoutbidmsg(string reference, string name, string mobile)
        {
            //string authKey = "105048AuvvUDCr56c199f0";
            string authkey = "338500AD9H4VOHQl5f3135e8P1";
            //Multiple mobiles numbers separated by comma
            string mobiles = mobile;
            //Sender ID,While using route4 sender id should be 6 characters long.
            //string sender = "ASTGUR";
            string sender = "ASTGUR";
            //Your message to send, Add URL encoding here.
            string message = HttpUtility.UrlEncode("Dear " + name + ", please note you have been outbid on Lot No." + reference.Trim() + ". Place a new bid on www.astaguru.com or mobile App");
            string route = "4";
            //string country = "91";

            //Prepare you post parameters
            StringBuilder sbPostData = new StringBuilder();
            sbPostData.AppendFormat("authkey={0}", authkey);
            sbPostData.AppendFormat("&mobiles={0}", mobiles);
            sbPostData.AppendFormat("&message={0}", message);
            sbPostData.AppendFormat("&sender={0}", sender);
            sbPostData.AppendFormat("&route={0}", route);

            try
            {
                //Call Send SMS API
                string sendSMSUri = "http://api.msg91.com/api/sendhttp.php";
                //Create HTTPWebrequest
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                //Prepare and Add URL Encoded data
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] data = encoding.GetBytes(sbPostData.ToString());
                //Specify post method
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/x-www-form-urlencoded";
                httpWReq.ContentLength = data.Length;
                using (Stream stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                //Get the response
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string responseString = reader.ReadToEnd();

                //Close the response
                reader.Close();
                response.Close();
            }
            catch (SystemException ex)
            {

            }
        }


        //created on 12_11_2020
        public string CurrentProxyMail(Auction AUC)
        {
            string basePathUrl = Common.baseUrlPath;
            string FromEmail = ConfigurationManager.AppSettings["fromId"];
            string Password = ConfigurationManager.AppSettings["smtpPassword"];
            string ToEmail = AUC.email;
            try
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(FromEmail, "contact@astaguru.com");
                msg.To.Add(ToEmail);
                msg.Subject = "Intimation about Proxy-Bid Approval";
                string str = string.Empty;
                str = "Dear  " + AUC.name + " " + AUC.lastname;
                str += "<br><br>We are glad to inform you that your Proxy Bid amount of Rs. " + Astaguru_webapi.Services.Common.rupeeFormat(AUC.ProxyAmt.ToString()) + "($ " + Astaguru_webapi.Services.Common.DollerFormat(AUC.ProxyAmtus.ToString()) + ") for Lot No " + AUC.reference + ", part of our '" + AUC.Auctionname + "' Auction dated " + AUC.auctiondate + "has been accepted.";
                str += "<br><br>For any further assistance please feel free to write to us at contact@astaguru.com or call us on 91-22 2204 8138/39. We will be glad to assist you.";
                str += "<br><br><br>Thank You.";
                str += "<br><br><br>Team Astaguru.";
                //msg.Body = str;
                msg.IsBodyHtml = true;
                string html = str;
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, new ContentType("text/html"));
                msg.AlternateViews.Add(htmlView);
                //SmtpClient sc = new SmtpClient("sarpstechnologies.com", 25);
                SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["smtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]));
                sc.Host = "smtp.gmail.com";
                sc.Port = 587;
                sc.UseDefaultCredentials = true;
                // sc.Host = "sarpstechnologies.com";
                //  sc.Port = 25;
                sc.Credentials = new NetworkCredential(FromEmail, Password);
                sc.EnableSsl = true; // Hide this If u use instead of gmail
                sc.Send(msg);

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


    }
}