﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class GetBidpriceController : ApiController
    {
        Utility util = new Utility();
        HttpResponseMessage response;
        public HttpResponseMessage Get(int productID)
        {

            List<BidPrice> objAuctionlist = new List<BidPrice>();

            DataTable dt = new DataTable();
            dt = util.Display("Exec spGetBidPrice "+ productID + "");
           
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    BidPrice objAuction = new BidPrice();
                
                    objAuction.pricers = int.Parse(dr["pricers"].ToString());
                    objAuction.priceus = int.Parse(dr["priceus"].ToString());
             
                    objAuction.myBidClosingTime = dr["myBidClosingTime"].ToString();
                    objAuction.timeRemains = dr["timeRemains"].ToString();


                    DateTime newbidclosetime = Convert.ToDateTime(dr["Bidclosingtime"].ToString());
                    objAuction.Bidclosingtime = string.Format("{0:yyyy-MM-dd hh:mm:ss}", newbidclosetime);

                    DateTime tempcurrent = Convert.ToDateTime(dr["currentDate"].ToString());
                    objAuction.currentDate = string.Format("{0:yyyy-MM-dd hh:mm:ss}", tempcurrent);

                    if (dr["MyUserID"].ToString() == null || dr["MyUserID"].ToString() == "")
                    {
                        objAuction.MyUserID = 0;
                    }
                    else
                    {
                        objAuction.MyUserID = int.Parse(dr["MyUserID"].ToString());
                    }




                    objAuctionlist.Add(objAuction);

                }

            }


            response = Request.CreateResponse(HttpStatusCode.OK, objAuctionlist);
            return response;

        }
    }
}
