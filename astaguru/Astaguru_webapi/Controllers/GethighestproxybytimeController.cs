﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class GethighestproxybytimeController : ApiController
    {
        Utility util = new Utility();
        HttpResponseMessage response;

        public HttpResponseMessage Get(int productid)
        {
            int value = 0;
            DataTable dt = new DataTable();
            dt = util.Display("exec CRUDBid 'Gethighestproxybytime',0,0,'','','',0,0,'','',0,0,0,0,''," + productid + "");
            if (dt.Rows.Count > 0)
            {
                int bidpricevalue = int.Parse(dt.Rows[0]["Bidpricers"].ToString());
                DataTable dt1 = new DataTable();
                dt1 = util.Display("exec CRUDBid 'CheckmultipleGethighestproxybytime',0, 0,'','',''," + bidpricevalue + ", 0,'','',0,0,0,0,''," + productid + "");
                if (dt1.Rows.Count == 2)
                {
                    value = int.Parse(dt.Rows[0]["UserId"].ToString());

                }
            }
            else
            {
                value = 0;
            }

            response = Request.CreateResponse(HttpStatusCode.OK, value);
            return response;
        }
    }
}
