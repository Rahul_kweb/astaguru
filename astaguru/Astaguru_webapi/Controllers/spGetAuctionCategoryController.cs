﻿using Astaguru_webapi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class spGetAuctionCategoryController : ApiController
    {
        Utility util = new Utility();
        HttpResponseMessage response = new HttpResponseMessage();
        public HttpResponseMessage Get(string auctionType,int auctionID)
        {


            List<Artist> objAuctionlist = new List<Artist>();
            DataTable dt = new DataTable();

            dt = util.Display("Exec spGetAuctionCategory '"+ auctionType + "',"+ auctionID + "");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    Artist objartist = new Artist();
                    objartist.FirstName = dr["FirstName"].ToString();
                    objartist.LastName = dr["LastName"].ToString();
                    objartist.artistid = int.Parse(dr["artistid"].ToString());
                    objAuctionlist.Add(objartist);



                }
            }


            response = Request.CreateResponse(HttpStatusCode.OK, objAuctionlist);
            return response;

        }
    }
}
