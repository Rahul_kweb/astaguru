﻿using Astaguru_webapi.Models;
using Astaguru_webapi.Services.UserService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Astaguru_webapi.Controllers
{
    public class UpcomingProxyBidController : ApiController
    {
        Utility util = new Utility();
        CurrentAuctionService CAS = new CurrentAuctionService();
        UserService US = new UserService();

        HttpResponseMessage response;
        public HttpResponseMessage Get(int inSiteUserID, int inProductID, int inProxyAmt, int inProxyAmtus,string inCreatedby,int inAuctionid, int bidByVal,string deviceTocken,string OSversion,string modelName,string ipAddress,string userLocation,string inFullAddress,string inShortAddress,string inLatitude,string inLongitude, int amountlimtuser,int isOldUser,string Country,int Ownerid)
        {
            DataTable dt = new DataTable();
            List<Upcomming_Proxy> objUpcomming_Proxylist = new List<Upcomming_Proxy>();

            try
            {

                //int amountlimt = AUC.amountlimt;
                int userid = Convert.ToInt32(inSiteUserID);   //COMC.checkUserSessionValue();
                int amountlimt = Convert.ToInt32(amountlimtuser);
               

                if (userid > 0)
                {
                    if (isOldUser == 0)
                    {
                        if (Country == "India")
                        {
                            if (inProxyAmt >= amountlimt)
                            {
                                Upcomming_Proxy objupcom = new Upcomming_Proxy();
                                objupcom.currentStatus = "3";
                                objupcom.msg = "Your Bid Limit Exceeded. Kindly Contact with Your Administrator";
                                objupcom.auctionDate = "";
                                objUpcomming_Proxylist.Add(objupcom);

                                response = Request.CreateResponse(HttpStatusCode.OK, objUpcomming_Proxylist);
                                return response;
                            }
                        }
                        else
                        {
                            if (inProxyAmtus >= amountlimt)
                            {
                                Upcomming_Proxy objupcom = new Upcomming_Proxy();
                                objupcom.currentStatus = "3";
                                objupcom.msg = "Your Bid Limit Exceeded. Kindly Contact with Your Administrator";
                                objupcom.auctionDate = "";
                                objUpcomming_Proxylist.Add(objupcom);

                                response = Request.CreateResponse(HttpStatusCode.OK, objUpcomming_Proxylist);
                                return response;
                            }
                        }
                    }

                    if (Ownerid == userid)
                    {
                        Upcomming_Proxy objupcom = new Upcomming_Proxy();
                        objupcom.currentStatus = "5";
                        objupcom.msg = "Sorry, you’re not permitted to bid on this lot. Please contact admin for details.";
                        objupcom.auctionDate = "";
                        objUpcomming_Proxylist.Add(objupcom);

                        response = Request.CreateResponse(HttpStatusCode.OK, objUpcomming_Proxylist);
                        return response;
                    }



                    dt = util.Display("Exec spUpcomingProxyBid "+ inSiteUserID + ","+ inProductID + ","+ inProxyAmt + ","+ inProxyAmtus + ",'"+ inCreatedby + "',"+ inAuctionid + ","+ bidByVal + ",'"+ deviceTocken + "','"+ OSversion + "','"+ modelName + "','"+ ipAddress + "','"+ userLocation + "','"+ inFullAddress + "','"+ inShortAddress + "','"+ inLatitude + "','"+ inLongitude + "','"+ amountlimt + "'");
                    if (dt.Rows.Count > 0)
                    {
                        Upcomming_Proxy objupcom = new Upcomming_Proxy();
                        foreach (DataRow dr in dt.Rows)
                        {
                            
                            objupcom.currentStatus = dr["currentStatus"].ToString().Trim();
                            objupcom.msg = dr["msg"].ToString();
                            objupcom.auctionDate = dr["auctionDate"].ToString();
                            objUpcomming_Proxylist.Add(objupcom);

                            if (objupcom.currentStatus == "1")
                            {

                                // Update Bid Limit Function
                                if (isOldUser == 0)
                                {
                                    int updateLimit = amountlimt - inProxyAmt;
                                    US.UpdateBidLimit(userid, updateLimit);

                                }
                            }
                      
                        }

                        response = Request.CreateResponse(HttpStatusCode.OK, objUpcomming_Proxylist);
                        return response;
                    }



                }
            }
            catch (Exception ex)
            {
                Upcomming_Proxy objupcom = new Upcomming_Proxy();
                objupcom.currentStatus = "4";
                objupcom.msg = "Something went wrong,try after some time";
                objupcom.auctionDate = "";
                objUpcomming_Proxylist.Add(objupcom);

                response = Request.CreateResponse(HttpStatusCode.BadRequest, objUpcomming_Proxylist);
                return response;
            }

            return response;

        }
    }
}
