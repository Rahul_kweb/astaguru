﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class EmailSend
    {
        public string reply_to_email { get; set; }

        public string body_text { get; set; }

        public string from_email { get; set; }

        public string subject { get; set; }

        public string reply_to_name { get; set; }

        public string template { get; set; }

        public IList<to> to { get; set; }


    }

    public class to
    {
        public string name { get; set; }

        public string email { get; set; }
    }
}