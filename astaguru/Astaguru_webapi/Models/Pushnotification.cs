﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class Pushnotification
    {
        public int NotificationId { get; set; }
        public int userid { get; set; }
        public string device_id { get; set; }

        public string device_type { get; set; }

        public string subject { get; set; }

        public string shortdescription { get; set; }
        public string description { get; set; }

        public string CreatedDate { get; set; }
    }
}