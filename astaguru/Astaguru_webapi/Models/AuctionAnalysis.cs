﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class AuctionAnalysis
    {
        public string auctionAnalysisID { get; set; }

        public string auctionTitle { get; set; }


        public string numberOfLots { get; set; }

        public string lotPercentage { get; set; }
        public string winningValueRs { get; set; }

        public string winingValueUs { get; set; }
        public string buyersPreminum { get; set; }

        public string typeFlg { get; set; }


    }

    public class AuctionListAuctionAnalysis
    {
        public string Auctionname { get; set; }

        public string Date { get; set; }

        public string DollarRate { get; set; }
    }

    public class AuctionforAuctionAnalysis
    {
        public string productid { get; set; }

        public string title { get; set; }

        public string pricers { get; set; }


        public string priceus { get; set; }


        public string FirstName { get; set; }

        public string LastName { get; set; }
    }


}