﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class AuctionProductdetails
    {
        public int productid { get; set; }

        public string title { get; set; }

        public string description { get; set; }

        public int artistid { get; set; }

        public int pricers { get; set; }

        public int priceus { get; set; }

        public int categoryid { get; set; }

        public int styleid { get; set; }
        public int mediumid { get; set; }

        public string featured { get; set; }

        public string collectors { get; set; }

        public string thumbnail { get; set; }

        public string image { get; set; }

        public string productsize { get; set; }

        public int timecount { get; set; }
        public string productdate { get; set; }

        public string reference { get; set; }

        public string proxy { get; set; }

        public int proxyamount { get; set; }

        public int proxyuserid { get; set; }

        public string Pproxyusername { get; set; }


        //public string myBidClosingTime { get; set; }

        //public int timeRemains { get; set; }

        //[DisplayFormat(DataFormatString = "{0:dd MMM yyyy hh:mm:ss}")]
        public string Bidclosingtime { get; set; }

        //public string currentDate { get; set; }
        public string Online { get; set; }

        public string estamiate { get; set; }

        public string smallimage { get; set; }
        public int pricelow { get; set; }

        public int pricehigh { get; set; }

        public string active { get; set; }

        public string Prdescription { get; set; }

        public string PrVat { get; set; }

        public string HumanFigure { get; set; }

        public int Threshhold { get; set; }

        public string Abrasion { get; set; }

        public string Blistering { get; set; }

        public string Damage { get; set; }

        public string Cupping { get; set; }

        public string Discoloration { get; set; }

        public string Deterioration { get; set; }

        public string Cracking { get; set; }

        public string Scratches { get; set; }

        public string Fungus { get; set; }

        public string Restoration { get; set; }

        public string ConditionDetails { get; set; }

        public string Flaking { get; set; }

        public string Crease { get; set; }

        public string view1 { get; set; }
        public string view2 { get; set; }
        public string view3 { get; set; }
        public string view4 { get; set; }

        public int isMargin { get; set; }

        public string listImage { get; set; }

        //public string FirstName { get; set; }
        //public int Myreference { get; set; }

        //public string LastName { get; set; }

        //public string Picture { get; set; }

        //public string Profile { get; set; }

        //public string category { get; set; }

        //public string style { get; set; }
        //public string medium { get; set; }

        //public string Auctionname { get; set; }

        //public string auctionBanner { get; set; }

        //public int DollarRate { get; set; }

        //public int MyUserID { get; set; }
        //public int auctionType { get; set; }

        public int isInternational { get; set; }

        public int isInternationalGST { get; set; }

        public int astaguruPrice { get; set; }

        public int usedGoodPercentage { get; set; }

        public int nonExportable { get; set; }



        //public IEnumerable<Artist> artist_by_artistid { get; set; }
        //public IEnumerable<Category> category_by_categoryid { get; set; }

        //public IEnumerable<Medium> medium_by_mediumid { get; set; }


        public Artist artist_by_artistid { get; set; }
        public Category category_by_categoryid { get; set; }

        public Medium medium_by_mediumid { get; set; }




    }

    public class Artist
    {
        public int artistid { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }
        public string artistofthemonth { get; set; }


        public string Profile { get; set; }
        public string Picture { get; set; }

        public string Title { get; set; }
    }


    public class Category
    {
        public int categoryid { get; set; }
        public string category { get; set; }

        public string PrVat { get; set; }
    }

    public class Medium
    {
        public int mediumid { get; set; }
        public string medium { get; set; }


    }


    public class resourceArtist
    {
        public List<Artist> resource { get; set; }

    }

}