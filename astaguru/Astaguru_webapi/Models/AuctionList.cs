﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class AuctionList
    {
        public int AuctionId { get; set; }

        public string Auctionname { get; set; }

        public string Date { get; set; }

        public int DollarRate { get; set; }

        public string image { get; set;}

        public string auctiondate { get; set; }


        public string auctiontitle { get; set; }

        public string bidpdf { get; set; }

        public string status { get; set; }

        public string totalSaleValueRs { get; set;}

        public string totalSaleValueUs { get; set; }

        public string auctionBanner { get; set; }

        public int upcomingCountVal { get; set; }

        public int auctionType { get; set;}

    }


    public class resourceAuctionlist
    {
        public List<AuctionList> resource { get; set; }
    }
}