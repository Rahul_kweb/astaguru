﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using WebGrease.Activities;
using Repository.Lib;


namespace Astaguru_webapi.Models
{
    public class Utility
    {
        #region Connection
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ConnectionString);
        #endregion

        #region Execute Query
        public bool Execute(SqlCommand cmd)
        {
            try
            {

                con.Open();
                cmd.Connection = con;
                int i = cmd.ExecuteNonQuery();
                return (i > 0);
            }
            catch (Exception ex)
            {
                //LogError log = new LogError();
                //log.HandleException(ex);

                return false;
                //throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region Get Data Into Table by command
        public DataTable Display(SqlCommand cmd) //using overloading methord
        {
            try
            {
                con.Open();
                cmd.Connection = con;
                SqlDataAdapter da = new SqlDataAdapter(cmd); //get data into adapter

                DataTable dt = new DataTable();
                da.Fill(dt);    //store into table
                return dt;
            }
            catch (Exception ex)
            {
                //LogError log = new LogError();
                //log.HandleException(ex);

                return null;
                //throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region Get Data Into Table by query
        public DataTable Display(string query) //using overloading methord
        {
            try
            {
                con.Open();

                SqlDataAdapter da = new SqlDataAdapter(query, con); //get data into adapter

                DataTable dt = new DataTable();
                da.Fill(dt);    //store into table

                con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                //LogError log = new LogError();
                //log.HandleException(ex);

                return null;
                //throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region Get Data Into Set by command
        public DataSet Display1(SqlCommand cmd) //using overloading methord
        {
            try
            {
                con.Open();
                cmd.Connection = con;
                SqlDataAdapter da = new SqlDataAdapter(cmd); //get data into adapter

                DataSet ds = new DataSet();
                da.Fill(ds);    //store into table
                return ds;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                return null;
                //throw ex;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion

        #region Get Data Into Set by query
        public DataSet Display1(string query) //using overloading methord
        {
            try
            {
                con.Open();

                SqlDataAdapter da = new SqlDataAdapter(query, con); //get data into adapter

                DataSet ds = new DataSet();
                da.Fill(ds);    //store into table

                con.Close();
                return ds;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                return null;
                //throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        #endregion


        #region Send Email
        public bool SendEmail(string Emailbody, string[] Toemailids, string Subject, string FilePath, Stream input)
        {
            bool result = false;
            #region Send Mail
            try
            {
                string EmailUserName = ConfigurationManager.AppSettings["smtpUser"].ToString();
                string EmailPassword = ConfigurationManager.AppSettings["smtpPassword"].ToString();
                string EmailHost = ConfigurationManager.AppSettings["smtpHost"].ToString();
                string EmailPort = ConfigurationManager.AppSettings["smtpPort"].ToString();
                bool EnableSsl = true;
                //bool EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"].ToString());
                string subject = Subject;

                string ToEmailid = string.Empty;
                for (int i = 0; i < Toemailids.Length; i++)
                {
                    ToEmailid += Toemailids[i] + ",";
                }
                if (ToEmailid != "")
                {
                    ToEmailid = ToEmailid.Substring(0, ToEmailid.Length - 1);
                }

                string toEmail = ToEmailid;
                string body = Emailbody;


                System.Net.Mail.MailMessage Msg = new System.Net.Mail.MailMessage();
                // Sender e-mail address.
                Msg.From = new MailAddress(EmailUserName, "Astaguru");
                // Recipient e-mail address.
                Msg.To.Add(toEmail);
                Msg.Subject = subject;
                Msg.Body = body;
                Msg.IsBodyHtml = true;

                //file upload
                if (FilePath != "")
                {
                    Attachment attach = new Attachment(input, FilePath);
                    Msg.Attachments.Add(attach);
                }


                // your remote SMTP server IP.
                SmtpClient smtp = new SmtpClient();

                smtp.Host = EmailHost;

                smtp.Port = int.Parse(EmailPort);
                // smtp.Port = 25;
                smtp.Credentials = new System.Net.NetworkCredential(EmailUserName, EmailPassword);

                smtp.EnableSsl = EnableSsl;
                //smtp.UseDefaultCredentials = false;

                smtp.Send(Msg);
                // strSuccess = "Success";

                result = true; //success
            }
            catch (Exception ex)
            {
                Log log = new Log();
                //log.HandleException(ex);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

                result = false; //failed
                //  ViewBag.Error = "Try again after some time.";
                ///return View();
            }

            return result;
            #endregion
        }
        #endregion


        public bool IsValidImageFileExtension(string extension)
        {
            return (extension == ".jpg" || extension == ".jpeg" || extension == ".png" || extension == ".bmp");
        }


    }
}