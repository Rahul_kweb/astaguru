﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class UserEmailMobileVerified
    {
        public string userid { get; set; }

        public string MobileVerified { get; set; }

        public string EmailVerified { get; set; }
    }

    public class UserEmailMobileVerifiedresource
    {
        
        public IList<UserEmailMobileVerified> resource { get; set; }
    }
}