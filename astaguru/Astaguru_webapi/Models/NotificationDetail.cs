﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class NotificationDetail
    {
        public string NotificationID { get; set; }
        public string UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IsRead { get; set; }
        public string NotificationBody { get; set; }
        public string CreatedDate { get; set; }

        public string msg { get; set; }
    }
}