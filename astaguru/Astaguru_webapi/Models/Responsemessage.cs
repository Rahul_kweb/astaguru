﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class Responsemessage
    {
        public int currentStatus { get; set; }

        public string msg { get; set; }

        public string mobileNum { get; set; }

        public string emailID { get; set; }

        public string Username { get; set; }

        public int BidAmountRs { get; set; }

        public int BidAmountUs { get; set; }


        public int outBidAmountRs{ get; set; }

        public int outBidAmountUs { get; set; }


        public int lastBidpriceRs { get; set; }

        public int lastBidpriceUs { get; set; }
    }
}