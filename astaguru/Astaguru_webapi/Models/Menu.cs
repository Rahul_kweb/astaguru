﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class Menu
    {
        public int Id { get; set; }

        public string Keys { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }

        public string Icon { get; set; }

    }
}