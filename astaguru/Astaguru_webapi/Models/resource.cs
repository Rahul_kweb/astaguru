﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class resource
    {
        public int userid { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string password { get; set; }

        public int confirmbid { get; set; }

        public string applyforbid { get; set; }

        public string MobileVerified { get; set; }

        public string EmailVerified { get; set; }


        public string BillingName { get; set; }
        public string BillingAddress { get; set; }

        public string billingAddress2 { get; set; }

        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingCountry { get; set; }
        public string BillingZip { get; set; }
        public string BillingEmail { get; set; }
        public string BillingTelephone { get; set; }

        public string nickname { get; set; }

        public string countryCode { get; set; }

        public string Mobile { get; set; }

        public string OTPNumber { get; set; }

        public string aboutId { get; set; }
        public string about { get; set; }

        public int interestedId { get; set; }
        public string interest { get; set; }


        public int countryid { get; set; }

        public int stateid { get; set; }

        public int cityid { get; set; }

        public string countryname { get; set; }
        public string statename { get; set; }

        public string cityname { get; set; }

        public string name { get; set; }


        public string address1 { get; set; }
        public string address2 { get; set; }

        public string zip { get; set; }

        public string telephone { get; set; }

        public string fax { get; set; }

        public string admin { get; set; }

        public string lastname { get; set; }

        public string activationcode { get; set; }

        public string SmsCode { get; set; }

        public string city { get; set; }

        public string state { get; set; }
        public string country { get; set; }



        public int genderid { get; set; }

        public int bday { get; set; }

        public int bmonth { get; set; }
        public int byear { get; set; }
        public string interestedIds { get; set; }


        public string GSTIN { get; set; }
        public string panCard { get; set; }
        public string acNumber { get; set; }
        public string holderName { get; set; }
        public string ifscCode { get; set; }
        public string branchName { get; set; }
        public string swiftCode { get; set; }
        public string imagePanCard { get; set; }
        public string imageAadharCard { get; set; }


        public string companyName { get; set; }


        public string aadharCard { get; set; }

        public int amountlimt { get; set; }


        public int bCountryid { get; set; }
        public int bStateid { get; set; }
        public int bCityid { get; set; }

        public int isOldUser { get; set; }

        public string RegistrationDate { get; set; }

        public string LastLoggedDate { get; set; }


        public int Visits { get; set; }

        public int buy { get; set; }

        public int anonumber { get; set; }

        public string Subscribe { get; set; }

        public string blocked { get; set; }

        public string ip { get; set; }

        public string IPCountry { get; set; }

        public string t_username { get; set; }

        public string t_password { get; set; }
        public string t_firstname { get; set; }

        public string t_address1 { get; set; }

        public string t_address2 { get; set; }

        public string t_City { get; set; }



        public string t_State { get; set; }

        public string t_Country { get; set; }
        public string t_zip { get; set; }

        public string t_telephone { get; set; }

        public string t_fax { get; set; }

        public string t_email { get; set; }


        public string t_billingname { get; set; }

        public string t_billingaddress { get; set; }
        public string t_billingcity { get; set; }

        public string t_billingstate { get; set; }

        public string t_billingcountry { get; set; }



        public string t_billingzip { get; set; }


        public string t_lastname { get; set; }

        public string t_nickname { get; set; }
        public string t_mobile { get; set; }

        public string t_billingemail { get; set; }

        public string t_billingtelephone { get; set; }

        public string applyforchange { get; set; }



        public string otptime { get; set; }


        public string chatdept { get; set; }


        public string deviceTocken { get; set; }


        public string androidDeviceTocken { get; set; }
    }
}