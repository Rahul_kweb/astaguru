﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class bidartistuser
    {
        public int bidartistuserid { get; set; }
        public int userid { get; set; }
        public int artistid { get; set; }

        public string msg { get; set; }

    }

    public class resorcebidartistuser
    {
       public List<bidartistuser> resource { get; set; }
    }
}