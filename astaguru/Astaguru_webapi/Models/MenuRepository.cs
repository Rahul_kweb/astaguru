﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class MenuRepository
    {
       static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ConnectionString);
        public static List<Menu> Get()
        {
            // ResponseModel _objResponseModel = new ResponseModel();
            List<Menu> objmenulist = new List<Menu>();

            DataTable dt = new DataTable();
            SqlDataAdapter ad = new SqlDataAdapter("select * from Menu", con);
            ad.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Menu objmenu = new Menu();
                    objmenu.Id = int.Parse(dr["Id"].ToString());
                    objmenu.Keys = dr["Keys"].ToString();
                    objmenu.Title = dr["Title"].ToString();
                    objmenu.Url = dr["Url"].ToString();
                    objmenu.Icon = dr["Icon"].ToString();
                    objmenulist.Add(objmenu);

                }

            }          
            return objmenulist;

        }

    }
}