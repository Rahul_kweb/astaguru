﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru_webapi.Models
{
    public class Bidrecord
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Thumbnail { get; set; }
        public string Bidpricers { get; set; }

        public string Bidpriceus { get; set; }

        public string Username { get; set; }
        public string daterec { get; set; }
        public string UserId { get; set; }
        public string Bidrecordid { get; set; }

        public string validbidpricers { get; set; }

        public string validbidpriceus { get; set; }
        public string currentbid { get; set; }
        public string recentbid { get; set; }
        public string anoname { get; set; }

        public string productid { get; set; }

        public string Auctionid { get; set; }

        public string proxy { get; set; }
    }
}