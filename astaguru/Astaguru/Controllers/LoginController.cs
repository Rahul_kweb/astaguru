﻿using Astaguru.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        Utility util = new Utility();

        [HttpGet]
        public ActionResult Index(string Usertype)
        {
            Login log = new Login();
            if (Usertype == "SuperAdmin")
            {
                ViewBag.Title = "Super Admin Login";
            }
            else
            {
                ViewBag.Title = "Billing Login";
            }
            log.UserType = Usertype;
            return View(log);
        }




        #region Generic Login Code
        [HttpGet]
        public ActionResult Login(string Usertype)
        {
            Login log = new Login();
            if (Usertype == "SuperAdmin")
            {
                ViewBag.Title = "Super Admin Login";
            }
            else
            {
                ViewBag.Title = "Billing Login";
            }
            log.UserType = Usertype;
            return View(log);
        }

        public JsonResult LogProcess(Login logobj)
        {
            var response = "";

            try
            {
                switch (logobj.UserType)
                {
                    case "SuperAdmin":
                        response = SuperAdminLogin(logobj.Username, logobj.Password);
                        break;

                    case "Billing":
                        response = BillingLogin(logobj.Username, logobj.Password);
                        break;

                    default:
                        response = "";
                        break;
                }
            }
            catch
            {
                response = "";
            }
            string Url = string.Empty;
            string Responsetype = response == "" ? "Fail" : "Success";

            return Json(new { Url = response, Responsetype = Responsetype }, JsonRequestBehavior.AllowGet);
        }

        public string SuperAdminLogin(string username, string password)
        {
            string response = string.Empty;
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_SuperAdminLogin 'GET_LOGIN','','" + username + "','" + password + "'");
            if (dt.Rows.Count > 0)
            {
                HttpContext.Response.Cookies["SuperAdminusername"].Value = dt.Rows[0]["username"].ToString();
                HttpContext.Response.Cookies["SuperAdminusername"].Expires = DateTime.Now.AddHours(1);

                HttpContext.Response.Cookies["SuperAdminpasssword"].Value = dt.Rows[0]["password"].ToString();
                HttpContext.Response.Cookies["SuperAdminpasssword"].Expires = DateTime.Now.AddHours(1);

                HttpContext.Response.Cookies["SuperAdminName"].Value = dt.Rows[0]["Name"].ToString();
                HttpContext.Response.Cookies["SuperAdminName"].Expires = DateTime.Now.AddHours(1);

                HttpContext.Response.Cookies["SuperAdminId"].Value = dt.Rows[0]["Id"].ToString();
                HttpContext.Response.Cookies["SuperAdminId"].Expires = DateTime.Now.AddHours(1);

                response = "/AdminCms/index/";
            }
            else
            {
                response = "";
            }
            return response;
        }

        public string BillingLogin(string username, string password)
        {
            string response = string.Empty;
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_SuperAdminLogin 'GET_LOGIN','','" + username + "','" + password + "'");
            if (dt.Rows.Count > 0)
            {
                HttpContext.Response.Cookies["Billingusername"].Value = dt.Rows[0]["username"].ToString();
                HttpContext.Response.Cookies["Billingusername"].Expires = DateTime.Now.AddHours(1);

                HttpContext.Response.Cookies["Billingpasssword"].Value = dt.Rows[0]["password"].ToString();
                HttpContext.Response.Cookies["Billingpasssword"].Expires = DateTime.Now.AddHours(1);

                HttpContext.Response.Cookies["BillingName"].Value = dt.Rows[0]["Name"].ToString();
                HttpContext.Response.Cookies["BillingName"].Expires = DateTime.Now.AddHours(1);

                HttpContext.Response.Cookies["BillingId"].Value = dt.Rows[0]["Id"].ToString();
                HttpContext.Response.Cookies["BillingId"].Expires = DateTime.Now.AddHours(1);

                response = "/BillingDashboard/index/";
            }
            else
            {
                response = "";
            }
            return response;
        }
        #endregion

        #region Generic Forgot Password

        public JsonResult ForgotPassword(Login logobj)
        {
            DataTable dt = new DataTable();
            var response = "";

            try
            {
                switch (logobj.UserType)
                {
                    case "SuperAdmin":
                        dt = util.Display("Exec Proc_SuperAdminLogin 'FORGOT_PASSWORD','','','','" + logobj.Emailid + "'");
                        if (dt.Rows.Count > 0)
                        {
                            response = SendEmail(dt.Rows[0]["Name"].ToString(), dt.Rows[0]["Username"].ToString(), dt.Rows[0]["Password"].ToString(), logobj.UserType, dt.Rows[0]["Emailid"].ToString());
                        }
                        else
                        {
                            response = "";
                        }
                        break;

                    case "Billing":
                        dt = util.Display("Exec Proc_SuperAdminLogin 'FORGOT_PASSWORD','','','','" + logobj.Emailid + "'");
                        if (dt.Rows.Count > 0)
                        {
                            response = SendEmail(dt.Rows[0]["Name"].ToString(), dt.Rows[0]["Username"].ToString(), dt.Rows[0]["Password"].ToString(), logobj.UserType, dt.Rows[0]["Emailid"].ToString());
                        }
                        else
                        {
                            response = "";
                        }
                        break;

                    default:
                        response = "";
                        break;
                }
            }
            catch
            {
                response = "";
            }
            string Msg = string.Empty;
            string Responsetype = response == "" ? "Fail" : "Success";

            return Json(new { Msg = response, Responsetype = Responsetype }, JsonRequestBehavior.AllowGet);
        }

        public string SendEmail(string name, string userid, string password, string usertype, string ToEmailid)
        {
            string response = string.Empty;
            if (name != "")
            {
                StringBuilder sb = new StringBuilder();
                string Name = name;

                sb.Append("<table border='0' cellspacing='0' cellpadding='10' width='500' align='center' style='border: 1px solid #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 14px; border-collapse: collapse; line-height: 20px;'>");
                sb.Append("<tr><td colspan='2' align='center'><img src='/Content/images/alchemy_logo.png' style='width: auto;max-height:100px;' /><br /><hr color='#04869a' /></td></tr>");
                sb.Append("<tr><td colspan='2'>Dear " + Name + ",<br /> Find your login details below :</td> </tr>");
                sb.Append("<tr bgcolor='#f4f4f4'><td width='90'>User Name:</td><td><strong> " + userid + " </strong> </td> </tr>");
                sb.Append("<tr bgcolor='#f4f4f4'><td>Password :</td><td><strong> " + password + " </strong> </td></tr>");
                sb.Append("<tr bgcolor='#f4f4f4'><td colspan='2' align='center' height='5'></td></tr>");
                sb.Append("<tr><td colspan='2' align='center' height='1'></td></tr>");
                sb.Append("<tr><td colspan='2'>Regards,<br /><strong><font color='#04869a'>Patvin</font></strong></td></tr>");
                sb.Append("<tr><td colspan='2' align='center' height='1'></td></tr>");
                sb.Append("</table>");

                String[] emailid = new String[] { ToEmailid };


                bool result = util.SendEmail(sb.ToString(), emailid, "Patvin Forgot Password", "", null);

                if (result)
                {
                    response = "sent";
                }
                else
                {
                    response = "";
                }


            }
            else
            {
                response = "";
            }
            return response;
        }

        #endregion

        #region Generic Logout

        public ActionResult LogOut(string Usertype)
        {

            if (Usertype == "SuperAdmin")
            {
                if (Request.Cookies["SuperAdminusername"] != null && Request.Cookies["SuperAdminusername"] != null)
                {
                    HttpCookie myCookie = new HttpCookie("SuperAdminusername");
                    myCookie.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(myCookie);

                    HttpCookie myCookie1 = new HttpCookie("SuperAdminpasssword");
                    myCookie1.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(myCookie1);

                    HttpCookie myCookie2 = new HttpCookie("SuperAdminName");
                    myCookie1.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(myCookie2);

                    HttpCookie myCookie3 = new HttpCookie("SuperAdminId");
                    myCookie1.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(myCookie3);


                    return RedirectToAction("Index", "Login", new { Usertype = "SuperAdmin" });
                }
            }
            else if (Usertype == "Billing")
            {
                if (Request.Cookies["Billingusername"] != null && Request.Cookies["Billingpasssword"] != null)
                {
                    HttpCookie myCookie = new HttpCookie("Billingusername");
                    myCookie.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(myCookie);

                    HttpCookie myCookie1 = new HttpCookie("Billingpasssword");
                    myCookie1.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(myCookie1);

                    HttpCookie myCookie2 = new HttpCookie("BillingName");
                    myCookie2.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(myCookie2);

                    HttpCookie myCookie3 = new HttpCookie("BillingId");
                    myCookie3.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(myCookie3);

                    HttpCookie myCookie4 = new HttpCookie("ShowHideMenu");
                    myCookie4.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(myCookie4);

                    HttpCookie myCookie5 = new HttpCookie("ShopAction");
                    myCookie5.Expires = DateTime.Now.AddDays(-1d);
                    Response.Cookies.Add(myCookie5);


                    return RedirectToAction("Index", "Login", new { Usertype = "Billing" });
                }
            }
            else
            {
                return RedirectToAction("Index", "Login", new { Usertype = "SuperAdmin" });
            }
            return RedirectToAction("Index", "Login", new { Usertype = "SuperAdmin" });
        }
        #endregion

    }
}