﻿using Astaguru.Authentication;
using Astaguru.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using System.Web.UI;
using iTextSharp.text.pdf;
using System.Net.Mail;
using System.Net;
using System.Web.Script.Serialization;
using iTextSharp.tool.xml;
using Dapper;
using Astaguru.Services.UserService;
using Astaguru.Services;
using Astaguru.Models.AdminCms;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml;

namespace Astaguru.Controllers
{
    [CustomAdminAccessFilter]
    public class AdminCmsController : Controller
    {
        // GET: AdminCms
        Utility util = new Utility();



        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --- Admin Index --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult Index()
        {
            //var count = Counting();
            //ViewBag.CurrentOpening = count["CurrentOpening"].ToString();
            //ViewBag.ApplyForJob = count["ApplyForJob"].ToString();
            //ViewBag.ContactUs = count["ContactUs"].ToString();
            //ViewBag.RequestACallback = count["RequestACallback"].ToString();
            return View();
        }

        public Dictionary<string, string> Counting()
        {
            Dictionary<string, string> objDic = new Dictionary<string, string>();
            StringBuilder strbuil = new StringBuilder();
            DataSet ds = new DataSet();
            ds = util.Display1("Exec Proc_CMS 'adminIndexCount'");
            if (ds.Tables.Count > 0)
            {
                //CurrentOpening
                objDic.Add(ds.Tables[0].Rows[0]["Tbl"].ToString(), ds.Tables[0].Rows[0]["Count"].ToString());

                //ApplyForJob
                objDic.Add(ds.Tables[1].Rows[0]["Tbl"].ToString(), ds.Tables[1].Rows[0]["Count"].ToString());

                //ContactUs
                objDic.Add(ds.Tables[2].Rows[0]["Tbl"].ToString(), ds.Tables[2].Rows[0]["Count"].ToString());

                //RequestACallback
                objDic.Add(ds.Tables[3].Rows[0]["Tbl"].ToString(), ds.Tables[3].Rows[0]["Count"].ToString());
            }
            return objDic;
        }

        #endregion


        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--- Admin Profile Section Start---xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult UpdateProfile()
        {
            DataTable dt = GetAdminDataForProfileUpdate();
            Login log = new Login();

            log.Id = dt.Rows[0]["Id"].ToString();
            log.Name = dt.Rows[0]["Name"].ToString();
            log.Emailid = dt.Rows[0]["Emailid"].ToString();
            log.Mobileno = dt.Rows[0]["Mobileno"].ToString();
            return View(log);
        }

        public DataTable GetAdminDataForProfileUpdate()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = util.Display("Execute Proc_SuperAdminLogin 'GETDETAIL_FOR_UPDATE'");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }

        [HttpPost]
        public JsonResult UpdateProfile(Login logobj)
        {
            var response = "";

            try
            {
                response = UpdateProfileDetails(logobj.Id, logobj.Name, logobj.Emailid, logobj.Mobileno);
            }
            catch
            {
                response = "";
            }
            string Url = string.Empty;
            string Responsetype = response == "" ? "Fail" : "Success";

            return Json(new { Responsetype = Responsetype }, JsonRequestBehavior.AllowGet);
        }

        public string UpdateProfileDetails(string id, string name, string emailid, string mobileno)
        {
            string Response = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_SuperAdminLogin"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PARA", "UPDATEPROFILEDETAILS");
                    cmd.Parameters.AddWithValue("@NAME", name);
                    cmd.Parameters.AddWithValue("@EMAILID", emailid);
                    cmd.Parameters.AddWithValue("@MOBILENO", mobileno);
                    cmd.Parameters.AddWithValue("@ID", id);
                    if (util.Execute(cmd))
                    {
                        Response = "success";
                    }
                    else
                    {
                        Response = "";
                    }
                }
            }
            catch
            {

                Response = "";
            }
            return Response;
        }


        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ChangePassword(Login obj)
        {
            using (SqlCommand cmd = new SqlCommand("Proc_SuperAdminLogin"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PARA", "CHANGE_PASSWORD");
                cmd.Parameters.AddWithValue("@USERNAME", Request.Cookies["SuperAdminusername"].Value);
                cmd.Parameters.AddWithValue("@PASSWORD", obj.OldPassword);
                cmd.Parameters.AddWithValue("@NEWPASSWORD", obj.Password);
                DataTable dt = new DataTable();
                dt = util.Display(cmd);
                if (dt.Rows[0]["update"].ToString() == "correct")
                {
                    ViewBag.message = "Success";
                    ViewBag.message1 = "Your Password has been changed Successfully!";
                }
                else
                {
                    ViewBag.message = "fail";
                    ViewBag.message1 = "Sorry ! Password Could not be changed Successfully!";
                }
                ModelState.Clear(); //make blank to class property
            }
            return View();
        }

        #endregion xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--- Billing Profile Section End---xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


        #region xxxxxxxxxxxxxxxxxxxxxxxxx--- Add Category ---xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 

        public ActionResult Category()
        {
            Category objauction = new Category();
            objauction.GridList = GetcategoryList();
            return View(objauction);
        }

        public List<Category> GetcategoryList()
        {
            try
            {
                List<Category> objAuctionlist = new List<Category>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute spGetcategory");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Category objauc = new Category();
                        objauc.Sr = dr["Sr"].ToString();
                        objauc.categoryid = int.Parse(dr["categoryid"].ToString());
                        objauc.category = dr["Category"].ToString();


                        objAuctionlist.Add(objauc);
                    }
                }
                return objAuctionlist;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }



        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult Category(Category auc)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (auc.categoryid == 0)
                    {
                        if (Savecategory(auc))
                        {
                            ModelState.Clear();
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Category added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Category");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Category could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Category");
                        }
                    }
                    else
                    {
                        if (Updatecategory(auc))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Category updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Category");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Category could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Category");
                        }
                    }
                }
                return View("Category", auc);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }

        }

        public bool Savecategory(Category auc)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("spAddCategory"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pinCategory", auc.category);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        public bool Updatecategory(Category auc)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("spUpdateCategory"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Update");
                cmd.Parameters.AddWithValue("@pinCategoryId", auc.categoryid);
                cmd.Parameters.AddWithValue("@pinCategory", auc.category);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }



        public ActionResult EditCategory(int? Id)
        {
            Category auc = new Category();
            DataTable dt = new DataTable();
            dt = util.Display("Execute spUpdateCategory 'GetById', '" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                auc.categoryid = Convert.ToInt32(dt.Rows[0]["categoryid"].ToString());
                auc.category = dt.Rows[0]["category"].ToString();

                auc.GridList = GetcategoryList();

                ViewBag.ActionType = "Update";
                return View("Category", auc);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("Category", auc);
            }
        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --- Medium --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult Medium()
        {
            Medium auc = new Medium();
            auc.GridList = GetmediumList();
            return View(auc);
        }

        public List<Medium> GetmediumList()
        {
            try
            {
                List<Medium> objMediumlist = new List<Medium>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute spGetmedium");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Medium objauc = new Medium();
                        objauc.Sr = dr["Sr"].ToString();
                        objauc.mediumid = int.Parse(dr["mediumid"].ToString());
                        objauc.medium = dr["medium"].ToString();


                        objMediumlist.Add(objauc);
                    }
                }
                return objMediumlist;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Medium(Medium auc)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (auc.mediumid == 0)
                    {
                        if (Savemedium(auc))
                        {
                            ModelState.Clear();
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Medium added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Medium");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Medium could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Medium");
                        }
                    }
                    else
                    {
                        if (Updatemedium(auc))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Medium updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Medium");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Medium could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Medium");
                        }
                    }
                }
                return View("Medium", auc);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }

        }

        public bool Savemedium(Medium auc)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("spAddMedium"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pinMedium", auc.medium);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        public bool Updatemedium(Medium auc)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("spUpdateMedium"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Update");
                cmd.Parameters.AddWithValue("@pinMediumId", auc.mediumid);
                cmd.Parameters.AddWithValue("@pinMedium", auc.medium);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }



        public ActionResult Editmedium(int? Id)
        {
            Medium auc = new Medium();
            DataTable dt = new DataTable();
            dt = util.Display("Execute spUpdateMedium 'GetById', '" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                auc.mediumid = Convert.ToInt32(dt.Rows[0]["mediumid"].ToString());
                auc.medium = dt.Rows[0]["medium"].ToString();

                auc.GridList = GetmediumList();

                ViewBag.ActionType = "Update";
                return View("Medium", auc);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("Medium", auc);
            }
        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxx --- Style --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult Style()
        {
            Style auc = new Style();
            auc.GridList = Getstylelist();
            return View(auc);
        }

        public List<Style> Getstylelist()
        {
            try
            {
                List<Style> objMediumlist = new List<Style>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute spGetstyle");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Style objauc = new Style();
                        objauc.Sr = dr["Sr"].ToString();
                        objauc.styleid = int.Parse(dr["styleid"].ToString());
                        objauc.style = dr["style"].ToString();


                        objMediumlist.Add(objauc);
                    }
                }
                return objMediumlist;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Style(Style auc)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (auc.styleid == 0)
                    {
                        if (Savestyle(auc))
                        {
                            ModelState.Clear();
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Style added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Style");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Style could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Style");
                        }
                    }
                    else
                    {
                        if (Updatestyle(auc))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Style updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Style");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Style could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Style");
                        }
                    }
                }
                return View("Style", auc);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }

        }

        public bool Savestyle(Style auc)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("spAddStyle"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pinStyle", auc.style);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        public bool Updatestyle(Style auc)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("spUpdateStyle"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Update");
                cmd.Parameters.AddWithValue("@pinStyleId", auc.styleid);
                cmd.Parameters.AddWithValue("@pinStyle", auc.style);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult Editstyle(int? Id)
        {
            Style auc = new Style();
            DataTable dt = new DataTable();
            dt = util.Display("Execute spUpdateStyle 'GetById', '" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                auc.styleid = Convert.ToInt32(dt.Rows[0]["styleid"].ToString());
                auc.style = dt.Rows[0]["style"].ToString();

                auc.GridList = Getstylelist();

                ViewBag.ActionType = "Update";
                return View("Style", auc);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("Style", auc);
            }
        }


        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxx ------ Manage Auction List --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult AuctionList()
        {
            AuctionList auc = new AuctionList();
            auc.GridList = GetAuctionlist();
            return View(auc);
        }

        public List<AuctionList> GetAuctionlist()
        {
            try
            {
                List<AuctionList> objAuctionlist = new List<AuctionList>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_AuctionList 'Get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        AuctionList objauc = new AuctionList();

                        objauc.Sr = dr["Sr"].ToString();

                        objauc.Auctionid = int.Parse(dr["AuctionId"].ToString());
                        objauc.Auctionname = dr["Auctionname"].ToString();
                        objauc.Date = dr["Date"].ToString();
                        if (dr["DollarRate"].ToString() != "")
                        {
                            objauc.DollarRate = Convert.ToDecimal(dr["DollarRate"].ToString());
                        }
                        else
                        {
                            objauc.DollarRate = 0;
                        }

                        objauc.image = dr["image"].ToString();
                        objauc.auctiondate = dr["auctiondate"].ToString();
                        objauc.status = dr["status"].ToString();
                        objauc.auctionType = int.Parse(dr["auctionType"].ToString());

                        //string totalrs = dr["totalSaleValueRs"].ToString();
                        //totalrs = totalrs.Replace(",", "");
                        //objauc.totalrs = int.Parse(totalrs);

                        objauc.totalSaleValueRs = dr["totalSaleValueRs"].ToString();
                        objauc.totalSaleValueUs = dr["totalSaleValueUs"].ToString();

                        objauc.auctionBanner = dr["auctionBanner"].ToString();
                        objauc.auctionWebImage = dr["auctionWebImage"].ToString();
                        objauc.recentAuctionBanner = dr["recentAuctionBanner"].ToString();

                        objAuctionlist.Add(objauc);
                    }
                }
                return objAuctionlist;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AuctionList(AuctionList objAuction, HttpPostedFileBase[] Image, HttpPostedFileBase[] auctionBanner, HttpPostedFileBase[] auctionWebImage, HttpPostedFileBase[] recentAuctionBanner)
        {
            try
            {
                if (objAuction.Auctionid != 0)
                {
                    ModelState.Remove("Image");
                    ModelState.Remove("auctionBanner");
                    ModelState.Remove("auctionWebImage");
                    ModelState.Remove("recentAuctionBanner");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFileImage = "~/images/auction/";
                    string VirtualFileauctionBanner = "~/images/banner/";
                    string VirtualauctionWebImage = "~/Content/webImages/";
                    string VirtualauctionrecentAuctionBanner = "~/Content/recentAuctionBanner/";
                    string Img = string.Empty;
                    string Imgbanner = string.Empty;
                    string Imgweb = string.Empty;
                    string Imgrecent = string.Empty;



                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("AuctionList");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "Image-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFileImage + fileName));
                                    VirtualFileImage = VirtualFileImage.Replace("~", "");
                                    Img += VirtualFileImage + fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objAuction.image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (auctionBanner != null)
                    {
                        foreach (HttpPostedFileBase file in auctionBanner)
                        {

                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("AuctionList");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "Auctionbanner-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFileauctionBanner + fileName));
                                    VirtualFileauctionBanner = VirtualFileauctionBanner.Replace("~", "");
                                    Imgbanner += VirtualFileauctionBanner + fileName + ",";
                                }
                            }
                        }

                        if (Imgbanner != "")
                        {
                            objAuction.auctionBanner = Imgbanner.Substring(0, Imgbanner.Length - 1);
                        }
                    }

                    if (auctionWebImage != null)
                    {
                        foreach (HttpPostedFileBase file in auctionWebImage)
                        {

                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("AuctionList");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "AuctionWebImage-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualauctionWebImage + fileName));
                                    Imgweb += fileName + ",";
                                }
                            }
                        }

                        if (Imgweb != "")
                        {
                            objAuction.auctionWebImage = Imgweb.Substring(0, Imgweb.Length - 1);
                        }
                    }

                    if (recentAuctionBanner != null)
                    {
                        foreach (HttpPostedFileBase file in recentAuctionBanner)
                        {

                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("AuctionList");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "RecentAuctionBanner-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualauctionrecentAuctionBanner + fileName));
                                    Imgrecent += fileName + ",";
                                }
                            }
                        }

                        if (Imgrecent != "")
                        {
                            objAuction.recentAuctionBanner = Imgrecent.Substring(0, Imgrecent.Length - 1);
                        }
                    }



                    if (objAuction.Auctionid == 0)
                    {
                        if (SaveAuctionList(objAuction))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Auction added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("AuctionList");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Auction could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("AuctionList");
                        }
                    }
                    else
                    {
                        if (UpdateAuctionList(objAuction))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Auction updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("AuctionList");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Auction could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("AuctionList");
                        }
                    }
                }
                return View("AuctionList", objAuction);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("AuctionList");
            }

        }



        public bool SaveAuctionList(AuctionList objAuction)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_AuctionList"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "Add");
                cmd.Parameters.AddWithValue("@Auctionname", objAuction.Auctionname);
                cmd.Parameters.AddWithValue("@Date", objAuction.Date);
                cmd.Parameters.AddWithValue("@DollarRate", objAuction.DollarRate);
                cmd.Parameters.AddWithValue("@image", objAuction.image);
                cmd.Parameters.AddWithValue("@auctiondate", objAuction.auctiondate);
                cmd.Parameters.AddWithValue("@auctiontitle", objAuction.Auctionname);
                cmd.Parameters.AddWithValue("@status", objAuction.status);
                cmd.Parameters.AddWithValue("@auctionType", objAuction.auctionType);
                cmd.Parameters.AddWithValue("@totalSaleValueRs", objAuction.totalSaleValueRs);
                cmd.Parameters.AddWithValue("@totalSaleValueUs", objAuction.totalSaleValueUs);
                cmd.Parameters.AddWithValue("@auctionBanner", objAuction.auctionBanner);
                cmd.Parameters.AddWithValue("@auctionWebImage", objAuction.auctionWebImage);
                cmd.Parameters.AddWithValue("@recentAuctionBanner", objAuction.recentAuctionBanner);
                cmd.Parameters.AddWithValue("@Slug", Generateslg(objAuction.Auctionname));



                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateAuctionList(AuctionList objAuction)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_AuctionList"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "Update");
                cmd.Parameters.AddWithValue("@AuctionId", objAuction.Auctionid);
                cmd.Parameters.AddWithValue("@Auctionname", objAuction.Auctionname);
                cmd.Parameters.AddWithValue("@Date", objAuction.Date);
                cmd.Parameters.AddWithValue("@DollarRate", objAuction.DollarRate);
                cmd.Parameters.AddWithValue("@image", objAuction.image);
                cmd.Parameters.AddWithValue("@auctiondate", objAuction.auctiondate);
                cmd.Parameters.AddWithValue("@auctiontitle", objAuction.Auctionname);
                cmd.Parameters.AddWithValue("@status", objAuction.status);
                cmd.Parameters.AddWithValue("@auctionType", objAuction.auctionType);
                cmd.Parameters.AddWithValue("@totalSaleValueRs", objAuction.totalSaleValueRs);
                cmd.Parameters.AddWithValue("@totalSaleValueUs", objAuction.totalSaleValueUs);
                cmd.Parameters.AddWithValue("@auctionBanner", objAuction.auctionBanner);
                cmd.Parameters.AddWithValue("@auctionWebImage", objAuction.auctionWebImage);
                cmd.Parameters.AddWithValue("@recentAuctionBanner", objAuction.recentAuctionBanner);
                cmd.Parameters.AddWithValue("@Slug", Generateslg(objAuction.Auctionname));

                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditAuctionList(int? Id)
        {
            AuctionList objauc = new AuctionList();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_AuctionList 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objauc.Auctionid = int.Parse(dt.Rows[0]["AuctionId"].ToString());
                objauc.Auctionname = dt.Rows[0]["Auctionname"].ToString();
                objauc.Date = dt.Rows[0]["Date"].ToString();
                if (dt.Rows[0]["DollarRate"].ToString() != "")
                {
                    objauc.DollarRate = Convert.ToDecimal(dt.Rows[0]["DollarRate"].ToString());
                }
                else
                {
                    objauc.DollarRate = 0;
                }
                objauc.image = dt.Rows[0]["image"].ToString();
                objauc.imgPreview = dt.Rows[0]["Image"].ToString();
                objauc.auctiondate = dt.Rows[0]["auctiondate"].ToString();
                objauc.status = dt.Rows[0]["status"].ToString();
                objauc.auctionType = int.Parse(dt.Rows[0]["auctionType"].ToString());
                objauc.totalSaleValueRs = dt.Rows[0]["totalSaleValueRs"].ToString();
                objauc.totalSaleValueUs = dt.Rows[0]["totalSaleValueUs"].ToString();
                objauc.auctionBanner = dt.Rows[0]["auctionBanner"].ToString();
                objauc.imgPreview1 = dt.Rows[0]["auctionBanner"].ToString();
                objauc.auctionWebImage = dt.Rows[0]["auctionWebImage"].ToString();
                objauc.imgPreview2 = dt.Rows[0]["auctionWebImage"].ToString();
                objauc.recentAuctionBanner = dt.Rows[0]["recentAuctionBanner"].ToString();
                objauc.imgPreview3 = dt.Rows[0]["recentAuctionBanner"].ToString();

                objauc.GridList = GetAuctionlist();

                ViewBag.ActionType = "Update";
                return View("AuctionList", objauc);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("Services", objauc);
            }
        }

        public ActionResult DeleteAuctionList(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_AuctionList"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "Delete");
                    cmd.Parameters.AddWithValue("@AuctionId", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("AuctionList");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("AuctionList");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("AuctionList");
                //throw;
            }

        }

        #endregion

        #region xxxxxxxxxxxxxxxxxx -- Artist Profile --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult Artist()
        {
            Artist auc = new Artist();
            auc.GridList = GetArtistList();
            return View(auc);
        }

        public List<Artist> GetArtistList()
        {
            try
            {
                List<Artist> objAuctionlist = new List<Artist>();
                DataTable dt = new DataTable();
                dt = util.Display("Exec spupdateartist 'Get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Artist auc = new Artist();
                        auc.Sr = dr["sr"].ToString();
                        auc.artistid = int.Parse(dr["artistid"].ToString());
                        auc.firstname = dr["firstname"].ToString();
                        auc.lastname = dr["lastname"].ToString();
                        auc.Profile = dr["Profile"].ToString();
                        auc.Picture = dr["Picture"].ToString();


                        objAuctionlist.Add(auc);
                    }
                }
                return objAuctionlist;

            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Artist(Artist objAuction, HttpPostedFileBase[] Picture)
        {
            try
            {
                if (objAuction.artistid != 0)
                {
                    ModelState.Remove("Picture");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFileImage = "~/images/Artist-images/";
                    string Img = string.Empty;

                    if (Profile != null)
                    {
                        foreach (HttpPostedFileBase file in Picture)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("Artist");
                                }
                                else
                                {
                                    var fileName = "Artist-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFileImage + fileName));
                                    VirtualFileImage = VirtualFileImage.Replace("~", "");
                                    Img += VirtualFileImage + fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objAuction.Picture = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objAuction.artistid == 0)
                    {
                        if (SaveArtist(objAuction))
                        {
                            ModelState.Clear();
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Artist added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Artist");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Artist could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Artist");
                        }
                    }
                    else
                    {
                        if (UpdateArtist(objAuction))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Artist updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Artist");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Artist could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Artist");
                        }
                    }
                }
                return View("Artist", objAuction);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }

        }

        public bool SaveArtist(Artist auc)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("spAddartist"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@pinname", auc.firstname);
                cmd.Parameters.AddWithValue("@pinLastname", auc.lastname);
                cmd.Parameters.AddWithValue("@pinprofile", auc.Profile);
                cmd.Parameters.AddWithValue("@pinpicture", "/images/Artist-images/" + auc.Picture);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        public bool UpdateArtist(Artist auc)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("spupdateartist"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Update");
                cmd.Parameters.AddWithValue("@pinartistId", auc.artistid);
                cmd.Parameters.AddWithValue("@pinname", auc.firstname);
                cmd.Parameters.AddWithValue("@pinLastname", auc.lastname);
                cmd.Parameters.AddWithValue("@pinprofile", auc.Profile);
                cmd.Parameters.AddWithValue("@pinpicture", "/images/Artist-images/" + auc.Picture);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }



        public ActionResult EditArtist(int? Id)
        {
            Artist auc = new Artist();
            DataTable dt = new DataTable();
            dt = util.Display("Execute spupdateartist 'GetById', '" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                auc.artistid = Convert.ToInt32(dt.Rows[0]["artistid"].ToString());
                auc.firstname = dt.Rows[0]["firstname"].ToString();
                auc.lastname = dt.Rows[0]["lastname"].ToString();
                auc.Profile = dt.Rows[0]["profile"].ToString();
                auc.Picture = dt.Rows[0]["picture"].ToString();
                auc.imgPreview = dt.Rows[0]["picture"].ToString();
                auc.GridList = GetArtistList();

                ViewBag.ActionType = "Update";
                return View("Artist", auc);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("Artist", auc);
            }
        }



        public ActionResult DeleteArtist(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("delete from artist where artistid='" + Id + "'"))
                {
                    //cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.AddWithValue("@Para", "Delete");
                    //cmd.Parameters.AddWithValue("@artistid", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Artist");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Artist");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("Artist");
                //throw;
            }

        }


        #endregion

        #region xxxxxxxxxxxxxxxxxx ----- All Users ----- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult Users()
        {
            User objuser = new User();
            objuser.GridList = GetUserList();
            return View(objuser);
        }

        public List<User> GetUserList()
        {
            try
            {
                List<User> objlist = new List<User>();
                DataTable dt = new DataTable();
                dt = util.Display("Exec Proc_Users 'Get'");
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        User objuser = new User();
                        objuser.Sr = dr["Sr"].ToString();
                        objuser.userid = int.Parse(dr["userid"].ToString());
                        objuser.username = dr["username"].ToString();
                        objuser.password = dr["password"].ToString();
                        objuser.name = dr["name"].ToString();
                        objuser.address1 = dr["address1"].ToString();
                        objuser.address2 = dr["address2"].ToString();
                        objuser.city = dr["city"].ToString();
                        objuser.state = dr["state"].ToString();
                        objuser.country = dr["country"].ToString();
                        objuser.zip = dr["zip"].ToString();
                        objuser.BillingTelephone = dr["telephone"].ToString();
                        objuser.Mobile = dr["Mobile"].ToString();
                        objuser.email = dr["email"].ToString();
                        objuser.RegistrationDate = dr["RegistrationDate"].ToString();
                        objuser.LastLoggedDate = dr["LastLoggedDate"].ToString();

                        if (int.Parse(dr["confirmbid"].ToString()) == 1)
                        {
                            objuser.bconfirmbid = true;
                        }
                        else
                        {
                            objuser.bconfirmbid = false;
                        }

                        //objuser.confirmbid = int.Parse(dr["confirmbid"].ToString());


                        if (dr["amountlimt"].ToString().Trim() != "")
                        {
                            objuser.amountlimt = int.Parse(dr["amountlimt"].ToString());
                        }
                        else
                        {
                            objuser.amountlimt = 0;
                        }

                        objuser.lastname = dr["lastname"].ToString();

                        if (dr["applyforbid"].ToString().Replace(" ", string.Empty) == "1")
                        {
                            objuser.bapplyforbid = true;
                        }
                        else
                        {
                            objuser.bapplyforbid = false;
                        }



                        if (dr["MobileVerified"].ToString().Trim() == "True")
                        {
                            objuser.bMobileVerified = true;
                        }
                        else if (dr["MobileVerified"].ToString().Trim() == null || dr["MobileVerified"].ToString().Trim() == "")
                        {
                            objuser.bMobileVerified = false;
                        }
                        else
                        {
                            objuser.bMobileVerified = false;
                        }

                        //string value = dr["EmailVerified"].ToString().Trim();

                        if (dr["EmailVerified"].ToString().Trim() == "True")
                        {
                            objuser.bEmailVerified = true;
                        }
                        else if (dr["EmailVerified"].ToString().Trim() == null || dr["EmailVerified"].ToString().Trim() == "")
                        {
                            objuser.bEmailVerified = false;
                        }
                        else
                        {
                            objuser.bEmailVerified = false;
                        }




                        if (int.Parse(dr["isOldUser"].ToString()) == 1)
                        {
                            objuser.bisOldUser = true;
                        }
                        else
                        {
                            objuser.bisOldUser = false;
                        }

                        objuser.BillingName = dr["BillingName"].ToString();
                        objuser.BillingAddress = dr["BillingAddress"].ToString();
                        objuser.BillingCity = dr["BillingCity"].ToString();
                        objuser.BillingState = dr["BillingState"].ToString();
                        objuser.BillingCountry = dr["BillingCountry"].ToString();
                        objuser.BillingZip = dr["BillingZip"].ToString();
                        objuser.GSTIN = dr["GSTIN"].ToString();
                        objuser.panCard = dr["panCard"].ToString();
                        objuser.aadharCard = dr["aadharCard"].ToString();


                        objlist.Add(objuser);
                    }
                }
                return objlist;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Users(User objuser)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (objuser.userid == 0)
                    {

                    }
                    else
                    {
                        if (Updateuser(objuser))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "User updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Users");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "User could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Users");
                        }
                    }
                }
                return View("Users", objuser);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }

        }

        public bool Updateuser(User objuser)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Users"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Update");
                cmd.Parameters.AddWithValue("@userid", objuser.userid);
                cmd.Parameters.AddWithValue("@username", objuser.username);
                cmd.Parameters.AddWithValue("@password", objuser.password);
                cmd.Parameters.AddWithValue("@name", objuser.name);
                cmd.Parameters.AddWithValue("@lastname", objuser.lastname);
                cmd.Parameters.AddWithValue("@address1", objuser.address1);
                cmd.Parameters.AddWithValue("@address2", objuser.address2);
                cmd.Parameters.AddWithValue("@city", objuser.city);
                cmd.Parameters.AddWithValue("@state", objuser.state);
                cmd.Parameters.AddWithValue("@country", objuser.country);
                cmd.Parameters.AddWithValue("@zip", objuser.zip);
                cmd.Parameters.AddWithValue("@telephone", objuser.BillingTelephone);
                cmd.Parameters.AddWithValue("@Mobile", objuser.Mobile);
                cmd.Parameters.AddWithValue("@Email", objuser.email);

                cmd.Parameters.AddWithValue("@confirmbid", objuser.bconfirmbid);
                cmd.Parameters.AddWithValue("@amountlimt", objuser.amountlimt);
                if (objuser.bapplyforbid == true)
                {
                    cmd.Parameters.AddWithValue("@applyforbid", 1);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@applyforbid", 0);
                }
                cmd.Parameters.AddWithValue("@MobileVerified", objuser.bMobileVerified);
                cmd.Parameters.AddWithValue("@EmailVerified", objuser.bEmailVerified);
                cmd.Parameters.AddWithValue("@isOldUser", objuser.bisOldUser);


                cmd.Parameters.AddWithValue("@BillingName", objuser.BillingName);
                cmd.Parameters.AddWithValue("@BillingAddress", objuser.BillingAddress);
                cmd.Parameters.AddWithValue("@BillingCity", objuser.BillingCity);
                cmd.Parameters.AddWithValue("@BillingState", objuser.BillingState);
                cmd.Parameters.AddWithValue("@BillingCountry", objuser.BillingCountry);
                cmd.Parameters.AddWithValue("@BillingZip", objuser.BillingZip);
                cmd.Parameters.AddWithValue("@GSTIN", objuser.GSTIN);
                cmd.Parameters.AddWithValue("@panCard", objuser.panCard);
                cmd.Parameters.AddWithValue("@aadharCard", objuser.aadharCard);

                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditUser(int? userid)
        {

            User model = new User();
            model.GridList = GetUserList();
            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_Users 'GetById', '" + userid + "'");
            if (dt.Rows.Count > 0)
            {
                model.userid = Convert.ToInt32(dt.Rows[0]["userid"].ToString());
                model.username = dt.Rows[0]["username"].ToString();
                model.password = dt.Rows[0]["password"].ToString();
                model.name = dt.Rows[0]["name"].ToString();
                model.address1 = dt.Rows[0]["address1"].ToString();
                model.address2 = dt.Rows[0]["address2"].ToString();
                model.city = dt.Rows[0]["city"].ToString();
                model.state = dt.Rows[0]["state"].ToString();
                model.country = dt.Rows[0]["country"].ToString();
                model.zip = dt.Rows[0]["zip"].ToString();
                model.BillingTelephone = dt.Rows[0]["telephone"].ToString();
                model.Mobile = dt.Rows[0]["Mobile"].ToString();
                model.email = dt.Rows[0]["email"].ToString();

                model.BillingName = dt.Rows[0]["BillingName"].ToString();
                model.BillingAddress = dt.Rows[0]["BillingAddress"].ToString();
                model.BillingCity = dt.Rows[0]["BillingCity"].ToString();
                model.BillingState = dt.Rows[0]["BillingState"].ToString();
                model.BillingCountry = dt.Rows[0]["BillingCountry"].ToString();
                model.BillingZip = dt.Rows[0]["BillingZip"].ToString();
                model.GSTIN = dt.Rows[0]["GSTIN"].ToString();
                model.panCard = dt.Rows[0]["panCard"].ToString();
                model.aadharCard = dt.Rows[0]["aadharCard"].ToString();

                model.lastname = dt.Rows[0]["lastname"].ToString();


                if ((Convert.ToInt32(dt.Rows[0]["confirmbid"].ToString()) == 1))
                {
                    model.bconfirmbid = true;
                }
                else
                {
                    model.bconfirmbid = false;
                }


                if (dt.Rows[0]["amountlimt"].ToString().Trim() != "")
                {
                    model.amountlimt = int.Parse(dt.Rows[0]["amountlimt"].ToString());
                }
                else
                {
                    model.amountlimt = 0;
                }
                //model.amountlimt = Convert.ToInt32(dt.Rows[0]["amountlimt"].ToString());

                if (int.Parse(dt.Rows[0]["applyforbid"].ToString()) == 1)
                {
                    model.bapplyforbid = true;
                }
                else
                {
                    model.bapplyforbid = false;

                }
                //model.bMobileVerified = Convert.ToBoolean(dt.Rows[0]["MobileVerified"].ToString());
                if (dt.Rows[0]["MobileVerified"].ToString().Trim() == "True")
                {
                    model.bMobileVerified = true;
                }
                else if (dt.Rows[0]["MobileVerified"].ToString().Trim() == null || dt.Rows[0]["MobileVerified"].ToString().Trim() == "")
                {
                    model.bMobileVerified = false;
                }
                else
                {
                    model.bMobileVerified = false;
                }


                //model.bEmailVerified = Convert.ToBoolean(dt.Rows[0]["EmailVerified"].ToString());

                if (dt.Rows[0]["EmailVerified"].ToString().Trim() == "True")
                {
                    model.bEmailVerified = true;
                }
                else if (dt.Rows[0]["EmailVerified"].ToString().Trim() == null || dt.Rows[0]["EmailVerified"].ToString().Trim() == "")
                {
                    model.bEmailVerified = false;
                }
                else
                {
                    model.bEmailVerified = false;
                }




                if (int.Parse(dt.Rows[0]["isOldUser"].ToString()) == 1)
                {
                    model.bisOldUser = true;
                }
                else
                {
                    model.bisOldUser = false;
                }


                return View("partialEditUser", model);

            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("partialEditUser", model);
            }
        }

        public ActionResult DeleteUser(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("delete from users where userid='" + Id + "'"))
                {
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Users");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Users");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("Users");
                //throw;
            }

        }


        #endregion

        #region xxxxxxxxxxxxxxxxxx ----- Applied for Bidding Users ----- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult AppliedforBidding()
        {

            User objuser = new User();
            objuser.GridList = GetListAppliedForBidding();
            return View(objuser);

        }

        public List<User> GetListAppliedForBidding()
        {
            try
            {
                List<User> objlist = new List<User>();
                DataTable dt = new DataTable();
                dt = util.Display("select ROW_NUMBER() over (order by userid desc) as Sr,* from users where applyforbid='1' and confirmbid='0' order by userid desc");
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        User objuser = new User();
                        objuser.Sr = dr["Sr"].ToString();
                        objuser.userid = int.Parse(dr["userid"].ToString());
                        objuser.username = dr["username"].ToString();
                        objuser.password = dr["password"].ToString();
                        objuser.name = dr["name"].ToString();
                        objuser.address1 = dr["address1"].ToString();
                        objuser.address2 = dr["address2"].ToString();
                        objuser.city = dr["city"].ToString();
                        objuser.state = dr["state"].ToString();
                        objuser.country = dr["country"].ToString();
                        objuser.zip = dr["zip"].ToString();
                        objuser.BillingTelephone = dr["telephone"].ToString();
                        objuser.Mobile = dr["Mobile"].ToString();
                        objuser.email = dr["email"].ToString();
                        objuser.RegistrationDate = dr["RegistrationDate"].ToString();
                        objuser.LastLoggedDate = dr["LastLoggedDate"].ToString();

                        if (int.Parse(dr["confirmbid"].ToString()) == 1)
                        {
                            objuser.bconfirmbid = true;
                        }
                        else
                        {
                            objuser.bconfirmbid = false;
                        }

                        //objuser.confirmbid = int.Parse(dr["confirmbid"].ToString());
                        if (dr["amountlimt"].ToString().Trim() != "")
                        {
                            objuser.amountlimt = int.Parse(dr["amountlimt"].ToString());
                        }
                        else
                        {
                            objuser.amountlimt = 0;
                        }

                        objuser.lastname = dr["lastname"].ToString();

                        if (dr["applyforbid"].ToString().Replace(" ", string.Empty) == "1")
                        {
                            objuser.bapplyforbid = true;
                        }
                        else
                        {
                            objuser.bapplyforbid = false;
                        }

                        if (dr["MobileVerified"].ToString().Trim() == "True")
                        {
                            objuser.bMobileVerified = true;
                        }
                        else if (dr["MobileVerified"].ToString().Trim() == null || dr["MobileVerified"].ToString().Trim() == "")
                        {
                            objuser.bMobileVerified = false;
                        }
                        else
                        {
                            objuser.bMobileVerified = false;
                        }

                        //string value = dr["EmailVerified"].ToString().Trim();

                        if (dr["EmailVerified"].ToString().Trim() == "True")
                        {
                            objuser.bEmailVerified = true;
                        }
                        else if (dr["EmailVerified"].ToString().Trim() == null || dr["EmailVerified"].ToString().Trim() == "")
                        {
                            objuser.bEmailVerified = false;
                        }
                        else
                        {
                            objuser.bEmailVerified = false;
                        }

                        if (int.Parse(dr["isOldUser"].ToString()) == 1)
                        {
                            objuser.bisOldUser = true;
                        }
                        else
                        {
                            objuser.bisOldUser = false;
                        }

                        objuser.BillingName = dr["BillingName"].ToString();
                        objuser.BillingAddress = dr["BillingAddress"].ToString();
                        objuser.BillingCity = dr["BillingCity"].ToString();
                        objuser.BillingState = dr["BillingState"].ToString();
                        objuser.BillingCountry = dr["BillingCountry"].ToString();
                        objuser.BillingZip = dr["BillingZip"].ToString();
                        objuser.GSTIN = dr["GSTIN"].ToString();
                        objuser.panCard = dr["panCard"].ToString();
                        objuser.aadharCard = dr["aadharCard"].ToString();


                        objlist.Add(objuser);
                    }
                }
                return objlist;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AppliedforBidding(User objuser)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (objuser.userid == 0)
                    {

                    }
                    else
                    {
                        if (UpdateAppliedforBiddingr(objuser))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "User updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("AppliedforBidding");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "User could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("AppliedforBidding");
                        }
                    }
                }
                return View("AppliedforBidding", objuser);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }

        }

        public bool UpdateAppliedforBiddingr(User objuser)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Users"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Update");
                cmd.Parameters.AddWithValue("@userid", objuser.userid);
                cmd.Parameters.AddWithValue("@username", objuser.username);
                cmd.Parameters.AddWithValue("@password", objuser.password);
                cmd.Parameters.AddWithValue("@name", objuser.name);
                cmd.Parameters.AddWithValue("@lastname", objuser.lastname);
                cmd.Parameters.AddWithValue("@address1", objuser.address1);
                cmd.Parameters.AddWithValue("@address2", objuser.address2);
                cmd.Parameters.AddWithValue("@city", objuser.city);
                cmd.Parameters.AddWithValue("@state", objuser.state);
                cmd.Parameters.AddWithValue("@country", objuser.country);
                cmd.Parameters.AddWithValue("@zip", objuser.zip);
                cmd.Parameters.AddWithValue("@telephone", objuser.BillingTelephone);
                cmd.Parameters.AddWithValue("@Mobile", objuser.Mobile);
                cmd.Parameters.AddWithValue("@Email", objuser.email);

                cmd.Parameters.AddWithValue("@confirmbid", objuser.bconfirmbid);
                cmd.Parameters.AddWithValue("@amountlimt", objuser.amountlimt);
                if (objuser.bapplyforbid == true)
                {
                    cmd.Parameters.AddWithValue("@applyforbid", 1);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@applyforbid", 0);
                }
                cmd.Parameters.AddWithValue("@MobileVerified", objuser.bMobileVerified);
                cmd.Parameters.AddWithValue("@EmailVerified", objuser.bEmailVerified);
                cmd.Parameters.AddWithValue("@isOldUser", objuser.bisOldUser);

                cmd.Parameters.AddWithValue("@BillingName", objuser.BillingName);
                cmd.Parameters.AddWithValue("@BillingAddress", objuser.BillingAddress);
                cmd.Parameters.AddWithValue("@BillingCity", objuser.BillingCity);
                cmd.Parameters.AddWithValue("@BillingState", objuser.BillingState);
                cmd.Parameters.AddWithValue("@BillingCountry", objuser.BillingCountry);
                cmd.Parameters.AddWithValue("@BillingZip", objuser.BillingZip);
                cmd.Parameters.AddWithValue("@GSTIN", objuser.GSTIN);
                cmd.Parameters.AddWithValue("@panCard", objuser.panCard);
                cmd.Parameters.AddWithValue("@aadharCard", objuser.aadharCard);

                if (util.Execute(cmd))
                {

                    if (objuser.bconfirmbid == true)
                    {
                        StringBuilder strbuild = new StringBuilder();
                        strbuild.Append("<p>Dear " + objuser.name + "</p><br/>");
                        strbuild.Append("<p>We would like to give you a warm welcome into the AstaGuru family. You have bidding access and can now partake in the upcoming auctions.<br/>");
                        strbuild.Append("In case you have any queries with regards to the Lots that are part of the auction or the bidding process, please feel free to contact us on 91-22 2204 8138/39 or write to us at contact@astaguru.com. Our team will be glad to assist you with the same.</p><br/>");
                        strbuild.Append("Kindly click the link below to view our upcoming auctions.<br/>");
                        strbuild.Append("<a href='http://www.astaguru.com'>http://www.astaguru.com</a></p><br/>");
                        strbuild.Append("<p><a href='https://astaguru.com/TermsAndConditions'>The auction will be governed by the conditions for sale accepted by you when you had originally registered, however if you would like to review them again, please Click Here</a></p>");
                        strbuild.Append("<p>Your AstaGuru Login-ID and Password are given below. Please use the same Login-Id and Password to bid in the auction.</p><br/>");
                        strbuild.Append("<p>Login-ID:" + objuser.username + "<br/>");
                        strbuild.Append("Password:" + objuser.password + "</p><br/><br/>");
                        strbuild.Append("<p>Good Luck & Happy Bidding</p><br/><br/>");
                        strbuild.Append("<p>Warm Regards,<br/>");
                        strbuild.Append("The Team of AstaGuru</p>");



                        string ToEmailid = objuser.email;
                        String[] emailid = new String[] { ToEmailid };

                        //var path1 = "~/Content/uploads/Resume/" + objApplyForJob.Resume;
                        //var path = Server.MapPath(path1);
                        util.SendEmail(strbuild.ToString(), emailid, "Congratulation, you have bidding access.", "", null);
                    }
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditAppliedforBidding(int? userid)
        {

            User model = new User();
            model.GridList = GetUserList();
            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_Users 'GetById', '" + userid + "'");
            if (dt.Rows.Count > 0)
            {
                model.userid = Convert.ToInt32(dt.Rows[0]["userid"].ToString());
                model.username = dt.Rows[0]["username"].ToString();
                model.password = dt.Rows[0]["password"].ToString();
                model.name = dt.Rows[0]["name"].ToString();
                model.address1 = dt.Rows[0]["address1"].ToString();
                model.address2 = dt.Rows[0]["address2"].ToString();
                model.city = dt.Rows[0]["city"].ToString();
                model.state = dt.Rows[0]["state"].ToString();
                model.country = dt.Rows[0]["country"].ToString();
                model.zip = dt.Rows[0]["zip"].ToString();
                model.BillingTelephone = dt.Rows[0]["telephone"].ToString();
                model.Mobile = dt.Rows[0]["Mobile"].ToString();
                model.email = dt.Rows[0]["email"].ToString();

                model.BillingName = dt.Rows[0]["BillingName"].ToString();
                model.BillingAddress = dt.Rows[0]["BillingAddress"].ToString();
                model.BillingCity = dt.Rows[0]["BillingCity"].ToString();
                model.BillingState = dt.Rows[0]["BillingState"].ToString();
                model.BillingCountry = dt.Rows[0]["BillingCountry"].ToString();
                model.BillingZip = dt.Rows[0]["BillingZip"].ToString();
                model.GSTIN = dt.Rows[0]["GSTIN"].ToString();
                model.panCard = dt.Rows[0]["panCard"].ToString();
                model.aadharCard = dt.Rows[0]["aadharCard"].ToString();

                model.lastname = dt.Rows[0]["lastname"].ToString();


                if ((Convert.ToInt32(dt.Rows[0]["confirmbid"].ToString()) == 1))
                {
                    model.bconfirmbid = true;
                }
                else
                {
                    model.bconfirmbid = false;
                }


                if (dt.Rows[0]["amountlimt"].ToString().Trim() != "")
                {
                    model.amountlimt = int.Parse(dt.Rows[0]["amountlimt"].ToString());
                }
                else
                {
                    model.amountlimt = 0;
                }
                //model.amountlimt = Convert.ToInt32(dt.Rows[0]["amountlimt"].ToString());

                if (int.Parse(dt.Rows[0]["applyforbid"].ToString()) == 1)
                {
                    model.bapplyforbid = true;
                }
                else
                {
                    model.bapplyforbid = false;

                }
                //model.bMobileVerified = Convert.ToBoolean(dt.Rows[0]["MobileVerified"].ToString());
                if (dt.Rows[0]["MobileVerified"].ToString().Trim() == "True")
                {
                    model.bMobileVerified = true;
                }
                else if (dt.Rows[0]["MobileVerified"].ToString().Trim() == null || dt.Rows[0]["MobileVerified"].ToString().Trim() == "")
                {
                    model.bMobileVerified = false;
                }
                else
                {
                    model.bMobileVerified = false;
                }


                //model.bEmailVerified = Convert.ToBoolean(dt.Rows[0]["EmailVerified"].ToString());

                if (dt.Rows[0]["EmailVerified"].ToString().Trim() == "True")
                {
                    model.bEmailVerified = true;
                }
                else if (dt.Rows[0]["EmailVerified"].ToString().Trim() == null || dt.Rows[0]["EmailVerified"].ToString().Trim() == "")
                {
                    model.bEmailVerified = false;
                }
                else
                {
                    model.bEmailVerified = false;
                }




                if (int.Parse(dt.Rows[0]["isOldUser"].ToString()) == 1)
                {
                    model.bisOldUser = true;
                }
                else
                {
                    model.bisOldUser = false;
                }


                return View("partialEditAppliedForBidding", model);

            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("partialEditAppliedForBidding", model);
            }
        }

        public ActionResult DeleteAppliedforBidding(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("delete from users where userid='" + Id + "'"))
                {
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Users");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("AppliedforBidding");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("AppliedforBidding");
                //throw;
            }

        }


        #endregion

        #region xxxxxxxxxxxxxxxxxxxx --- Confirm for Bidding Users --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult ConfirmforBidding()
        {
            User objuser = new User();
            objuser.GridList = GetListConfirmforBidding();
            return View(objuser);
        }

        public List<User> GetListConfirmforBidding()
        {
            try
            {
                List<User> objlist = new List<User>();
                DataTable dt = new DataTable();
                dt = util.Display("select ROW_NUMBER() over (order by userid desc) as Sr,* from users where applyforbid='1' and confirmbid=1 order by userid desc");
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        User objuser = new User();
                        objuser.Sr = dr["Sr"].ToString();
                        objuser.userid = int.Parse(dr["userid"].ToString());
                        objuser.username = dr["username"].ToString();
                        objuser.password = dr["password"].ToString();
                        objuser.name = dr["name"].ToString();
                        objuser.address1 = dr["address1"].ToString();
                        objuser.address2 = dr["address2"].ToString();
                        objuser.city = dr["city"].ToString();
                        objuser.state = dr["state"].ToString();
                        objuser.country = dr["country"].ToString();
                        objuser.zip = dr["zip"].ToString();
                        objuser.BillingTelephone = dr["telephone"].ToString();
                        objuser.email = dr["email"].ToString();
                        objuser.RegistrationDate = dr["RegistrationDate"].ToString();
                        objuser.LastLoggedDate = dr["LastLoggedDate"].ToString();

                        if (int.Parse(dr["confirmbid"].ToString()) == 1)
                        {
                            objuser.bconfirmbid = true;
                        }
                        else
                        {
                            objuser.bconfirmbid = false;
                        }

                        //objuser.confirmbid = int.Parse(dr["confirmbid"].ToString());

                        if (dr["amountlimt"].ToString().Trim() != "")
                        {
                            objuser.amountlimt = int.Parse(dr["amountlimt"].ToString());
                        }
                        else
                        {
                            objuser.amountlimt = 0;
                        }

                        objuser.lastname = dr["lastname"].ToString();

                        if (dr["applyforbid"].ToString().Replace(" ", string.Empty) == "1")
                        {
                            objuser.bapplyforbid = true;
                        }
                        else
                        {
                            objuser.bapplyforbid = false;
                        }


                        if (dr["MobileVerified"].ToString().Trim() == "True")
                        {
                            objuser.bMobileVerified = true;
                        }
                        else if (dr["MobileVerified"].ToString().Trim() == null || dr["MobileVerified"].ToString().Trim() == "")
                        {
                            objuser.bMobileVerified = false;
                        }
                        else
                        {
                            objuser.bMobileVerified = false;
                        }

                        //string value = dr["EmailVerified"].ToString().Trim();

                        if (dr["EmailVerified"].ToString().Trim() == "True")
                        {
                            objuser.bEmailVerified = true;
                        }
                        else if (dr["EmailVerified"].ToString().Trim() == null || dr["EmailVerified"].ToString().Trim() == "")
                        {
                            objuser.bEmailVerified = false;
                        }
                        else
                        {
                            objuser.bEmailVerified = false;
                        }

                        if (int.Parse(dr["isOldUser"].ToString()) == 1)
                        {
                            objuser.bisOldUser = true;
                        }
                        else
                        {
                            objuser.bisOldUser = false;
                        }
                        objlist.Add(objuser);
                    }
                }
                return objlist;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }
        #endregion

        #region xxxxxxxxxx ---- Not Apply for Auction Users --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult NotApplyforAuction()
        {
            User objuser = new User();
            objuser.GridList = GetListNotApplyforAuction();
            return View(objuser);
        }
        public List<User> GetListNotApplyforAuction()
        {
            try
            {
                List<User> objlist = new List<User>();
                DataTable dt = new DataTable();
                dt = util.Display("select ROW_NUMBER() over (order by userid desc) as Sr, * from users where applyforbid='0' order by userid desc");
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        User objuser = new User();
                        objuser.Sr = dr["Sr"].ToString();
                        objuser.userid = int.Parse(dr["userid"].ToString());
                        objuser.username = dr["username"].ToString();
                        objuser.password = dr["password"].ToString();
                        objuser.name = dr["name"].ToString();
                        objuser.address1 = dr["address1"].ToString();
                        objuser.address2 = dr["address2"].ToString();
                        objuser.city = dr["city"].ToString();
                        objuser.state = dr["state"].ToString();
                        objuser.country = dr["country"].ToString();
                        objuser.zip = dr["zip"].ToString();
                        objuser.BillingTelephone = dr["telephone"].ToString();
                        objuser.email = dr["email"].ToString();
                        objuser.RegistrationDate = dr["RegistrationDate"].ToString();
                        objuser.LastLoggedDate = dr["LastLoggedDate"].ToString();

                        if (int.Parse(dr["confirmbid"].ToString()) == 1)
                        {
                            objuser.bconfirmbid = true;
                        }
                        else
                        {
                            objuser.bconfirmbid = false;
                        }

                        //objuser.confirmbid = int.Parse(dr["confirmbid"].ToString());

                        if (dr["amountlimt"].ToString().Trim() != "")
                        {
                            objuser.amountlimt = int.Parse(dr["amountlimt"].ToString());
                        }
                        else
                        {
                            objuser.amountlimt = 0;
                        }


                        objuser.lastname = dr["lastname"].ToString();

                        if (dr["applyforbid"].ToString().Replace(" ", string.Empty) == "1")
                        {
                            objuser.bapplyforbid = true;
                        }
                        else
                        {
                            objuser.bapplyforbid = false;
                        }

                        if (dr["MobileVerified"].ToString().Trim() == "True")
                        {
                            objuser.bMobileVerified = true;
                        }
                        else if (dr["MobileVerified"].ToString().Trim() == null || dr["MobileVerified"].ToString().Trim() == "")
                        {
                            objuser.bMobileVerified = false;
                        }
                        else
                        {
                            objuser.bMobileVerified = false;
                        }

                        //string value = dr["EmailVerified"].ToString().Trim();

                        if (dr["EmailVerified"].ToString().Trim() == "True")
                        {
                            objuser.bEmailVerified = true;
                        }
                        else if (dr["EmailVerified"].ToString().Trim() == null || dr["EmailVerified"].ToString().Trim() == "")
                        {
                            objuser.bEmailVerified = false;
                        }
                        else
                        {
                            objuser.bEmailVerified = false;
                        }

                        if (int.Parse(dr["isOldUser"].ToString()) == 1)
                        {
                            objuser.bisOldUser = true;
                        }
                        else
                        {
                            objuser.bisOldUser = false;
                        }
                        objlist.Add(objuser);
                    }
                }
                return objlist;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }
        #endregion

        #region xxxxxxxxxxxxxxxxxxxxx --- Manage Painting and Products ---  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult PaintingAndProduct()
        {
            Auction objauction = new Auction();
            //ViewBag.AuctionId = objauction.Auctionid.ToString();
            objauction.AuctionNameList = BindAuctionName(objauction);
            objauction.ArtistList = BindArtist(objauction);
            objauction.CategoryList = Bindcategory(objauction);
            objauction.StyleList = BindStyle(objauction);
            objauction.MediumList = Bindmedium(objauction);
            //objauction.GridList = GetProductAndPaintingList(objauction.Auctionid);
            return View(objauction);
        }

        public List<SelectListItem> BindAuctionName(Auction obj)
        {
            DataTable dt = new DataTable();
            dt = util.Display("Execute spaddauctionadmin 'GetAuctionName'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["Auctionname"].ToString();
                    list.Value = dr["Auctionid"].ToString();

                    if (obj.Auctionid == int.Parse(dr["Auctionid"].ToString()))
                    {
                        list.Selected = true;
                    }

                    objList.Add(list);
                }

            }
            return objList.ToList();
        }

        public List<SelectListItem> BindArtist(Auction obj)
        {
            DataTable dt = new DataTable();
            dt = util.Display("Execute spaddauctionadmin 'GetArtist'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["artist"].ToString();
                    list.Value = dr["artistid"].ToString();

                    if (obj.artistid == int.Parse(dr["artistid"].ToString()))
                    {
                        list.Selected = true;
                    }

                    objList.Add(list);
                }

            }
            return objList.ToList();
        }

        public List<SelectListItem> Bindcategory(Auction obj)
        {
            DataTable dt = new DataTable();
            dt = util.Display("Execute spaddauctionadmin 'GetCategory'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["category"].ToString();
                    list.Value = dr["categoryid"].ToString();

                    if (obj.categoryid == int.Parse(dr["categoryid"].ToString()))
                    {
                        list.Selected = true;
                    }

                    objList.Add(list);
                }

            }
            return objList.ToList();
        }

        public List<SelectListItem> BindStyle(Auction obj)
        {
            DataTable dt = new DataTable();
            dt = util.Display("Execute spaddauctionadmin 'GetStyle'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["style"].ToString();
                    list.Value = dr["styleid"].ToString();

                    if (obj.categoryid == int.Parse(dr["styleid"].ToString()))
                    {
                        list.Selected = true;
                    }

                    objList.Add(list);
                }

            }
            return objList.ToList();
        }

        public List<SelectListItem> Bindmedium(Auction obj)
        {
            DataTable dt = new DataTable();
            dt = util.Display("Execute spaddauctionadmin 'GetMedium'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["medium"].ToString();
                    list.Value = dr["mediumid"].ToString();

                    if (obj.categoryid == int.Parse(dr["mediumid"].ToString()))
                    {
                        list.Selected = true;
                    }

                    objList.Add(list);
                }

            }
            return objList.ToList();
        }

        public ActionResult ShowDatabyAuction(int auctionId)
        {
            Auction obj = new Auction();



            obj.AuctionNameList = BindAuctionName(obj);
            obj.ArtistList = BindArtist(obj);
            obj.CategoryList = Bindcategory(obj);
            obj.StyleList = BindStyle(obj);
            obj.MediumList = Bindmedium(obj);


            obj.GridList = GetProductAndPaintingList(auctionId);


            return View("PaintingAndProduct", obj);


        }

        public List<Auction> GetProductAndPaintingList(int auctionId)
        {
            try
            {
                List<Auction> objlist = new List<Auction>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute spaddauctionadmin 'Get',0,'','','',0,0,0,0,'',0,0,'','','','','','','','',0,0,0,''," + auctionId + "");
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Auction objAuction = new Auction();

                        objAuction.Sr = dr["Sr"].ToString();

                        objAuction.Auctionname = dr["Auctionname"].ToString();
                        objAuction.productid = int.Parse(dr["productid"].ToString());
                        objAuction.reference = dr["reference"].ToString();

                        objAuction.title = dr["title"].ToString();
                        objAuction.Prdescription = dr["Prdescription"].ToString();
                        objAuction.description = dr["description"].ToString();
                        objAuction.artist = dr["artist"].ToString();
                        objAuction.category = dr["category"].ToString();
                        objAuction.style = dr["style"].ToString();
                        objAuction.medium = dr["medium"].ToString();
                        objAuction.productsize = dr["productsize"].ToString();
                        objAuction.pricers = int.Parse(dr["pricers"].ToString());
                        objAuction.priceus = int.Parse(dr["priceus"].ToString());
                        objAuction.image = dr["image"].ToString();
                        objAuction.thumbnail = dr["thumbnail"].ToString();
                        objAuction.smallimage = dr["smallimage"].ToString();
                        objAuction.listImage = dr["listImage"].ToString();
                        objAuction.productdate = dr["productdate"].ToString();
                        objAuction.BidclosingtimeAdmin = dr["bidclosingtime"].ToString();
                        objAuction.collectors = dr["collectors"].ToString();
                        objAuction.estamiate = dr["estamiate"].ToString();
                        objAuction.isInternational = int.Parse(dr["isInternational"].ToString());
                        objAuction.isInternationalGST = int.Parse(dr["isInternationalGST"].ToString());
                        objAuction.astaguruPrice = int.Parse(dr["astaguruPrice"].ToString());
                        if (dr["usedGoodPercentage"].ToString() == null || dr["usedGoodPercentage"].ToString() == "" || dr["usedGoodPercentage"].ToString() == "0")
                        {
                            objAuction.usedGoodPercentage = 0;
                        }
                        else
                        {
                            objAuction.usedGoodPercentage = 1;
                        }

                        if (dr["nonExportable"].ToString() == null || dr["nonExportable"].ToString() == "" || dr["nonExportable"].ToString() == "0")
                        {
                            objAuction.nonExportable = 0;
                        }
                        else
                        {
                            objAuction.nonExportable = 1;
                        }

                        objAuction.PrVat = dr["PrVat"].ToString();

                  

                        if (dr["Ownerid"].ToString() == "")
                        {
                            objAuction.Ownerid = 0;
                        }
                        else
                        {
                            objAuction.Ownerid = int.Parse(dr["Ownerid"].ToString().Trim());
                        }

                        
                        

                        objlist.Add(objAuction);


                    }
                }
                return objlist;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PaintingAndProduct(Auction objauction, HttpPostedFileBase[] Image, HttpPostedFileBase[] thumbnail, HttpPostedFileBase[] smallimage, HttpPostedFileBase[] listImage)
        {

            try
            {
                int AuctionId = objauction.Auctionid;

                if (objauction.productid != 0)
                {
                    ModelState.Remove("Image");
                    ModelState.Remove("thumbnail");
                    ModelState.Remove("smallimage");
                    ModelState.Remove("listImage");

                }

                var errors = ModelState.Values.SelectMany(v => v.Errors);    ///  check model error

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/paintings/";
                    string VirtualFilelistview = "~/paintings/listview/";

                    string Img = string.Empty;
                    string thumbimg = string.Empty;
                    string smallimg = string.Empty;
                    string listimg = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("PaintingAndProduct");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "Image-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objauction.image = Img.Substring(0, Img.Length - 1);
                        }
                    }



                    if (thumbnail != null)
                    {
                        foreach (HttpPostedFileBase file in thumbnail)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("PaintingAndProduct");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "Thumbimg-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    thumbimg += fileName + ",";
                                }
                            }
                        }

                        if (thumbimg != "")
                        {
                            objauction.thumbnail = thumbimg.Substring(0, thumbimg.Length - 1);
                        }
                    }

                    if (smallimage != null)
                    {
                        foreach (HttpPostedFileBase file in smallimage)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("PaintingAndProduct");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "smallimage-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    smallimg += fileName + ",";
                                }
                            }
                        }

                        if (smallimg != "")
                        {
                            objauction.smallimage = smallimg.Substring(0, smallimg.Length - 1);
                        }
                    }


                    if (listImage != null)
                    {
                        foreach (HttpPostedFileBase file in listImage)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("PaintingAndProduct");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "listImage-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFilelistview + fileName));
                                    listimg += fileName + ",";
                                }
                            }
                        }

                        if (listimg != "")
                        {
                            objauction.listImage = listimg.Substring(0, listimg.Length - 1);
                        }
                    }

                    if (objauction.productid == 0)
                    {
                        if (SavePaintingAndProduct(objauction))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("PaintingAndProduct");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("PaintingAndProduct");
                        }
                    }
                    else
                    {
                        if (UpdatePaintingAndProduct(objauction))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("PaintingAndProduct");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("PaintingAndProduct");
                        }
                    }
                }
                return View("PaintingAndProduct", objauction);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("PaintingAndProduct");
            }
        }


        public bool SavePaintingAndProduct(Auction objauction)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("spaddauctionadmin"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@pinonline", objauction.Auctionid);
                cmd.Parameters.AddWithValue("@pinReference", objauction.reference);
                cmd.Parameters.AddWithValue("@pintitle", objauction.title);
                cmd.Parameters.AddWithValue("@pinAntiDesc", objauction.Prdescription);
                cmd.Parameters.AddWithValue("@pindescription", objauction.description);
                cmd.Parameters.AddWithValue("@pinartistid", objauction.artistid);
                cmd.Parameters.AddWithValue("@pincategory", objauction.categoryid);
                cmd.Parameters.AddWithValue("@pinstyle", objauction.styleid);
                cmd.Parameters.AddWithValue("@pinMedium", objauction.mediumid);
                cmd.Parameters.AddWithValue("@productsize", objauction.productsize);
                cmd.Parameters.AddWithValue("@pinpricers", objauction.pricers);

                objauction.priceus = objauction.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                cmd.Parameters.AddWithValue("@pinpriceus", objauction.priceus);

                cmd.Parameters.AddWithValue("@pinimage", objauction.image);
                cmd.Parameters.AddWithValue("@pinthumbnail", objauction.thumbnail);
                cmd.Parameters.AddWithValue("@pinsmallimage", objauction.smallimage);
                cmd.Parameters.AddWithValue("@pindate", objauction.productdate);
                //cmd.Parameters.AddWithValue("@pinbidclosingtime", objauction.Bidclosingtime);
                cmd.Parameters.AddWithValue("@pinbidclosingtime", objauction.BidclosingtimeAdmin);
                cmd.Parameters.AddWithValue("@pinCollectors", objauction.collectors);
                cmd.Parameters.AddWithValue("@pinestimate", objauction.estamiate);
                cmd.Parameters.AddWithValue("@pinfeatured", objauction.pricers);
                cmd.Parameters.AddWithValue("@pinproxy", 0);
                cmd.Parameters.AddWithValue("@pinproxyamount", 0);
                cmd.Parameters.AddWithValue("@pinuserid", 0);
                cmd.Parameters.AddWithValue("@pinproxyusername", "");
                cmd.Parameters.AddWithValue("@pintimecount", 0);

                cmd.Parameters.AddWithValue("@pinlistimage", objauction.listImage);


                cmd.Parameters.AddWithValue("@isInternational", objauction.isInternational);
                cmd.Parameters.AddWithValue("@isInternationalGST", objauction.isInternationalGST);
                cmd.Parameters.AddWithValue("@astaguruPrice", objauction.astaguruPrice);
                cmd.Parameters.AddWithValue("@usedGoodPercentage", objauction.usedGoodPercentage);
                cmd.Parameters.AddWithValue("@nonExportable", objauction.nonExportable);

                cmd.Parameters.AddWithValue("@Abrasion", objauction.Abrasion);
                cmd.Parameters.AddWithValue("@Blistering", objauction.Blistering);
                cmd.Parameters.AddWithValue("@Deterioration", objauction.Deterioration);
                cmd.Parameters.AddWithValue("@Cracking", objauction.Cracking);
                cmd.Parameters.AddWithValue("@Crease", objauction.Crease);
                cmd.Parameters.AddWithValue("@Cupping", objauction.Cupping);
                cmd.Parameters.AddWithValue("@Damage", objauction.Damage);
                cmd.Parameters.AddWithValue("@Discoloration", objauction.Discoloration);
                cmd.Parameters.AddWithValue("@Flaking", objauction.Flaking);
                cmd.Parameters.AddWithValue("@Fungus", objauction.Fungus);
                cmd.Parameters.AddWithValue("@Scratches", objauction.Scratches);
                cmd.Parameters.AddWithValue("@Restoration", objauction.Restoration);
                cmd.Parameters.AddWithValue("@ConditionDetails", objauction.Conditiondetails);

                cmd.Parameters.AddWithValue("@PrVat", objauction.PrVat);

                cmd.Parameters.AddWithValue("@Ownerid", objauction.Ownerid);


                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdatePaintingAndProduct(Auction objauction)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("spaddauctionadmin"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@pinproductid", objauction.productid);
                cmd.Parameters.AddWithValue("@pinonline", objauction.Auctionid);
                cmd.Parameters.AddWithValue("@pinReference", objauction.reference);
                cmd.Parameters.AddWithValue("@pintitle", objauction.title);
                cmd.Parameters.AddWithValue("@pinAntiDesc", objauction.Prdescription);
                cmd.Parameters.AddWithValue("@pindescription", objauction.description);
                cmd.Parameters.AddWithValue("@pinartistid", objauction.artistid);
                cmd.Parameters.AddWithValue("@pincategory", objauction.categoryid);
                cmd.Parameters.AddWithValue("@pinstyle", objauction.styleid);
                cmd.Parameters.AddWithValue("@pinMedium", objauction.mediumid);
                cmd.Parameters.AddWithValue("@productsize", objauction.productsize);
                cmd.Parameters.AddWithValue("@pinpricers", objauction.pricers);

                objauction.priceus = objauction.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                cmd.Parameters.AddWithValue("@pinpriceus", objauction.priceus);

                cmd.Parameters.AddWithValue("@pinimage", objauction.image);
                cmd.Parameters.AddWithValue("@pinthumbnail", objauction.thumbnail);
                cmd.Parameters.AddWithValue("@pinsmallimage", objauction.smallimage);
                cmd.Parameters.AddWithValue("@pindate", objauction.productdate);
                //cmd.Parameters.AddWithValue("@pinbidclosingtime", objauction.Bidclosingtime);
                cmd.Parameters.AddWithValue("@pinbidclosingtime", objauction.BidclosingtimeAdmin);
                cmd.Parameters.AddWithValue("@pinCollectors", objauction.collectors);
                cmd.Parameters.AddWithValue("@pinestimate", objauction.estamiate);
                cmd.Parameters.AddWithValue("@pinfeatured", objauction.pricers);


                cmd.Parameters.AddWithValue("@pinlistimage", objauction.listImage);

                cmd.Parameters.AddWithValue("@isInternational", objauction.isInternational);
                cmd.Parameters.AddWithValue("@isInternationalGST", objauction.isInternationalGST);
                cmd.Parameters.AddWithValue("@astaguruPrice", objauction.astaguruPrice);
                cmd.Parameters.AddWithValue("@usedGoodPercentage", objauction.usedGoodPercentage);
                cmd.Parameters.AddWithValue("@nonExportable", objauction.nonExportable);

                cmd.Parameters.AddWithValue("@Abrasion", objauction.Abrasion);
                cmd.Parameters.AddWithValue("@Blistering", objauction.Blistering);
                cmd.Parameters.AddWithValue("@Deterioration", objauction.Deterioration);
                cmd.Parameters.AddWithValue("@Cracking", objauction.Cracking);
                cmd.Parameters.AddWithValue("@Crease", objauction.Crease);
                cmd.Parameters.AddWithValue("@Cupping", objauction.Cupping);
                cmd.Parameters.AddWithValue("@Damage", objauction.Damage);
                cmd.Parameters.AddWithValue("@Discoloration", objauction.Discoloration);
                cmd.Parameters.AddWithValue("@Flaking", objauction.Flaking);
                cmd.Parameters.AddWithValue("@Fungus", objauction.Fungus);
                cmd.Parameters.AddWithValue("@Scratches", objauction.Scratches);
                cmd.Parameters.AddWithValue("@Restoration", objauction.Restoration);
                cmd.Parameters.AddWithValue("@ConditionDetails", objauction.Conditiondetails);

                cmd.Parameters.AddWithValue("@PrVat", objauction.PrVat);

                cmd.Parameters.AddWithValue("@Ownerid", objauction.Ownerid);

                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        public ActionResult EditPaintingAndProduct(int? Id)
        {
            Auction obj = new Auction();
            DataTable dt = new DataTable();
            dt = util.Display("Exec spaddauctionadmin 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                obj.Auctionid = int.Parse(dt.Rows[0]["Online"].ToString());
                obj.productid = int.Parse(dt.Rows[0]["productid"].ToString());
                obj.reference = dt.Rows[0]["reference"].ToString();
                obj.title = dt.Rows[0]["title"].ToString();
                obj.description = dt.Rows[0]["description"].ToString();
                obj.artistid = int.Parse(dt.Rows[0]["artistid"].ToString());
                obj.categoryid = int.Parse(dt.Rows[0]["categoryid"].ToString());
                obj.styleid = int.Parse(dt.Rows[0]["styleid"].ToString());
                obj.mediumid = int.Parse(dt.Rows[0]["mediumid"].ToString());
                obj.productsize = dt.Rows[0]["productsize"].ToString();
                obj.pricers = int.Parse(dt.Rows[0]["pricers"].ToString());
                obj.priceus = int.Parse(dt.Rows[0]["priceus"].ToString());
                obj.image = dt.Rows[0]["image"].ToString();
                obj.imgPreview = dt.Rows[0]["image"].ToString();
                obj.thumbnail = dt.Rows[0]["thumbnail"].ToString();
                obj.imgPreview1 = dt.Rows[0]["thumbnail"].ToString();
                obj.smallimage = dt.Rows[0]["smallimage"].ToString();
                obj.imgPreview2 = dt.Rows[0]["smallimage"].ToString();
                obj.listImage = dt.Rows[0]["listImage"].ToString();
                obj.imgPreview3 = dt.Rows[0]["listImage"].ToString();
                obj.productdate = dt.Rows[0]["productdate"].ToString();
                //obj.Bidclosingtime = dt.Rows[0]["bidclosingtime"].ToString();
                DateTime newbidclosetime = Convert.ToDateTime(dt.Rows[0]["bidclosingtime"].ToString());
                obj.BidclosingtimeAdmin = string.Format("{0:yyyy-MM-dd hh:mm:ss}", newbidclosetime);
                obj.collectors = dt.Rows[0]["collectors"].ToString();
                obj.estamiate = dt.Rows[0]["estamiate"].ToString();
                obj.isInternational = int.Parse(dt.Rows[0]["isInternational"].ToString());
                obj.isInternationalGST = int.Parse(dt.Rows[0]["isInternationalGST"].ToString());
                obj.astaguruPrice = int.Parse(dt.Rows[0]["astaguruPrice"].ToString());
                if (dt.Rows[0]["usedGoodPercentage"].ToString() == null || dt.Rows[0]["usedGoodPercentage"].ToString() == "")
                {
                    obj.usedGoodPercentage = 0;
                }
                else
                {
                    obj.usedGoodPercentage = int.Parse(dt.Rows[0]["usedGoodPercentage"].ToString());
                }

                if (dt.Rows[0]["nonExportable"].ToString() == null || dt.Rows[0]["nonExportable"].ToString() == "" || dt.Rows[0]["nonExportable"].ToString() == "0")
                {
                    obj.nonExportable = 0;
                }
                else
                {
                    obj.nonExportable = 1;
                }

                //obj.PrVat;

                obj.Abrasion = dt.Rows[0]["Abrasion"].ToString().Trim();
                obj.Blistering = dt.Rows[0]["Blistering"].ToString().Trim();
                obj.Deterioration = dt.Rows[0]["Deterioration"].ToString().Trim();
                obj.Cracking = dt.Rows[0]["Cracking"].ToString().Trim();
                obj.Crease = dt.Rows[0]["Crease"].ToString().Trim();
                obj.Cupping = dt.Rows[0]["Cupping"].ToString().Trim();
                obj.Damage = dt.Rows[0]["Damage"].ToString().Trim();
                obj.Discoloration = dt.Rows[0]["Discoloration"].ToString().Trim();
                obj.Flaking = dt.Rows[0]["Flaking"].ToString().Trim();
                obj.Fungus = dt.Rows[0]["Fungus"].ToString().Trim();
                obj.Scratches = dt.Rows[0]["Scratches"].ToString().Trim();
                obj.Restoration = dt.Rows[0]["Restoration"].ToString().Trim();
                obj.Conditiondetails = dt.Rows[0]["ConditionDetails"].ToString().Trim();

                obj.PrVat = dt.Rows[0]["PrVat"].ToString().Trim();

                if (dt.Rows[0]["Ownerid"].ToString() == "")
                {
                    obj.Ownerid = 0;
                }
                else
                {
                    obj.Ownerid = int.Parse(dt.Rows[0]["Ownerid"].ToString().Trim());
                }

           



                obj.AuctionNameList = BindAuctionName(obj);
                obj.ArtistList = BindArtist(obj);
                obj.CategoryList = Bindcategory(obj);
                obj.StyleList = BindStyle(obj);
                obj.MediumList = Bindmedium(obj);
                obj.GridList = GetProductAndPaintingList(obj.Auctionid);

                ViewBag.ActionType = "Update";
                return View("PaintingAndProduct", obj);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("PaintingAndProduct", obj);
            }
        }

        public ActionResult DeletePaintingAndProduct(int Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("spaddauctionadmin"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@pinproductid", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("PaintingAndProduct");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("PaintingAndProduct");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("PaintingAndProduct");
                //throw;
            }

        }


        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxx --- Proxy Bid Approval ---- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult ProxyBidApproval()
        {
            Auction objauc = new Auction();
            objauc.GridList = getProxyBidApprovalList();
            return View(objauc);
        }

        public List<Auction> getProxyBidApprovalList()
        {
            try
            {
                List<Auction> objList = new List<Auction>();
                DataTable dt = new DataTable();
                dt = util.Display("Exec Proc_ProxyBidApproval 'Get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Auction objauc = new Auction();
                        objauc.Sr = dr["Sr"].ToString();
                        objauc.proxyid = int.Parse(dr["Proxyid"].ToString());
                        objauc.userid = int.Parse(dr["userid"].ToString());
                        objauc.username = dr["username"].ToString();
                        objauc.name = dr["name"].ToString();
                        objauc.email = dr["email"].ToString();
                        objauc.Auctionname = dr["Auctionname"].ToString();
                        objauc.artist = dr["Artistname"].ToString();
                        objauc.auctiondate = dr["auctiondate"].ToString();
                        objauc.reference = dr["reference"].ToString();
                        objauc.title = dr["title"].ToString();
                        objauc.thumbnail = dr["thumbnail"].ToString();
                        objauc.ProxyAmt = int.Parse(dr["ProxyAmt"].ToString());
                        objauc.ProxyAmtus = int.Parse(dr["ProxyAmtus"].ToString());
                        if (Convert.ToBoolean(dr["status"].ToString()) == true)
                        {
                            objauc.status = "Confirmed";
                        }
                        else
                        {
                            objauc.status = "";
                        }
                        //objauc.status = dr["status"].ToString();
                        objauc.ConfirmedBy = dr["ConfirmedBy"].ToString();
                        objauc.ConfirmedDt = dr["ConfirmedDt"].ToString();



                        objList.Add(objauc);
                    }
                }
                return objList;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        public ActionResult AcceptProxy(int? id)
        {

            Auction objauc = new Auction();

            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_ProxyBidApproval 'GetById'," + id + "");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    objauc.Sr = dr["Sr"].ToString();
                    objauc.proxyid = int.Parse(dr["Proxyid"].ToString());
                    objauc.username = dr["username"].ToString();
                    objauc.name = dr["name"].ToString();
                    objauc.email = dr["email"].ToString();
                    objauc.Auctionname = dr["Auctionname"].ToString();
                    objauc.auctiondate = dr["auctiondate"].ToString();
                    objauc.reference = dr["reference"].ToString();
                    objauc.title = dr["title"].ToString();
                    objauc.thumbnail = dr["thumbnail"].ToString();
                    objauc.ProxyAmt = int.Parse(dr["ProxyAmt"].ToString());
                    objauc.ProxyAmtus = int.Parse(dr["ProxyAmtus"].ToString());
                    objauc.status = dr["status"].ToString();
                    objauc.ConfirmedBy = dr["ConfirmedBy"].ToString();
                    objauc.ConfirmedDt = dr["ConfirmedDt"].ToString();

                }
            }

            using (SqlCommand cmd = new SqlCommand("Proc_ProxyBidApproval"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Accept_proxy");
                cmd.Parameters.AddWithValue("@Proxyid", id);
                //cmd.Parameters.AddWithValue("@status", true);      
                //cmd.Parameters.AddWithValue("@ConfirmedBy", "Astaguru");
                //cmd.Parameters.AddWithValue("@ConfirmedDt", DateTime.Now);


                if (util.Execute(cmd))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("Dear " + objauc.name + ", ");
                    sb.AppendFormat("<br><br>We are glad to inform you that your Proxy Bid amount of Rs." + Common.rupeeFormat(objauc.ProxyAmt.ToString()) + " ($" + Common.DollerFormat(objauc.ProxyAmtus.ToString()) + ") for Lot No #" + objauc.reference + ", part of our '" + objauc.Auctionname + "' Auction dated " + objauc.auctiondate + " has been accepted.");
                    sb.AppendFormat("<br><br><br>For any further assistance please feel free to write to us at, contact@astaguru.com or call us on 91-22 2204 8138/39. We will be glad to assist you.");
                    sb.AppendFormat("<br><br><br> Thank You, ");
                    sb.AppendFormat("<br><br><br>Team Astaguru.");


                    string ToEmailid = objauc.email;
                    String[] emailid = new String[] { ToEmailid };

                    //var path1 = "~/Content/uploads/Resume/" + objApplyForJob.Resume;
                    //var path = Server.MapPath(path1);
                    util.SendEmail(sb.ToString(), emailid, "Intimation about Proxy-Bid Approval", "", null);
                }
                else
                {

                }
            }

            objauc.GridList = getProxyBidApprovalList();

            //return View("ProxyBidApproval", objauc);
            return RedirectToAction("ProxyBidApproval");
        }

        public ActionResult RejectProxy(int? id)
        {

            Auction objauc = new Auction();

            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_ProxyBidApproval 'GetById'," + id + "");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    objauc.Sr = dr["Sr"].ToString();
                    objauc.proxyid = int.Parse(dr["Proxyid"].ToString());
                    objauc.username = dr["username"].ToString();
                    objauc.name = dr["name"].ToString();
                    objauc.email = dr["email"].ToString();
                    objauc.Auctionname = dr["Auctionname"].ToString();
                    objauc.auctiondate = dr["auctiondate"].ToString();
                    objauc.reference = dr["reference"].ToString();
                    objauc.title = dr["title"].ToString();
                    objauc.thumbnail = dr["thumbnail"].ToString();
                    objauc.ProxyAmt = int.Parse(dr["ProxyAmt"].ToString());
                    objauc.ProxyAmtus = int.Parse(dr["ProxyAmtus"].ToString());
                    objauc.status = dr["status"].ToString();
                    objauc.ConfirmedBy = dr["ConfirmedBy"].ToString();
                    objauc.ConfirmedDt = dr["ConfirmedDt"].ToString();
                    objauc.isOldUser = int.Parse(dr["isolduser"].ToString());
                    objauc.amountlimt = int.Parse(dr["amountlimt"].ToString());
                    objauc.userid = int.Parse(dr["userid"].ToString());

                }
            }

            using (SqlCommand cmd = new SqlCommand("Proc_ProxyBidApproval"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Reject_proxy");
                cmd.Parameters.AddWithValue("@Proxyid", id);
                cmd.Parameters.AddWithValue("@userid", objauc.userid);

                objauc.amountlimt = objauc.amountlimt + objauc.ProxyAmt;
                cmd.Parameters.AddWithValue("@amountlimt", objauc.amountlimt);



                if (util.Execute(cmd))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("Dear " + objauc.name + ", ");
                    sb.AppendFormat("<br><br>We regret to inform you that your Proxy Bid amount of Rs." + Common.rupeeFormat(objauc.ProxyAmt.ToString()) + "($ " + Common.DollerFormat(objauc.ProxyAmtus.ToString()) + ") for Lot No #" + objauc.reference + ", part of our '" + objauc.Auctionname + "' Auction dated " + objauc.auctiondate + " has been unsuccessful.");
                    sb.AppendFormat("<br><br><br>For any further assistance please feel free to write to us at, contact@astaguru.com or call us on 91-22 2204 8138/39. We will be glad to assist you.");
                    sb.AppendFormat("<br><br><br> Thank You, ");
                    sb.AppendFormat("<br><br><br>Team Astaguru.");


                    string ToEmailid = objauc.email;
                    String[] emailid = new String[] { ToEmailid };

                    //var path1 = "~/Content/uploads/Resume/" + objApplyForJob.Resume;
                    //var path = Server.MapPath(path1);
                    util.SendEmail(sb.ToString(), emailid, "Intimation about Proxy-Bid Rejection", "", null);
                }
                else
                {

                }
            }

            objauc.GridList = getProxyBidApprovalList();

            return RedirectToAction("ProxyBidApproval");
            //return View("ProxyBidApproval", objauc);
        }



        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxx ---  Top Value Bid   --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult Topbidvalue()
        {
            Auction objauc = new Auction();
            objauc.GridList = GetTopbidvalueList();
            return View(objauc);

        }

        public List<Auction> GetTopbidvalueList()
        {
            try
            {
                List<Auction> objList = new List<Auction>();
                DataTable dt = new DataTable();
                dt = util.Display("Exec Proc_Bidrecord 'GetbidbyTopValue'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Auction objauc = new Auction();
                        //objauc.Sr = dr["Sr"].ToString();
                        objauc.userid = int.Parse(dr["MyUserID"].ToString());
                        objauc.artist = dr["Artistname"].ToString();
                        objauc.thumbnail = dr["thumbnail"].ToString();
                        objauc.reference = dr["reference"].ToString();
                        objauc.pricers = int.Parse(dr["pricers"].ToString());
                        objauc.priceus = int.Parse(dr["priceus"].ToString());
                        objauc.username = dr["Pproxyusername"].ToString();
                        objauc.proxyusername = dr["anoname"].ToString();
                        objauc.bidcount = int.Parse(dr["Bidcount"].ToString());
                        objauc.daterec = dr["daterec"].ToString();


                        objList.Add(objauc);
                    }
                }
                return objList;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }


        public ActionResult Bidrecorduserdetail(int? id)
        {

            try
            {
                List<User> objList = new List<User>();
                DataTable dt = new DataTable();
                dt = util.Display("Exec Proc_Bidrecord 'GetBiduserbyId'," + id + "");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        User objuser = new User();
                        //objauc.Sr = dr["Sr"].ToString();
                        objuser.userid = int.Parse(dr["userid"].ToString());
                        objuser.username = dr["Username"].ToString();
                        objuser.password = dr["Password"].ToString();
                        objuser.name = dr["Name"].ToString();
                        objuser.lastname = dr["lastname"].ToString();
                        objuser.address1 = dr["Address1"].ToString();
                        objuser.address2 = dr["Address2"].ToString();
                        objuser.city = dr["City"].ToString();
                        objuser.state = dr["State"].ToString();
                        objuser.zip = dr["Zip"].ToString();
                        objuser.country = dr["Country"].ToString();
                        objuser.BillingTelephone = dr["Telephone"].ToString();
                        //objuser. = dr["Fax"].ToString();
                        objuser.email = dr["Email"].ToString();
                        objuser.RegistrationDate = dr["RegistrationDate"].ToString();
                        objuser.LastLoggedDate = dr["LastLoggedDate"].ToString();
                        objuser.Visits = int.Parse(dr["Visits"].ToString());
                        objList.Add(objuser);
                    }
                }

                User obj = new User();
                obj.GridList = objList;

                return View(obj);
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }
        #endregion


        #region xxxxxxxxxxxxxxxxxxxxx ---  Most Number Of Bid --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult MostNumberofBid()
        {
            Auction objauc = new Auction();
            objauc.GridList = GetMostNumberofBid();
            return View(objauc);
        }

        public List<Auction> GetMostNumberofBid()
        {
            try
            {
                List<Auction> objList = new List<Auction>();
                DataTable dt = new DataTable();
                dt = util.Display("Exec Proc_Bidrecord 'GetuserbymostbidCount'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Auction objauc = new Auction();
                        //objauc.Sr = dr["Sr"].ToString();
                        objauc.userid = int.Parse(dr["MyUserID"].ToString());
                        objauc.artist = dr["Artistname"].ToString();
                        objauc.thumbnail = dr["thumbnail"].ToString();
                        objauc.reference = dr["reference"].ToString();
                        objauc.pricers = int.Parse(dr["pricers"].ToString());
                        objauc.priceus = int.Parse(dr["priceus"].ToString());
                        objauc.username = dr["Username"].ToString();
                        objauc.proxyusername = dr["anoname"].ToString();
                        objauc.bidcount = int.Parse(dr["Bidcount"].ToString());
                        objauc.daterec = dr["daterec"].ToString();


                        objList.Add(objauc);
                    }
                }
                return objList;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }
        #endregion


        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxx --- Recent Bid List --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult RecentBid()
        {
            Auction objauc = new Auction();
            objauc.GridList = GetRecentBidList();
            return View(objauc);
        }

        public List<Auction> GetRecentBidList()
        {
            try
            {
                List<Auction> objList = new List<Auction>();
                DataTable dt = new DataTable();
                dt = util.Display("Exec Proc_Bidrecord 'GetUserByRecentBid'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow drproduct in dt.Rows)
                    {
                        DataTable dtData = new DataTable();
                        dtData = util.Display("Exec Proc_Bidrecord 'GetUserByRecentBidData',0," + drproduct["productid"] + "");

                        if (dtData.Rows.Count > 0)
                        {
                            foreach (DataRow drData in dtData.Rows)
                            {
                                Auction objauc = new Auction();
                                //objauc.Sr = dr["Sr"].ToString();
                                objauc.userid = int.Parse(drData["userid"].ToString());
                                objauc.artist = drData["Artistname"].ToString();
                                objauc.thumbnail = drData["thumbnail"].ToString();
                                objauc.reference = drData["reference"].ToString();
                                objauc.productid = int.Parse(drData["productid"].ToString());
                                objauc.pricers = int.Parse(drData["Bidpricers"].ToString());
                                objauc.priceus = int.Parse(drData["Bidpriceus"].ToString());
                                objauc.username = drData["Username"].ToString();
                                objauc.proxyusername = drData["anoname"].ToString();
                                objauc.bidcount = int.Parse(drData["Bidcount"].ToString());
                                objauc.daterec = drData["daterec"].ToString();


                                objList.Add(objauc);
                            }
                        }

                    }

                }
                return objList;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        #endregion


        #region xxxxxxxxxxxxxxxxxxxxxxxx --- send Email winnner --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult SendEmailWinner()
        {
            Auction objauc = new Auction();
            objauc.GridList = getSendEmailWinnerList();
            return View(objauc);
        }

        public List<Auction> getSendEmailWinnerList()
        {
            try
            {
                List<Auction> objList = new List<Auction>();
                DataTable dt = new DataTable();

                dt = util.Display("Exec Proc_SendmailBidderwinner 'GetsendmailbidderwinnerAllproduct'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow drproduct in dt.Rows)
                    {
                        DataTable dtData = new DataTable();
                        dtData = util.Display("Exec Proc_SendmailBidderwinner 'GetsendmailbidderwinnerAllproductData'," + drproduct["productid"] + "");

                        if (dtData.Rows.Count > 0)
                        {
                            foreach (DataRow drData in dtData.Rows)
                            {
                                Auction objauc = new Auction();
                                //objauc.Sr = dr["Sr"].ToString()        
                                objauc.productid = int.Parse(drData["productid"].ToString());
                                objauc.userid = int.Parse(drData["userid"].ToString());
                                objauc.username = drData["Username"].ToString();
                                objauc.name = drData["name"].ToString();
                                objauc.lastname = drData["lastname"].ToString();
                                objauc.fullAddress = drData["Address1"].ToString();
                                objauc.shortAddress = drData["Address2"].ToString();
                                objauc.city = drData["city"].ToString();
                                objauc.state = drData["state"].ToString();
                                objauc.country = drData["country"].ToString();
                                objauc.zip = drData["zip"].ToString();
                                objauc.telephone = drData["telephone"].ToString();
                                objauc.email = drData["email"].ToString();
                                objauc.image = drData["thumbnail"].ToString();
                                objauc.reference = drData["reference"].ToString();
                                objauc.artist = drData["Artistname"].ToString();
                                objauc.title = drData["title"].ToString();
                                objauc.medium = drData["medium"].ToString();
                                objauc.productsize = drData["productsize"].ToString();
                                objauc.pricers = int.Parse(drData["Bidpricers"].ToString());
                                objauc.priceus = int.Parse(drData["Bidpriceus"].ToString());
                                objauc.Auctionname = drData["Auctionname"].ToString();
                                objauc.auctiondate = drData["AuctionDate"].ToString();
                                objauc.mobile = drData["Mobile"].ToString();
                                objauc.PrVat = drData["PrVat"].ToString();


                                objList.Add(objauc);
                            }
                        }

                    }

                }
                return objList;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        public ActionResult SendEmail(string[] productIDs)
        {
            foreach (string id in productIDs)
            {
                DataTable dt = new DataTable();
                dt = util.Display("Exec Proc_SendmailBidderwinner 'Getwinnerbyproductid'," + id + "");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Auction objauc = new Auction();
                        //objauc.Sr = dr["Sr"].ToString();
                        objauc.productid = int.Parse(dr["productid"].ToString());
                        objauc.userid = int.Parse(dr["userid"].ToString());
                        objauc.username = dr["Username"].ToString();
                        objauc.name = dr["name"].ToString();
                        objauc.lastname = dr["lastname"].ToString();
                        objauc.fullAddress = dr["Address1"].ToString();
                        objauc.shortAddress = dr["Address2"].ToString();
                        objauc.city = dr["city"].ToString();
                        objauc.state = dr["state"].ToString();
                        objauc.country = dr["country"].ToString();
                        objauc.zip = dr["zip"].ToString();
                        objauc.telephone = dr["telephone"].ToString();
                        objauc.email = dr["email"].ToString();
                        objauc.image = dr["thumbnail"].ToString();
                        objauc.reference = dr["reference"].ToString();
                        objauc.artist = dr["Artistname"].ToString();
                        objauc.title = dr["title"].ToString();
                        objauc.medium = dr["medium"].ToString();
                        objauc.productsize = dr["productsize"].ToString();
                        objauc.pricers = int.Parse(dr["Bidpricers"].ToString());
                        objauc.priceus = int.Parse(dr["Bidpriceus"].ToString());
                        objauc.Auctionname = dr["Auctionname"].ToString();
                        objauc.auctiondate = dr["AuctionDate"].ToString();
                        objauc.mobile = dr["Mobile"].ToString();
                        objauc.PrVat = dr["PrVat"].ToString();
                        objauc.isInternational = int.Parse(dr["isInternational"].ToString());
                        objauc.isInternationalGST = int.Parse(dr["isInternationalGST"].ToString());
                        objauc.astaguruPrice = int.Parse(dr["isusedgood"].ToString());     ///astaguruprice is used for check isusedgood(Second handed) or not
                        if (dr["usedGoodPercentage"].ToString() == null || dr["usedGoodPercentage"].ToString() == "")
                        {
                            objauc.usedGoodPercentage = 0;
                        }
                        else
                        {
                            objauc.usedGoodPercentage = int.Parse(dr["usedGoodPercentage"].ToString());
                        }



                        long Purchase_value, gstvalue, customduty, margin, Total_Amount, Final_Total;

                        if (objauc.country == "India")
                        {
                            if (objauc.isInternational == 1 && objauc.astaguruPrice == 1)
                            {
                                long Taxable_Amount = 0;
                                Purchase_value = objauc.pricers - (long.Parse(objauc.pricers.ToString()) * objauc.usedGoodPercentage / 100);             /// with usedgood gst

                                customduty = long.Parse(objauc.pricers.ToString()) * (11 / 100);
                                margin = long.Parse(objauc.pricers.ToString()) * (15 / 100);

                                Taxable_Amount = objauc.pricers + margin + customduty - Purchase_value;
                                gstvalue = (long.Parse(Taxable_Amount.ToString()) * objauc.isInternationalGST / 100);

                                Total_Amount = objauc.pricers + customduty + margin;
                            
                                Final_Total = Total_Amount + gstvalue;                                            ///  with internation Gst

                            }
                            else if (objauc.isInternational == 1 && objauc.astaguruPrice != 1)
                            {
                                customduty = long.Parse(objauc.pricers.ToString()) * 11 / 100;
                                margin = long.Parse(objauc.pricers.ToString()) * 15 / 100;
                                Total_Amount = objauc.pricers + customduty + margin;
                                gstvalue = (long.Parse(Total_Amount.ToString()) * objauc.isInternationalGST / 100);
                                Final_Total = Total_Amount + gstvalue;                                             ///  with internation Gst
                            }

                            else if (objauc.astaguruPrice == 1 && objauc.isInternational != 1)
                            {
                                long Taxable_Amount = 0;
                                Purchase_value = objauc.pricers - (long.Parse(objauc.pricers.ToString()) * objauc.usedGoodPercentage / 100);            /// with usedgood gst

                                margin = long.Parse(objauc.pricers.ToString()) * 15 / 100;
                                Taxable_Amount = objauc.pricers + margin - Purchase_value;
                               
                                gstvalue = (long.Parse(Taxable_Amount.ToString()) * int.Parse(objauc.PrVat) / 100);

                                Total_Amount = objauc.pricers + margin;
                                Final_Total = Total_Amount + gstvalue;

                            }
                            else
                            {
                                margin = long.Parse(objauc.pricers.ToString()) * 15 / 100;
                                Total_Amount = objauc.pricers + margin;
                                gstvalue = (long.Parse(Total_Amount.ToString()) * int.Parse(objauc.PrVat) / 100);
                                Final_Total = Total_Amount + gstvalue;

                            }
                        }
                        else
                        {

                            margin = objauc.priceus * 15 / 100;
                            Total_Amount = objauc.priceus + margin;
                            gstvalue = 0;
                            Final_Total = objauc.priceus + margin;

                        }

                        StringBuilder sb = new StringBuilder();


                        sb.Append("Dear " + objauc.name + " "+ objauc.lastname + ",<br/><br/>");
                        sb.Append("It gives us great joy to inform you, that you have won Lot No " + objauc.reference + " from our " + objauc.Auctionname + ", dated " + objauc.auctiondate + ". Herewith below we have mentioned the specifics of the Lot acquired along with a snapshot of the Art work.<br/><br/>");
                        sb.Append("We are glad that our endeavors to enrich your life with Art has been successful.<br/><br/>");
                        sb.Append("We will have a follow up mail sent out to you with a detailed pro-forma invoice.<br/><br/>");
                        sb.Append("Your billing details,<br/>");
                        sb.Append(objauc.username + "<br/>");
                        sb.Append(objauc.fullAddress + ",<br/>");
                        if (objauc.shortAddress != null || objauc.shortAddress != "")
                        {
                            sb.Append(objauc.shortAddress + ",<br/>");
                        }
                        sb.Append(objauc.city + ",<br/>");
                        sb.Append(objauc.state + ",<br/>");
                        sb.Append(objauc.country + ",<br/>");
                        sb.Append(objauc.zip + ",<br/>");
                        sb.Append(objauc.email + "<br/><br/>");
                        sb.Append("Painting details as follows<br /><br />");
                        sb.Append("<table border=2 bordercolor=black><tr><td>Image</td><td>Description</td><td>Price</td></tr><tr>");
                        sb.Append("<td><img src='http://www.astaguru.com/" + objauc.image + "' style='height:250px;width:200px'></td>");
                        sb.Append("<td>");
                        if (objauc.country == "India")
                        {
                            if (objauc.isInternational == 1)
                            {
                                sb.Append("Value of Item Including 15% Margin and 11% Custom Duty <br /><br />");
                            }
                            else
                            {
                                sb.Append("Value of Item Including 15% Margin<br /><br />");
                            }
                            sb.Append("<b>Lot.No.</b>- " + objauc.reference + "<br>");
                            sb.Append("<b>Title</b> - " + objauc.title + "<br>");
                            if (objauc.isInternational == 1)
                            {
                                sb.Append("<b>Size</b> - " + objauc.productsize + "in<br><b>GST on hammer price (" + objauc.isInternationalGST + "%)</b><br>&nbsp;</td><td algin=top>Rs." + Common.rupeeFormat(Total_Amount.ToString()) + "<br><br><br>");

                            }
                            else
                            {
                                sb.Append("<b>Size</b> - " + objauc.productsize + "in<br><b>GST on hammer price (" + objauc.PrVat + "%)</b><br>&nbsp;</td><td algin=top>Rs." + Common.rupeeFormat(Total_Amount.ToString()) + "<br><br><br>");

                            }
                            sb.Append("<br/>");
                            sb.Append("<br>Rs." + gstvalue + "<br>");
                            sb.Append("<br/>");
                            sb.Append("</td></tr>");
                            sb.Append("<tr><td align=right colspan=3><b>Total</b> - Rs." + Common.rupeeFormat(Final_Total.ToString()) + "</td></tr></table><br>");
                        }

                        else
                        {
                            sb.Append("Value of Item Including 15% Margin<br /><br />");
                            sb.Append("<b>Lot.No.</b>- " + objauc.reference + "<br>");
                            sb.Append("<b>Title</b> - " + objauc.title + "<br>");
                            sb.Append("<b>Size</b> - " + objauc.productsize + "in<br><br>&nbsp;</td><td algin=top>$" + Common.DollerFormat(Total_Amount.ToString()) + "<br><br><br>");
                            sb.Append("<br/>");
                            sb.Append("</td></tr>");
                            sb.Append("<tr><td align=right colspan=3><b>Total</b> - $" + Common.DollerFormat(Final_Total.ToString()) + "</td></tr></table><br>");
                        }

                        sb.Append("<br><br>Please do feel free to contact us for any further clarifications at, contact@astaguru.com or call us on 91-22 2204 8138/39. We will be glad to resolve your queries.");
                        sb.Append("<br><br><br>Warmest Regards,<br>&nbsp;Team AstaGuru");


                        string ToEmailid = objauc.email;
                        String[] emailid = new String[] { ToEmailid };

                        //var path1 = "~/Content/uploads/Resume/" + objApplyForJob.Resume;
                        //var path = Server.MapPath(path1);

                        string subject = "Congratulations Dear " + objauc.username + ", on winning Lot No " + objauc.reference + " from our " + objauc.Auctionname + ", " + objauc.auctiondate;
                        util.SendEmailwinner(sb.ToString(), emailid, subject, "", null);
                    }
                }
            }
            return Json("Email Sent successfully!");
        }

        public ActionResult SendEmailWithPerformaInvoice(string[] productIDs)
        {

            string date = productIDs[productIDs.Length - 1];                                //get Date 
            int Invoice_No = int.Parse(productIDs[productIDs.Length - 2]);                  // get Invoice No
            string Invoice_start = productIDs[productIDs.Length - 3];                       //get Invoice start 



            productIDs = productIDs.Take(productIDs.Count() - 1).ToArray();                 //remove Date 
            productIDs = productIDs.Take(productIDs.Count() - 1).ToArray();                 //remove Invoice No
            productIDs = productIDs.Take(productIDs.Count() - 1).ToArray();                 //remove Invoice start


            foreach (string id in productIDs)
            {

                DataTable dt = new DataTable();
                dt = util.Display("Exec Proc_SendmailBidderwinner 'Getwinnerbyproductid'," + id + "");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        AuctionFor_Invoice objauc = new AuctionFor_Invoice();
                        //objauc.Sr = dr["Sr"].ToString();
                        objauc.productid = int.Parse(dr["productid"].ToString());
                        objauc.userid = int.Parse(dr["userid"].ToString());
                        objauc.username = dr["Username"].ToString();
                        objauc.name = dr["name"].ToString();
                        objauc.lastname = dr["lastname"].ToString();
                        objauc.fullAddress = dr["Address1"].ToString();
                        objauc.shortAddress = dr["Address2"].ToString();
                        objauc.city = dr["city"].ToString();
                        objauc.state = dr["state"].ToString();
                        objauc.country = dr["country"].ToString();
                        objauc.zip = dr["zip"].ToString();
                        objauc.telephone = dr["telephone"].ToString();
                        objauc.email = dr["email"].ToString();
                        objauc.image = dr["thumbnail"].ToString();
                        objauc.reference = dr["reference"].ToString();
                        objauc.artist = dr["Artistname"].ToString();
                        objauc.title = dr["title"].ToString();
                        objauc.medium = dr["medium"].ToString();
                        objauc.productdate = dr["productdate"].ToString();
                        objauc.productsize = dr["productsize"].ToString();
                        objauc.pricers = int.Parse(dr["Bidpricers"].ToString());
                        objauc.priceus = int.Parse(dr["Bidpriceus"].ToString());
                        objauc.Auctionname = dr["Auctionname"].ToString();
                        objauc.auctiondate = dr["AuctionDate"].ToString();
                        objauc.mobile = dr["Mobile"].ToString();
                        objauc.PrVat = dr["PrVat"].ToString();
                        objauc.isInternational = int.Parse(dr["isInternational"].ToString());
                        objauc.isInternationalGST = int.Parse(dr["isInternationalGST"].ToString());
                        objauc.astaguruPrice = int.Parse(dr["isusedgood"].ToString());     ///astaguruprice is used for check isusedgood(Second handed) or not
                        if (dr["usedGoodPercentage"].ToString() == null || dr["usedGoodPercentage"].ToString() == "")
                        {
                            objauc.usedGoodPercentage = 0;
                        }
                        else
                        {
                            objauc.usedGoodPercentage = int.Parse(dr["usedGoodPercentage"].ToString());
                        }



                        double Purchase_value, gstvalue = 0, customduty=0, margin, Total_Amount, Final_Total, CGST = 0, SGST = 0, CGST_value = 0, SGST_value = 0, Taxable_Amount=0, product_price=0;
                      
                        if (objauc.isInternational == 1)
                        {
                            CGST = objauc.isInternationalGST / 2;
                            SGST = objauc.isInternationalGST / 2;
                        }
                        else
                        {
                            CGST = Convert.ToDouble(objauc.PrVat.ToString()) / 2;
                            SGST = Convert.ToDouble(objauc.PrVat.ToString()) / 2;
                      
                        }


                        if (objauc.country == "India")
                        {

                            if (objauc.isInternational == 1 && objauc.astaguruPrice == 1 && objauc.state == "Maharashtra")
                            {

                                Purchase_value = objauc.pricers - (long.Parse(objauc.pricers.ToString()) * objauc.usedGoodPercentage / 100);  /// with usedgood gst

                                customduty = long.Parse(objauc.pricers.ToString()) * (11 / 100);
                                margin = long.Parse(objauc.pricers.ToString()) * (15 / 100);
                                Taxable_Amount = objauc.pricers + customduty + margin - Purchase_value;          // for second hand product taxable amount
                                
                                CGST_value = (long.Parse(Taxable_Amount.ToString()) * CGST / 100);
                                SGST_value = (long.Parse(Taxable_Amount.ToString()) * SGST / 100);

                                product_price = objauc.pricers + margin;                                        //product price (objauc.pricers + margin)

                                Total_Amount = objauc.pricers + customduty + margin;                           // sub total (objauc.pricers + customduty + margin)

                                CGST_value= Math.Round(CGST_value);                                            //Round double value
                                SGST_value= Math.Round(SGST_value);                                            //Round double value                       

                                Final_Total = Total_Amount + CGST_value + SGST_value;                         // Final Total (Total_Amount + Tax)

                                Final_Total = Math.Round(Final_Total);                                          //Round double value
                            }

                            else  if (objauc.isInternational == 1 && objauc.astaguruPrice == 1 && objauc.state != "Maharashtra")
                            {
                         
                                Purchase_value = objauc.pricers - (long.Parse(objauc.pricers.ToString()) * objauc.usedGoodPercentage / 100);           /// with usedgood gst

                                customduty = long.Parse(objauc.pricers.ToString()) * (11 / 100);
                                margin = long.Parse(objauc.pricers.ToString()) * (15 / 100);
                                Taxable_Amount = objauc.pricers + customduty + margin - Purchase_value;                         /// for second hand product taxable amount
                                gstvalue = (long.Parse(Taxable_Amount.ToString()) * objauc.isInternationalGST / 100);
                                gstvalue = Math.Round(gstvalue);                                                             //Round double value
                     

                                product_price = objauc.pricers + margin;                                                   //product price (objauc.pricers + margin)

                                Total_Amount = objauc.pricers + customduty + margin;

                                Final_Total = Total_Amount + gstvalue;                                                    ///  with internation Gst

                                Final_Total = Math.Round(Final_Total);                                                      //Round double value

                            }

                            else if (objauc.isInternational == 1 && objauc.astaguruPrice != 1 && objauc.state == "Maharashtra")
                            {
                                customduty = long.Parse(objauc.pricers.ToString()) * 11 / 100;
                                margin = long.Parse(objauc.pricers.ToString()) * 15 / 100;

                                product_price = objauc.pricers + margin;                                                   //product price (objauc.pricers + margin)

                                Total_Amount = objauc.pricers + customduty + margin;
                                CGST_value = (long.Parse(Total_Amount.ToString()) * CGST / 100);
                                SGST_value = (long.Parse(Total_Amount.ToString()) * SGST / 100);

                                CGST_value = Math.Round(CGST_value);                                            //Round double value
                                SGST_value = Math.Round(SGST_value);                                            //Round double value   

                                Final_Total = Total_Amount + CGST_value + SGST_value;

                                Final_Total = Math.Round(Final_Total);                                         //Round double value
                            }

                            else if (objauc.astaguruPrice == 1 && objauc.isInternational != 1 && objauc.state != "Maharashtra")
                            {
                     
                                Purchase_value = objauc.pricers - (long.Parse(objauc.pricers.ToString()) * objauc.usedGoodPercentage / 100);            /// with usedgood gst
                                margin = long.Parse(objauc.pricers.ToString()) * 15 / 100;
                                 
                                Taxable_Amount = objauc.pricers + margin - Purchase_value;                                    // for second hand product taxable amount
                                gstvalue = (long.Parse(Taxable_Amount.ToString()) * int.Parse(objauc.PrVat) / 100);
                                gstvalue = Math.Round(gstvalue);                                                             //Round double value

                                product_price = objauc.pricers + margin;                                                   //product price (objauc.pricers + margin)

                                Total_Amount = objauc.pricers + margin;

                                Final_Total = Total_Amount + gstvalue;

                                Final_Total = Math.Round(Final_Total);                                                    //Round double value

                            }

                            else if (objauc.astaguruPrice == 1 && objauc.isInternational != 1 && objauc.state == "Maharashtra")
                            {
                              
                                Purchase_value = objauc.pricers - (long.Parse(objauc.pricers.ToString()) * objauc.usedGoodPercentage / 100);      /// with usedgood gst                                                       

                                margin = long.Parse(objauc.pricers.ToString()) * 15 / 100;
                                Taxable_Amount = objauc.pricers + margin - Purchase_value;                          // for second hand product taxable amount

                                CGST_value = (long.Parse(Taxable_Amount.ToString()) * CGST / 100);
                                SGST_value = (long.Parse(Taxable_Amount.ToString()) * SGST / 100);

                                CGST_value = Math.Round(CGST_value);                                            //Round double value
                                SGST_value = Math.Round(SGST_value);                                            //Round double value   

                                product_price = objauc.pricers + margin;                                                   //product price (objauc.pricers + margin)

                                Total_Amount = objauc.pricers + margin;
                                Final_Total = Total_Amount + CGST_value + SGST_value;
                                Final_Total = Math.Round(Final_Total);                                             //Round double value
                            }


                            else if (objauc.isInternational == 1 && objauc.astaguruPrice != 1 && objauc.state != "Maharashtra")
                            {
                                customduty = long.Parse(objauc.pricers.ToString()) * 11 / 100;
                                margin = long.Parse(objauc.pricers.ToString()) * 15 / 100;

                                product_price = objauc.pricers + margin;                                                   //product price (objauc.pricers + margin)

                                Total_Amount = objauc.pricers + customduty + margin;
                                gstvalue = (long.Parse(Total_Amount.ToString()) * objauc.isInternationalGST / 100);
                                gstvalue = Math.Round(gstvalue);                                                             //Round double value

                                Final_Total = Total_Amount + gstvalue;                                             ///  with internation Gst
                                Final_Total = Math.Round(Final_Total);                                              //Round double value
                            }

                                                                       
                            else if (objauc.state == "Maharashtra")
                            {
                                margin = long.Parse(objauc.pricers.ToString()) * 15 / 100;

                                product_price = objauc.pricers + margin;                                                   //product price (objauc.pricers + margin)

                                Total_Amount = objauc.pricers + margin;
                                CGST_value = (long.Parse(Total_Amount.ToString()) * CGST / 100);
                                SGST_value = (long.Parse(Total_Amount.ToString()) * SGST / 100);

                                CGST_value = Math.Round(CGST_value);                                            //Round double value
                                SGST_value = Math.Round(SGST_value);                                            //Round double value   

                                Final_Total = Total_Amount + CGST_value + SGST_value;
                                Final_Total = Math.Round(Final_Total);                                          //Round double value

                            }
                            else
                            {
                                margin = long.Parse(objauc.pricers.ToString()) * 15 / 100;

                                product_price = objauc.pricers + margin;                                                   //product price (objauc.pricers + margin)

                                Total_Amount = objauc.pricers + margin;
                                gstvalue = (long.Parse(Total_Amount.ToString()) * int.Parse(objauc.PrVat) / 100);
                                gstvalue = Math.Round(gstvalue);                                                             //Round double value

                                Final_Total = Total_Amount + gstvalue;
                                Final_Total = Math.Round(Final_Total);                                                          //Round double value
                            }

                        }
                        else
                        {
                     

                            margin = objauc.priceus * 15 / 100;

                            product_price = objauc.priceus + margin;                                                   //product price (objauc.pricers + margin)

                            Total_Amount = objauc.priceus + margin;
                            gstvalue = 0;                                                                                    //No Tax for international users
                            Final_Total = objauc.priceus + margin;
                            Final_Total = Math.Round(Final_Total);                                                          //Round double value

                        }
                        ///  create performa invoice pdf
                        using (StringWriter sw = new StringWriter())
                        {
                            using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                            {

                                //StringBuilder sbPerformaInvoice = new StringBuilder();
                                string sbPerformaInvoice;
                                sbPerformaInvoice = "<table width='100%' align='center' border='0' cellpadding='10' cellspacing='0' style='font-family: Roboto; font-size: 14px; font-weight: normal;'>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td colspan='3' style='background-color:#000;'>";
                                sbPerformaInvoice += "<img src='https://www.astaguru.com:84/Content/images/logo.png' height='60'/></td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td  colspan='2' style='font-weight: bold;'>Div.of Safset Agencies Pvt.Ltd.</td>";
                                sbPerformaInvoice += "<td width='253' style='font-weight: bold;'>PROFORMA INVOICE</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td colspan='2'></td>";
                                sbPerformaInvoice += "<td>";
                                sbPerformaInvoice += "<table width='100%' border='1' cellpadding='5' cellspacing='0' style='font-size: 11px;  border-collapse: collapse; font-weight: bold;'>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td>Invoice Number:</td>";
                                if ((Invoice_No > 10 && Invoice_No < 100) || Invoice_No == 10)
                                {
                                    sbPerformaInvoice += "<td>" + Invoice_start + "-0" + Invoice_No + "</td>";
                                }
                                else if (Invoice_No > 100 || Invoice_No == 100)
                                {
                                    sbPerformaInvoice += "<td>" + Invoice_start + "-" + Invoice_No + "</td>";
                                }
                                else if (Invoice_No < 10)
                                {
                                    sbPerformaInvoice += "<td>" + Invoice_start + "-00" + Invoice_No + "</td>";
                                }
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td>Date:</td>";
                                sbPerformaInvoice += "<td>" + date + "</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "</table>";
                                sbPerformaInvoice += "</td>";
                                sbPerformaInvoice += "</tr>";
                                //sbPerformaInvoice += "<tr>";
                                //sbPerformaInvoice += "<td height='50' colspan='3'>&nbsp;            &nbsp;</td>";
                                //sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td colspan='3'>";
                                sbPerformaInvoice += "<table width='100%' cellpadding='5' cellspacing='0' border='1' style='font-size: 11px; font-weight: bold; border-collapse:collapse;'>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td width='250'>Customer Name:</td>";
                                sbPerformaInvoice += "<td>Address</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td>" + objauc.name + " "+ objauc.lastname + "</td>";
                                sbPerformaInvoice += "<td>" + objauc.fullAddress +" "+ objauc.shortAddress + "</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "</table>";
                                sbPerformaInvoice += "</td>";
                                sbPerformaInvoice += "</tr>";
                                //sbPerformaInvoice += "<tr>";
                                //sbPerformaInvoice += "<td colspan='3'>&nbsp;</td>";
                                //sbPerformaInvoice += "</tr>";
                                //sbPerformaInvoice += "<tr>";
                                //sbPerformaInvoice += "<td height='50' colspan='3'>&nbsp;            &nbsp;</td>";
                                //sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td colspan='3'>";
                                sbPerformaInvoice += "<table width='100%' border='1' cellpadding='5' cellspacing='0' style='font-size: 11px; text-align: center; font-weight: bold; border-collapse: collapse;'>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td width='32%'>Port of Loading and Country of Origin</td>";
                                sbPerformaInvoice += "<td width='30%' style='font-weight: normal;'>MUMBAI MAHARASHTRA INDIA</td>";
                                sbPerformaInvoice += "<td width='13%'>Delivery</td>";
                                sbPerformaInvoice += "<td width='25%' style='font-weight: normal;'>" + objauc.city + " " + objauc.state + " " + objauc.country + "</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "</table>";
                                sbPerformaInvoice += "</td>";
                                sbPerformaInvoice += "</tr>";
                                //sbPerformaInvoice += "<tr>";
                                //sbPerformaInvoice += "<td height='50' colspan='3'>&nbsp;            &nbsp;</td>";
                                //sbPerformaInvoice += "</tr>";
                                //sbPerformaInvoice += "<tr>";
                                //sbPerformaInvoice += "<td colspan='3'>&nbsp;</td>";
                                //sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td colspan='3'>";
                                sbPerformaInvoice += "<table width='100%' border='1' cellpadding='5' cellspacing='0' style='font-size: 11px; text-align: center; font-weight: bold; border-collapse: collapse;'>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td width='32%' style='text-align: center;'>Image</td>";
                                sbPerformaInvoice += "<td colspan='2' style='text-align: center;'>Description Of Goods</td>";
                                sbPerformaInvoice += "<td width='14%' style='text-align: center;'>Quantity</td>";
                                sbPerformaInvoice += "<td width='21%' style='text-align: center;'>Product Price</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td rowspan='8'>";
                                sbPerformaInvoice += "<img src='https://astaguru.com/" + objauc.image + "' width='130' height='200' /></td>";
                                sbPerformaInvoice += "<td colspan='2' style='font-weight: normal;'>Value of Item including 15% margin</td>";
                                sbPerformaInvoice += "<td rowspan='8' style='text-align: center'>1 </td>";
                                if (objauc.country == "India")
                                {
                                    sbPerformaInvoice += "<td rowspan='8' style='text-align: center'>" + Common.rupeeFormat(product_price.ToString()) + "</td>";
                                }
                                else
                                {
                                  
                                   sbPerformaInvoice += "<td rowspan='8' style='text-align: center'>  " + Common.DollerFormat(product_price.ToString()) + "</td>";

                                }
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td colspan='2' style='font-weight: normal;'>&nbsp;</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td colspan='2' style='text-align: left; font-weight: normal;'>Lot.No.: " + objauc.reference + "</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                //sbPerformaInvoice += "<td colspan='2' style='text-align: left; font-weight: normal;'>Artist Name: " + objauc.artist + "</td>";
                                sbPerformaInvoice += "<td colspan='2' style='text-align: left; font-weight: normal;'>" + objauc.artist + "</td>";
                                sbPerformaInvoice += "</tr>";

                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td colspan='2' style='text-align: left; font-weight: normal;'>Title: " + objauc.title + "</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td colspan='2' style='text-align: left; font-weight: normal;'>Medium: "+ objauc.medium + "</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td colspan='2' style='text-align: left; font-weight: normal;'>Year: "+ objauc.productdate + "</td>";
                                sbPerformaInvoice += "</tr>";

                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td colspan='2' style='text-align: left; font-weight: normal;'>Size: " + objauc.productsize + "</td>";
                                sbPerformaInvoice += "</tr>";

                                if (objauc.isInternational == 1 && objauc.country == "India")
                                {
                                    sbPerformaInvoice += "<tr>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    sbPerformaInvoice += "<td colspan='2' style='text-align: left;'>Reimbursment of Import Duty 11% on Hammer Price of " + Common.rupeeFormat(objauc.pricers.ToString()) + "</td>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    sbPerformaInvoice += "<td>"+ Common.rupeeFormat(customduty.ToString()) +"</td>";
                                    sbPerformaInvoice += "</tr>";
                                }
                                else if(objauc.isInternational == 1 && objauc.country != "India")
                                {
                                    sbPerformaInvoice += "<tr>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    sbPerformaInvoice += "<td colspan='2' style='text-align: left;'>Reimbursment of Import Duty 11% on Hammer Price of " + Common.DollerFormat(objauc.pricers.ToString()) + "</td>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    sbPerformaInvoice += "<td>"+ Common.DollerFormat(customduty.ToString()) + "</td>";
                                    sbPerformaInvoice += "</tr>";
                                }
                               

                             
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td>&nbsp;</td>";
                                sbPerformaInvoice += "<td colspan='2' style='text-align: left; font-weight: bold;'>Sub Total</td>";
                                sbPerformaInvoice += "<td>&nbsp;</td>";
                                if (objauc.country == "India")
                                {
                                    sbPerformaInvoice += "<td style='text-align: left; font-weight: bold;text-align: right;'>" + Common.rupeeFormat(Total_Amount.ToString()) + "</td>";
                                }
                                else
                                {
                                    sbPerformaInvoice += "<td style='text-align: left; font-weight: bold;text-align: right;'>" + Common.DollerFormat(Total_Amount.ToString()) + "</td>";

                                }
                                sbPerformaInvoice += "</tr>";
                                if (objauc.country == "India")
                                {
                                    sbPerformaInvoice += "<tr>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    sbPerformaInvoice += "<td colspan='2' style='text-align: left; font-weight: normal;'>IGST " + objauc.PrVat + "% on lot</td>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    if (objauc.state != "maharashtra")
                                    {
                                        
                                        sbPerformaInvoice += "<td style='text-align: left; font-weight: normal; text-align: right;'>" + Common.rupeeFormat(gstvalue.ToString()) + "</td>";
                                    }
                                    else
                                    {
                                        sbPerformaInvoice += "<td style='text-align: left; font-weight: normal; text-align: right;'>&nbsp;</td>";
                                    }
                                        sbPerformaInvoice += "</tr>";
                                    sbPerformaInvoice += "<tr>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    sbPerformaInvoice += " <td colspan='2' style='text-align: left; font-weight: normal;'>CGST " + CGST + "% on lot</td>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    if (objauc.state == "Maharashtra")
                                    {
                                       
                                        sbPerformaInvoice += "<td style='text-align: left; font-weight: normal; text-align: right'>" + Common.rupeeFormat(CGST_value.ToString()) + "</td>";
                                    }
                                    else
                                    {
                                        sbPerformaInvoice += "<td style='text-align: left; font-weight: normal; text-align: right'>&nbsp;</td>";
                                    }
                                        sbPerformaInvoice += "</tr>";
                                    sbPerformaInvoice += "<tr>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    sbPerformaInvoice += "<td colspan='2' style='text-align: left; font-weight: normal; padding: 0px 10px;'>SGST " + SGST + "% on lot</td>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    if (objauc.state == "Maharashtra")
                                    {
                                        
                                        sbPerformaInvoice += "<td style='text-align: left; font-weight: normal; text-align: right'>" + Common.rupeeFormat(SGST_value.ToString()) + "</td>";
                                    }
                                    else
                                    {
                                        sbPerformaInvoice += "<td style='text-align: left; font-weight: normal; text-align: right'>&nbsp;</td>";
                                    }
                                        sbPerformaInvoice += "</tr>";
                                    sbPerformaInvoice += "<tr>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    sbPerformaInvoice += "<td colspan='2' style='text-align: left; font-weight: normal;'>Total Invoice Amount</td>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    sbPerformaInvoice += "<td style='text-align: left; font-weight: normal; text-align: right'>" + Common.rupeeFormat(Final_Total.ToString()) + "</td>";
                                    sbPerformaInvoice += "</tr>";
                                }
                                else
                                {
                                    sbPerformaInvoice += "<tr>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    sbPerformaInvoice += "<td colspan='2' style='text-align: left; font-weight: normal;'>IGST " + objauc.PrVat + "% on lot</td>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    sbPerformaInvoice += "<td style='text-align: left; font-weight: normal; text-align: right;'>" + Common.DollerFormat(gstvalue.ToString()) + "</td>";
                                    sbPerformaInvoice += "</tr>";
                                    sbPerformaInvoice += "<tr>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    sbPerformaInvoice += "<td colspan='2' style='text-align: left; font-weight: normal;'>Total Invoice Amount</td>";
                                    sbPerformaInvoice += "<td>&nbsp;</td>";
                                    sbPerformaInvoice += "<td style='text-align: left; font-weight: normal; text-align: right;'>" + Common.DollerFormat(Final_Total.ToString()) + "</td>";
                                    sbPerformaInvoice += "</tr>";
                                }
                                sbPerformaInvoice += "</table>";
                                sbPerformaInvoice += "</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";

                                if (objauc.country == "India")
                                {
                                    NumberToWords objnum = new NumberToWords();
                                    string number = Final_Total.ToString();
                                    number = objnum.ConvertAmount(double.Parse(number));
                                    sbPerformaInvoice += "<td  colspan='3' style='font-size: 10px;'><strong>Amount(IN WORDS) " + number + "</strong></td>";
                                }
                                else
                                {
                                    NumberToWords objnum = new NumberToWords();
                                    string number = Final_Total.ToString();
                                    number = objnum.MoneyToWordsDollare(double.Parse(number));
                                    sbPerformaInvoice += "<td  colspan='3' style='font-size: 10px;'><strong>Amount(IN WORDS) " + number + "</strong></td>";

                                }
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td  colspan='3' style='font-size: 10px;'><strong>Note</strong>:Duties,Taxes and any other charges, wherever applicable at the shipping destination, must be paid directly by the buyer to the respective authorities.</td> ";
                                sbPerformaInvoice += "</tr>";
                                //sbPerformaInvoice += "<tr>";
                                //sbPerformaInvoice += "<td colspan='3'>&nbsp;</td>";
                                //sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td colspan='3' style='font-size: 10px; font-weight: bold;'>THIS IS A COMPUTER GENERATED INVOICE AND DOES NOT REQUIRE ANY SIGNATURE</td>";
                                sbPerformaInvoice += "</tr>";

                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td colspan='3'>";
                                sbPerformaInvoice += "<table width='100%' cellpadding='5' cellspacing='0' style='font-size: 10px; border: 1px solid #333; font-weight: bold;'>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td width='37%' style='text-align: left; font-weight: normal;'>Payment Infomation:</td>";
                                sbPerformaInvoice += "<td colspan='2' style='text-align: left; font-weight: normal; '>Payment by Direct Wire Transfer</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: normal;'>Payment by Cheque</td>";
                                sbPerformaInvoice += "<td style='font-weight: normal;'>&nbsp;</td>";
                                sbPerformaInvoice += "<td style='font-weight: normal;'>&nbsp;</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: bold;'>Please make your cheque in favour of :</td>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: normal; padding:'>Name of the Benficiary:</td>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: bold;'>Astaguru.coms</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: bold;'>Astaguru.com</td>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: normal;'>Bank Name :</td>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: bold;'>ICICI Bank,</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td>&nbsp;</td>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: normal;'>Bank Address:</td>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: normal;'>240 Navsari Building, D. N.Road, Fort, Mumbai - 400001. India.</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td>&nbsp;</td>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: normal;'>Account No:</td>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: bold;'>623505385049</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td>&nbsp;</td>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: normal;'>Swift Code:</td>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: normal;'>ICICINBBCTS</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td>&nbsp;</td>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: normal;'>RTGS / NEFT IFSC Code:</td>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: normal;'>ICIC0006235</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td>&nbsp;</td>";
                                sbPerformaInvoice += "<td width='22%' valign='top' style='text-align: left; font-weight: normal;'>IBAN Number :</td>";
                                sbPerformaInvoice += "<td width='41%' style='text-align: left; font-weight: normal;'>DE92501108006231605970(For Euro Payments)</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td>&nbsp;</td>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: normal;'>IBAN Number :</td>";
                                sbPerformaInvoice += "<td style='text-align: left; font-weight: normal;'>ABA FED No. 021000021 (For USD Payments)</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "</table>";
                                sbPerformaInvoice += "</td>";
                                sbPerformaInvoice += "</tr>";
                                //sbPerformaInvoice += "<tr>";
                                //sbPerformaInvoice += "<td height='50' colspan='3'>&nbsp;            &nbsp;</td>";
                                //sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += " <tr border='1' cellpadding='5'>";
                                sbPerformaInvoice += "<td colspan='3'>";
                                sbPerformaInvoice += " <table width='100%' border='0' cellpadding='5' cellspacing='0' style='font-size: 9px; text-align: center; font-weight: bold; border-collapse: collapse;'>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td style='font-size: 9px;'>GSTTIN No. : 27AABCS9352H1Z4</td>";
                                sbPerformaInvoice += "<td>&nbsp;</td>";
                                sbPerformaInvoice += "<td style='font-size: 9px;'>With effect from 1st July 2017</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td style='border-top: 1px solid #333; padding: 10px 0px; font-size: 9px'>IEC Code: 307004121</td>";
                                sbPerformaInvoice += "<td style='border-top: 1px solid #333; padding: 10px 0px; font-size: 9px'>PAN No: AABCS9352H</td>";
                                sbPerformaInvoice += "<td style='border-top: 1px solid #333; padding: 10px 0px; font-size: 9px'>CIN: U51209MH1966PTCO013575</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "</table>";
                                sbPerformaInvoice += "</td>";
                                sbPerformaInvoice += "</tr>";

                                sbPerformaInvoice += "<tr>";
                                sbPerformaInvoice += "<td colspan='3' style='font-size: 9px; text-align: center; border-top: 1px solid #333; font-weight: bold; padding: 10px 0px;'>FGP Centre, Commercial Union House, 3rd Floor, 9, Wallace Street, Fort, Mumbai - 400 001, India.<br />";
                                sbPerformaInvoice += " Tel: 91 - 22 - 2204 8138 / 39 Fax: 91 - 22 - 2204 8140 Email: contact @astaguru.com</td>";
                                sbPerformaInvoice += "</tr>";
                                sbPerformaInvoice += "</table>";

                                ViewBag.Content = sbPerformaInvoice.ToString();




                                StringReader sr = new StringReader(ViewBag.Content);

                                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);


                                using (MemoryStream memoryStream = new MemoryStream())
                                {
                                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                                    pdfDoc.Open();

                                    XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                                    //HTMLWorker htmlWorker = new HTMLWorker(pdfDoc);
                                    //htmlWorker.Parse(new StringReader(sbPerformaInvoice.ToString()));


                                    pdfDoc.Close();
                                    byte[] bytes = memoryStream.ToArray();
                                    memoryStream.Close();

                                    ///  used for send mail content
                                    StringBuilder sb = new StringBuilder();

                                    sb.Append("<p>Dear " + objauc.name + " "+objauc.lastname+",</p>");
                                    sb.Append("<p>Please find attached a detailed pro-forma invoice for your winning bid with regards to Lot #" + objauc.reference + " at the recently concluded '" + objauc.Auctionname + "' Auction, " + objauc.auctiondate + ". Please verify and ensure that all the mentioned details are accurate.</p>");
                                    sb.Append("<p>Once again Congratulations on your purchase of Lot#" + objauc.reference + " from our auction - '" + objauc.Auctionname + "'. The pro-forma invoice for the winning bid value and the buyer's premium applicable on your purchase is mentioned for your reference.</p>");
                                    if (objauc.country == "India")
                                    {
                                        sb.Append("The total amount of your pro-forma invoice is Rs." + Common.rupeeFormat(Final_Total.ToString()) + "(including margin)");
                                    }
                                    else
                                    {
                                        sb.Append("The total amount of your pro-forma invoice is $ " + Common.DollerFormat(Final_Total.ToString()) + "(including margin)");
                                    }
                                    sb.Append("<br/><br/>");
                                    sb.Append("<u><b>Please note:</b></u>");
                                    sb.Append("<ul>");
                                    sb.Append("<li>Payment must be completed within 7 business days of your receipt of this email. Details on how to complete payment are given on the invoice.</li>");
                                    sb.Append("<li>After receiving the payment, we will take approximately 3 business days to settle your payment (except USD Cheques) and you will receive a payment confirmation email from us.</li>");
                                    sb.Append("<li>Packing and Freight charges for domestic (except Mumbai) and international shipments will be charged separately depending on the size of the shipment</li>");
                                    sb.Append("<li>Please also note for international shipments from India the additional charges calculated are only till the destination port. Import-related duties, taxes delivery and any other charges, wherever applicable, will be directly paid by the buyer.</li>");
                                    sb.Append("<li>Shipping companies only offer closed deliveries. Should you wish for a open delivery of the lot, the same can be arranged on special request and at an additional cost.</li>");
                                    sb.Append("</ul>");
                                    sb.Append("<br/><br/>Please do feel free to contact us for any further clarifications at, contact@astaguru.com or call us on 91-22 2204 8138/39. We will be glad to resolve your queries.");
                                    sb.Append("<br/><br/><br/>Warmest Regards,<br/>&nbsp;Team AstaGuru");

                                    MailMessage mm = new MailMessage("contact@astaguru.com", objauc.email);


                                    //mm.CC.Add("admin@astaguru.com");
                                    mm.Subject = "Detailed Pro-Forma Invoice for Lot No " + objauc.reference + " from our '" + objauc.Auctionname + "' Auction, " + objauc.auctiondate;
                                    mm.Body = sb.ToString();
                                    mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "Performainvoice.pdf"));
                                    mm.IsBodyHtml = true;
                                    SmtpClient smtp = new SmtpClient();
                                    smtp.Host = ConfigurationManager.AppSettings["smtpHost"].ToString();
                                    smtp.EnableSsl = true;
                                    NetworkCredential NetworkCred = new NetworkCredential();
                                    NetworkCred.UserName = ConfigurationManager.AppSettings["smtpUser"].ToString();
                                    NetworkCred.Password = ConfigurationManager.AppSettings["smtpPassword"].ToString();
                                    smtp.UseDefaultCredentials = true;
                                    smtp.Credentials = NetworkCred;
                                    smtp.Port = 587;
                                    smtp.Send(mm);



                                }
                            }
                        }
                    }
                }
                Invoice_No++;
            }
            return Json("Email Sent successfully!");

        }
        #endregion


        #region xxxxxxxxxxxxxxxx --- Get Top Proxy Bid Paintings --- xxxxxxxxxxxxxxxxxxxxxx
        public ActionResult GetProxyBidPaintings()
        {
            Auction objauc = new Auction();
            objauc.GridList = GetProxyBidPaintingList();
            return View(objauc);
        }

        public List<Auction> GetProxyBidPaintingList()
        {
            try
            {
                List<Auction> objList = new List<Auction>();
                DataTable dt = new DataTable();
                dt = util.Display("Exec Proc_ProxyPainting 'GetTopProxyPainting' ");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Auction objauc = new Auction();
                        //objauc.Sr = dr["Sr"].ToString();
                        objauc.productid = int.Parse(dr["Productid"].ToString());
                        objauc.thumbnail = dr["thumbnail"].ToString();
                        objauc.productid = int.Parse(dr["Productid"].ToString());
                        objauc.pricers = int.Parse(dr["pricers"].ToString());
                        objauc.priceus = int.Parse(dr["priceus"].ToString());
                        objauc.ProxyAmt = int.Parse(dr["ProxyAmt"].ToString());
                        objauc.ProxyAmtus = int.Parse(dr["ProxyAmtus"].ToString());
                        objauc.artist = dr["ArtistName"].ToString();
                        objauc.reference = dr["reference"].ToString();
                        objauc.username = dr["Username"].ToString();
                        objauc.userid = int.Parse(dr["userid"].ToString());

                        objList.Add(objauc);
                    }
                }
                return objList;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxx -- Add video ----- xxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult Addvideo()
        {
            Video obj = new Video();
            obj.CategoryList = BindcategoryforVideo(obj);
            obj.GridList = GetvideoList();
            return View(obj);

        }


        public List<SelectListItem> BindcategoryforVideo(Video obj)
        {
            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_video 'Getcategoryforvideo'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["category"].ToString();
                    list.Value = dr["categoryid"].ToString();

                    if (obj.categoryid == int.Parse(dr["categoryid"].ToString()))
                    {
                        list.Selected = true;
                    }

                    objList.Add(list);
                }

            }
            return objList.ToList();
        }

        public List<Video> GetvideoList()
        {
            try
            {
                List<Video> objlist = new List<Video>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_video 'Get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Video obj = new Video();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Title = dr["Title"].ToString();
                        obj.VideoUrl = dr["VideoUrl"].ToString();
                        obj.PostDate = dr["PostDate"].ToString();
                        obj.category = dr["Category"].ToString();
                        obj.categoryid = int.Parse(dr["Categoryid"].ToString());
                        //obj.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());

                        objlist.Add(obj);
                    }
                }
                return objlist;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }



        [HttpPost]
        public ActionResult Addvideo(Video objvideo)
        {
            try
            {

                if (ModelState.IsValid)
                {

                    if (objvideo.Id == 0)
                    {
                        if (Savevideo(objvideo))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Addvideo");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Addvideo");
                        }
                    }
                    else
                    {
                        if (Updatevideo(objvideo))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Addvideo");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Addvideo");
                        }
                    }
                }
                return View("Addvideo", objvideo);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("Addvideo");
            }

        }


        public bool Savevideo(Video obj)
        {
            bool response;
            string postDate = DateTime.ParseExact(obj.PostDate, "dd-MM-yyyy", null).ToString("yyyy/MM/dd");
            using (SqlCommand cmd = new SqlCommand("Proc_video"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Add");
                cmd.Parameters.AddWithValue("@Title", obj.Title);
                cmd.Parameters.AddWithValue("@VideoUrl", obj.VideoUrl);
                cmd.Parameters.AddWithValue("@PostDate", postDate);
                cmd.Parameters.AddWithValue("@Categoryid", obj.categoryid);

                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool Updatevideo(Video obj)
        {
            bool response;
            string postDate = DateTime.ParseExact(obj.PostDate, "dd-MM-yyyy", null).ToString("yyyy/MM/dd");
            using (SqlCommand cmd = new SqlCommand("Proc_video"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Update");
                cmd.Parameters.AddWithValue("@Id", obj.Id);
                cmd.Parameters.AddWithValue("@Title", obj.Title);
                cmd.Parameters.AddWithValue("@VideoUrl", obj.VideoUrl);
                cmd.Parameters.AddWithValue("@PostDate", postDate);
                cmd.Parameters.AddWithValue("@Categoryid", obj.categoryid);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult Editvideo(int? Id)
        {
            Video obj = new Video();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_video 'GetById','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                obj.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                obj.Title = dt.Rows[0]["Title"].ToString();
                obj.VideoUrl = dt.Rows[0]["VideoUrl"].ToString();
                obj.PostDate = dt.Rows[0]["PostDate"].ToString();
                obj.categoryid = int.Parse(dt.Rows[0]["Categoryid"].ToString());


                obj.CategoryList = BindcategoryforVideo(obj);
                obj.GridList = GetvideoList();

                ViewBag.ActionType = "Update";
                return View("Addvideo", obj);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("Addvideo", obj);
            }
        }

        public ActionResult Deletevideo(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_video"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "Delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Addvideo");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Addvideo");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("Addvideo");
                //throw;
            }

        }







        #endregion

        #region xxxxxxxxxxxx - Manage Blog ---- xxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult Blog()
        {
            Blog obj = new Blog();
            obj.CategoryList = BindcategoryforBlog(obj);
            obj.Gridlist = GetBlogList();
            return View(obj);
        }

        public List<Blog> GetBlogList()
        {
            try
            {
                List<Blog> objBlogList = new List<Blog>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_Blog 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Blog obj = new Blog();
                        obj.Sr = dr["Sr"].ToString();
                        obj.BlogId = int.Parse(dr["BlogId"].ToString());
                        obj.Title = dr["Title"].ToString();
                        obj.Image = dr["Image"].ToString();
                        obj.PostDate = dr["PostDate"].ToString();
                        obj.Description = dr["Description"].ToString();
                        obj.imgPreview = dr["Image"].ToString();
                        obj.category = dr["Category"].ToString();
                        obj.CreatedBy = dr["CreatedBy"].ToString();
                        obj.categoryid = int.Parse(dr["Categoryid"].ToString());



                        objBlogList.Add(obj);
                    }
                }
                return objBlogList;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }


        public List<SelectListItem> BindcategoryforBlog(Blog obj)
        {
            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_Blog 'Getcategoryforblog'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["category"].ToString();
                    list.Value = dr["categoryid"].ToString();

                    if (obj.categoryid == int.Parse(dr["categoryid"].ToString()))
                    {
                        list.Selected = true;
                    }

                    objList.Add(list);
                }

            }
            return objList.ToList();
        }



        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Blog(Blog objBlog, HttpPostedFileBase[] Image)
        {
            try
            {
                if (objBlog.BlogId != 0)
                {
                    ModelState.Remove("Image");
                }

                var errors = ModelState.Values.SelectMany(v => v.Errors);
                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/Blog/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("Blog");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "Blog-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objBlog.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objBlog.BlogId == 0)
                    {
                        if (SaveBlog(objBlog))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Blog");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Blog");
                        }
                    }
                    else
                    {
                        if (UpdateBlog(objBlog))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Blog");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Blog");
                        }
                    }
                }
                return View("Blog", objBlog);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("Blog");
            }

        }

        public bool SaveBlog(Blog objBlog)
        {
            bool response;
            string postDate = DateTime.ParseExact(objBlog.PostDate, "dd-MM-yyyy", null).ToString("yyyy/MM/dd");

            using (SqlCommand cmd = new SqlCommand("Proc_Blog"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Title", objBlog.Title);
                cmd.Parameters.AddWithValue("@Image", objBlog.Image);
                cmd.Parameters.AddWithValue("@PostDate", postDate);
                cmd.Parameters.AddWithValue("@Description", objBlog.Description);
                cmd.Parameters.AddWithValue("@Categoryid", objBlog.categoryid);
                cmd.Parameters.AddWithValue("@Slug", Generateslg(objBlog.Title));
                cmd.Parameters.AddWithValue("@CreatedBy", objBlog.CreatedBy);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateBlog(Blog objBlog)
        {
            bool response;
            string postDate = DateTime.ParseExact(objBlog.PostDate, "dd-MM-yyyy", null).ToString("yyyy/MM/dd");

            using (SqlCommand cmd = new SqlCommand("Proc_Blog"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@BlogId", objBlog.BlogId);
                cmd.Parameters.AddWithValue("@Title", objBlog.Title);
                cmd.Parameters.AddWithValue("@Image", objBlog.Image);
                cmd.Parameters.AddWithValue("@PostDate", postDate);
                cmd.Parameters.AddWithValue("@Description", objBlog.Description);
                cmd.Parameters.AddWithValue("@Categoryid", objBlog.categoryid);
                cmd.Parameters.AddWithValue("@Slug", Generateslg(objBlog.Title));
                cmd.Parameters.AddWithValue("@CreatedBy", objBlog.CreatedBy);

                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        public ActionResult EditBlog(int? Id)
        {
            Blog objBlog = new Blog();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Blog 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objBlog.BlogId = Convert.ToInt32(dt.Rows[0]["BlogId"].ToString());
                objBlog.Title = dt.Rows[0]["Title"].ToString();
                objBlog.Image = dt.Rows[0]["Image"].ToString();
                objBlog.imgPreview = dt.Rows[0]["Image"].ToString();
                objBlog.PostDate = dt.Rows[0]["PostDate"].ToString();
                objBlog.Description = dt.Rows[0]["Description"].ToString();
                objBlog.categoryid = int.Parse(dt.Rows[0]["Categoryid"].ToString());
                objBlog.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();

                objBlog.Gridlist = GetBlogList();
                objBlog.CategoryList = BindcategoryforBlog(objBlog);


                ViewBag.ActionType = "Update";
                return View("Blog", objBlog);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("Blog", objBlog);
            }
        }

        public ActionResult DeleteBlog(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_Blog"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@BlogId", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Blog");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Blog");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("Blog");
                //throw;
            }

        }


        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -- Page CMS Master --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult CMSPage(int Id)
        {
            CMSPage objCMSPage = new CMSPage();
            objCMSPage.PageId = Id;
            objCMSPage.CMSPageList = GetCMSPageList(Id);
            return View(objCMSPage);
        }

        public List<CMSPage> GetCMSPageList(int? Id)
        {
            try
            {
                List<CMSPage> objCMSPage = new List<CMSPage>();
                DataSet ds = new DataSet();
                ds = util.Display1("Execute Proc_PageCMSMaster 'get',0,'" + Id + "'");
                if (ds.Tables.Count > 0)
                {
                    ViewBag.PageName = ds.Tables[1].Rows[0]["PageName"].ToString();
                    ViewBag.PageId = ds.Tables[1].Rows[0]["PageId"].ToString();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CMSPage cmsPage = new CMSPage();
                        cmsPage.Sr = dr["Sr"].ToString();
                        cmsPage.CMSId = int.Parse(dr["CmsId"].ToString());
                        cmsPage.PageId = int.Parse(dr["PageId"].ToString());
                        cmsPage.Title = dr["Title"].ToString();
                        cmsPage.Description = dr["Description"].ToString();
                        cmsPage.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());

                        objCMSPage.Add(cmsPage);
                    }
                }
                return objCMSPage;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CMSPage(CMSPage objCMSPage)
        {
            int Id = objCMSPage.PageId;
            try
            {
                if (ModelState.IsValid)
                {
                    if (objCMSPage.CMSId == 0)
                    {
                        if (SaveCMSPage(objCMSPage))
                        {
                            ViewBag.PageId = objCMSPage.PageId;
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CMSPage", new { Id });
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CMSPage", new { Id });
                        }
                    }
                    else
                    {
                        if (UpdateCMSPage(objCMSPage))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CMSPage", new { Id });
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CMSPage", new { Id });
                        }
                    }
                }
                return View("CMSPage", new { Id });
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                //return RedirectToAction("CMSPage", new { Id });
                return View("CMSPage", new { Id });
            }

        }

        public bool SaveCMSPage(CMSPage objCMSPage)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_PageCMSMaster"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@PageId", objCMSPage.PageId);
                cmd.Parameters.AddWithValue("@Title", objCMSPage.Title);
                cmd.Parameters.AddWithValue("@Description", objCMSPage.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objCMSPage.DisplayOrder);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateCMSPage(CMSPage objCMSPage)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_PageCMSMaster"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@CmsId", objCMSPage.CMSId);
                cmd.Parameters.AddWithValue("@PageId", objCMSPage.PageId);
                cmd.Parameters.AddWithValue("@Title", objCMSPage.Title);
                cmd.Parameters.AddWithValue("@Description", objCMSPage.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objCMSPage.DisplayOrder);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditCMSPage(int CMSId)
        {
            CMSPage objCMSPage = new CMSPage();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_PageCMSMaster 'getbyId','" + CMSId + "'");
            if (dt.Rows.Count > 0)
            {
                objCMSPage.CMSId = int.Parse(dt.Rows[0]["CmsId"].ToString());
                objCMSPage.PageId = int.Parse(dt.Rows[0]["PageId"].ToString());
                objCMSPage.Title = dt.Rows[0]["Title"].ToString();
                objCMSPage.Description = dt.Rows[0]["Description"].ToString();
                objCMSPage.DisplayOrder = int.Parse(dt.Rows[0]["DisplayOrder"].ToString());

                objCMSPage.CMSPageList = GetCMSPageList(objCMSPage.PageId);

                ViewBag.ActionType = "Update";
                return View("CMSPage", objCMSPage);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("CMSPage", objCMSPage);
            }
        }

        public ActionResult DeleteCMSPage(int? Id, int CMSId)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_PageCMSMaster"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@PageId", Id);
                    cmd.Parameters.AddWithValue("@CmsId", CMSId);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CMSPage", new { Id });
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CMSPage", new { Id });
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("CMSPage");
                //throw;
            }

        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -- CMS --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult CMS(int Id)
        {
            CMS cms = new CMS();
            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_CMS 'get','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                cms.CmsId = int.Parse(dt.Rows[0]["CmsId"].ToString());
                cms.Heading = dt.Rows[0]["Heading"].ToString();
                cms.Description = dt.Rows[0]["Description"].ToString();

                ViewBag.ActionType = "Update";
                return View("CMS", cms);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("CMS", cms);
            }
            //cms.CmsId = Id;
            //ViewBag.ActionType = "Update";
            //return View(objCMS);
        }

        public List<CMS> GetCMSList(int? Id)
        {
            try
            {
                List<CMS> objCMS = new List<CMS>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_CMS 'get','" + Id + "'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        CMS cms = new CMS();
                        cms.CmsId = int.Parse(dr["CmsId"].ToString());
                        cms.Heading = dr["Heading"].ToString();
                        cms.Description = dr["Description"].ToString();

                        objCMS.Add(cms);
                    }
                }
                return objCMS;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CMS(CMS objCMS)
        {
            int Id = objCMS.CmsId;
            try
            {
                if (ModelState.IsValid)
                {
                    if (UpdateCMS(objCMS))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record updated successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CMS", new { Id });
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not updated successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CMS", new { Id });
                    }
                }
                return View("CMS", new { Id });
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                //return RedirectToAction("CMSPage", new { Id });
                return View("CMS", new { Id });
            }

        }

        public bool UpdateCMS(CMS objCMS)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_CMS"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@CmsId", objCMS.CmsId);
                cmd.Parameters.AddWithValue("@Heading", objCMS.Heading);
                cmd.Parameters.AddWithValue("@Description", objCMS.Description);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxx -- Add MetaTag -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult MetaTag()
        {
            MetaTag objMetaTag = new MetaTag();
            objMetaTag.MetaTagList = GetMetaTagList();
            return View(objMetaTag);

        }
        public List<MetaTag> GetMetaTagList()
        {
            try
            {
                List<MetaTag> objMetaTag = new List<MetaTag>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_MetaTag 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        MetaTag obj = new MetaTag();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Title = dr["Title"].ToString();
                        obj.PageUrl = dr["PageUrl"].ToString();
                        obj.MetaKeyword = dr["MetaKeywords"].ToString();
                        obj.MetaDescription = dr["MetaDescription"].ToString();

                        objMetaTag.Add(obj);
                    }
                }
                return objMetaTag;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MetaTag(MetaTag objMetaTag)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    if (objMetaTag.Id == 0)
                    {
                        if (SaveMetaTag(objMetaTag))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("MetaTag");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("MetaTag");
                        }
                    }
                    else
                    {
                        if (UpdateMetaTag(objMetaTag))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("MetaTag");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("MetaTag");
                        }
                    }
                }
                return View("MetaTag", objMetaTag);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("MetaTag");
            }

        }

        public bool SaveMetaTag(MetaTag objMetaTag)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_MetaTag"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@PageUrl", objMetaTag.PageUrl);
                cmd.Parameters.AddWithValue("@Title", objMetaTag.Title);
                cmd.Parameters.AddWithValue("@MetaKeywords", objMetaTag.MetaKeyword);
                cmd.Parameters.AddWithValue("@MetaDescription", objMetaTag.MetaDescription);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateMetaTag(MetaTag objMetaTag)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_MetaTag"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objMetaTag.Id);
                cmd.Parameters.AddWithValue("@PageUrl", objMetaTag.PageUrl);
                cmd.Parameters.AddWithValue("@Title", objMetaTag.Title);
                cmd.Parameters.AddWithValue("@MetaKeywords", objMetaTag.MetaKeyword);
                cmd.Parameters.AddWithValue("@MetaDescription", objMetaTag.MetaDescription);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditMetaTag(int? Id)
        {
            MetaTag objMetaTag = new MetaTag();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_MetaTag 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objMetaTag.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objMetaTag.PageUrl = dt.Rows[0]["PageUrl"].ToString();
                objMetaTag.Title = dt.Rows[0]["Title"].ToString();
                objMetaTag.MetaKeyword = dt.Rows[0]["MetaKeywords"].ToString();
                objMetaTag.MetaDescription = dt.Rows[0]["MetaDescription"].ToString();


                objMetaTag.MetaTagList = GetMetaTagList();

                ViewBag.ActionType = "Update";
                return View("MetaTag", objMetaTag);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("MetaTag", objMetaTag);
            }
        }

        public ActionResult DeleteMetaTag(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_MetaTag"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("MetaTag");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("MetaTag");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("MetaTag");
                //throw;
            }

        }

        #endregion


        #region xxxxxxxxxxxxxxxxxxx --- Manage Media News --- xxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult MediaNews()
        {
            MediaNews objNewsRoom = new MediaNews();
            objNewsRoom.GridList = GetMediaNewsList();
            return View(objNewsRoom);

        }

        public List<MediaNews> GetMediaNewsList()
        {
            try
            {
                List<MediaNews> objNewsRoom = new List<MediaNews>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_MediaNews 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        MediaNews obj = new MediaNews();
                        obj.Sr = dr["Sr"].ToString();
                        obj.NewsId = int.Parse(dr["NewsId"].ToString());
                        obj.Image = dr["Image"].ToString();
                        obj.Title = dr["Title"].ToString();
                        obj.Description = dr["Description"].ToString();
                        obj.Type = dr["Type"].ToString();
                        obj.Date = dr["Date"].ToString();
                        obj.Slug = dr["Slug"].ToString();
                        obj.CreatedBy = dr["CreatedBy"].ToString();
                        obj.VideoUrl = dr["VideoUrl"].ToString();

                        objNewsRoom.Add(obj);
                    }
                }
                return objNewsRoom;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MediaNews(MediaNews objNewsRoom, HttpPostedFileBase[] Image)
        {
            try
            {

                if (objNewsRoom.NewsId == 0)
                {
                    if (objNewsRoom.Type == "Url")
                    {
                        ModelState.Remove("Image");
                    }
                    else if (objNewsRoom.Type == "Image")
                    {
                        ModelState.Remove("VideoUrl");
                    }
                }
                else
                {
                    ModelState.Remove("VideoUrl");
                    ModelState.Remove("Image");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/MediaNews/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("MediaNews");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "MediaNews-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objNewsRoom.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objNewsRoom.NewsId == 0)
                    {
                        if (SaveMediaNews(objNewsRoom))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("MediaNews");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("MediaNews");
                        }
                    }
                    else
                    {
                        if (UpdateMediaNews(objNewsRoom))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("MediaNews");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("MediaNews");
                        }
                    }
                }
                return View("MediaNews", objNewsRoom);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("NewsRoom");
            }

        }

        public bool SaveMediaNews(MediaNews objNewsRoom)
        {
            bool response;
            string postDate = DateTime.ParseExact(objNewsRoom.Date, "dd-MM-yyyy", null).ToString("yyyy/MM/dd");
            using (SqlCommand cmd = new SqlCommand("Proc_MediaNews"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Type", objNewsRoom.Type);
                cmd.Parameters.AddWithValue("@Title", objNewsRoom.Title);
                cmd.Parameters.AddWithValue("@Image", objNewsRoom.Image);
                cmd.Parameters.AddWithValue("@Date", postDate);
                cmd.Parameters.AddWithValue("@Description", objNewsRoom.Description);
                cmd.Parameters.AddWithValue("@CreatedBy", objNewsRoom.CreatedBy);
                cmd.Parameters.AddWithValue("@Slug", Generateslg(objNewsRoom.Title));
                cmd.Parameters.AddWithValue("@VideoUrl", objNewsRoom.VideoUrl);

                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateMediaNews(MediaNews objNewsRoom)
        {
            string postDate = DateTime.ParseExact(objNewsRoom.Date, "dd-MM-yyyy", null).ToString("yyyy/MM/dd");
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_MediaNews"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@NewsId", objNewsRoom.NewsId);
                cmd.Parameters.AddWithValue("@Type", objNewsRoom.Type);
                cmd.Parameters.AddWithValue("@Title", objNewsRoom.Title);
                cmd.Parameters.AddWithValue("@Image", objNewsRoom.Image);
                cmd.Parameters.AddWithValue("@Date", postDate);
                cmd.Parameters.AddWithValue("@Description", objNewsRoom.Description);
                cmd.Parameters.AddWithValue("@CreatedBy", objNewsRoom.CreatedBy);
                cmd.Parameters.AddWithValue("@Slug", Generateslg(objNewsRoom.Title));
                cmd.Parameters.AddWithValue("@VideoUrl", objNewsRoom.VideoUrl);

                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditMediaNews(int? NewsId)
        {
            MediaNews objNewsRoom = new MediaNews();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_MediaNews 'getbyId','" + NewsId + "'");
            if (dt.Rows.Count > 0)
            {
                objNewsRoom.NewsId = Convert.ToInt32(dt.Rows[0]["NewsId"].ToString());
                objNewsRoom.Image = dt.Rows[0]["Image"].ToString();
                objNewsRoom.imgPreview = dt.Rows[0]["Image"].ToString();
                objNewsRoom.Title = dt.Rows[0]["Title"].ToString();
                objNewsRoom.Type = dt.Rows[0]["Type"].ToString();
                objNewsRoom.Description = dt.Rows[0]["Description"].ToString();
                objNewsRoom.Date = dt.Rows[0]["Date"].ToString();
                objNewsRoom.Slug = dt.Rows[0]["Slug"].ToString();
                objNewsRoom.CreatedBy = dt.Rows[0]["CreatedBy"].ToString();
                objNewsRoom.VideoUrl = dt.Rows[0]["VideoUrl"].ToString();
                objNewsRoom.GridList = GetMediaNewsList();

                ViewBag.ActionType = "Update";
                return View("MediaNews", objNewsRoom);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("MediaNews", objNewsRoom);
            }
        }

        public ActionResult DeleteMediaNews(int? NewsId)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_MediaNews"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@NewsId", NewsId);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("MediaNews");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("MediaNews");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("MediaNews");
                //throw;
            }

        }


        #endregion

        #region xxxxxxxxxxxxxxxxxx --Faq-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult faq()
        {
            Faq obj = new Faq();
            obj.GridList = GetFaqList();
            return View(obj);
        }


        public List<Faq> GetFaqList()
        {
            try
            {
                List<Faq> objFaq = new List<Faq>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_Faq 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Faq obj = new Faq();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Title = dr["Title"].ToString();
                        obj.Category = dr["Category"].ToString();
                        obj.Description = dr["Description"].ToString();


                        objFaq.Add(obj);
                    }
                }
                return objFaq;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Faq(Faq objFaq)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    if (objFaq.Id == 0)
                    {
                        if (SaveFaq(objFaq))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Faq");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Faq");
                        }
                    }
                    else
                    {
                        if (UpdateFaq(objFaq))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Faq");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Faq");
                        }
                    }
                }
                return View("Faq", objFaq);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("Faq");
            }

        }

        public bool SaveFaq(Faq objFaq)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Faq"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Category", objFaq.Category);
                cmd.Parameters.AddWithValue("@Title", objFaq.Title);
                cmd.Parameters.AddWithValue("@Description", objFaq.Description);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateFaq(Faq objFaq)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Faq"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objFaq.Id);
                cmd.Parameters.AddWithValue("@Category", objFaq.Category);
                cmd.Parameters.AddWithValue("@Title", objFaq.Title);
                cmd.Parameters.AddWithValue("@Description", objFaq.Description);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditFaq(int? Id)
        {
            Faq objFaq = new Faq();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Faq 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objFaq.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objFaq.Title = dt.Rows[0]["Title"].ToString();
                objFaq.Category = dt.Rows[0]["Category"].ToString();
                objFaq.Description = dt.Rows[0]["Description"].ToString();


                objFaq.GridList = GetFaqList();

                ViewBag.ActionType = "Update";
                return View("Faq", objFaq);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("Faq", objFaq);
            }
        }

        public ActionResult DeleteFaq(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_Faq"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Faq");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Faq");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("Faq");
                //throw;
            }

        }


        #endregion

        #region xxxxxxxxxxxxxx-- Career Current Vacancy -- xxxxxxxxxxxxxxxx
        public ActionResult CurrentVacancy()
        {
            CurrentVacancy obj = new CurrentVacancy();
            obj.GridList = GetCurrentVacancyList();
            return View(obj);
        }

        public List<CurrentVacancy> GetCurrentVacancyList()
        {
            try
            {
                List<CurrentVacancy> objCurrentVacancy = new List<CurrentVacancy>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_Current_Vacancy 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        CurrentVacancy obj = new CurrentVacancy();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Job_Title = dr["Job_Title"].ToString();
                        obj.Business_Unit = dr["Business_Unit"].ToString();
                        obj.Job_Responsibility = dr["Job_Responsibility"].ToString();

                        obj.Responsibilities = dr["Responsibilities"].ToString();
                        obj.Functional_Skills = dr["Functional_Skills"].ToString();
                        obj.Technical_skills = dr["Technical_skills"].ToString();
                        obj.Qualification = dr["Qualification"].ToString();
                        obj.Salary = dr["Salary"].ToString();
                        obj.Experience = dr["Experience"].ToString();


                        objCurrentVacancy.Add(obj);
                    }
                }
                return objCurrentVacancy;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CurrentVacancy(CurrentVacancy objCurrentVacancy)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    if (objCurrentVacancy.Id == 0)
                    {
                        if (SaveCurrentVacancy(objCurrentVacancy))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CurrentVacancy");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CurrentVacancy");
                        }
                    }
                    else
                    {
                        if (UpdateCurrentVacancy(objCurrentVacancy))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CurrentVacancy");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CurrentVacancy");
                        }
                    }
                }
                return View("CurrentVacancy", objCurrentVacancy);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("CurrentVacancy");
            }

        }

        public bool SaveCurrentVacancy(CurrentVacancy objCurrentVacancy)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Current_Vacancy"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Job_Title", objCurrentVacancy.Job_Title);
                cmd.Parameters.AddWithValue("@Business_Unit", objCurrentVacancy.Business_Unit);
                cmd.Parameters.AddWithValue("@Job_Responsibility", objCurrentVacancy.Job_Responsibility);
                cmd.Parameters.AddWithValue("@Responsibilities", objCurrentVacancy.Responsibilities);
                cmd.Parameters.AddWithValue("@Functional_Skills", objCurrentVacancy.Functional_Skills);
                cmd.Parameters.AddWithValue("@Technical_skills", objCurrentVacancy.Technical_skills);
                cmd.Parameters.AddWithValue("@Qualification", objCurrentVacancy.Qualification);
                cmd.Parameters.AddWithValue("@Salary", objCurrentVacancy.Salary);
                cmd.Parameters.AddWithValue("@Experience", objCurrentVacancy.Experience);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateCurrentVacancy(CurrentVacancy objCurrentVacancy)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Current_Vacancy"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objCurrentVacancy.Id);
                cmd.Parameters.AddWithValue("@Job_Title", objCurrentVacancy.Job_Title);
                cmd.Parameters.AddWithValue("@Business_Unit", objCurrentVacancy.Business_Unit);
                cmd.Parameters.AddWithValue("@Job_Responsibility", objCurrentVacancy.Job_Responsibility);
                cmd.Parameters.AddWithValue("@Responsibilities", objCurrentVacancy.Responsibilities);
                cmd.Parameters.AddWithValue("@Functional_Skills", objCurrentVacancy.Functional_Skills);
                cmd.Parameters.AddWithValue("@Technical_skills", objCurrentVacancy.Technical_skills);
                cmd.Parameters.AddWithValue("@Qualification", objCurrentVacancy.Qualification);
                cmd.Parameters.AddWithValue("@Salary", objCurrentVacancy.Salary);
                cmd.Parameters.AddWithValue("@Experience", objCurrentVacancy.Experience);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditCurrentVacancy(int? Id)
        {
            CurrentVacancy objCurrentVacancy = new CurrentVacancy();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Current_Vacancy 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objCurrentVacancy.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objCurrentVacancy.Job_Title = dt.Rows[0]["Job_Title"].ToString();
                objCurrentVacancy.Business_Unit = dt.Rows[0]["Business_Unit"].ToString();
                objCurrentVacancy.Job_Responsibility = dt.Rows[0]["Job_Responsibility"].ToString();
                objCurrentVacancy.Responsibilities = dt.Rows[0]["Responsibilities"].ToString();
                objCurrentVacancy.Functional_Skills = dt.Rows[0]["Functional_Skills"].ToString();
                objCurrentVacancy.Technical_skills = dt.Rows[0]["Technical_skills"].ToString();
                objCurrentVacancy.Qualification = dt.Rows[0]["Qualification"].ToString();
                objCurrentVacancy.Salary = dt.Rows[0]["Salary"].ToString();
                objCurrentVacancy.Experience = dt.Rows[0]["Experience"].ToString();


                objCurrentVacancy.GridList = GetCurrentVacancyList();

                ViewBag.ActionType = "Update";
                return View("CurrentVacancy", objCurrentVacancy);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("CurrentVacancy", objCurrentVacancy);
            }
        }

        public ActionResult DeleteCurrentVacancy(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_Current_Vacancy"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CurrentVacancy");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CurrentVacancy");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("CurrentVacancy");
                //throw;
            }

        }

        #endregion



        #region xxxxxxxxxxxxxx  -----firebasenotification ---- xxxxxxxxxxxxxxxxxxxxx 
        public ActionResult Notification()
        {
            Notification obj = new Notification();
            obj.GridList = BindNotificationlist();
            return View(obj);
        }

        public List<Notification> BindNotificationlist()
        {
            try
            {
                List<Notification> objNotificationlist = new List<Notification>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_PushNotification 'getusernotificationuser'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Notification obj = new Notification();

                        obj.Sr = int.Parse(dr["Sr"].ToString());
                        obj.userid = int.Parse(dr["userid"].ToString());
                        obj.device_id = dr["device_id"].ToString();
                        obj.username = dr["username"].ToString();



                        objNotificationlist.Add(obj);
                    }
                }
                return objNotificationlist;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        public ActionResult SendNotification(string[] userIds)
        {
            string desc = userIds[userIds.Length - 1];                //get description 
            string subject = userIds[userIds.Length - 2];               // get title

            string shortdescription;
            if (desc.Length > 50)
            {
                shortdescription = desc.Substring(0, 50) + "...";
            }
            else
            {
                shortdescription = desc + "...";
            }


            userIds = userIds.Take(userIds.Count() - 1).ToArray();              //remove description 
            userIds = userIds.Take(userIds.Count() - 1).ToArray();              //remove title
            if (userIds.Length > 0 && subject != "" && desc != "")
            {
                foreach (string userId in userIds)
                {

                    DataTable dt = new DataTable();
                    dt = util.Display("Exec Proc_PushNotification 'Getuserdevicebyid'," + userId + "");
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            Notification obj = new Notification();
                            obj.device_id = dr["device_id"].ToString();
                            obj.userid = int.Parse(dr["userId"].ToString());

                            using (SqlCommand cmd = new SqlCommand("Proc_PushNotification"))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@Para", "Add");
                                cmd.Parameters.AddWithValue("@userid", obj.userid);
                                cmd.Parameters.AddWithValue("@device_id", obj.device_id);
                                cmd.Parameters.AddWithValue("@subject", subject);
                                cmd.Parameters.AddWithValue("@description", desc);

                                if (util.Execute(cmd))
                                {
                                    try
                                    {
                                        var applicationID = "AAAAa-wlbPw:APA91bEBK31iJ8EdA6cElzIxaPfyTgfS77poQUuIkc1qbJezwifX1Q6hnLjED6IycmhyM5eZGwPdh-XJCvUSX8-UcOCLfd-rB_oThZhQ8_sZqOOTOOqSsGsdMjCGFA5-DgxGvUGlHc9j";

                                        //var senderId = "57-------55";

                                        string deviceId = obj.device_id;

                                        WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                                        tRequest.Method = "post";

                                        tRequest.ContentType = "application/json";

                                        var data = new

                                        {

                                            to = deviceId,

                                            notification = new

                                            {

                                                body = desc,

                                                title = subject,

                                                shortdesc = shortdescription
                                                //icon = "myicon"

                                            }
                                        };

                                        var serializer = new JavaScriptSerializer();

                                        var json = serializer.Serialize(data);

                                        Byte[] byteArray = Encoding.UTF8.GetBytes(json);


                                        tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                                        //tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                                        tRequest.ContentLength = byteArray.Length;


                                        using (Stream dataStream = tRequest.GetRequestStream())
                                        {

                                            dataStream.Write(byteArray, 0, byteArray.Length);


                                            using (WebResponse tResponse = tRequest.GetResponse())
                                            {

                                                using (Stream dataStreamResponse = tResponse.GetResponseStream())
                                                {

                                                    using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                                    {

                                                        String sResponseFromServer = tReader.ReadToEnd();

                                                        string str = sResponseFromServer;

                                                    }
                                                }
                                            }
                                        }
                                    }

                                    catch (Exception ex)
                                    {

                                        string str = ex.Message;

                                    }
                                }
                                else
                                {

                                }
                            }


                        }
                    }


                }

            }




            return Json("Notification sent successfully!");


        }


        public ActionResult GetNotificationListByUserId(int userid)
        {
            Notification objnotify = new Notification();
            List<Notification> objNotificationlist = new List<Notification>();
            try
            {

                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_PushNotification 'GetuserAllnotificationbyId'," + userid + "");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Notification obj = new Notification();

                        obj.Sr = int.Parse(dr["Sr"].ToString());
                        obj.userid = int.Parse(dr["userid"].ToString());
                        obj.device_id = dr["device_id"].ToString();
                        obj.username = dr["username"].ToString();
                        obj.subject = dr["subject"].ToString();
                        obj.description = dr["description"].ToString();




                        objNotificationlist.Add(obj);
                    }
                }

            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }

            objnotify.GridList = objNotificationlist;

            return View(objnotify);
        }



        #endregion

        #region  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --- Start Auction --- xxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult StartAuction()
        {
            return View();
        }

        public Auction StartAuctionnew()
        {
            Auction bidrecordfinal = new Auction();
            CurrentAuctionService CAS = new CurrentAuctionService();
            int onlineid = int.Parse(ConfigurationManager.AppSettings["CurrentAuction"].ToString());
            //int onlineid = 54;
            //List<Auction> proxyinfo = new List<Auction>();
            //List<Auction> acutiondata = new List<Auction>();
            List<Auction> listAuc = new List<Auction>();
            List<Auction> listProxyAuc = new List<Auction>();
            Auction Auc = new Auction();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_startAuction 'Getdistinctproductid'," + onlineid + "");

            int curprice = 0;
            int pricers = 0;
            if (dt.Rows.Count > 0)
            {
                //int intproductid = 0;
                //int pricers = 0;
                //int pricelow = 0;

                foreach (DataRow proxyitem in dt.Rows)
                {
                    Auc.Online = onlineid;
                    Auc.productid = int.Parse(proxyitem["productid"].ToString());
                    listAuc = GetAuctiondata(Auc);
                    listProxyAuc = GetProxyInfo(Auc);

                    int currentuser = 0;

                    Auc.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(listAuc[0].pricers, 1).ToString());
                    Auc.nextValidBidUs = Auc.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);


                    while (listProxyAuc.Count > 0)
                    {
                        if (listProxyAuc.Count == 1 && currentuser == listProxyAuc[0].userid)
                        {
                            Auc.recentbid = 1;
                            Auc.currentbid = 1;
                            Auc.userid = listProxyAuc[0].userid;
                            CAS.UpdateRecentCurrentBid(Auc);
                            //return Auc;
                            break;

                        }

                        foreach (var proxy in listProxyAuc)
                        {
                            // Added to skip record if same user come again
                            if (currentuser == proxy.userid)
                            {
                                continue;
                            }


                            // Need To Check This
                            //if (!userlist.Contains(proxy.userid))
                            //{
                            //    userlist.Add(proxy.userid);
                            //    aucBidRecord = CAS.getBidUserList(AUC);

                            //    foreach (var userBid in aucBidRecord)
                            //    {
                            //        userBid.currentbid = 0;
                            //    }
                            //}

                            pricers = proxy.ProxyAmt;

                            if (Auc.nextValidBidRs == 0)

                            {
                                Auc.nextValidBidRs = proxy.pricers;
                            }


                            #region
                            if (proxy.ProxyAmt > Auc.nextValidBidRs)
                            #endregion
                            {

                                Auction bidRecord = new Auction();

                                List<Auction> listProxyBid = new List<Auction>();
                                listProxyBid = CAS.GetproxyUser(Auc);
                                if (listProxyBid.Count > 0)
                                {
                                    foreach (var proxyuser in listProxyBid)
                                    {
                                        bidRecord.firstname = proxyuser.firstname;
                                        bidRecord.lastname = proxyuser.lastname;
                                        bidRecord.thumbnail = proxyuser.thumbnail;
                                        bidRecord.productid = proxyuser.productid;
                                        bidRecord.pricers = proxyuser.pricers;

                                        //AUC.mailPreprice = AUC.pricers;
                                        //AUC.mailPrepriceUs = AUC.mailPreprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);

                                        bidRecord.nickname = proxyuser.nickname;
                                        bidRecord.priceus = bidRecord.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;

                                        if (proxyuser.ProxyAmt == Auc.nextValidBidRs)
                                        {
                                            Auc.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(Auc.nextValidBidRs, 1).ToString());
                                            bidRecord.nextValidBidRs = Auc.nextValidBidRs; // Next valid bid
                                            bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                                            curprice = bidRecord.nextValidBidRs;
                                        }
                                        else
                                        {
                                            bidRecord.nextValidBidRs = Auc.nextValidBidRs; // Next valid bid
                                            bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                                            curprice = bidRecord.nextValidBidRs;
                                        }

                                        // bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                                        // bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                                        // curprice = bidRecord.nextValidBidRs;
                                        //bidRecord.daterec = Now : Already taken in query

                                        bidRecord.reference = proxyuser.reference;
                                        bidRecord.anoname = proxyuser.nickname;
                                        bidRecord.username = proxyuser.username;
                                        bidRecord.currentbid = 0;
                                        bidRecord.recentbid = 0;
                                        bidRecord.userid = proxyuser.userid;
                                        bidRecord.proxy = 1;
                                        //bidRecord.Online = proxyuser.Auctionid;
                                        bidRecord.Online = Auc.Online;
                                        Auc.pricers = Auc.nextValidBidRs;
                                        bidRecord.nickname = proxyuser.nickname;
                                        bidRecord.isOldUser = proxyuser.isOldUser;

                                        // New 
                                        bidrecordfinal = bidRecord;

                                        Auc.Bidrecordid = CAS.InsertBidRecordForAdminStartAuctoion(bidRecord);
                                    }
                                }
                                else
                                {
                                    bidRecord.firstname = proxy.firstname;
                                    bidRecord.lastname = proxy.lastname;
                                    bidRecord.thumbnail = proxy.thumbnail;
                                    bidRecord.productid = proxy.productid;
                                    bidRecord.pricers = proxy.pricers;

                                    Auc.mailPreprice = Auc.pricers;
                                    Auc.mailPrepriceUs = Auc.mailPreprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);

                                    bidRecord.nickname = proxy.nickname;
                                    bidRecord.priceus = bidRecord.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;

                                    bidRecord.nextValidBidRs = Auc.nextValidBidRs; // Next valid bid
                                    bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                                    curprice = bidRecord.nextValidBidRs;


                                    // bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                                    // bidRecord.nextValidBidUs = bidRecord.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.nextValidBidUs;
                                    // curprice = bidRecord.nextValidBidRs;
                                    //bidRecord.daterec = Now : Already taken in query

                                    bidRecord.reference = proxy.reference;
                                    bidRecord.anoname = proxy.nickname;
                                    bidRecord.username = proxy.username;
                                    bidRecord.currentbid = 0;
                                    bidRecord.recentbid = 0;
                                    bidRecord.userid = proxy.userid;
                                    bidRecord.proxy = 1;
                                    //bidRecord.Online = proxy.Auctionid;
                                    bidRecord.Online = Auc.Online;
                                    Auc.pricers = Auc.nextValidBidRs;
                                    bidRecord.nickname = proxy.nickname;
                                    bidRecord.isOldUser = proxy.isOldUser;

                                    // New 
                                    bidrecordfinal = bidRecord;

                                    Auc.Bidrecordid = CAS.InsertBidRecordForAdminStartAuctoion(bidRecord);
                                }
                                if (Auc.Bidrecordid > 0)
                                {
                                    Auc.nextValidBidRs = bidRecord.nextValidBidRs;
                                    Auc.nextValidBidUs = Auc.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); //bidRecord.nextValidBidUs;
                                    CAS.UpdateAcutionPrice(Auc);
                                }
                                else
                                {
                                    //Failed
                                }

                                Auc.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(Auc.nextValidBidRs, 1).ToString());
                                currentuser = bidRecord.userid;

                            }
                            //created on 26-01-2020
                            #region
                            else if (proxy.ProxyAmt == Auc.nextValidBidRs)
                            {
                                Auction bidRecord = new Auction();

                                List<Auction> listProxyBid = new List<Auction>();
                                listProxyBid = CAS.Getsameproxy(Auc);
                                foreach (var proxynew in listProxyBid)
                                {
                                    bidRecord.firstname = proxynew.firstname;
                                    bidRecord.lastname = proxynew.lastname;
                                    bidRecord.thumbnail = proxynew.thumbnail;
                                    bidRecord.productid = proxynew.productid;
                                    bidRecord.pricers = proxynew.pricers;
                                    bidRecord.nickname = proxynew.nickname;
                                    bidRecord.priceus = proxynew.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                                    // AUC.priceus;
                                    //bidRecord.nextValidBidRs = AUC.nextValidBidRs; // Next valid bid
                                    //bidRecord.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                                    //curprice = bidRecord.nextValidBidRs; // bidRecord.nextValidBidRs;
                                    //bidRecord.daterec = Now : Already taken in query
                                    //created on 12_02_2020
                                    bidRecord.nextValidBidRs = Auc.nextValidBidRs; // Next valid bid
                                    bidRecord.nextValidBidUs = Auc.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                                    curprice = Auc.nextValidBidRs;


                                    bidRecord.reference = proxynew.reference;
                                    bidRecord.anoname = proxynew.nickname;
                                    bidRecord.username = proxynew.username;
                                    bidRecord.currentbid = 0;
                                    bidRecord.recentbid = 0;
                                    bidRecord.userid = proxynew.userid;
                                    bidRecord.proxy = 1;
                                    //bidRecord.Online = Auc.Auctionid;
                                    bidRecord.Online = Auc.Online;
                                    Auc.pricers = Auc.nextValidBidRs;
                                    // AUC.pricers = proxy.ProxyAmt;
                                    bidRecord.nickname = proxynew.nickname;
                                    bidRecord.isOldUser = proxynew.isOldUser;

                                    bidrecordfinal = bidRecord;
                                    Auc.Bidrecordid = CAS.InsertBidRecordForAdminStartAuctoion(bidRecord);

                                }

                                if (Auc.Bidrecordid > 0)
                                {
                                    Auc.nextValidBidRs = bidRecord.nextValidBidRs;
                                    Auc.nextValidBidUs = Auc.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); //bidRecord.nextValidBidUs;
                                    CAS.UpdateAcutionPrice(Auc);
                                }
                                else
                                {
                                    //Failed
                                }



                                Auc.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(Auc.nextValidBidRs, 1).ToString());
                                currentuser = bidRecord.userid;
                            }
                            #endregion
                            else if (proxy.ProxyAmt > curprice)
                            {
                                Auc.curprice = curprice;
                                //created on 25-01-2020
                                #region
                                Auction bidRecord = new Auction();

                                List<Auction> listProxyBid = new List<Auction>();
                                listProxyBid = CAS.GetUpdatedProxyuser(Auc);
                                if (listProxyBid.Count > 0)
                                {
                                    foreach (var proxynew in listProxyBid)
                                    {
                                        if (proxynew.ProxyAmt > curprice)
                                        {
                                            bidRecord.firstname = proxynew.firstname;
                                            bidRecord.lastname = proxynew.lastname;
                                            bidRecord.thumbnail = proxynew.thumbnail;
                                            bidRecord.productid = proxynew.productid;
                                            bidRecord.pricers = proxynew.pricers;
                                            bidRecord.nickname = proxynew.nickname;
                                            bidRecord.priceus = proxynew.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;
                                            bidRecord.nextValidBidRs = proxynew.ProxyAmt; // Next valid bid
                                            bidRecord.nextValidBidUs = proxynew.ProxyAmt / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                                            curprice = proxynew.ProxyAmt; // bidRecord.nextValidBidRs;
                                                                          //bidRecord.daterec = Now : Already taken in query

                                            bidRecord.reference = proxynew.reference;
                                            bidRecord.anoname = proxynew.nickname;
                                            bidRecord.username = proxynew.username;
                                            bidRecord.currentbid = 0;
                                            bidRecord.recentbid = 0;
                                            bidRecord.userid = proxynew.userid;
                                            bidRecord.proxy = 1;
                                            //bidRecord.Online = proxynew.Auctionid;
                                            bidRecord.Online = Auc.Online;
                                            Auc.pricers = Auc.nextValidBidRs;
                                            bidRecord.nickname = proxynew.nickname;
                                            bidRecord.isOldUser = proxynew.isOldUser;

                                            bidrecordfinal = bidRecord;
                                            Auc.Bidrecordid = CAS.InsertBidRecordForAdminStartAuctoion(bidRecord);
                                            proxy.ProxyAmt = proxynew.ProxyAmt;
                                        }
                                        else if (proxynew.ProxyAmt == curprice)
                                        {
                                            bidRecord.firstname = proxynew.firstname;
                                            bidRecord.lastname = proxynew.lastname;
                                            bidRecord.thumbnail = proxynew.thumbnail;
                                            bidRecord.productid = proxynew.productid;
                                            bidRecord.pricers = proxynew.pricers;
                                            bidRecord.nickname = proxynew.nickname;
                                            bidRecord.priceus = Auc.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); // AUC.priceus;
                                            bidRecord.nextValidBidRs = proxynew.ProxyAmt; // Next valid bid
                                            bidRecord.nextValidBidUs = proxynew.ProxyAmt / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// proxy.ProxyAmtus;
                                            curprice = proxynew.ProxyAmt; // bidRecord.nextValidBidRs;
                                                                          //bidRecord.daterec = Now : Already taken in query

                                            bidRecord.reference = proxynew.reference;
                                            bidRecord.anoname = proxynew.nickname;
                                            bidRecord.username = proxynew.username;
                                            bidRecord.currentbid = 0;
                                            bidRecord.recentbid = 0;
                                            bidRecord.userid = proxynew.userid;
                                            bidRecord.proxy = 1;
                                            bidRecord.Online = Auc.Online;
                                            Auc.pricers = Auc.nextValidBidRs;
                                            bidRecord.nickname = proxynew.nickname;
                                            bidRecord.isOldUser = proxynew.isOldUser;

                                            bidrecordfinal = bidRecord;
                                            Auc.Bidrecordid = CAS.InsertBidRecordForAdminStartAuctoion(bidRecord);
                                            proxy.ProxyAmt = proxynew.ProxyAmt;
                                        }
                                    }
                                }
                                #endregion


                                if (Auc.Bidrecordid > 0)
                                {
                                    Auc.nextValidBidRs = proxy.ProxyAmt;
                                    Auc.nextValidBidUs = Auc.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]); //proxy.ProxyAmtus;
                                    CAS.UpdateAcutionPrice(Auc);
                                }
                                else
                                {
                                    //Failed
                                }

                                Auc.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(proxy.ProxyAmt, 1).ToString());
                                currentuser = bidRecord.userid;
                            }

                        }
                        Auc.curprice = curprice;
                        listProxyAuc = CAS.GetUpdatedProxyInfo(Auc);

                    }

                }

            }


            DataTable dtgetproduct = new DataTable();
            dtgetproduct = util.Display("Exec Proc_startAuction 'GetAllStartauctionproduct'");
            if (dtgetproduct.Rows.Count > 0)
            {
                foreach (DataRow dr in dtgetproduct.Rows)
                {
                    Auction objauc = new Auction();
                    objauc.productid = int.Parse(dr["productid"].ToString());
                    objauc.Online = onlineid;
                    Sendmailstartauction(objauc);
                }

            }

            if (listProxyAuc.Count == 0)
            {

                if (bidrecordfinal.userid > 0)
                {
                    return bidrecordfinal;
                }
                else
                {
                    return null;
                }



            }
            else
            {
                Auc.nextValidBidRs = 0;
                Auc.recentbid = 1;
                Auc.currentbid = 1;
            }

            return Auc;

        }


        public void Sendmailstartauction(Auction Auc)
        {

            DataTable dthighestuser = util.Display("Exec Proc_startAuction 'GetWinnerbyprodID'," + Auc.Online + "," + Auc.productid + "");
            int highestuserid = 0;
            if (dthighestuser.Rows.Count > 0)
            {
                highestuserid = int.Parse(dthighestuser.Rows[0]["userid"].ToString());

                using (SqlCommand cmd = new SqlCommand("Proc_startAuction"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "update_Islead");
                    cmd.Parameters.AddWithValue("@userid ", highestuserid);
                    cmd.Parameters.AddWithValue("@Productid", Auc.productid);
                    cmd.Parameters.AddWithValue("@Auctionid", Auc.Online);

                    if (util.Execute(cmd))
                    {

                    }
                    else
                    {

                    }
                }

            }

            DataTable dtoutbidusers = util.Display("Exec Proc_startAuction 'Getoutbiduser'," + Auc.Online + "," + Auc.productid + "," + highestuserid + "");
            if (dtoutbidusers.Rows.Count > 0)
            {
                foreach (DataRow droutbiduser in dtoutbidusers.Rows)
                {
                    int outbiduserid = int.Parse(droutbiduser["Userid"].ToString());
                    DataTable dt = util.Display("Exec Proc_startAuction 'GetoutbidUsersbyaucprodoIDdetail'," + Auc.Online + "," + Auc.productid + "," + outbiduserid + "");

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            string basePathUrl = "http://www.astaguru.com";
                            Auction objauc = new Auction();
                            UserService US = new UserService();
                            objauc.userid = int.Parse(dr["userid"].ToString());
                            objauc.username = dr["Username"].ToString();
                            objauc.reference = dr["Reference"].ToString();
                            objauc.Bidpricers = int.Parse(dr["Bidpricers"].ToString());
                            objauc.Bidpriceus = int.Parse(dr["Bidpriceus"].ToString());
                            objauc.thumbnail = dr["Thumbnail"].ToString();
                            objauc.pricers = int.Parse(dr["pricers"].ToString());
                            objauc.priceus = int.Parse(dr["priceus"].ToString());
                            objauc.title = dr["title"].ToString();
                            objauc.email = dr["email"].ToString();
                            objauc.ProxyAmt = int.Parse(dr["ProxyAmt"].ToString());
                            //objauc.ProxyAmtus = int.Parse(dr["ProxyAmtus"].ToString());
                            objauc.isOldUser = int.Parse(dr["isOldUser"].ToString());
                            objauc.amountlimt = int.Parse(dr["amountlimt"].ToString());

                            if (objauc.isOldUser == 0)
                            {
                                int updateLimit = objauc.amountlimt + objauc.ProxyAmt;
                                US.UpdateBidLimit(objauc.userid, updateLimit);
                            }

                            objauc.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(objauc.pricers, 1).ToString()); // Next Valid Bid
                            objauc.nextValidBidUs = objauc.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<b>Dear " + objauc.username + "</b><br><br>We would like to bring it to your notice that you have been outbid on Lot# " + objauc.reference + ", in the ongoing AstaGuru Online Auction. Your highest bid was on");
                            sb.Append("Rs." + Common.rupeeFormat(objauc.Bidpricers.ToString()) + "($" + objauc.Bidpricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + ")" + "The current highest bid stands at Rs." + Common.rupeeFormat(objauc.pricers.ToString()) + "($" + objauc.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + "). Continue to contest for Lot# " + objauc.reference + ", please place your updated bid here <a href=\"" + basePathUrl + "/Home/LotDetails?productid=" + objauc.productid + "&page=" + objauc.page + "\">Click here.</a><br><br><br>");
                            sb.Append("Lot No : " + objauc.reference + " <br>Title :" + objauc.title + "<br>");
                            sb.Append("Current Highest Bid : Rs." + Common.rupeeFormat(objauc.pricers.ToString()) + " ($" + objauc.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + ")<br>Next Incremental Bid Amount : Rs." + Common.rupeeFormat(objauc.nextValidBidRs.ToString()) + " ($" + objauc.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + ")<br><br><br><img src='http://www.astaguru.com/" + objauc.thumbnail + "'><br><br><br>");
                            sb.Append("In case you have any queries with regards to the Lots that are part of the auction or the bidding process, please feel free to contact us on 91-22 2204 8138/39 or write to us at contact@astaguru.com. Our team will be glad to assist you with the same.<br><br><br>");
                            sb.Append("Warmest Regards,<br>Team AstaGuru.");


                            string ToEmailid = objauc.email;
                            String[] emailid = new String[] { ToEmailid };


                            string subject = "AstaGuru - You have been Outbid on Lot# " + objauc.reference + "";
                            util.SendEmail(sb.ToString(), emailid, subject, "", null);
                        }

                    }

                    else
                    {
                        //user done proxy in upcoming auction but due to very less proxy it will not store in bid record on start auction click button

                        DataTable dtoutbidlessproxyamounytuser = util.Display("Exec Proc_startAuction 'GetProxyusernotinBidrecord'," + Auc.Online + "," + Auc.productid + "," + outbiduserid + "");

                        if (dtoutbidlessproxyamounytuser.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dtoutbidlessproxyamounytuser.Rows)
                            {
                                string basePathUrl = "http://www.astaguru.com";
                                Auction objauc = new Auction();
                                UserService US = new UserService();
                                objauc.userid = int.Parse(dr["userid"].ToString());
                                objauc.username = dr["username"].ToString();
                                objauc.reference = dr["Reference"].ToString();
                                objauc.thumbnail = dr["Thumbnail"].ToString();
                                objauc.pricers = int.Parse(dr["pricers"].ToString());
                                objauc.priceus = int.Parse(dr["priceus"].ToString());
                                objauc.title = dr["title"].ToString();
                                objauc.email = dr["email"].ToString();
                                objauc.ProxyAmt = int.Parse(dr["ProxyAmt"].ToString());
                                //objauc.ProxyAmtus = int.Parse(dr["ProxyAmtus"].ToString());
                                objauc.isOldUser = int.Parse(dr["isOldUser"].ToString());
                                objauc.amountlimt = int.Parse(dr["amountlimt"].ToString());

                                if (objauc.isOldUser == 0)
                                {
                                    int updateLimit = objauc.amountlimt + objauc.ProxyAmt;
                                    US.UpdateBidLimit(objauc.userid, updateLimit);
                                }

                                objauc.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(objauc.pricers, 1).ToString()); // Next Valid Bid
                                objauc.nextValidBidUs = objauc.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);


                                StringBuilder sb = new StringBuilder();
                                sb.Append("<b>Dear " + objauc.username + "</b><br><br>We would like to bring it to your notice that you have been outbid on Lot# " + objauc.reference + ", in the ongoing AstaGuru Online Auction. Your highest bid was on");
                                sb.Append("Rs." + Common.rupeeFormat(objauc.ProxyAmt.ToString()) + "($" + objauc.ProxyAmt / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + ")" + "The current highest bid stands at Rs." + Common.rupeeFormat(objauc.pricers.ToString()) + "($" + objauc.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + "). Continue to contest for Lot# " + objauc.reference + ", please place your updated bid here <a href=\"" + basePathUrl + "/Home/LotDetails?productid=" + objauc.productid + "&page=" + objauc.page + "\">Click here.</a><br><br><br>");
                                sb.Append("Lot No : " + objauc.reference + " <br>Title :" + objauc.title + "<br>");
                                sb.Append("Current Highest Bid : Rs." + Common.rupeeFormat(objauc.pricers.ToString()) + " ($" + objauc.pricers / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + ")<br>Next Incremental Bid Amount : Rs." + Common.rupeeFormat(objauc.nextValidBidRs.ToString()) + " ($" + objauc.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]) + ")<br><br><br><img src='http://www.astaguru.com/" + objauc.thumbnail + "'><br><br><br>");
                                sb.Append("In case you have any queries with regards to the Lots that are part of the auction or the bidding process, please feel free to contact us on 91-22 2204 8138/39 or write to us at contact@astaguru.com. Our team will be glad to assist you with the same.<br><br><br>");
                                sb.Append("Warmest Regards,<br>Team AstaGuru.");


                                string ToEmailid = objauc.email;
                                String[] emailid = new String[] { ToEmailid };


                                string subject = "AstaGuru - You have been Outbid on Lot# " + objauc.reference + "";
                                util.SendEmail(sb.ToString(), emailid, subject, "", null);
                            }
                        }
                    }


                }
            }
        }


        public List<Auction> GetAuctiondata(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                //AUC.nextValidBidRs;
                var para = new DynamicParameters();
                para.Add("@Para", "GetstartauctionData");
                para.Add("@Auctionid", AUC.Online);
                para.Add("@productid", AUC.productid);
                return con.Query<Auction>("Proc_startAuction", para, null, true, 0, CommandType.StoredProcedure).ToList();
            }
        }

        public List<Auction> GetProxyInfo(Auction AUC)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                //AUC.nextValidBidRs;
                var para = new DynamicParameters();
                para.Add("@Para", "Getstartauctionproxyinfo");
                para.Add("@Auctionid", AUC.Online);
                para.Add("@productid", AUC.productid);
                return con.Query<Auction>("Proc_startAuction", para, null, true, 0, CommandType.StoredProcedure).ToList();
            }
        }

        #endregion


        #region xxxxxxxxxxxxxxxxxxxxx---   Main Invoice ---  xxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult Invoice()
        {
            //string path = Server.MapPath("\\Asta_Invoice\\" + "13th - 14th May, 2019_Modern Indian Art-No Reserve Auction_2_22_5_2020_39_951.pdf");
            Invoice objInvoice = new Invoice();
            objInvoice.AuctionNameList = BindInvoiceAuctionName(objInvoice);
            return View(objInvoice);
        }


        public List<SelectListItem> BindInvoiceAuctionName(Invoice obj)
        {
            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_Invoice 'GetAuctionName'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {

          
                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["Auctionname"].ToString();
                    list.Value = dr["Auctionid"].ToString();

                    if (obj.Auctionname == dr["Auctionname"].ToString())
                    {
                        list.Selected = true;
                    }

                    objList.Add(list);


                }

            }
            return objList.ToList();
        }


        public JsonResult BindAuctionDate(int Id)
        {
            DataTable dt = new DataTable();
            Invoice obj = new Invoice();
            dt = util.Display("Execute Proc_Invoice 'GetAuctionDate'," + Id + "");
            List<SelectListItem> li = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    //li.Add(new SelectListItem { Text = dr["auctiondate"].ToString(), Value = dr["AuctionId"].ToString() });
                    li.Add(new SelectListItem { Text = dr["auctiondate"].ToString(), Value = dr["auctiondate"].ToString() });
                    //obj.SubId = int.Parse(dr["SubId"].ToString());


                }
            }


            return Json(li, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult BindAuctionDate(string Id)
        //{
        //    DataTable dt = new DataTable();
        //    Invoice obj = new Invoice();
        //    dt = util.Display("Execute Proc_Invoice 'GetAuctionDate',0,'" + Id + "'");
        //    List<SelectListItem> li = new List<SelectListItem>();
        //    if (dt.Rows.Count > 0)
        //    {
        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            li.Add(new SelectListItem { Text = dr["auctiondate"].ToString(), Value = dr["AuctionId"].ToString() });



        //        }
        //    }


        //    return Json(li, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult BindAuctionLot(int Id)
        {
            DataTable dt = new DataTable();
            Invoice obj = new Invoice();
            dt = util.Display("Execute Proc_Invoice 'GetAuctionLot'," + Id + "");
            List<SelectListItem> li = new List<SelectListItem>();

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    li.Add(new SelectListItem { Text = dr["reference"].ToString().Replace(" ", ""), Value = dr["reference"].ToString().Replace(" ", "") });
                    //obj.SubId = int.Parse(dr["SubId"].ToString());


                }
            }


            return Json(li, JsonRequestBehavior.AllowGet);
        }


        public JsonResult getdata(string Id)
        {
            DataTable dt = new DataTable();

            dt = util.Display("Execute Proc_Invoice 'Getwinneruserdata',0,'','" + Id + "'");
            List<Invoice> li = new List<Invoice>();

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Invoice obj = new Invoice();
                    obj.Auctionname = dr["Auctionname"].ToString();
                    obj.auctiondate = dr["auctiondate"].ToString();

                    obj.BillingName = dr["BillingName"].ToString();
                    obj.GSTIN = dr["GSTIN"].ToString();
                    obj.BillingAddress = dr["BillingAddress"].ToString();
                    obj.DeliveryAddress = dr["Deliveryaddress"].ToString();


                    obj.reference = dr["Reference"].ToString().Trim();
                    obj.ArtistName = dr["ArtistName"].ToString();
                    obj.title = dr["title"].ToString();
                    obj.medium = dr["medium"].ToString();
                    obj.productsize = dr["productsize"].ToString();
                    obj.Bidpricers = int.Parse(dr["Bidpricers"].ToString());
                    obj.Bidpriceus = int.Parse(dr["Bidpriceus"].ToString());
                    obj.thumbnail = dr["Thumbnail"].ToString();
                    obj.auctionType = int.Parse(dr["auctionType"].ToString());
                    obj.userCountry = dr["country"].ToString();
                    obj.PrVat = dr["PrVat"].ToString().Trim();
                    obj.isInternational = int.Parse(dr["isInternational"].ToString().Trim());
                    obj.isInternationalGST = int.Parse(dr["isInternationalGST"].ToString().Trim());
                    obj.astaguruPrice = int.Parse(dr["astaguruPrice"].ToString().Trim());
                    if (dr["usedGoodPercentage"].ToString().Trim() == null || dr["usedGoodPercentage"].ToString().Trim() == "")
                    {
                        obj.usedGoodPercentage = 0;
                    }
                    else
                    {
                        obj.usedGoodPercentage = int.Parse(dr["usedGoodPercentage"].ToString().Trim());
                    }

                    obj.state = dr["state"].ToString().Trim();

                    li.Add(obj);

                }
            }
            else
            {

            }
            return Json(li, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Invoice(Invoice objInvoice, FormCollection fobj)
        {
            string sbMainInvoice = "";

            StringReader sr;

            int value = objInvoice.Auctionid;
            BindAuctionLot(value);


            string AuctionType = objInvoice.auctionType.ToString();
            string state = objInvoice.state;
            string Country = objInvoice.userCountry;

            int PrVat = int.Parse(objInvoice.PrVat);
            int isInternational = int.Parse(objInvoice.isInternational.ToString());
            int isInternationalGST = int.Parse(objInvoice.isInternationalGST.ToString());
            int astaguruPrice = int.Parse(objInvoice.astaguruPrice.ToString());
            int usedGoodPercentage = int.Parse(objInvoice.usedGoodPercentage.ToString());



            //string AuctionName = objInvoice.Auctionname;
            //string AuctionDate = objInvoice.auctiondate;
            //string Lot = objInvoice.referenceid.ToString();

            string AuctionName = fobj["hiddenAuction"].ToString();
            string AuctionDate = fobj["hiddenauctiondate"].ToString();
            string Lot = fobj["hiddenauctionlot"].ToString();


            string Invoice_No = objInvoice.Invoice_No;

            string Invoice_Date = objInvoice.Invoice_Date;

            string E_way_bill_No;
            if (objInvoice.E_way_bill_No == null)
            {
                E_way_bill_No = "";
            }
            else
            {
                E_way_bill_No = objInvoice.E_way_bill_No.ToString();
            }

            string BillingName = objInvoice.BillingName;
            string GSTIN = objInvoice.GSTIN;

            string BillingAddress;
            if (objInvoice.BillingAddress == null)
            {
                BillingAddress = "";
            }
            else
            {
                BillingAddress = objInvoice.BillingAddress;
            }

            string DeliveryAddress;
            if (objInvoice.DeliveryAddress == null)
            {
                DeliveryAddress = "";
            }
            else
            {
                DeliveryAddress = objInvoice.DeliveryAddress;
            }


            string Product_Auction_Type = objInvoice.auctionType.ToString();
            string PHSN_Code = objInvoice.HSN_code.ToString();
            string Artist_Name = objInvoice.ArtistName;
            string Product_Title = objInvoice.title;


            string Medium = objInvoice.medium;
            string Product_Size = objInvoice.productsize;
            int BidpriceRs = objInvoice.Bidpricers;
            int BidpriceUs = objInvoice.Bidpriceus;

            string Product_Image = objInvoice.thumbnail;


            int FHSN_Code = int.Parse(objInvoice.FreightHSN_Code.ToString());
            int FIGST = int.Parse(objInvoice.Freight_IGST.ToString());
            int FCGST = int.Parse(objInvoice.Freight_CGST.ToString());
            int FSGST = int.Parse(objInvoice.Freight_SGST.ToString());
            int FAmount = int.Parse(objInvoice.Freight_Amount.ToString());


            long TotalAmountWithFinal_Famountwith_Tax = 0, TotalAmountWithFinal_Famount = 0, ToTal_Tax = 0, Purchase_value, gstvalue = 0, customduty, margin, Total_Amount, Final_Total, CGST = 6, SGST = 6, CGST_value = 0, SGST_value = 0, Taxable_Amount;

            if (Country == "India")
            {

                if (isInternational == 1 && astaguruPrice == 1 && state != "Maharashtra")
                {
                    Purchase_value = BidpriceRs - (long.Parse(BidpriceRs.ToString()) * usedGoodPercentage / 100);            /// with usedgood gst
                    customduty = long.Parse(BidpriceRs.ToString()) * (11 / 100);
                    margin = long.Parse(BidpriceRs.ToString()) * (15 / 100);
                    Taxable_Amount = BidpriceRs + customduty + margin - Purchase_value;              
                    gstvalue = (long.Parse(Taxable_Amount.ToString()) * isInternationalGST / 100);
                    Total_Amount = BidpriceRs + customduty + margin;
                    Final_Total = Total_Amount + gstvalue;                                            ///  with internation Gst


                }
                else if (isInternational == 1 && astaguruPrice == 1 && state == "Maharashtra")
                {
                    Purchase_value = BidpriceRs - (long.Parse(BidpriceRs.ToString()) * usedGoodPercentage / 100);
                    customduty = long.Parse(BidpriceRs.ToString()) * (11 / 100);
                    margin = long.Parse(BidpriceRs.ToString()) * (15 / 100);
                    Taxable_Amount = BidpriceRs + customduty + margin - Purchase_value;    
                    CGST_value = (long.Parse(Taxable_Amount.ToString()) * CGST / 100);
                    SGST_value = (long.Parse(Taxable_Amount.ToString()) * SGST / 100);
                    Total_Amount = BidpriceRs + customduty + margin;
                    Final_Total = Total_Amount + CGST_value + SGST_value;
                }
                else if (isInternational == 1 && astaguruPrice != 1 && state != "Maharashtra")
                {
                    customduty = long.Parse(BidpriceRs.ToString()) * 11 / 100;
                    margin = long.Parse(BidpriceRs.ToString()) * 15 / 100;
                    Total_Amount = BidpriceRs + customduty + margin;
                    gstvalue = (long.Parse(Total_Amount.ToString()) * isInternationalGST / 100);
                    Final_Total = Total_Amount + gstvalue;                                             ///  with internation Gst

                }

                else if (isInternational == 1 && astaguruPrice != 1 && state == "Maharashtra")
                {
                    customduty = long.Parse(BidpriceRs.ToString()) * 11 / 100;
                    margin = long.Parse(BidpriceRs.ToString()) * 15 / 100;
                    Total_Amount = BidpriceRs + customduty + margin;
                    CGST_value = (long.Parse(Total_Amount.ToString()) * CGST / 100);
                    SGST_value = (long.Parse(Total_Amount.ToString()) * SGST / 100);
                    Final_Total = Total_Amount + CGST_value + SGST_value;
                }

                else if (astaguruPrice == 1 && isInternational != 1 && state != "Maharashtra")
                {
                    Purchase_value = BidpriceRs - (long.Parse(BidpriceRs.ToString()) * usedGoodPercentage / 100);          /// with usedgood gst
                    margin = long.Parse(BidpriceRs.ToString()) * 15 / 100;
                    Taxable_Amount = BidpriceRs + margin - Purchase_value;         
                    gstvalue = (long.Parse(Taxable_Amount.ToString()) * PrVat / 100);
                    Total_Amount = BidpriceRs + margin;
                    Final_Total = Total_Amount + gstvalue;


                }
                else if (astaguruPrice == 1 && isInternational != 1 && state == "Maharashtra")
                {
                    Purchase_value = BidpriceRs - (long.Parse(BidpriceRs.ToString()) * usedGoodPercentage / 100);          /// with usedgood gst
                    margin = long.Parse(BidpriceRs.ToString()) * 15 / 100;
                    Taxable_Amount = BidpriceRs + margin - Purchase_value;                   
                    CGST_value = (long.Parse(Taxable_Amount.ToString()) * CGST / 100);
                    SGST_value = (long.Parse(Taxable_Amount.ToString()) * SGST / 100);
                    Total_Amount = BidpriceRs + margin;
                    Final_Total = Total_Amount + CGST_value + SGST_value;
                }
                else if (state == "maharashtra")
                {
                    margin = long.Parse(BidpriceRs.ToString()) * 15 / 100;
                    Total_Amount = BidpriceRs + margin;
                    CGST_value = (long.Parse(Total_Amount.ToString()) * CGST / 100);
                    SGST_value = (long.Parse(Total_Amount.ToString()) * SGST / 100);
                    Final_Total = Total_Amount + CGST_value + SGST_value;

                }
                else
                {
                    margin = long.Parse(BidpriceRs.ToString()) * 15 / 100;
                    Total_Amount = BidpriceRs + margin;
                    gstvalue = (long.Parse(Total_Amount.ToString()) * PrVat / 100);
                    Final_Total = Total_Amount + gstvalue;

                }

                if (FAmount > 0)
                {
                    TotalAmountWithFinal_Famount = Total_Amount + FAmount;

                    if (FIGST > 0)
                    {
                        TotalAmountWithFinal_Famountwith_Tax = Final_Total + FIGST + FAmount;
                    }
                    else
                    {
                        TotalAmountWithFinal_Famountwith_Tax = Final_Total + FCGST + FSGST + FAmount;
                    }

                }
                else
                {
                    TotalAmountWithFinal_Famount = Total_Amount;
                    TotalAmountWithFinal_Famountwith_Tax = Final_Total;
                }



                if (FIGST > 0)
                {
                    ToTal_Tax = gstvalue + FIGST;
                }
                else if (FCGST > 0 && FSGST > 0)
                {
                    ToTal_Tax = CGST_value + SGST_value + FCGST + FSGST;
                }
                else if (gstvalue > 0)
                {
                    ToTal_Tax = gstvalue;
                }
                else if (CGST_value > 0 && SGST_value > 0)
                {
                    ToTal_Tax = CGST_value + SGST_value;
                }

            }
            else
            {

                margin = BidpriceUs * 15 / 100;
                Total_Amount = BidpriceUs + margin;
                gstvalue = 0;
                Final_Total = BidpriceUs + margin;


                if (FAmount > 0)
                {
                    TotalAmountWithFinal_Famount = BidpriceUs + FAmount;

                    if (FIGST > 0)
                    {
                        TotalAmountWithFinal_Famountwith_Tax = Final_Total + FIGST + FAmount;
                    }


                }
                else
                {
                    TotalAmountWithFinal_Famount = Final_Total;
                    TotalAmountWithFinal_Famountwith_Tax = Final_Total;
                }

                if (FIGST > 0)
                {
                    ToTal_Tax = gstvalue + FIGST;
                }

                else if (gstvalue > 0)
                {
                    ToTal_Tax = gstvalue;
                }


            }


            using (StringWriter sw = new StringWriter())
            {

                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {

                    sbMainInvoice = "<table width=100% cellpadding='3' cellspacing='0' style='border:1px solid #000;border-collapse: collapse;font-size:10px'>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<th colspan='4 ' style='border-bottom:1px solid #000;text-align:center;font-size:15px'>TAX INVOICE</th>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<th colspan='4 ' style='font-size: 12px;text-align:center'>( Rule 7, Section 31 )</th>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<th  width='37%'></th>";
                    sbMainInvoice += "<th width='29%'></th>";
                    sbMainInvoice += "<th width='17%'></th>";
                    sbMainInvoice += "<th width='17%'>Original</th>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td style='font-weight:bold'>Supplier Name & Address</td>";
                    sbMainInvoice += "<td >GSTIN No : 27AABCS9352H1Z4</td>";
                    sbMainInvoice += "<td colspan='2' style='font-weight:bold'>" + AuctionDate + "  " + AuctionName + "</td>";
                    //sbMainInvoice += "<td style='font-weight:bold'></td>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td style='font-weight:bold;'>AstaGuru.com (Div. of Safset Agencies Pvt. Ltd.)</td>";
                    sbMainInvoice += "<td>PAN No : AABCS9352H</td>";
                    sbMainInvoice += "<td style='font-weight:bold;border:1px solid #000;'>Invoice No</td>";
                    sbMainInvoice += "<td style='font-weight:bold;border:1px solid #000;border-right:0px;'>" + Invoice_No.ToString() + "</td>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td>FGP Centre, Commercial Union House, 3rd Flr., 9 Wallace Street Fort, Mumbai 400 001.";
                    sbMainInvoice += "</td>";
                    sbMainInvoice += "<td>CIN No : U51209MH1966PTC013575</td>";
                    sbMainInvoice += "<td style='font-weight:bold;border:1px solid #000;'>Date :</td>";
                    sbMainInvoice += "<td style='font-weight:bold;border:1px solid #000; border-right:0px;'>" + Invoice_Date.ToString() + "</td>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td height = '91' colspan = '2' > &nbsp;  &nbsp;</td >";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td></td>";
                    sbMainInvoice += "<td></td>";
                    sbMainInvoice += "<td  colspan='2 ' style='border:1px solid #000; border-right:0px;'>E way bill No :" + E_way_bill_No.ToString() + "</td>";
                    sbMainInvoice += " </tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td height = '91' colspan = '2' > &nbsp;  &nbsp;</td >";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "</table>";
                    sbMainInvoice += "<table width=100% cellpadding='10' style='border:1px solid #000;border-collapse: collapse;font-size:12px;padding-bottom:30px'>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td width=33.333% align='center' style='border:1px solid #000; '>Customer Name & GSTIN Details</td>";
                    sbMainInvoice += "<td width=33.333% align='center' style='border:1px solid #000; '>Billing Address </td>";
                    sbMainInvoice += "<td width=33.333% align='center' style='border:1px solid #000; '>Delivery Address </td>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td rowspan='3' align='center' style='border:1px solid #000;font-size:16px;font-weight:bold '>" + BillingName.ToString() + "<br/> " + GSTIN.ToString() + "</td>";
                    sbMainInvoice += "<td rowspan='3' style='border:1px solid #000; '>" + BillingAddress.ToString() + "</td>";
                    sbMainInvoice += "<td rowspan='3' style='border:1px solid #000; '>" + DeliveryAddress.ToString() + "</td>";
                    sbMainInvoice += " </tr>";
                    sbMainInvoice += "</table>";
                    sbMainInvoice += "<table width=100% cellpadding='5' style='border:1px solid #000;border-collapse: collapse;font-size:12px'>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td width=33.333% rowspan='2' align='center' style='border:1px solid #000;'>Whether the tax is payable on reverse charge : Yes/ No</td>";
                    sbMainInvoice += "<td width=33.333% style='border:1px solid #000; '>Date & Time Of Preparation : </td>";
                    sbMainInvoice += "<td width=33.333% style='border:1px solid #000; '>&nbsp;</td>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td style='border:1px solid #000; '>Date & Time Of Removal :</td>";
                    sbMainInvoice += "<td style='border:1px solid #000; '> &nbsp;</td>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td style='border:1px solid #000; '></td>";
                    sbMainInvoice += "<td style='border:1px solid #000; '>Mode Of Transportation :</td>";
                    sbMainInvoice += "<td style='border:1px solid #000; '> &nbsp;</td>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td style='border:1px solid #000; '></td>";
                    sbMainInvoice += "<td style='border:1px solid #000; '>Vehicle Regn. No</td>";
                    sbMainInvoice += "<td px style='border:1px solid #000; '> &nbsp;</td>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "</table>";
                    sbMainInvoice += "<table width=100% cellpadding='5' style='border:1px solid #000;border-collapse: collapse;font-size:12px'>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td width=7% align='center' style='border:1px solid #000;font-weight:bold;font-size:10px; '>Sl No</td>";
                    sbMainInvoice += "<td width=30% align='center' style='border:1px solid #000;font-weight:bold;font-size:10px; '>Description And Specification Of Goods/Services</td>";
                    sbMainInvoice += "<td width=7% align='center' style='border:1px solid #000;font-weight:bold;font-size:10px; '> HSN Code </td>";
                    sbMainInvoice += "<td width=9% align='center' style='border:1px solid #000;font-weight:bold;font-size:10px; '> Qty With Unit </td>";
                    sbMainInvoice += "<td width=9% align='center' style='border:1px solid #000;font-weight:bold;font-size:10px; '> Discount/ Abatement </td>";
                    sbMainInvoice += "<td width=7% align='center' style='border:1px solid #000;font-weight:bold;font-size:10px; '> IGST @ </td>";
                    sbMainInvoice += "<td width=7% align='center' style='border:1px solid #000;font-weight:bold;font-size:10px; '> CGST@ </td>";
                    sbMainInvoice += "<td width=7% align='center' style='border:1px solid #000;font-weight:bold;font-size:10px; '> SGST @ </td>";
                    sbMainInvoice += "<td width=16% align='center' style='border:1px solid #000;font-weight:bold;font-size:10px; '> Amount ( Rs ) </td>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td style='border:1px solid #000;'>1</td>";
                    if (AuctionType.ToString().Trim() == "1")
                    {
                        sbMainInvoice += "<td style='border:1px solid #000;'>Lot No.: "+ Lot.ToString() + "<br/>Artist: " + Artist_Name.ToString() + "<br/>Title: " + Product_Title.ToString() + "<br/>Medium: " + Medium.ToString() + "<br/>Size: " + Product_Size.ToString() + "<br/>Image:<br/><img src='https://www.astaguru.com/" + Product_Image.ToString() + "' style='height:175px;width:130px;'/></td>";
                    }
                    else
                    {
                        sbMainInvoice += "<td style='border:1px solid #000;'>Lot No.: " + Lot.ToString() + "<br/>Title: " + Product_Title.ToString() + "Size: " + Product_Size.ToString() + "<br/>Image:<br/><img src='https://www.astaguru.com/" + Product_Image.ToString() + "' style='height:175pxwidth:130px;'/></td>";

                    }
                    sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + PHSN_Code + "</td>";
                    sbMainInvoice += "<td align='right' style='border:1px solid #000; '>1.00</td>";
                    sbMainInvoice += "<td align='right' style='border:1px solid #000; '></td>";
                    if (Country == "India")
                    {
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.rupeeFormat(gstvalue.ToString()) + "</td>";
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.rupeeFormat(CGST_value.ToString()) + "</td>";
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.rupeeFormat(SGST_value.ToString()) + "</td>";
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.rupeeFormat(Total_Amount.ToString()) + "</td>";
                    }
                    else
                    {
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.DollerFormat(gstvalue.ToString()) + "</td>";
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.DollerFormat(CGST_value.ToString()) + "</td>";
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.DollerFormat(SGST_value.ToString()) + "</td>";
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.DollerFormat(Total_Amount.ToString()) + "</td>";
                    }
                    
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td style='border:1px solid #000; '>2</td>";
                    sbMainInvoice += "<td style='border:1px solid #000; '>Freight and Packing (In-Transit Crate)</td>";
                    sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + FHSN_Code + "</td>";
                    sbMainInvoice += "<td align='right' style='border:1px solid #000; '>1.00</td>";
                    sbMainInvoice += "<td align='right' style='border:1px solid #000; '></td>";
                    if (Country == "India")
                    {
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.rupeeFormat(FIGST.ToString()) + "</td>";
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.rupeeFormat(FCGST.ToString()) + "</td>";
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.rupeeFormat(FSGST.ToString()) + "</td>";
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.rupeeFormat(FAmount.ToString()) + "</td>";
                    }
                    else
                    {
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.DollerFormat(FIGST.ToString()) + "</td>";
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.DollerFormat(FCGST.ToString()) + "</td>";
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.DollerFormat(FSGST.ToString()) + "</td>";
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.DollerFormat(FAmount.ToString()) + "</td>";
                    }
              
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "</table>";
                    sbMainInvoice += "<table width=100% cellpadding='5' style='border:1px solid #000;border-collapse: collapse;font-size:12px'>";
                    if (Country == "India")
                    {
                        NumberToWords objnum = new NumberToWords();
                        string number = TotalAmountWithFinal_Famountwith_Tax.ToString();
                        number = objnum.ConvertAmount(double.Parse(number));

                        sbMainInvoice += "<tr>";
                        sbMainInvoice += "<td width=15% align='center' rowspan='2' style='border:1px solid #000;font-weight:bold; '>Invoice Value In Words. (Rupees)</td>";
                        sbMainInvoice += "<td width=45% align='left' rowspan='2' style='border:1px solid #000;font-weight:bold; '>" + number + "</td>";
                        sbMainInvoice += "<td width=20% align='left' style='border:1px solid #000; '> Total Taxable Amount</td>";
                        sbMainInvoice += "<td width=20% align='right' style='border:1px solid #000; '> " + Common.rupeeFormat(TotalAmountWithFinal_Famount.ToString()) + " </td>";
                        sbMainInvoice += "</tr>";
                        sbMainInvoice += "<tr>";
                        sbMainInvoice += "<td align='left' style='border:1px solid #000; '>Total Tax </td>";
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.rupeeFormat(ToTal_Tax.ToString()) + "</td>";
                        sbMainInvoice += "</tr>";
                    }
                    else
                    {
                        NumberToWords objnum = new NumberToWords();
                        string number = TotalAmountWithFinal_Famountwith_Tax.ToString();
                        number = objnum.MoneyToWordsDollare(double.Parse(number));

                        sbMainInvoice += "<tr>";
                        sbMainInvoice += "<td width=15% align='center' rowspan='2' style='border:1px solid #000;font-weight:bold; '>Invoice Value In Words. (Rupees)</td>";
                        sbMainInvoice += "<td width=45% align='left' rowspan='2' style='border:1px solid #000;font-weight:bold; '>" + number + "</td>";
                        sbMainInvoice += "<td width=20% align='left' style='border:1px solid #000; '> Total Taxable Amount</td>";
                        sbMainInvoice += "<td width=20% align='right' style='border:1px solid #000; '> " + Common.DollerFormat(TotalAmountWithFinal_Famount.ToString()) + " </td>";
                        sbMainInvoice += "</tr>";
                        sbMainInvoice += "<tr>";
                        sbMainInvoice += "<td align='left' style='border:1px solid #000; '>Total Tax </td>";
                        sbMainInvoice += "<td align='right' style='border:1px solid #000; '>" + Common.DollerFormat(ToTal_Tax.ToString()) + "</td>";
                        sbMainInvoice += "</tr>";
                    }
                    sbMainInvoice += "</table>";
                    sbMainInvoice += "<table width=100% cellpadding='5' style='border:1px solid #000;border-top:0px;border-collapse: unset;font-size:12px'>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td width=15% align='center' rowspan='7' style='border:1px solid #000; '>Terms of Payment: </td>";
                    sbMainInvoice += "<td width=45% align='left' style='border-right:1px solid #000; '>The wire transfer details are as follows: </td>";
                    sbMainInvoice += "<td width=20% align='left' style='border:1px solid #000;font-weight:bold; '> Total Invoice Value.</td>";
                    if (Country == "India")
                    {
                        sbMainInvoice += "<td width=20% align='right' style='border:1px solid #000;font-weight:bold; '>" + Common.rupeeFormat(TotalAmountWithFinal_Famountwith_Tax.ToString()) + "</td>";
                    }
                    else
                    {
                        sbMainInvoice += "<td width=20% align='right' style='border:1px solid #000;font-weight:bold; '>" + Common.DollerFormat(TotalAmountWithFinal_Famountwith_Tax.ToString()) + "</td>";
                    }
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td align='left' style='border-right:1px solid #000; '>Name of the Beneficiary: Astaguru.com </td>";
                    sbMainInvoice += "<td align='center' colspan='2'></td>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td align='left' style='border-right:1px solid #000; '>Bank Name : ICICI Bank, </td>";
                    sbMainInvoice += "<td align='center' colspan='2'></td>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td align='left' style='border-right:1px solid #000; '>Bank Address: 240 Navsari Building, D. N. Road, Fort, Mumbai - 400001. India.</td>";
                    sbMainInvoice += "<td align='center' colspan='2'></td>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td align='left' style='border-right:1px solid #000; '>Account No: 623505385049</td>";
                    sbMainInvoice += "<td align='center' colspan='2'></td>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td align='left' style='border-right:1px solid #000; '>Swift Code: ICICINBBCTS</td>";
                    sbMainInvoice += "<td align='center' colspan='2'></td>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "<tr>";
                    sbMainInvoice += "<td align='left'style='border-right:1px solid #000; '>RTGS / NEFT IFSC Code : ICIC0006235</td>";
                    sbMainInvoice += "<td align='center' colspan='2'>Authorised Signatory</td>";
                    sbMainInvoice += "</tr>";
                    sbMainInvoice += "</table>";



                    ViewBag.Content = sbMainInvoice.ToString();

                    //sr = new StringReader(ViewBag.Content);
                    //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);



                    //using (MemoryStream memoryStream = new MemoryStream())
                    //{
                    //    string strFileName = AuctionDate + "_" + AuctionName + "_" + Lot + "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Second.ToString() + "_" + DateTime.Now.Millisecond.ToString() + ".pdf";


                    //    var output = new FileStream(Path.Combine(Request.PhysicalApplicationPath + "\\Asta_Invoice\\", strFileName), FileMode.Create);



                    //    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, output);

                    //    pdfDoc.Open();

                    //    XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);

                    //    pdfDoc.Close();


                    //    byte[] bytes = memoryStream.ToArray();

                    //    memoryStream.Close();


                    //}
                }


            }


            MemoryStream stream = new System.IO.MemoryStream();
            string strFileName1 = AuctionDate + "_" + AuctionName + "_" + Lot + "_" + DateTime.Now.ToString() + ".pdf";

            string valuepdf = sbMainInvoice.ToString();
            StringReader srnew = new StringReader(valuepdf);
            Document pdfDocNew = new Document(PageSize.A4);
            PdfWriter writerNew = PdfWriter.GetInstance(pdfDocNew, stream);
            pdfDocNew.Open();
            XMLWorkerHelper.GetInstance().ParseXHtml(writerNew, pdfDocNew, srnew);
            pdfDocNew.Close();



            return File(stream.ToArray(), "application/pdf", strFileName1);

        }


        #endregion


        public string Generateslg(string strtext)
        {
            string str = strtext.ToString().ToLower();
            str = Regex.Replace(str, @"[^0-9a-zA-Z]+", "-");
            return str;
        }


        #region xxxxxxxxxxxxxx --- shipping Api --- xxxxxxxxxxxxxxxxxxx

        public ActionResult Shipping()
        {
            Shipping objShipping = new Shipping();
            objShipping.AuctionNameList = BindShippingAuctionName(objShipping);
            objShipping.GridList = GetShippingList();
            return View(objShipping);
            //return View();
        }


        public List<Shipping> GetShippingList()
        {
            try
            {
                List<Shipping> objShippinglist = new List<Shipping>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_ShippingOrder 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Shipping objShipping = new Shipping();
                        objShipping.Id = Convert.ToInt32(dr["Id"].ToString());
                        objShipping.Auctionname = dr["AuctionName"].ToString();
                        objShipping.auctiondate = dr["AuctionDate"].ToString();
                        objShipping.reference = dr["Reference"].ToString();
                        objShipping.auctionType = int.Parse(dr["AuctionType"].ToString());
                        objShipping.ArtistName = dt.Rows[0]["ArtistName"].ToString();
                        objShipping.Productid = int.Parse(dr["Productid"].ToString());
                        objShipping.title = dr["ProductTitle"].ToString();
                        objShipping.thumbnail = dr["Image"].ToString();
                        objShipping.Mobile = dr["Mobile"].ToString();
                        objShipping.Email = dr["Email"].ToString();
                        objShipping.PaymentType = dr["PaymentMode"].ToString();
                        objShipping.PaymentStatus = dr["PaymentStatus"].ToString();
                        objShipping.PaymentDetails = dr["PaymentDetail"].ToString();
                        objShipping.ShippingServiceProvider = dr["ServiceProvider"].ToString();
                        objShipping.AWBNumber = dr["AWB_Number"].ToString();
                        objShipping.BillingName = dr["CustomerName"].ToString();
                        objShipping.DeliveryAddress = dr["DeliveryAddress"].ToString();
                        objShipping.DeliveryStatus = dr["DeliveryStatus"].ToString();
                        objShipping.ExpectedDeliveryDate = dr["ExpectedDeliveryDate"].ToString();
                        objShipping.StatusDate = dr["StatusDate"].ToString();


                        objShippinglist.Add(objShipping);
                    }
                }
                return objShippinglist;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }


        public List<SelectListItem> BindShippingAuctionName(Shipping obj)
        {
            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_Invoice 'GetAuctionName'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {


                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["Auctionname"].ToString();
                    list.Value = dr["Auctionid"].ToString();

                    if (obj.Auctionname == dr["Auctionname"].ToString())
                    {
                        list.Selected = true;
                    }

                    objList.Add(list);


                }

            }
            return objList.ToList();
        }


        //public JsonResult BindAuctionDate(int Id)
        //{
        //    DataTable dt = new DataTable();
        //    Invoice obj = new Invoice();
        //    dt = util.Display("Execute Proc_Invoice 'GetAuctionDate'," + Id + "");
        //    List<SelectListItem> li = new List<SelectListItem>();
        //    if (dt.Rows.Count > 0)
        //    {
        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            li.Add(new SelectListItem { Text = dr["auctiondate"].ToString(), Value = dr["AuctionId"].ToString() });
        //            //obj.SubId = int.Parse(dr["SubId"].ToString());


        //        }
        //    }


        //    return Json(li, JsonRequestBehavior.AllowGet);
        //}


        //public JsonResult BindAuctionLot(int Id)
        //{
        //    DataTable dt = new DataTable();
        //    Invoice obj = new Invoice();
        //    dt = util.Display("Execute Proc_Invoice 'GetAuctionLot'," + Id + "");
        //    List<SelectListItem> li = new List<SelectListItem>();

        //    if (dt.Rows.Count > 0)
        //    {
        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            li.Add(new SelectListItem { Text = dr["reference"].ToString().Replace(" ", ""), Value = dr["reference"].ToString().Replace(" ", "") });
        //            //obj.SubId = int.Parse(dr["SubId"].ToString());


        //        }
        //    }


        //    return Json(li, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult getdataShipping(string Id)
        {
            DataTable dt = new DataTable();

            dt = util.Display("Execute Proc_ShippingOrder 'Getwinneruserdata',0,'','','','','" + Id + "'");
            List<Shipping> li = new List<Shipping>();

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    Shipping obj = new Shipping();
                    obj.Auctionname = dr["Auctionname"].ToString();
                    obj.auctiondate = dr["auctiondate"].ToString();

                    obj.BillingName = dr["BillingName"].ToString();
                    obj.DeliveryAddress = dr["Deliveryaddress"].ToString();
                    obj.reference = dr["Reference"].ToString().Trim();
                    obj.ArtistName = dr["ArtistName"].ToString();

                    obj.Productid = int.Parse(dr["Productid"].ToString());
                    obj.title = dr["title"].ToString();

                    //obj.Bidpricers = int.Parse(dr["Bidpricers"].ToString());
                    //obj.Bidpriceus = int.Parse(dr["Bidpriceus"].ToString());

                    obj.thumbnail = dr["Thumbnail"].ToString();
                    obj.auctionType = int.Parse(dr["auctionType"].ToString());
                    obj.Country = dr["country"].ToString();

                    obj.Mobile = dr["mobile"].ToString();
                    obj.Email = dr["Email"].ToString();



                    //obj.state = dr["state"].ToString().Trim();

                    li.Add(obj);

                }
            }
            else
            {

            }
            return Json(li, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Shipping(Shipping objShipping, FormCollection fobj)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    if (objShipping.ShippingServiceProvider== "Bluedart")
                    {


                        WebRequest request = WebRequest.Create("https://api.bluedart.com/servlet/RoutingServlet?handler=tnt&action=custawbquery&loginid=BOM00001&awb=awb&numbers="+ objShipping.AWBNumber + "&format=xml&lickey=4ecbd06dc0b9737d69120029cb05c9df%20&verno=1&scan=2");

                        try
                        {
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            WebResponse response = request.GetResponse();
                            using (var sr = new System.IO.StreamReader(response.GetResponseStream()))
                            {
                                XDocument xmlDoc = new XDocument();
                                try
                                {
                                    xmlDoc = XDocument.Parse(sr.ReadToEnd());

                                    XmlDocument myXmlDocument1 = new XmlDocument();
                                    myXmlDocument1.LoadXml(xmlDoc.ToString());
                                    XmlNodeList list1 = myXmlDocument1.SelectNodes("/ShipmentData/Shipment");
                                    foreach (XmlNode stats1 in list1)
                                    {
                                
                                        objShipping.BillingName= stats1["CustomerName"].InnerText;
                                        objShipping.DeliveryAddress = stats1["DeliveryLocation"].InnerText;
                                        objShipping.DeliveryStatus = stats1["Status"].InnerText;
                                        objShipping.ExpectedDeliveryDate = stats1["ExpectedDeliveryDate"].InnerText;
                                        objShipping.StatusDate = stats1["StatusDate"].InnerText;                             

                                    }




                                }
                                catch (Exception ex)
                                {
                                    // handle if necessary
                                }
                            }
                        }
                        catch (WebException ex)
                        {
                            Response.Write(ex);
                            // handle if necessary    
                        }



                    }



                    string AuctionName = fobj["hiddenAuction"].ToString();
                    string AuctionDate = fobj["hiddenauctiondate"].ToString();
                    string Lot = fobj["hiddenauctionlot"].ToString();

                    string AuctionDate2 = fobj["auctiondate"].ToString();

                    if (objShipping.Id == 0)
                    {
                        if (SaveShipping(objShipping, AuctionName, AuctionDate, Lot))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Shipping");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Shipping");
                        }
                    }
                    else
                    {
                        if (UpdateShipping(objShipping,AuctionName, AuctionDate, Lot))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Shipping");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Shipping");
                        }
                   
                    }
                }
                return View("Shipping", objShipping);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("Shipping");
            }


        }


        public bool SaveShipping(Shipping objShipping, string AuctionName, string AuctionDate, string Lot)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_ShippingOrder"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Auctionid", objShipping.Auctionid);
                cmd.Parameters.AddWithValue("@Productid", objShipping.Productid);
                cmd.Parameters.AddWithValue("@AuctionName", AuctionName);
                cmd.Parameters.AddWithValue("@AuctionDate", AuctionDate);
                cmd.Parameters.AddWithValue("@Reference", Lot);          
                cmd.Parameters.AddWithValue("@AuctionType", objShipping.auctionType);
                cmd.Parameters.AddWithValue("@ProductTitle", objShipping.title);
                cmd.Parameters.AddWithValue("@Image", objShipping.thumbnail);
                cmd.Parameters.AddWithValue("@PaymentMode", objShipping.PaymentType);
                cmd.Parameters.AddWithValue("@PaymentStatus", objShipping.PaymentStatus);
                cmd.Parameters.AddWithValue("@PaymentDetail", objShipping.PaymentDetails);
                cmd.Parameters.AddWithValue("@ServiceProvider", objShipping.ShippingServiceProvider);
                cmd.Parameters.AddWithValue("@AWB_Number", objShipping.AWBNumber);
                cmd.Parameters.AddWithValue("@CustomerName", objShipping.BillingName);
                cmd.Parameters.AddWithValue("@DeliveryAddress", objShipping.DeliveryAddress);
                cmd.Parameters.AddWithValue("@DeliveryStatus", objShipping.DeliveryStatus);
                cmd.Parameters.AddWithValue("@ExpectedDeliveryDate", objShipping.ExpectedDeliveryDate);
                cmd.Parameters.AddWithValue("@StatusDate", objShipping.StatusDate);

                cmd.Parameters.AddWithValue("@Email", objShipping.Email);
                cmd.Parameters.AddWithValue("@Mobile", objShipping.Mobile);
                cmd.Parameters.AddWithValue("@ArtistName", objShipping.ArtistName);

                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateShipping(Shipping objShipping, string AuctionName, string AuctionDate, string Lot)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_ShippingOrder"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objShipping.Id);
                cmd.Parameters.AddWithValue("@Auctionid", objShipping.Auctionid);
                cmd.Parameters.AddWithValue("@Productid", objShipping.Productid);
                cmd.Parameters.AddWithValue("@AuctionName", AuctionName);
                cmd.Parameters.AddWithValue("@AuctionDate", AuctionDate);
                cmd.Parameters.AddWithValue("@Reference", Lot);

                cmd.Parameters.AddWithValue("@AuctionType", objShipping.auctionType);
                cmd.Parameters.AddWithValue("@ProductTitle", objShipping.title);
                cmd.Parameters.AddWithValue("@Image", objShipping.thumbnail);
                cmd.Parameters.AddWithValue("@PaymentMode", objShipping.PaymentType);
                cmd.Parameters.AddWithValue("@PaymentStatus", objShipping.PaymentStatus);
                cmd.Parameters.AddWithValue("@PaymentDetail", objShipping.PaymentDetails);
                cmd.Parameters.AddWithValue("@ServiceProvider", objShipping.ShippingServiceProvider);
                cmd.Parameters.AddWithValue("@AWB_Number", objShipping.AWBNumber);
                cmd.Parameters.AddWithValue("@CustomerName", objShipping.BillingName);
                cmd.Parameters.AddWithValue("@DeliveryAddress", objShipping.DeliveryAddress);
                cmd.Parameters.AddWithValue("@DeliveryStatus", objShipping.DeliveryStatus);
                cmd.Parameters.AddWithValue("@ExpectedDeliveryDate", objShipping.ExpectedDeliveryDate);
                cmd.Parameters.AddWithValue("@StatusDate", objShipping.StatusDate);

                cmd.Parameters.AddWithValue("@Email", objShipping.Email);
                cmd.Parameters.AddWithValue("@Mobile", objShipping.Mobile);
                cmd.Parameters.AddWithValue("@ArtistName", objShipping.ArtistName);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }



        public ActionResult EditShipping(int? Id)
        {
            Shipping objShipping = new Shipping();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_ShippingOrder 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objShipping.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objShipping.Auctionid = int.Parse(dt.Rows[0]["Auctionid"].ToString());
                objShipping.Auctionname = dt.Rows[0]["AuctionName"].ToString();
                objShipping.auctiondate = dt.Rows[0]["AuctionDate"].ToString();
                objShipping.reference = dt.Rows[0]["Reference"].ToString();
                objShipping.auctionType = int.Parse(dt.Rows[0]["AuctionType"].ToString());
                objShipping.ArtistName = dt.Rows[0]["ArtistName"].ToString();
                objShipping.Productid = int.Parse(dt.Rows[0]["Productid"].ToString());
                objShipping.title = dt.Rows[0]["ProductTitle"].ToString();
                objShipping.thumbnail = dt.Rows[0]["Image"].ToString();
                objShipping.Mobile = dt.Rows[0]["Mobile"].ToString();
                objShipping.Email = dt.Rows[0]["Email"].ToString();
                objShipping.PaymentType = dt.Rows[0]["PaymentMode"].ToString();
                objShipping.PaymentStatus = dt.Rows[0]["PaymentStatus"].ToString();
                objShipping.PaymentDetails = dt.Rows[0]["PaymentDetail"].ToString();
                objShipping.ShippingServiceProvider = dt.Rows[0]["ServiceProvider"].ToString();
                objShipping.AWBNumber = dt.Rows[0]["AWB_Number"].ToString();
                objShipping.BillingName = dt.Rows[0]["CustomerName"].ToString();
                objShipping.DeliveryAddress = dt.Rows[0]["DeliveryAddress"].ToString();
                objShipping.DeliveryStatus = dt.Rows[0]["DeliveryStatus"].ToString();
                objShipping.ExpectedDeliveryDate = dt.Rows[0]["ExpectedDeliveryDate"].ToString();
                objShipping.StatusDate = dt.Rows[0]["StatusDate"].ToString();


                objShipping.AuctionNameList = BindShippingAuctionName(objShipping);
                objShipping.GridList = GetShippingList();

                ViewBag.ActionType = "Update";
                return View("Shipping", objShipping);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("Shipping", objShipping);
            }
        }


        public ActionResult DeleteShipping(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_ShippingOrder"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Shipping");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Shipping");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("Shipping");
                //throw;
            }

        }


        #endregion

    }

}