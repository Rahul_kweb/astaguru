﻿using Astaguru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Controllers
{
    public class FilterController : Controller
    {
        //
        // GET: /Filter/
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Filter(Auction AUC)
        {
            string substrArtist = string.Empty;
            string substrDept = string.Empty;
            string substrStyle = string.Empty;
            string substrMedium = string.Empty;
            string substrPrice = string.Empty;
            string MainQuery = string.Empty;

            if (AUC.chkArtist != null)
            {
                substrArtist = GenerateSubquery(AUC.chkArtist, "v2_defaultlots.artistid ");
            }
            if (AUC.chkDept != null)
            {
                substrDept = GenerateSubquery(AUC.chkDept, "v2_defaultlots.categoryid ");
            }
            if (AUC.chkMedium != null)
            {
                substrMedium = GenerateSubquery(AUC.chkMedium, "v2_defaultlots.mediumid ");
            }

            MainQuery = substrArtist + substrDept + substrMedium;
            MainQuery = MainQuery.Substring(0,MainQuery.Length - 4);
            Session["Subquery"] = MainQuery;

            Session["FilterArtist"] = AUC.chkArtist;
            Session["FilterDept"] = AUC.chkDept;
            Session["FilterMedium"] = AUC.chkMedium;

            return Json(new { Success = "Subquery Successfully Generated" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpcomingFilter(Auction AUC)
        {
            string substrArtist = string.Empty;
            string substrDept = string.Empty;
            string substrStyle = string.Empty;
            string substrMedium = string.Empty;
            string substrPrice = string.Empty;
            string MainQuery = string.Empty;

            if (AUC.chkArtist != null)
            {
                substrArtist = GenerateSubquery(AUC.chkArtist, "artist.artistid ");
            }
            if (AUC.chkDept != null)
            {
                substrDept = GenerateSubquery(AUC.chkDept, "category.categoryid ");
            }
            if (AUC.chkMedium != null)
            {
                substrMedium = GenerateSubquery(AUC.chkMedium, "medium.mediumid ");
            }

            MainQuery = substrArtist + substrDept + substrMedium;
            Session["SubqueryUpcoming"] = MainQuery;

            Session["FilterArtistUpcoming"] = AUC.chkArtist;
            Session["FilterDeptUpcoming"] = AUC.chkDept;
            Session["FilterMediumUpcoming"] = AUC.chkMedium;

            return Json(new { Success = "Subquery Successfully Generated" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PastFilter(Auction AUC)
        {
            string substrArtist = string.Empty;
            string substrDept = string.Empty;
            string substrStyle = string.Empty;
            string substrMedium = string.Empty;
            string substrPrice = string.Empty;
            string MainQuery = string.Empty;

            if (AUC.chkArtist != null)
            {
                substrArtist = GenerateSubquery(AUC.chkArtist, "artist.artistid ");
            }
            if (AUC.chkDept != null)
            {
                substrDept = GenerateSubquery(AUC.chkDept, "category.categoryid ");
            }
            if (AUC.chkMedium != null)
            {
                substrMedium = GenerateSubquery(AUC.chkMedium, "medium.mediumid ");
            }

            MainQuery = substrArtist + substrDept + substrMedium;
            Session["SubqueryPast"] = MainQuery;

            Session["FilterArtistPast"] = AUC.chkArtist;
            Session["FilterDeptPast"] = AUC.chkDept;
            Session["FilterMediumPast"] = AUC.chkMedium;

            return Json(new { Success = "Subquery Successfully Generated" }, JsonRequestBehavior.AllowGet);
        }



        public string GenerateSubquery(List<string> FilterList, string queryobj)
        {
            List<string> tempList = new List<string>();
            tempList = FilterList;
            string Subquery = string.Empty;
            // queryobj = FP.Artist_id;
            Subquery = string.Join(",", tempList);
            Subquery = Subquery.Replace(",", " OR " + queryobj + " = ");
            Subquery = "" + queryobj + "= " + Subquery;
            Subquery = "(" + Subquery + ") AND ";
            //Subquery = "(" + Subquery + ") ";
            return Subquery;
        }


        public ActionResult FilterClear(Auction AUC)
        {
            Session["Subquery"] = null;
            Session["FilterArtist"] = null;
            Session["FilterDept"] = null;
            Session["FilterMedium"] = null;
            return Json(new { Success = "Filter Cleared" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpcomingFilterClear(Auction AUC)
        {
            Session["SubqueryUpcoming"] = null;
            Session["FilterArtistUpcoming"] = null;
            Session["FilterDeptUpcoming"] = null;
            Session["FilterMediumUpcoming"] = null;
            return Json(new { Success = "Filter Cleared" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PastFilterClear(Auction AUC)
        {
            Session["SubqueryPast"] = null;
            Session["FilterArtistPast"] = null;
            Session["FilterDeptPast"] = null;
            Session["FilterMediumPast"] = null;
            return Json(new { Success = "Filter Cleared" }, JsonRequestBehavior.AllowGet);
        }

	}
}