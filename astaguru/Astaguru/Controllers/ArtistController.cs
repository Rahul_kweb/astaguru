﻿using Astaguru.Models;
using Astaguru.Services.UserService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Controllers
{
    public class ArtistController : Controller
    {
        Auction AUC = new Auction();
        ArtistService AS = new ArtistService();
        //
        // GET: /Artist/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult artistProfile(int artistid)
        {
            AUC = AS.getArtistProfile(artistid);
            return View("~/Views/User/artistProfile.cshtml", AUC);
        }

        [HttpPost]
        public JsonResult AutoArtist(string Prefix)
        {
            Prefix = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Prefix.ToLower());
            Auction Obj = new Auction();

            //Note : you can bind same list from database  
            List<Auction> ObjList = AS.getArtistList(); //  new List<Register>()  


            //    //Searching records from list using LINQ query  
            var ArtistName = (from N in ObjList
                              where N.name.Contains(Prefix)
                              select new { N.artistid, N.name });

            if (!ArtistName.Any())
            {
                return Json(new { Status = "Failed" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(ArtistName, JsonRequestBehavior.AllowGet);
            }
        }

	}
}