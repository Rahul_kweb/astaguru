﻿using Astaguru.Models;
using Astaguru.Services.UserService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Controllers
{
    public class BidController : Controller
    {

        CurrentAuctionService CAS = new CurrentAuctionService();
        Auction auc = new Auction();
        CommonController COMC = new CommonController();
        List<Auction> listAuction = new List<Auction>();
        UserService US = new UserService();

        UserController uscon = new UserController();


        //
        // GET: /Bid/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult saveBid(Auction AUC)
        {
            int browsercurrentbid = AUC.checknextvalidbid;    ///get current bid value of product
          
            try
            {
                int isOldUser = AUC.isOldUser;
                //int amountlimt = AUC.amountlimt;
                string country = AUC.country;

                int userid = Convert.ToInt32(Session["userid"]);   //COMC.checkUserSessionValue();
                int amountlimt = CAS.Getuserlimit(userid);

                if (userid > 0)
                {
                    int isPresentArtist = CAS.CheckArtistAssginedbidUserlist(AUC);
                    if (isPresentArtist > 0)
                    {

                    }
                    else
                    {
                        int addArtist = CAS.AddArtisttoBidUserList(AUC);
                    }

                    AUC = CAS.GetCurrentAuctionDetail(AUC.productid);

                        if (browsercurrentbid != AUC.pricers)
                        {
                            return Json(new { status = "Bidpriceincrease" }, JsonRequestBehavior.AllowGet);
                        }
                    
                                   

                    AUC.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(AUC.pricers, 1).ToString());
                    AUC.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);

                    // is user bid limit Amount Exceed or not

                    if (isOldUser == 0)
                    {
                        if (country == "India")
                        {
                            if (AUC.nextValidBidRs > amountlimt)
                            {
                                return Json(new { status = "AmountLimitExceeded" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            if (AUC.nextValidBidUs > amountlimt)
                            {
                                return Json(new { status = "AmountLimitExceeded" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }

                    // check owner of paint if its match then user can not bid
                    if (AUC.Ownerid == userid)
                    {
                        return Json(new { status = "Ownerpainting" }, JsonRequestBehavior.AllowGet);
                    }

         

                    // Add 3 Mins if bid closing time less than equal to 3 min

                    if (AUC.timeRemains <= 180)
                    {
                        CAS.UpdateBidClosingTime(AUC.productid);
                    }


                    // Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(AUC.priceus, 2).ToString());

                    listAuction = CAS.getBidRecordList(AUC);
                    AUC.userid = (int)Session["userid"];
                    if (listAuction.Count > 0)
                    {
                        foreach (var record in listAuction)
                        {
                            record.recentbid = 0;
                            if (record.userid == AUC.userid)
                            {
                                record.currentbid = 0;
                            }
                            CAS.updateBid(record);
                        }
                    }

                    AUC.currentbid = 1;
                    AUC.recentbid = 0;
                    AUC.nickname = Session["nickname"].ToString();
                    AUC.username = Session["username"].ToString();
                    string ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (string.IsNullOrEmpty(ipAddress))
                    {
                        AUC.ipAddress = Request.ServerVariables["REMOTE_ADDR"];
                    }

                    AUC.Bidrecordid = CAS.InsertBidRecord(AUC);
                

                    // Update Bid Limit Function
                    if (isOldUser == 0)
                    {
                        //created on 28-1-20
                        #region
                        var currentLeadinguser = CAS.Getcurrentleadinguser(AUC.productid);
                        if (AUC.userid == currentLeadinguser.userid)
                        {
                            int updateLimit = currentLeadinguser.amountlimt - currentLeadinguser.Bidpricers;
                            US.UpdateBidLimit(currentLeadinguser.userid, updateLimit);
                            Session["BidLimit"] = updateLimit;
                            int isleadcurrent = 1;
                            CAS.Isleading(AUC.productid, currentLeadinguser.userid, currentLeadinguser.Bidpricers, isleadcurrent);

                            Auction objAction = CAS.GetcurrentpriceuserAfterproxycheck(AUC.productid, AUC.userid);
                            if (objAction.ProxyAmt >= currentLeadinguser.Bidpricers)
                            {

                                if (AUC.userid != objAction.userid)
                                {


                                    int updateLimit1 = objAction.amountlimt + objAction.ProxyAmt;
                                    US.UpdateBidLimit(objAction.userid, updateLimit1);
                                    Session["BidLimit"] = updateLimit1;
                                    int islessproxy = 0;     // use for 1 time when proxyamount out bid from current ammount
                                    CAS.Updateislesssproxy(objAction.userid, AUC.productid, islessproxy);

                                    int isleadoutbid = 0;
                                    CAS.Isleadingproxy(AUC.productid, objAction.userid, objAction.ProxyAmt, isleadoutbid);
                                }

                            }
                            else if (objAction.ProxyAmt < currentLeadinguser.Bidpricers && objAction.ProxyAmt != 0 && objAction.islessproxy == 0)
                            {

                                if (AUC.userid != objAction.userid)
                                {
                                    int updateLimit1 = objAction.amountlimt + objAction.ProxyAmt;
                                    US.UpdateBidLimit(objAction.userid, updateLimit1);
                                    Session["BidLimit"] = updateLimit1;
                                    int islessproxy = 1;
                                    CAS.Updateislesssproxy(objAction.userid, AUC.productid, islessproxy);

                                    int isleadoutbid = 0;
                                    CAS.Isleadingproxy(AUC.productid, objAction.userid, objAction.ProxyAmt, isleadoutbid);
                                }

                            }

                            else
                            {
                                Auction objAction1 = CAS.Getcurrentpriceuser(AUC.productid, AUC.userid);
                                if (AUC.userid != objAction1.userid)
                                {
                                    var outbidpricers = CAS.getoutbidamount(AUC.productid, objAction1.userid);  // get outbid user

                                    int updateLimit1 = objAction1.amountlimt + outbidpricers;
                                    US.UpdateBidLimit(objAction1.userid, updateLimit1);

                                    int isleadoutbid = 0;
                                    CAS.Isleading(AUC.productid, objAction1.userid, outbidpricers, isleadoutbid);
                                }
                            }

                        }
                        else
                        {
                            //int returnamount = CAS.Getcurrentpriceuser(AUC.productid, AUC.userid);
                            //int updateLimit = amountlimt + returnamount;
                            //US.UpdateBidLimit(userid, updateLimit);
                            //Session["BidLimit"] = updateLimit;
                        }
                        #endregion
                        //int updateLimit = amountlimt - AUC.nextValidBidRs;
                        //US.UpdateBidLimit(userid, updateLimit);
                        //Session["BidLimit"] = updateLimit;
                    }
                    else
                    {
                        var currentLeadinguser = CAS.Getcurrentleadinguser(AUC.productid);

                        Auction objAction = CAS.GetcurrentpriceuserAfterproxycheck(AUC.productid, AUC.userid);
                        if (objAction.ProxyAmt >= currentLeadinguser.Bidpricers)
                        {

                            if (currentLeadinguser.userid != objAction.userid)
                            {


                                int updateLimit1 = objAction.amountlimt + objAction.ProxyAmt;
                                US.UpdateBidLimit(objAction.userid, updateLimit1);
                                Session["BidLimit"] = updateLimit1;
                                int islessproxy = 0;     // use for 1 time when proxyamount out bid from current ammount
                                CAS.Updateislesssproxy(objAction.userid, AUC.productid, islessproxy);

                                int isleadoutbid = 0;
                                CAS.Isleadingproxy(AUC.productid, objAction.userid, objAction.ProxyAmt, isleadoutbid);
                            }

                        }
                        else if (objAction.ProxyAmt < currentLeadinguser.Bidpricers && objAction.ProxyAmt != 0 && objAction.islessproxy == 0)
                        {

                            if (currentLeadinguser.userid != objAction.userid)
                            {
                                int updateLimit1 = objAction.amountlimt + objAction.ProxyAmt;
                                US.UpdateBidLimit(objAction.userid, updateLimit1);
                                Session["BidLimit"] = updateLimit1;
                                int islessproxy = 1;
                                CAS.Updateislesssproxy(objAction.userid, AUC.productid, islessproxy);

                                int isleadoutbid = 0;
                                CAS.Isleadingproxy(AUC.productid, objAction.userid, objAction.ProxyAmt, isleadoutbid);
                            }

                        }

                        else
                        {
                            Auction objAction1 = CAS.Getcurrentpriceuser(AUC.productid, AUC.userid);
                            if (currentLeadinguser.userid != objAction1.userid)
                            {
                                var outbidpricers = CAS.getoutbidamount(AUC.productid, objAction1.userid);  // get outbid user

                                int updateLimit1 = objAction1.amountlimt + outbidpricers;
                                US.UpdateBidLimit(objAction1.userid, updateLimit1);

                                int isleadoutbid = 0;
                                CAS.Isleading(AUC.productid, objAction1.userid, outbidpricers, isleadoutbid);
                            }
                        }
                    }

                    if (AUC.Bidrecordid > 0)
                    {
                        CAS.UpdateAcutionPrice(AUC);

                    }
                    else
                    {
                        //Failed
                    }

                    AUC.pricers = AUC.nextValidBidRs; // Current bid value 
                    AUC.priceus = AUC.nextValidBidUs;

                    AUC.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(AUC.nextValidBidRs, 1).ToString());
                    AUC.nextValidBidUs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(AUC.nextValidBidUs, 2).ToString());

                    Auction returnAuc = COMC.AddProxybidRecords(AUC);

                    if (returnAuc != null)
                    {
                        // Update Bid Limit Function
                        if (returnAuc.isOldUser == 0)
                        {
                            //created on 03-02-20
                            #region
                            var currentLeadinguser = CAS.GetCurrentLeadingProxyuser(AUC.productid);


                            if (returnAuc.userid == currentLeadinguser.userid)
                            {

                                int updateLimit = currentLeadinguser.amountlimt - currentLeadinguser.ProxyAmt;
                                US.UpdateBidLimit(currentLeadinguser.userid, updateLimit);
                                Session["BidLimit"] = updateLimit;
                                int isleadcurrent = 1;
                                CAS.Isleadingproxy(AUC.productid, currentLeadinguser.userid, currentLeadinguser.ProxyAmt, isleadcurrent);

                                Auction objAction = CAS.Getcurrentpriceuser(AUC.productid, returnAuc.userid);

                                if (returnAuc.userid != objAction.userid)
                                {
                                    int checkforprocyuser = CAS.checkforproxyuser(AUC.productid, objAction.userid);
                                    if (checkforprocyuser > 0)
                                    {
                                        int outbidproxyamount = CAS.getoutbidproxyamount(AUC.productid, objAction.userid);

                                        int updateLimit1 = objAction.amountlimt + outbidproxyamount;
                                        US.UpdateBidLimit(objAction.userid, updateLimit1);
                                        int isleadoutbid = 0;
                                        CAS.Isleadingproxy(AUC.productid, objAction.userid, outbidproxyamount, isleadoutbid);
                                    }
                                    else
                                    {
                                        int outbidpricers = CAS.getoutbidamount(AUC.productid, objAction.userid);

                                        int updateLimit1 = objAction.amountlimt + outbidpricers;
                                        US.UpdateBidLimit(objAction.userid, updateLimit1);
                                        int isleadoutbid = 0;
                                        CAS.Isleading(AUC.productid, objAction.userid, outbidpricers, isleadoutbid);
                                    }

                                }
                            }
                            else
                            {
                                //int returnamount = CAS.Getcurrentpriceuser(AUC.productid, AUC.userid);
                                //int updateLimit = amountlimt + returnamount;
                                //US.UpdateBidLimit(userid, updateLimit);
                                //Session["BidLimit"] = updateLimit;
                            }
                            #endregion
                            //int updateLimit = amountlimt - AUC.nextValidBidRs;
                            //US.UpdateBidLimit(userid, updateLimit);
                            //Session["BidLimit"] = updateLimit;
                        }

                        else
                        {

                            //var currentLeadinguser = CAS.GetCurrentLeadingProxyuser(AUC.productid);
                            Auction objAction = CAS.Getcurrentpriceuser(AUC.productid, returnAuc.userid);

                            if (returnAuc.userid != objAction.userid)
                            {
                                int checkforprocyuser = CAS.checkforproxyuser(AUC.productid, objAction.userid);
                                if (checkforprocyuser > 0)
                                {
                                    int outbidproxyamount = CAS.getoutbidproxyamount(AUC.productid, objAction.userid);

                                    int updateLimit1 = objAction.amountlimt + outbidproxyamount;
                                    US.UpdateBidLimit(objAction.userid, updateLimit1);
                                    int isleadoutbid = 0;
                                    CAS.Isleadingproxy(AUC.productid, objAction.userid, outbidproxyamount, isleadoutbid);
                                }
                                else
                                {
                                    int outbidpricers = CAS.getoutbidamount(AUC.productid, objAction.userid);

                                    int updateLimit1 = objAction.amountlimt + outbidpricers;
                                    US.UpdateBidLimit(objAction.userid, updateLimit1);
                                    int isleadoutbid = 0;
                                    CAS.Isleading(AUC.productid, objAction.userid, outbidpricers, isleadoutbid);
                                }

                            }
                        }

                        var currentLeadinguserproxy = CAS.Getcurrentleadinguser(AUC.productid);          // get current leading user for outbid  another
                        Auction objActionproxy = CAS.Getcurrentoutbiduser(AUC.productid, currentLeadinguserproxy.userid);    // get out bid user
                        if ((int)Session["userid"] != returnAuc.userid)
                        {
                            Session["OutProxyErr"] = "You have been out bid due to proxy. Kindly bid again.";

                            return Json(new { status = "Out" }, JsonRequestBehavior.AllowGet);
                        }

                        return Json(new { status = "TEMP" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        AUC.recentbid = 1;
                        CAS.updateRecentBid(AUC);
                    }


                    var currentLeadinguseroutbid = CAS.Getcurrentleadinguser(AUC.productid);          // get current leading user for outbid  another
                    Auction objActionoutbid = CAS.Getcurrentoutbiduser(AUC.productid, currentLeadinguseroutbid.userid);    // get out bid user
                    if (currentLeadinguseroutbid.userid != objActionoutbid.userid)
                    {
                        //Session["OutProxyErr"] = "have been out bid due to proxy. Kindly bid again.";
                        // CAll Mail Function                      /// uncomment on 2/03/2020
                        if (objActionoutbid.userid > 0)
                        {
                            AUC.mailPreprice = objActionoutbid.Bidpricers;
                            AUC.curprice = currentLeadinguseroutbid.Bidpricers;
                            AUC.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(AUC.curprice, 1).ToString());
                            AUC.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                            //int page= Convert.ToInt32(Request["page"].ToString());
                            COMC.sendMailOutbiduser(AUC, objActionoutbid.userid, objActionoutbid.username);

                            COMC.sendoutbidmsg(AUC.reference, objActionoutbid.name, objActionoutbid.mobile);

                        }
                        return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
                    }

                    // Add to My Auction Gallary 
                    addToGallary(auc);
                    return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { status = "SessionExpired" }, JsonRequestBehavior.AllowGet);

                }

            }
            catch (Exception Ex)
            {
                return Json(new { status = "Exception" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult saveProxyBid(Auction AUC)
        {
            try
            {
                int isOldUser = AUC.isOldUser;
                string country = AUC.country;

                //int amountlimt = AUC.amountlimt;
                int userid = Convert.ToInt32(Session["userid"]);   //COMC.checkUserSessionValue();
                int amountlimt = CAS.Getuserlimit(userid);

                if (userid > 0)
                {
                    auc = CAS.GetCurrentAuctionDetail(AUC.productid);


                    auc.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(auc.pricers, 1).ToString()); // Next Valid Bid
                    auc.nextValidBidUs = auc.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);

                    //Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(auc.priceus, 2).ToString()); // Next Valid Bid

                    auc.userid = (int)Session["userid"];
                    auc.ProxyAmt = AUC.ProxyAmt;
                    auc.ProxyAmtus = AUC.ProxyAmt / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// auc.nextValidBidUs; //AUC.ProxyAmt; // / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);


                    if (isOldUser == 0)
                    {
                        if (country == "India")
                        {
                            if (AUC.ProxyAmt > amountlimt)
                            {
                                return Json(new { status = "AmountLimitExceeded" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            if (auc.ProxyAmtus > amountlimt)
                            {
                                return Json(new { status = "AmountLimitExceeded" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }

                    // check owner of paint if its match then user can not bid
                    if (auc.Ownerid == userid)
                    {
                        return Json(new { status = "Ownerpainting" }, JsonRequestBehavior.AllowGet);
                    }


                    // Add 3 Mins if bid closing time less than equal to 3 min

                    if (auc.timeRemains <= 180)
                    {
                        CAS.UpdateBidClosingTime(AUC.productid);
                    }


                    auc.username = Session["username"].ToString();
                    int Proxyid = CAS.InsertProxyBidRecord(auc);
                    auc.currentbid = 1;
                    auc.recentbid = 0;
                    auc.proxy = 1;
                    auc.nickname = Session["nickname"].ToString();
                    AUC.Bidrecordid = CAS.InsertBidRecord(auc);

                    // Update Bid Limit Function
                    //if (isOldUser == 0)
                    //{
                    //    int updateLimit = amountlimt - AUC.ProxyAmt;
                    //    US.UpdateBidLimit(userid, updateLimit);
                    //    Session["BidLimit"] = updateLimit;
                    //}

                    if (AUC.Bidrecordid > 0)
                    {
                        CAS.UpdateAcutionPrice(auc);
                    }
                    else
                    {
                        //Failed
                    }
                    auc.pricers = auc.nextValidBidRs; // Current bid value 
                    auc.priceus = auc.nextValidBidUs;
                    auc.curprice = auc.nextValidBidRs;

                    auc.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(auc.nextValidBidRs, 1).ToString()); // Next Valid Bid
                    auc.nextValidBidUs = auc.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(auc.nextValidBidUs, 2).ToString()); // Next Valid Bid

                    Auction returnAuc = COMC.AddProxybidRecords(auc);

                    if (returnAuc != null)
                    {

                        Auction proxyinfosame = CAS.Getproxyinfosame(auc.Online);

                        if (proxyinfosame != null)
                        {
                            if (returnAuc.userid != proxyinfosame.userid)
                            {
                                Auction bidrecord = new Auction();
                                bidrecord.firstname = proxyinfosame.firstname;
                                bidrecord.lastname = proxyinfosame.lastname;
                                bidrecord.thumbnail = proxyinfosame.thumbnail;
                                bidrecord.productid = returnAuc.productid;
                                bidrecord.nextValidBidRs = returnAuc.nextValidBidRs;
                                bidrecord.nextValidBidUs = returnAuc.nextValidBidUs; // Convert.ToInt32(returnAuc.Bidpricers) / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                                bidrecord.pricers = returnAuc.nextValidBidRs;
                                bidrecord.priceus = returnAuc.nextValidBidUs; //Convert.ToInt32(proxyinfosame.ProxyAmt) / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                                //bidrecord.daterec = DateTime.Now;
                                bidrecord.reference = proxyinfosame.reference;
                                bidrecord.nickname = proxyinfosame.nickname;
                                bidrecord.username = proxyinfosame.username;
                                bidrecord.currentbid = 1;
                                bidrecord.recentbid = 1;
                                bidrecord.userid = proxyinfosame.userid;
                                bidrecord.Auctionid = auc.Online;
                                bidrecord.proxy = 1;
                                // bidrecord.earlyproxy = 1;

                                //Auction bidRecord = new Auction();
                                CAS.InsertBidRecord(bidrecord);

                            }

                        }

                        // Send Email
                        if (AUC.LastBidId > 0)
                        {
                            if (returnAuc.userid != AUC.LastBidId)
                            {
                                returnAuc.LastBidId = AUC.LastBidId;
                                returnAuc.productid = auc.productid;
                                returnAuc.curpriceUs = returnAuc.curprice / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);// Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(auc.nextValidBidUs, 2).ToString());
                                COMC.sendMailOutbid(returnAuc);
                            }
                        }

                    }
                    else
                    {
                        AUC.recentbid = 1;
                        CAS.updateRecentBid(AUC);
                    }

                    // Update Bid Limit Function
                    //created on 28-1-20
                    //#region
                    if (isOldUser == 0)
                    {
                        var currentLeadinguser = CAS.GetCurrentLeadingProxyuser(AUC.productid);


                        if (AUC.userid == currentLeadinguser.userid)
                        {

                            //int updateLimit = currentLeadinguser.amountlimt - currentLeadinguser.ProxyAmt;

                            //int updateLimit = currentLeadinguser.amountlimt - currentLeadinguser.Bidpricers;

                            int updateLimit = currentLeadinguser.amountlimt - currentLeadinguser.ProxyAmt;
                            US.UpdateBidLimit(currentLeadinguser.userid, updateLimit);
                            Session["BidLimit"] = updateLimit;
                            int isleadcurrent = 1;
                            CAS.Isleadingproxy(AUC.productid, currentLeadinguser.userid, currentLeadinguser.ProxyAmt, isleadcurrent);

                            Auction objAction = CAS.Getcurrentpriceuser(AUC.productid, AUC.userid);

                            if (AUC.userid != objAction.userid)
                            {
                                int checkforprocyuser = CAS.checkforproxyuser(AUC.productid, objAction.userid);
                                if (checkforprocyuser > 0)
                                {
                                    int outbidproxyamount = CAS.getoutbidproxyamount(AUC.productid, objAction.userid);

                                    int updateLimit1 = objAction.amountlimt + outbidproxyamount;
                                    US.UpdateBidLimit(objAction.userid, updateLimit1);
                                    int isleadoutbid = 0;
                                    CAS.Isleadingproxy(AUC.productid, objAction.userid, outbidproxyamount, isleadoutbid);
                                }
                                else
                                {
                                    int outbidpricers = CAS.getoutbidamount(AUC.productid, objAction.userid);

                                    int updateLimit1 = objAction.amountlimt + outbidpricers;
                                    US.UpdateBidLimit(objAction.userid, updateLimit1);
                                    int isleadoutbid = 0;
                                    CAS.Isleading(AUC.productid, objAction.userid, outbidpricers, isleadoutbid);
                                }

                            }
                        }
                    }
                    else
                    {
                        var currentLeadinguser = CAS.Getcurrentleadinguser(AUC.productid);
                        Auction objAction = CAS.Getcurrentpriceuser(AUC.productid, AUC.userid);

                        if (currentLeadinguser.userid != objAction.userid)
                        {
                            int checkforprocyuser = CAS.checkforproxyuser(AUC.productid, objAction.userid);
                            if (checkforprocyuser > 0)
                            {
                                int outbidproxyamount = CAS.getoutbidproxyamount(AUC.productid, objAction.userid);

                                int updateLimit1 = objAction.amountlimt + outbidproxyamount;
                                US.UpdateBidLimit(objAction.userid, updateLimit1);
                                int isleadoutbid = 0;
                                CAS.Isleadingproxy(AUC.productid, objAction.userid, outbidproxyamount, isleadoutbid);
                            }
                            else
                            {
                                int outbidpricers = CAS.getoutbidamount(AUC.productid, objAction.userid);

                                int updateLimit1 = objAction.amountlimt + outbidpricers;
                                US.UpdateBidLimit(objAction.userid, updateLimit1);
                                int isleadoutbid = 0;
                                CAS.Isleading(AUC.productid, objAction.userid, outbidpricers, isleadoutbid);
                            }

                        }
                    }


                    var currentLeadinguseroutbid = CAS.Getcurrentleadinguser(AUC.productid);          // get current leading user for outbid  another
                    Auction objActionoutbid = CAS.Getcurrentoutbiduser(AUC.productid, currentLeadinguseroutbid.userid);    // get out bid user

                    //if ((int)Session["userid"] != currentLeadinguseroutbid.userid)
                    //{
                    //    if (objActionoutbid.userid > 0)
                    //    {
                    //        Session["OutProxyErr"] = "You have been out bid due to proxy. Kindly bid again.";
                    //        return Json(new { status = "Out" }, JsonRequestBehavior.AllowGet);
                    //    }

                    //}

                    if (currentLeadinguseroutbid.userid != objActionoutbid.userid)
                    {
                        //Session["OutProxyErr"] = "have been out bid due to proxy. Kindly bid again.";
                        // CAll Mail Function                      /// uncomment on 2/03/2020
                        if (objActionoutbid.userid > 0)
                        {
                            AUC.mailPreprice = objActionoutbid.Bidpricers;
                            AUC.curprice = currentLeadinguseroutbid.Bidpricers;
                           AUC.reference = auc.reference;
                            AUC.title = auc.title;
                            AUC.thumbnail = auc.thumbnail;

                            AUC.nextValidBidRs = Convert.ToInt32(Astaguru.Services.Common.getNextValidBidAmount(AUC.curprice, 1).ToString());
                            AUC.nextValidBidUs = AUC.nextValidBidRs / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CurrentDollarRate"]);
                            //int page = Convert.ToInt32(Request["page"].ToString());
                            COMC.sendMailOutbiduser(AUC, objActionoutbid.userid, objActionoutbid.username);

                            COMC.sendoutbidmsg(AUC.reference, objActionoutbid.name, objActionoutbid.mobile);
                        }

                        return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
                    }

                    // Add to My Auction Gallary 
                    addToGallary(AUC);
                    return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { status = "SessionExpired" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception Ex)
            {
                return Json(new { status = "Exception" }, JsonRequestBehavior.AllowGet);
            }

        }


        [HttpPost]
        public ActionResult addToGallary(Auction AUC)
        {
            try
            {
                int isPresentArtist = CAS.CheckArtistAssginedbidUserlist(AUC);

                if (isPresentArtist > 0)
                {
                    return Json(new { status = "Error" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    int addArtist = CAS.AddArtisttoBidUserList(AUC);
                    return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
                }
                //return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Exception" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult DeleteFromGallary(Auction AUC)
        {
            try
            {
                int Result = CAS.DeleteArtisttoBidUserList(AUC);
                return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
            {
                return Json(new { status = "Error" }, JsonRequestBehavior.AllowGet);
            }

        }

    }
}