﻿using Astaguru.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Controllers
{
    public class MobileAppController : Controller
    {
        Utility util = new Utility();

        // GET: MobileApp
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult AboutUs()
        {
            var data = CMS(1);
            TempData["desc"] = data;
            return View();
        }

        public ActionResult Services()
        {

            int pageid = 1;
            ViewBag.Services = GetPageCMSMaster(pageid);
            return View();
        }

        public ActionResult valuation()
        {

            ViewBag.Valuation = CMS(3);
            return View();
        }


        public ActionResult howtobuy()
        {
            int Pageid = 3;
            ViewBag.howtobuy = GetPageCMSMaster(Pageid);
            return View();
        }

        public ActionResult howToSell()
        {
            int Pageid = 4;
            ViewBag.howToSell = GetPageCMSMaster(Pageid);
            return View();
        }

        public ActionResult contactUs()
        {

            return View();
        }


        public ActionResult Careers()
        {

            ViewBag.CareerDesc = CMS(2);
            ViewBag.CurrentVacancy = CurrentVacancy();
            return View();
        }



        public string CurrentVacancy()
        {
            string response = "";
            StringBuilder strbuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Current_Vacancy 'get'");
            int Count = 1;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (Count == 1)
                    {
                        strbuild.Append("<div class='card'>");
                        strbuild.Append("<div class='card-header' id='headingOne'>");
                        strbuild.Append("<h5 class='mb-0'>");
                        strbuild.Append("<button class='btn btn-link' type='button' data-toggle='collapse' data-target='#" + Generateslg(dr["Job_Title"].ToString()) + "' aria-expanded='true' aria-controls='collapseOne'>");
                        strbuild.Append(dr["Job_Title"].ToString());
                        strbuild.Append("</button>");
                        strbuild.Append("</h5>");
                        strbuild.Append("</div>");
                        strbuild.Append("<div id=" + Generateslg(dr["Job_Title"].ToString()) + " class='collapse show' aria-labelledby='headingOne' data-parent='#accordionExample'>");
                        strbuild.Append("<div class='card-body'>");
                        strbuild.Append("<h5 class='sbold black'>Job Title:</h5>");
                        strbuild.Append("<p>" + dr["Job_Title"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Business Unit:</h5>");
                        strbuild.Append("<p>" + dr["Business_Unit"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Job Responsibility:</h5>");
                        strbuild.Append("<p>" + dr["Job_Responsibility"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Responsibilities:</h5>");
                        strbuild.Append(dr["Responsibilities"].ToString());
                        strbuild.Append("<h5 class='sbold black'>Functional Skills:</h5>");
                        strbuild.Append(dr["Functional_Skills"].ToString());
                        strbuild.Append("<h5 class='sbold black'>Technical Skills:</h5>");
                        strbuild.Append(dr["Technical_skills"].ToString());
                        strbuild.Append("<h5 class='sbold black'>Required Qualification:</h5>");
                        strbuild.Append("<p>" + dr["Qualification"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Salary:</h5>");
                        strbuild.Append("<p>" + dr["Salary"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Experience:</h5>");
                        strbuild.Append("<p>" + dr["Experience"].ToString() + "</p>");
                        strbuild.Append("<a href='#apply_now_form' class='a_scroll btn_grey_dark mobile_only extra-margin-top-20'><span>Apply Now</span></a>");
                        strbuild.Append("</div>");
                        strbuild.Append("</div>");
                        strbuild.Append("</div>");
                    }
                    else
                    {
                        strbuild.Append("<div class='card'>");
                        strbuild.Append("<div class='card-header' id='headingTwo'>");
                        strbuild.Append("<h5 class='mb-0'>");
                        strbuild.Append("<button class='btn btn-link collapsed' type='button' data-toggle='collapse' data-target='#" + Generateslg(dr["Job_Title"].ToString()) + "' aria-expanded='false' aria-controls='collapseTwo'>");
                        strbuild.Append(dr["Job_Title"].ToString());
                        strbuild.Append("</button>");
                        strbuild.Append("</h5>");
                        strbuild.Append("</div>");
                        strbuild.Append("<div id=" + Generateslg(dr["Job_Title"].ToString()) + " class='collapse' aria-labelledby='headingTwo' data-parent='#accordionExample'>");
                        strbuild.Append("<div class='card-body'>");
                        strbuild.Append("<h5 class='sbold black'>Job Title:</h5>");
                        strbuild.Append("<p>" + dr["Job_Title"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Business Unit:</h5>");
                        strbuild.Append("<p>" + dr["Business_Unit"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Job Responsibility:</h5>");
                        strbuild.Append("<p>" + dr["Job_Responsibility"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Responsibilities:</h5>");
                        strbuild.Append(dr["Responsibilities"].ToString());
                        strbuild.Append("<h5 class='sbold black'>Functional Skills:</h5>");
                        strbuild.Append(dr["Functional_Skills"].ToString());
                        strbuild.Append("<h5 class='sbold black'>Technical Skills:</h5>");
                        strbuild.Append(dr["Technical_skills"].ToString());
                        strbuild.Append("<h5 class='sbold black'>Required Qualification:</h5>");
                        strbuild.Append("<p>" + dr["Qualification"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Salary:</h5>");
                        strbuild.Append("<p>" + dr["Salary"].ToString() + "</p>");
                        strbuild.Append("<h5 class='sbold black'>Experience:</h5>");
                        strbuild.Append("<p>" + dr["Experience"].ToString() + "</p>");
                        strbuild.Append("<a href='#apply_now_form' class='a_scroll btn_grey_dark mobile_only extra-margin-top-20'><span>Apply Now</span></a>");
                        strbuild.Append("</div>");
                        strbuild.Append("</div>");
                        strbuild.Append("</div>");
                    }

                    Count++;
                }

            }

            response = strbuild.ToString();
            return response;
        }


        public ActionResult privacyPolicy()
        {

            ViewBag.Policy = CMS(4);
            return View();
        }

        public ActionResult TermsAndConditions()
        {
            int pageid = 2;
            ViewBag.TermsAndConditions = GetPageCMSMaster(pageid);
            return View();
        }

        public ActionResult Departments()
        {

            return View();
        }

        public ActionResult deptEstate()
        {

            return View();
        }

        public ActionResult deptFine()
        {

            return View("");
        }

        public ActionResult deptFurniture()
        {
            return View();
        }

        public ActionResult deptIndian()
        {

            return View();
        }

        public ActionResult deptModernArt()
        {

            return View();
        }

        public ActionResult deptJewellery()
        {

            return View();
        }

        public ActionResult deptMemorabilia()
        {

            return View();
        }

        public ActionResult deptNumismatic()
        {

            return View();
        }

        public ActionResult deptBooks()
        {

            return View();
        }

        public ActionResult deptScience()
        {

            return View();
        }

        public ActionResult deptSoutheast()
        {

            return View();
        }


        public ActionResult deptTexilesView1()
        {

            return View();
        }

        public ActionResult Timepieces()
        {
            return View();
        }

        public ActionResult deptClassic()
        {

            return View();
        }


        #region video
        public ActionResult video()
        {

            Video obj = new Video();
            obj.CategoryList = BindcategoryforVideo(obj);
            return View(obj);
        }

        [HttpPost]
        public ActionResult video(Video obj)
        {



            if (obj.categoryid != 0)
            {
                //ViewBag.categoryid = Catid[Catid.Length - 1];
                ViewBag.category = obj.categoryid;


            }


            obj.CategoryList = BindcategoryforVideo(obj);

            return View(obj);


        }               

        public List<SelectListItem> BindcategoryforVideo(Video obj)
        {
            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_video 'Getcategoryforvideo'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["category"].ToString();
                    list.Value = dr["categoryid"].ToString();

                    if (obj.categoryid == int.Parse(dr["categoryid"].ToString()))
                    {
                        list.Selected = true;
                    }

                    objList.Add(list);
                }

            }
            return objList.ToList();
        }

        #endregion


        #region media
        public ActionResult media()
        {

            StringBuilder strbuild = new StringBuilder();

            DataSet ds = new DataSet();
            ds = util.Display1("Exec Proc_MediaNews 'bindMediaNews'");
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        strbuild.Append("<div class='row py-3 px-1 bg-light mb-5 aos-item' data-aos='fade-up'>");
                        strbuild.Append("<div class='col-lg-4 imageData'>");
                        if (dr["Type"].ToString() == "Image")
                        {
                            strbuild.Append("<img src='/Content/uploads/MediaNews/" + dr["Image"].ToString() + "' class='img-fluid'>");
                        }
                        else
                        {
                            strbuild.Append("<iframe src=" + dr["VideoUrl"].ToString() + " frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>");
                        }

                        strbuild.Append("</div>");
                        strbuild.Append("<div class='col-lg-8 matter'>");
                        strbuild.Append("<h2>" + dr["Title"].ToString() + "</h2>");
                        strbuild.Append("<div class='date'><i class='fa fa-clock-o' aria-hidden='true'></i> " + dr["Date"].ToString() + "  /  " + dr["CreatedBy"].ToString() + " </div>");
                        strbuild.Append(dr["Description"].ToString());
                        strbuild.Append("<a href='/mediadetailMob/" + dr["Slug"].ToString() + "' class='readMore'>Read More</a>");

                        strbuild.Append("</div>");
                        strbuild.Append("</div>");




                    }
                }
            }



            ViewBag.media = strbuild.ToString();
            return View();
        }


        [HttpPost]
        public ActionResult media(FormCollection fobj)
        {

            string value = fobj["hiddenSearchvalue"].ToString();

            StringBuilder strbuild = new StringBuilder();

            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_MediaNews 'bindMediaNewsbySearch',0,'','','','','','','','','" + value + "'");
            if (dt.Rows.Count > 0)
            {


                foreach (DataRow dr in dt.Rows)
                {

                    strbuild.Append("<div class='row py-3 px-1 bg-light mb-5 aos-item' data-aos='fade-up'>");
                    strbuild.Append("<div class='col-lg-4 imageData'>");
                    if (dr["Type"].ToString() == "Image")
                    {
                        strbuild.Append("<img src='/Content/uploads/MediaNews/" + dr["Image"].ToString() + "' class='img-fluid'>");
                    }
                    else
                    {
                        strbuild.Append("<iframe src=" + dr["VideoUrl"].ToString() + " frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>");
                    }

                    strbuild.Append("</div>");
                    strbuild.Append("<div class='col-lg-8 matter'>");
                    strbuild.Append("<h2>" + dr["Title"].ToString() + "</h2>");
                    strbuild.Append("<div class='date'><i class='fa fa-clock-o' aria-hidden='true'></i> " + dr["Date"].ToString() + "  /  " + dr["CreatedBy"].ToString() + " </div>");
                    strbuild.Append(dr["Description"].ToString());
                    strbuild.Append("<a href='/mediadetail/" + dr["Slug"].ToString() + "' class='readMore'>Read More</a>");

                    strbuild.Append("</div>");
                    strbuild.Append("</div>");


                }

            }

            else
            {
                strbuild.Append("<h3 class='text-center'>");
                strbuild.Append("No data found");
                strbuild.Append("<h3>");
            }



            ViewBag.media = strbuild.ToString();
            return View();

        }

        public ActionResult mediadetailMob(string strNews)
        {
            var objNews = Bindmediadetail(strNews);
            ViewBag.mediadetail = objNews["NewsDetail"].ToString();
            return View();
        }

        public Dictionary<string, string> Bindmediadetail(string strNews)
        {
            Dictionary<string, string> objNews = new Dictionary<string, string>();

            StringBuilder strBuild = new StringBuilder();
            //StringBuilder strType = new StringBuilder();

            StringBuilder strMetaTitle = new StringBuilder();



            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_MediaNews 'bindMediaNewsDeatils',0,'','','','','','" + strNews + "'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strMetaTitle.Append(dr["Slug"].ToString());

                    strBuild.Append("<div class='container pt-4'>");
                    strBuild.Append("<div class='row align-items-center justify-content-center mb-4'>");
                    strBuild.Append("<div class='col-12  col-lg-10 mb-4'>");
                    strBuild.Append("<h1 class='bigHeading'>");
                    strBuild.Append(dr["Title"].ToString());
                    strBuild.Append("</h1>");
                    strBuild.Append("</div>");
                    strBuild.Append("<div class='col-12 col-lg-10'>");
                    strBuild.Append("<div class='author'>By <span>" + dr["CreatedBy"].ToString() + "</span></div>");
                    strBuild.Append("<div class='date'><i class='fa fa-clock-o' aria-hidden='true'></i>" + dr["Date"].ToString() + "</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("<div class='border-bottom-0 border mb-5'></div>");
                    strBuild.Append("<div class='container'>");
                    strBuild.Append("<div class='row justify-content-center mb-5'>");
                    strBuild.Append("<div class='col-12 col-lg-10 text-center'>");
                    strBuild.Append("<div class='mb-4 blogBanner'>");
                    if (dr["Type"].ToString() == "Image")
                    {
                        strBuild.Append("<img src='/Content/uploads/MediaNews/" + dr["Image"].ToString() + "'>");
                    }
                    else
                    {
                        strBuild.Append("<iframe src=" + dr["VideoUrl"].ToString() + " frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>");
                    }
                    //strBuild.Append("<img src='images/blog/blog_banner.jpg'>");
                    strBuild.Append("</div>");
                    strBuild.Append(dr["Description"].ToString());
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("<div class='border-bottom-0 border mb-5'></div>");


                    strBuild.Append("<div class='row mb-5 justify-content-center'>");
                    strBuild.Append("<div class='col-12 col-lg-10 social_share'>");
                    //strBuild.Append("<a href='#' class='share'><i class='fa fa-share-alt' aria-hidden='true'></i></a>");
                    //strBuild.Append("<a class='fb' target='_blank' href='http://www.facebook.com/sharer/sharer.php?u="+ Request.Url.ToString()+"'><i class='fa fa-facebook' aria-hidden='true'></i></a>");
                    strBuild.Append("<a href='#' class='fb'><i class='fa fa-facebook' aria-hidden='true'></i></a>");
                    strBuild.Append("<a href='#' class='twit'><i class='fa fa-twitter' aria-hidden='true'></i></a>");
                    strBuild.Append("<a href='#' class='pinterest'><i class='fa fa-pinterest-p' aria-hidden='true'></i></a>");
                    strBuild.Append("<a href='#' class='whatsApp'><i class='fa fa-whatsapp' aria-hidden='true'></i></a>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");

                    strBuild.Append("</div>");
                }

            }
            //objNews.Add("Type", strType.ToString());
            objNews.Add("NewsDetail", strBuild.ToString());

            //objNews.Add("MetatTitle", strMetaTitle.ToString());

            return objNews;
        }

        #endregion


        #region Blog
        public ActionResult blog()
        {

            Blog obj = new Blog();
            obj.CategoryList = BindcategoryforBlog(obj);

            return View(obj);


        }

        [HttpPost]
        public ActionResult blog(Blog obj)
        {



            if (obj.categoryid != 0)
            {
                //ViewBag.categoryid = Catid[Catid.Length - 1];
                ViewBag.category = obj.categoryid;


            }


            obj.CategoryList = BindcategoryforBlog(obj);

            return View(obj);


        }

        public List<SelectListItem> BindcategoryforBlog(Blog obj)
        {
            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_Blog 'Getcategoryforblog'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["category"].ToString();
                    list.Value = dr["categoryid"].ToString();

                    if (obj.categoryid == int.Parse(dr["categoryid"].ToString()))
                    {
                        list.Selected = true;
                    }

                    objList.Add(list);
                }

            }
            return objList.ToList();
        }
        public ActionResult blogdetailpageMob(string strblog)
        {

            var obj = Bindblogdetailpage(strblog);
            ViewBag.blogdetailpage = obj["Blogdetails"].ToString();
            return View();
        }

        public Dictionary<string, string> Bindblogdetailpage(string strblog)
        {
            Dictionary<string, string> objNews = new Dictionary<string, string>();

            StringBuilder strBuild = new StringBuilder();
            //StringBuilder strType = new StringBuilder();

            StringBuilder strMetaTitle = new StringBuilder();
            //StringBuilder strrecent = new StringBuilder();

            DataSet ds = new DataSet();
            ds = util.Display1("Exec Proc_Blog 'bindBlogbyId',0,'','','','','" + strblog + "'");
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        strMetaTitle.Append(dr["Slug"].ToString());

                        strBuild.Append("<div class='container pt-4'>");
                        strBuild.Append("<div class='row align-items-center justify-content-center mb-4'>");
                        strBuild.Append("<div class='col-12  col-lg-10 mb-4'>");
                        strBuild.Append("<h1 class='bigHeading'>");
                        strBuild.Append(dr["Title"].ToString());
                        strBuild.Append("</h1>");
                        strBuild.Append("</div>");
                        strBuild.Append("<div class='col-12 col-lg-10'>");
                        strBuild.Append("<div class='author'>By <span>" + dr["CreatedBy"].ToString() + "</span></div>");
                        strBuild.Append("<div class='date'><i class='fa fa-clock-o' aria-hidden='true'></i>" + dr["PostDate"].ToString() + "</div>");
                        strBuild.Append("</div>");
                        strBuild.Append("</div>");
                        strBuild.Append("</div>");
                        strBuild.Append("<div class='border-bottom-0 border mb-5'></div>");
                        strBuild.Append("<div class='container'>");
                        strBuild.Append("<div class='row justify-content-center mb-5'>");
                        strBuild.Append("<div class='col-12 col-lg-10 text-center'>");
                        strBuild.Append("<div class='mb-4 blogBanner'>");
                        strBuild.Append("<img src='/Content/uploads/Blog/" + dr["Image"].ToString() + "'>");
                        //strBuild.Append("<img src='images/blog/blog_banner.jpg'>");
                        strBuild.Append("</div>");
                        strBuild.Append(dr["Description"].ToString());
                        strBuild.Append("</div>");
                        strBuild.Append("</div>");
                        strBuild.Append("<div class='border-bottom-0 border mb-5'></div>");


                        strBuild.Append("<div class='row mb-5 justify-content-center'>");
                        strBuild.Append("<div class='col-12 col-lg-10 social_share'>");
                        //strBuild.Append("<a href='#' class='share'><i class='fa fa-share-alt' aria-hidden='true'></i></a>");
                        strBuild.Append("<a href='#' class='fb'><i class='fa fa-facebook' aria-hidden='true'></i></a>");
                        strBuild.Append("<a href='#' class='twit'><i class='fa fa-twitter' aria-hidden='true'></i></a>");
                        strBuild.Append("<a href='#' class='pinterest'><i class='fa fa-pinterest-p' aria-hidden='true'></i></a>");
                        strBuild.Append("<a href='#' class='whatsApp'><i class='fa fa-whatsapp' aria-hidden='true'></i></a>");
                        strBuild.Append("</div>");
                        strBuild.Append("</div>");


                        int categoryid = int.Parse(dr["Categoryid"].ToString());
                        DataTable dt = new DataTable();
                        dt = util.Display("Exec Proc_Blog 'GetRecentblogBycategory',0,'','','','','" + strblog + "'," + categoryid + "");
                        if (dt.Rows.Count > 0)
                        {
                            strBuild.Append("<div class='row justify-content-center pt-5'>");
                            foreach (DataRow drrecent in dt.Rows)
                            {
                                strBuild.Append("<div class='col-sm-6 col-lg-4 mb-5'>");
                                strBuild.Append("<a href='/blogdetailpageMob/" + drrecent["Slug"].ToString() + "' class='rounded borderLight box fadeAnim'>");
                                strBuild.Append("<div class='imageData'>");
                                strBuild.Append("<img src='/Content/uploads/Blog/" + drrecent["Image"].ToString() + "'' class='img-fluid fadeAnim'>");
                                strBuild.Append("</div>");
                                strBuild.Append("<div class='matter p-4'>");
                                strBuild.Append("<h2>Lorem Ipsum is simply dummy text of the printing </h2>");
                                strBuild.Append("<div class='date'><i class='fa fa-clock-o' aria-hidden='true'></i> " + dr["PostDate"].ToString() + "</div>");
                                strBuild.Append(dr["Description"].ToString());
                                strBuild.Append("</div>");
                                strBuild.Append("<div class='authorName'>");
                                strBuild.Append(dr["CreatedBy"].ToString());
                                strBuild.Append("</div>");
                                strBuild.Append("</a>");
                                strBuild.Append("</div>");
                            }
                            strBuild.Append("</div>");
                        }

                        strBuild.Append("</div>");

                    }

                }

            }
            //objNews.Add("Type", strType.ToString());
            objNews.Add("Blogdetails", strBuild.ToString());

            //objNews.Add("MetatTitle", strMetaTitle.ToString());

            return objNews;
        }


        #endregion


        #region Faq

        public ActionResult faq()
        {

            var obj = GetFaq();
            ViewBag.Menu = obj["Menu"].ToString();
            ViewBag.Faqdetail = obj["Faqetails"].ToString();
            return View();
        }

        public Dictionary<string, string> GetFaq()
        {
            Dictionary<string, string> objfaq = new Dictionary<string, string>();

            StringBuilder strmenu = new StringBuilder();
            StringBuilder strbuild = new StringBuilder();

            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Faq 'getCategory'");

            if (dt.Rows.Count > 0)
            {
                int count = 1;

                foreach (DataRow dr in dt.Rows)
                {

                    if (count == 1)
                    {
                        strmenu.Append("<li class='active'><a href='#" + dr["Category"] + "'>" + dr["Category"] + "</a></li>");
                        count++;
                    }
                    else
                    {
                        strmenu.Append("<li><a href='#" + dr["Category"] + "'>" + dr["Category"] + "</a></li>");
                    }


                    strbuild.Append("<div id=" + dr["Category"].ToString() + ">");
                    strbuild.Append("<h2 class='text-uppercase'>" + dr["Category"].ToString() + "</h2>");
                    strbuild.Append("<ul>");
                    DataTable dt1 = new DataTable();
                    dt1 = util.Display("Exec Proc_Faq 'bindFaq',0,'" + dr["Category"].ToString() + "'");
                    if (dt1.Rows.Count > 0)
                    {
                        int countNew = 1;
                        foreach (DataRow dr1 in dt1.Rows)
                        {

                            if (countNew == 1 & dr["Category"].ToString() == "Buyer")
                            {
                                strbuild.Append("<li class='active'>");
                                strbuild.Append("<a class='head'>" + dr1["Title"].ToString() + "</a>");
                                strbuild.Append("<div>");
                                strbuild.Append(dr1["description"].ToString());
                                strbuild.Append("</div>");
                                strbuild.Append("</li>");



                            }
                            else
                            {
                                strbuild.Append("<li>");
                                strbuild.Append("<a class='head'>" + dr1["Title"].ToString() + "</a>");
                                strbuild.Append("<div>");
                                strbuild.Append(dr1["description"].ToString());
                                strbuild.Append("</div>");
                                strbuild.Append("</li>");

                            }
                            countNew++;
                        }

                    }

                    strbuild.Append("</ul>");
                    strbuild.Append("</div>");



                }
            }


            objfaq.Add("Menu", strmenu.ToString());
            objfaq.Add("Faqetails", strbuild.ToString());


            return objfaq;
        }


        #endregion


        public string GetPageCMSMaster(int id)
        {
            string response = "";
            StringBuilder strbuild = new StringBuilder();

            DataSet ds = new DataSet();
            ds = util.Display1("Exec Proc_PageCMSMaster 'get',''," + id + "");
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    int count = 1;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (count == 1)
                        {
                            strbuild.Append("<div class='card'>");
                            strbuild.Append(" <div class='card-header' id='headingOne'>");

                            strbuild.Append("<h5 class='mb-0'>");
                            strbuild.Append("<button class='btn btn-link' type='button' data-toggle='collapse' data-target='#" + Generateslg(dr["Title"].ToString()) + "' aria-expanded='true' aria-controls='collapseOne'>");
                            strbuild.Append(dr["Title"].ToString());
                            strbuild.Append("</button>");
                            strbuild.Append(" </h5>");
                            strbuild.Append("</div>");
                            strbuild.Append("<div id = '" + Generateslg(dr["Title"].ToString()) + "' class='collapse show' aria-labelledby='headingOne' data-parent='#accordionExample'>");
                            strbuild.Append("<div class='card-body'>");
                            strbuild.Append(dr["Description"].ToString());
                            strbuild.Append("</div>");
                            strbuild.Append("</div>");
                            strbuild.Append("</div>");
                        }
                        else
                        {
                            strbuild.Append("<div class='card'>");
                            strbuild.Append("<div class='card-header' id='headingTwo'>");

                            strbuild.Append("<h5 class='mb-0'>");
                            strbuild.Append("<button class='btn btn-link collapsed' type='button' data-toggle='collapse' data-target='#" + Generateslg(dr["Title"].ToString()) + "' aria-expanded='false' aria-controls='collapseTwo'>");
                            strbuild.Append(dr["Title"].ToString());
                            strbuild.Append("</button>");
                            strbuild.Append(" </h5>");
                            strbuild.Append("</div>");
                            strbuild.Append("<div id = '" + Generateslg(dr["Title"].ToString()) + "' class='collapse' aria-labelledby='headingTwo' data-parent='#accordionExample'>");
                            strbuild.Append("<div class='card-body'>");
                            strbuild.Append(dr["Description"].ToString());
                            strbuild.Append("</div>");
                            strbuild.Append("</div>");
                            strbuild.Append("</div>");
                        }


                        count++;
                    }
                }
            }
            response = strbuild.ToString();
            return response;

        }


        public string CMS(int Id)
        {
            string response = "";
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_CMS 'bindCMS','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                response = dt.Rows[0]["Description"].ToString();
            }
            return response;
        }


        public string Generateslg(string strtext)
        {
            string str = strtext.ToString().ToLower();
            str = Regex.Replace(str, @"[^0-9a-zA-Z]+", "");
            return str;
        }

    }
}