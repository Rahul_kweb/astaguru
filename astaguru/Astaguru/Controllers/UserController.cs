﻿using Astaguru.Models;
using Astaguru.Services;
using Astaguru.Services.UserService;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Controllers
{
    public class UserController : Controller
    {

        UserService US = new UserService();

        //
        // GET: /User/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User UL)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    string userName = UL.email;
                    string password = UL.password;
                    UL = US.GetUserId(userName, password);

                    if (UL == null)
                    {
                        return Json(new { status = "Error", UL = UL }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (password == UL.password)
                        {
                            Session["userid"] = UL.userid;
                            Session["emailId"] = UL.email;
                            Session["username"] = UL.username;
                            Session["confirmbid"] = UL.confirmbid;
                            Session["applyforbid"] = UL.applyforbid;
                            Session["MobileVerified"] = UL.MobileVerified;
                            Session["EmailVerified"] = UL.EmailVerified;
                            Session["nickname"] = UL.nickname;

                            Session["name"] = UL.name;
                            Session["lastname"] = UL.lastname;
                            Session["BidLimit"] = UL.amountlimt;
                            Session["isOldUser"] = UL.isOldUser;
                            Session["BidCountry"] = UL.country;

                            return Json(new { status = "Success", userid = UL.userid, username = UL.username, confirmbid = UL.confirmbid, applyforbid = UL.applyforbid, MobileVerified = UL.MobileVerified, EmailVerified = UL.EmailVerified }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { status = "ErrorPassword", UL = UL }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = "Exception", UL = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = "Success", UL = UL }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Logout(User UL)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Session["userid"] = -1;
                    Session["emailId"] = null;
                    Session["username"] = null;
                    Session["confirmbid"] = null;
                    Session["applyforbid"] = null;
                    Session["MobileVerified"] = null;
                    Session["EmailVerified"] = null;
                    Session["GetCurrency"] = null;
                    Session["BidCountry"] = null;
                    
                    return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = "Exception", UL = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = "Success", UL = UL }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult ResendOTP(User UR)
        {
            //string Mobile = "91" + UR.Mobile;

            string CheckMobileno = US.CheckMobileNumber(UR.Mobile);

            if (CheckMobileno == null)
            {
                // OTP Logic 4 digit
                string numbers = "1234567890";
                string characters = numbers;
                int length = 4;
                string otp = string.Empty;
                for (int i = 0; i < length; i++)
                {
                    string character = string.Empty;
                    do
                    {
                        int index = new Random().Next(0, characters.Length);
                        character = characters.ToCharArray()[index].ToString();
                    } while (otp.IndexOf(character) != -1);
                    otp += character;
                }

                string FourdigitOTP = otp;

                //Value Store in Session
                Session["OTPCode"] = FourdigitOTP;

                // SMS Integration

                //String authkey = "105048AuvvUDCr56c199f0";
                //string sender = "ASTGUR";

                string authkey = "338500AD9H4VOHQl5f3135e8P1";
                string sender = "ASTGUR";

                string mobiles = UR.Mobile;
                String message = "Thank you for registering with Astaguru. Your OTP is " + FourdigitOTP;  
                string route = "4";
                //string country = "91";

                // use the API URL here  
                string strUrl = "http://api.msg91.com/api/sendhttp.php?authkey=" + authkey + "&mobiles=" + mobiles + "&message=" + message + "&sender=" + sender + "&route=" + route + "";
                // Create a request object  
                WebRequest request = HttpWebRequest.Create(strUrl);
                // Get the response back  
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream s = (Stream)response.GetResponseStream();
                StreamReader readStream = new StreamReader(s);
                string dataString = readStream.ReadToEnd();
                response.Close();
                s.Close();
                readStream.Close();
                return Json(new { Resendmsg = "OTP Failed" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { ResendmsgErr = "Entered number is already registered with our database" }, JsonRequestBehavior.AllowGet);
            }



        }


        [HttpPost]
        public ActionResult myProfileResendOTP(User UR)
        {
            //string Mobile = "91" + UR.Mobile;

            // OTP Logic 4 digit
            string numbers = "1234567890";
            string characters = numbers;
            int length = 4;
            string otp = string.Empty;
            for (int i = 0; i < length; i++)
            {
                string character = string.Empty;
                do
                {
                    int index = new Random().Next(0, characters.Length);
                    character = characters.ToCharArray()[index].ToString();
                } while (otp.IndexOf(character) != -1);
                otp += character;
            }

            string FourdigitOTP = otp;

            //Value Store in Session
            Session["OTPCode"] = FourdigitOTP;

            // SMS Integration

            //String authkey = "105048AuvvUDCr56c199f0";
            //string sender = "ASTGUR";

            string authkey = "338500AD9H4VOHQl5f3135e8P1";
            string sender = "ASTGUR";
            string mobiles = UR.Mobile;
            String message = "Thank you for registering with Astaguru. Your OTP is " + FourdigitOTP;
            
            string route = "4";
            //string country = "91";

            // use the API URL here  
            string strUrl = "http://api.msg91.com/api/sendhttp.php?authkey=" + authkey + "&mobiles=" + mobiles + "&message=" + message + "&sender=" + sender + "&route=" + route + "";
            // Create a request object  
            WebRequest request = HttpWebRequest.Create(strUrl);
            // Get the response back  
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream s = (Stream)response.GetResponseStream();
            StreamReader readStream = new StreamReader(s);
            string dataString = readStream.ReadToEnd();
            response.Close();
            s.Close();
            readStream.Close();
            return Json(new { Resendmsg = "OTP Failed" }, JsonRequestBehavior.AllowGet);


        }


        [HttpPost]
        public ActionResult CheckOTP(User UR)
        {
            string SessionOTP = Session["OTPCode"].ToString();
            if (SessionOTP == UR.OTPNumber)
            {
                if(UR.userid > 0)
                {
                    US.UpdateMobileVerified(UR.userid);
                    Session["MobileVerified"] = 1;
                    return Json(new { OTPmsg = "OTP Registerd Successfully" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Session["MobileVerified"] = 1;
                    return Json(new { OTPmsg = "OTP Registerd Successfully" }, JsonRequestBehavior.AllowGet);
                }
                
            }
            else
            {
                return Json(new { OTPErrmsg = "OTP Failed" }, JsonRequestBehavior.AllowGet);
            }

        }

        public string Anonymous()
        {       
            Random rnd = new Random();
            int randomPIN = (int)(rnd.Next(1, 9999));
            String str_pin = Convert.ToString(randomPIN);
            String nickname = "Anonymous"+str_pin;
            return nickname;
        }
        
        [HttpPost]
        public ActionResult SaveRegistration(User UL)
        {
            try
            {
                Random rnd = new Random();
                UL.activationcode = Guid.NewGuid().ToString() + rnd.Next(1, 9999).ToString();
                UL.nickname = Anonymous();

                // check nickname

                int userId = US.InsertRegistration(UL);

                if(userId > 0)
                {
                    string success = MailRegistered(UL);
                    if (success == "Success")
                    {
                        return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { status = "Exception", Message = success }, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception ex)
            {
                return Json(new { status = "Exception", UL = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = "Success", UL = UL }, JsonRequestBehavior.AllowGet);
        }


        //[HttpPost]
        //public ActionResult UpdateRegistration(User UL)
        //{
        //    try
        //    {
        //        int userId = US.UpdateRegistration(UL);
        //        return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { status = "Exception", UL = ex.Message }, JsonRequestBehavior.AllowGet);
        //    }
            
        //}

        [HttpPost]
        public ActionResult UpdateRegistration(FormCollection fc)
        {
            User UL = new User();
            try
            {

                UL.userid = Convert.ToInt32(fc["userid"]);

                UL.companyName = fc["companyName"].ToString();
                UL.BillingCountry = fc["BillingCountry"].ToString();
                UL.BillingAddress = fc["BillingAddress"].ToString();
                UL.billingAddress2 = fc["billingAddress2"].ToString();
                UL.BillingState = fc["BillingState"].ToString();
                UL.BillingZip = fc["BillingZip"].ToString();
                UL.BillingCity = fc["BillingCity"].ToString();
                UL.nickname = fc["nickname"].ToString();
                UL.GSTIN = fc["GSTIN"].ToString();
                UL.panCard = fc["panCard"].ToString();
                UL.acNumber = fc["acNumber"].ToString();
                UL.holderName = fc["holderName"].ToString();
                UL.ifscCode = fc["ifscCode"].ToString();
                UL.branchName = fc["branchName"].ToString();
                UL.swiftCode = fc["swiftCode"].ToString();
                UL.MobileVerified = Convert.ToInt32(fc["MobileVerified"]);
                UL.imagePanCard = fc["PanImageName"].ToString();
                UL.imageAadharCard = fc["AadharImageName"].ToString();
                UL.genderid = Convert.ToInt32(fc["isGender"].ToString());

                UL.bday = Convert.ToInt32(fc["bday"]);
                UL.bmonth = Convert.ToInt32(fc["bmonth"]);
                UL.byear = Convert.ToInt32(fc["byear"]);
                UL.interestedIds = fc["interestedIds"].ToString();

                UL.country = fc["country"].ToString();
                UL.state = fc["state"].ToString();
                UL.address1 = fc["address1"].ToString();
                UL.address2 = fc["address2"].ToString();
                UL.zip = fc["zip"].ToString();
                UL.city = fc["city"].ToString();
                UL.aadharCard = fc["aadharCard"].ToString();


                UL.countryid = Convert.ToInt32(fc["countryid"]);
                UL.stateid = Convert.ToInt32(fc["stateid"]);
                UL.cityid = Convert.ToInt32(fc["cityid"]);

                UL.bCountryid = Convert.ToInt32(fc["bCountryid"]);
                UL.bStateid = Convert.ToInt32(fc["bStateid"]);
                UL.bCityid = Convert.ToInt32(fc["bCityid"]);

            

                if (Request.Files["PanImage"] != null)
                {
                    HttpPostedFileBase file = Request.Files["PanImage"];
                    var allowedExtensions = new[] { ".jpg", ".jpeg", ".png", ".PNG", ".pdf", ".PDF" };
                    var fileName = Path.GetFileName(file.FileName); //getting only file name(ex-ganesh.jpg)  
                    var ext = Path.GetExtension(file.FileName); //getting the extension(ex-.jpg)  
                    if (allowedExtensions.Contains(ext)) //check what type of extension  
                    {
                        string name = Path.GetFileNameWithoutExtension(fileName); //getting file name without extension  
                        string myfile = name + ext; //appending the name with id

                        var path = Path.Combine(Server.MapPath("~/Content/panCardImage/"), myfile);
                        file.SaveAs(path);
                        UL.imagePanCard = myfile;
                    }
                    else
                    {
                        return Json(new { status = "Fail", msg = "File Format Not Supported" }, JsonRequestBehavior.AllowGet);
                    }
                }

                if (Request.Files["AadharImage"] != null)
                {
                    HttpPostedFileBase file = Request.Files["AadharImage"];
                    var allowedExtensions = new[] { ".jpg", ".jpeg", ".png", ".PNG", ".pdf", ".PDF" };
                    var fileName = Path.GetFileName(file.FileName); //getting only file name(ex-ganesh.jpg)  
                    var ext = Path.GetExtension(file.FileName); //getting the extension(ex-.jpg)  
                    if (allowedExtensions.Contains(ext)) //check what type of extension  
                    {
                        string name = Path.GetFileNameWithoutExtension(fileName); //getting file name without extension  
                        string myfile = name + ext; //appending the name with id

                        var path = Path.Combine(Server.MapPath("~/Content/aadharCardImage/"), myfile);
                        file.SaveAs(path);
                        UL.imageAadharCard = myfile;
                    }
                    else
                    {
                        return Json(new { status = "Fail", msg = "File Format Not Supported" }, JsonRequestBehavior.AllowGet);
                    }
                }

                int userId = US.UpdateRegistration(UL);


                // Image 
                //if (UL.imagePanCard == null || UL.imagePanCard == "" || UL.imageAadharCard == null || UL.imageAadharCard == "")
                //{
                //    if (Request.Files.Count > 0)
                //    {
                //        HttpFileCollectionBase files = Request.Files;

                //        if (UL.panCard != "")
                //        {
                //            HttpPostedFileBase file = files[0];
                //            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png", ".PNG", ".pdf", ".PDF" };
                //            var fileName = Path.GetFileName(file.FileName); //getting only file name(ex-ganesh.jpg)  
                //            var ext = Path.GetExtension(file.FileName); //getting the extension(ex-.jpg)  
                //            if (allowedExtensions.Contains(ext)) //check what type of extension  
                //            {
                //                string name = Path.GetFileNameWithoutExtension(fileName); //getting file name without extension  
                //                string myfile = name + ext; //appending the name with id

                //                var path = Path.Combine(Server.MapPath("~/Content/panCardImage/"), myfile);
                //                file.SaveAs(path);
                //                UL.imagePanCard = myfile;
                //                int userId = US.UpdateRegistration(UL);
                //               // return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
                //            }
                //            else
                //            {
                //                return Json(new { status = "Image Failed" }, JsonRequestBehavior.AllowGet);
                //            }
                //        }
                //        if (UL.aadharCard != "")
                //        {
                //            HttpPostedFileBase file = files[1];
                //            var allowedExtensions = new[] { ".jpg", ".jpeg", ".png", ".PNG", ".pdf", ".PDF" };
                //            var fileName = Path.GetFileName(file.FileName); //getting only file name(ex-ganesh.jpg)  
                //            var ext = Path.GetExtension(file.FileName); //getting the extension(ex-.jpg)  
                //            if (allowedExtensions.Contains(ext)) //check what type of extension  
                //            {
                //                string name = Path.GetFileNameWithoutExtension(fileName); //getting file name without extension  
                //                string myfile = name + ext; //appending the name with id

                //                var path = Path.Combine(Server.MapPath("~/Content/aadharCardImage/"), myfile);
                //                file.SaveAs(path);
                //                UL.imageAadharCard = myfile;
                //                int userId = US.UpdateRegistration(UL);
                //               // return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
                //            }
                //            else
                //            {
                //                return Json(new { status = "Image Failed" }, JsonRequestBehavior.AllowGet);
                //            }
                //        }

                //        return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);

                //        //for (int i = 0; i < files.Count; i++)
                //        //{
                //        //    //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                //        //    //string filename = Path.GetFileName(Request.Files[i].FileName);  
                //        //}
                //    }
                //    else
                //    {
                //        int userId = US.UpdateRegistration(UL);
                //        return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
                //    }
                //}
                //else
                //{
                //    int userId = US.UpdateRegistration(UL);
                //    return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
                //}
                 
                 
            return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
                
            }
            catch (Exception ex)
            {
                return Json(new { status = "Exception", UL = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult CheckData(User UL)
        {
           try
            {
                // Check Email
                int isEmail = US.GetCheckEmail(UL);

                // Check Username
                string isUsername = US.GetCheckUsername(UL);

                // Check Mobile
                string isMobile = US.CheckMobileNumber(UL.Mobile);

                return Json(new { status = "Success", isEmail = isEmail, isUsername = isUsername, isMobile = isMobile }, JsonRequestBehavior.AllowGet);

            }
            catch(Exception Ex)
            {
                return Json(new { status = "Exception", Message = Ex.Message }, JsonRequestBehavior.AllowGet);
            }

         

            
        }


        public string MailRegistered(User UL)
        {
            string FromEmail = ConfigurationManager.AppSettings["fromId"];
            string Password = ConfigurationManager.AppSettings["smtpPassword"];
            string ToEmail = UL.email;
            string basePathURL = Common.baseUrlPath;
            try
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(FromEmail, "contact@astaguru.com");
                msg.To.Add(ToEmail);
                msg.Subject = "Warm Greetings from AstaGuru Online Auction House.";
                string str = string.Empty;
                str = "Dear " + UL.name +" "+ UL.lastname+",";
                str += "<br><br>Thank you for choosing AstaGuru Online Auction House. We are glad that you have given us this opportunity to cater to your art & antiquities requirements. Looking forward to building a longstanding relationship with you.";
                str += "<br><br>Please click on the link to complete the registration & verification process.<br>";
                str += "<a href=\"" + basePathURL + "/Home/emailVerification?activationcode="+UL.activationcode+"\">Click Here.</a>";
                str += "<br><br>In case you are unable to open the link, please write to us at, contact@astaguru.com or call us on 91-22 2204 8138/39. We will be glad to assist.";

                str += "<br><br>Thanking You & Warm Regards,";
                str += "<br><br><br>Team AstaGuru";

                //msg.Body = str;
                msg.IsBodyHtml = true;
                string html = str;
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, new ContentType("text/html"));
                msg.AlternateViews.Add(htmlView);
                //SmtpClient sc = new SmtpClient("sarpstechnologies.com", 25);
                SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["smtpHost"], Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]));
                sc.Host = "smtp.gmail.com";
                sc.Port = 587;
                sc.UseDefaultCredentials = true;
                // sc.Host = "sarpstechnologies.com";
                //  sc.Port = 25;
                sc.Credentials = new NetworkCredential(FromEmail, Password);
                sc.EnableSsl = true; // Hide this If u use instead of gmail
                sc.Send(msg);

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        [HttpPost]
        public ActionResult SendEmailVerification(User UL)
        {

            string success = MailRegistered(UL);
            if (success == "Success")
            {
                return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = "Exception", Message = success }, JsonRequestBehavior.AllowGet);
            }
          //  return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdatePassword(User UL)
        {
            try
            {
                int userId = US.UpdatePassword(UL);
                return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception Ex)
            {
                return Json(new { status = "Exception", Message = Ex.Message }, JsonRequestBehavior.AllowGet);
            }
            
        }



        /// created on 12_08_2020

        //public void sendoutbidmsg(string reference, string username, string mobile)
        //{
        //    string authKey = "105048AuvvUDCr56c199f0";
        //    //Multiple mobiles numbers separated by comma
        //    string mobileNumber = mobile;
        //    //Sender ID,While using route4 sender id should be 6 characters long.
        //    string senderId = "ASTGUR";
        //    //Your message to send, Add URL encoding here.
        //    string message = HttpUtility.UrlEncode("Dear "+ username + ", please note you have been outbid on Lot No."+ reference.Trim() + ". Place renew bid on www.astaguru.com or mobile App");



        //    //Prepare you post parameters
        //    StringBuilder sbPostData = new StringBuilder();
        //    sbPostData.AppendFormat("authkey={0}", authKey);
        //    sbPostData.AppendFormat("&mobiles={0}", mobileNumber);
        //    sbPostData.AppendFormat("&message={0}", message);
        //    sbPostData.AppendFormat("&sender={0}", senderId);
        //    sbPostData.AppendFormat("&route={0}", "4");

        //    try
        //    {
        //        //Call Send SMS API
        //        string sendSMSUri = "http://api.msg91.com/api/sendhttp.php";
        //        //Create HTTPWebrequest
        //        HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
        //        //Prepare and Add URL Encoded data
        //        UTF8Encoding encoding = new UTF8Encoding();
        //        byte[] data = encoding.GetBytes(sbPostData.ToString());
        //        //Specify post method
        //        httpWReq.Method = "POST";
        //        httpWReq.ContentType = "application/x-www-form-urlencoded";
        //        httpWReq.ContentLength = data.Length;
        //        using (Stream stream = httpWReq.GetRequestStream())
        //        {
        //            stream.Write(data, 0, data.Length);
        //        }
        //        //Get the response
        //        HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
        //        StreamReader reader = new StreamReader(response.GetResponseStream());
        //        string responseString = reader.ReadToEnd();

        //        //Close the response
        //        reader.Close();
        //        response.Close();
        //    }
        //    catch (SystemException ex)
        //    {

        //    }
        //}


    }
}
