﻿using Astaguru.Models;
using Astaguru.Services.UserService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Controllers
{
    public class APIController : Controller
    {
        //
        // GET: /API/
        UserService US = new UserService();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult updateProfile(FormCollection fc)
        {
            User UL = new User();

            try
            {

           
            UL.userid = Convert.ToInt32(fc["userid"]);

            UL.companyName = fc["companyName"].ToString();
            UL.BillingCountry = fc["BillingCountry"].ToString();
            UL.BillingAddress = fc["BillingAddress"].ToString();
            UL.billingAddress2 = fc["billingAddress2"].ToString();
            UL.BillingState = fc["BillingState"].ToString();
            UL.BillingZip = fc["BillingZip"].ToString();
            UL.BillingCity = fc["BillingCity"].ToString();
            UL.nickname = fc["nickname"].ToString();
            UL.GSTIN = fc["GSTIN"].ToString();
            UL.panCard = fc["panCard"].ToString();
            UL.acNumber = fc["acNumber"].ToString();
            UL.holderName = fc["holderName"].ToString();
            UL.ifscCode = fc["ifscCode"].ToString();
            UL.branchName = fc["branchName"].ToString();
            UL.swiftCode = fc["swiftCode"].ToString();
            UL.MobileVerified = Convert.ToInt32(fc["MobileVerified"]);
            UL.imagePanCard = fc["imagePanCard"].ToString();
            UL.imageAadharCard = fc["imageAadharCard"].ToString();
            UL.genderid = Convert.ToInt32(fc["genderid"]);

            UL.bday = Convert.ToInt32(fc["bday"]);
            UL.bmonth = Convert.ToInt32(fc["bmonth"]);
            UL.byear = Convert.ToInt32(fc["byear"]);
            UL.interestedIds = fc["interestedIds"].ToString();

            UL.country = fc["country"].ToString();
            UL.state = fc["state"].ToString();
            UL.address1 = fc["address1"].ToString();
            UL.address2 = fc["address2"].ToString();
            UL.zip = fc["zip"].ToString();
            UL.city = fc["city"].ToString();
            UL.aadharCard = fc["aadharCard"].ToString();


            UL.countryid = Convert.ToInt32(fc["countryid"]);
            UL.stateid = Convert.ToInt32(fc["stateid"]);
            UL.cityid = Convert.ToInt32(fc["cityid"]);

            UL.bCountryid = Convert.ToInt32(fc["bCountryid"]);
            UL.bStateid = Convert.ToInt32(fc["bStateid"]);
            UL.bCityid = Convert.ToInt32(fc["bCityid"]);

            string Result = string.Empty;

            if(Request.Files["PanImage"] != null)
            {
                HttpPostedFileBase file = Request.Files["PanImage"];
                var allowedExtensions = new[] { ".jpg", ".jpeg", ".png", ".PNG", ".pdf", ".PDF" };
                var fileName = Path.GetFileName(file.FileName); //getting only file name(ex-ganesh.jpg)  
                var ext = Path.GetExtension(file.FileName); //getting the extension(ex-.jpg)  
                if (allowedExtensions.Contains(ext)) //check what type of extension  
                {
                    string name = Path.GetFileNameWithoutExtension(fileName); //getting file name without extension  
                    string myfile = name + ext; //appending the name with id

                    var path = Path.Combine(Server.MapPath("~/Content/panCardImage/"), myfile);
                    file.SaveAs(path);
                    UL.imagePanCard = myfile;
                }
                else
                {
                    return Json(new { status = "Fail", msg = "File Format Not Supported" }, JsonRequestBehavior.AllowGet);
                }
            }

            if (Request.Files["AadharImage"] != null)
            {
                HttpPostedFileBase file = Request.Files["AadharImage"];
                var allowedExtensions = new[] { ".jpg", ".jpeg", ".png", ".PNG", ".pdf", ".PDF" };
                var fileName = Path.GetFileName(file.FileName); //getting only file name(ex-ganesh.jpg)  
                var ext = Path.GetExtension(file.FileName); //getting the extension(ex-.jpg)  
                if (allowedExtensions.Contains(ext)) //check what type of extension  
                {
                    string name = Path.GetFileNameWithoutExtension(fileName); //getting file name without extension  
                    string myfile = name + ext; //appending the name with id

                    var path = Path.Combine(Server.MapPath("~/Content/aadharCardImage/"), myfile);
                    file.SaveAs(path);
                    UL.imageAadharCard = myfile;
                }
                else
                {
                    return Json(new { status = "Fail", msg = "File Format Not Supported" }, JsonRequestBehavior.AllowGet);
                }
            }

                int userId = US.UpdateRegistration(UL);
                Result = "Success";

                return Json(new { status = Result, msg = "profile Updated Successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
            {
                return Json(new { status = "Fail", msg = Ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

	}
}