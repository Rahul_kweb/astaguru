$(document).resize(function(){
    var highestBox = 0;
        $('.equal_height_row .equal_height_col').each(function(){  
                if($(this).height() > highestBox){  
                highestBox = $(this).height();  
        }
    });    
    $('.equal_height_row .equal_height_col').height(highestBox);
});

$(document).ready(function () {
    //Upload file
    'use strict';

    ; (function (document, window, index) {
        var inputs = document.querySelectorAll('.inputfile');
        Array.prototype.forEach.call(inputs, function (input) {
            var label = input.nextElementSibling,
              labelVal = label.innerHTML;

            input.addEventListener('change', function (e) {
                var fileName = '';
                if (this.files && this.files.length > 1)
                    fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                else
                    fileName = e.target.value.split('\\').pop();

                if (fileName)
                    label.querySelector('span').innerHTML = fileName;
                else
                    label.innerHTML = labelVal;
            });

            // Firefox bug fix
            input.addEventListener('focus', function () { input.classList.add('has-focus'); });
            input.addEventListener('blur', function () { input.classList.remove('has-focus'); });
        });
    }(document, window, 0));

    $.fn.eqHeights = function () {
        var el = $(this);
        if (el.length > 0 && !el.data('eqHeights')) {
            $(window).bind('resize.eqHeights', function () {
                el.eqHeights();
            });
            el.data('eqHeights', true);
        }
        var panels = el.find(".panel-body");
        var fistoffset = panels.first().offset();
        var curHighest = 0;
        return panels.each(function () {
            var thisoffset = $(this).offset();
            var elHeight = $(this).height('auto').height();
            if (thisoffset.top == fistoffset.top) {
                if (elHeight > curHighest) {
                    curHighest = elHeight;
                }
            } else {
                curHighest = "auto";
            }
        }).height(curHighest);
    };
    $('.match_all').eqHeights();

});

//Cloud Zoom
$(function () {
    $('#zoom1').bind('click', function () {            // Bind a click event to a Cloud Zoom instance.
        var cloudZoom = $(this).data('CloudZoom');  // On click, get the Cloud Zoom object,
        cloudZoom.closeZoom();
        $.fancybox.open(cloudZoom.getGalleryList());// and pass Cloud Zoom's image list to Fancy Box.
        return false;
    });

    $('#slider1').Thumbelina({
        orientation: 'vertical',         // Use vertical mode (default horizontal).
        $bwdBut: $('#slider1 .top'),     // Selector to top button.
        $fwdBut: $('#slider1 .bottom')   // Selector to bottom button.
    });

    //Fancybox
    $(".fancybox").fancybox({
        openEffect: 'elastic',
        closeEffect: 'elastic',
        keys: false,
        arrows : false,
    });

});
CloudZoom.quickStart();

$(document).ready(function() {      
        $('#image-gallery').lightSlider({
           gallery:true,
           item:1,
           vertical:true,
           verticalHeight:435,
           vThumbWidth:100,
           thumbItem:5,
           thumbMargin:4,
           slideMargin:0,
            onSliderLoad: function() {
                $('#image-gallery').removeClass('cS-hidden');
            }  
        });

});
// initialize manually with a list of links
$('[data-gallery=manual]').click(function (e) {
      e.preventDefault();
      var items = [],
        options = {
          index: $(this).attr('data-index') || $(this).index(),
        };

      $('[data-gallery=manual]').each(function () {
        items.push({
          src: $(this).attr('href'),
          title: $(this).attr('data-title')
        });
      });

      new PhotoViewer(items, options);

    });

$(document).ready(function () {
    //Scroll To Top
    $(".a_scroll").on('click', function (event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function () {
                window.location.hash = hash;
            });
        }
    });

    $('.select_drop').selectric();

    $('.select_search').SumoSelect({ csvDispCount: 3, search: true, searchText: 'Enter here.' });

    //ToolTip
    $('[data-toggle="tooltip"]').tooltip();

    //Search Toggle
    $(".search_toggle").click(function () {
        $(".search_dropdown").toggle('200');
        $(".search_overlay").show();
        $(".search_dropdown input[type='text']").focus();
    });

    $(".search_overlay").click(function () {
        $(".search_dropdown").hide();
        $(".search_overlay").hide();
    });

    $('.read_more').readmore({
        speed: 500,
        collapsedHeight: 180,
    });

    $('#toggle').click(function () {
        $(this).toggleClass('active');
        $('#overlay').toggleClass('open');
    });

    $("#recent_auction").slick({
        dots: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    $("#banner_home_slider").slick({
        dots: true,
        arrows: false,
        infinite: true,
        speed: 500,
        autoplay: true,
        cssEase: 'linear'
    });

    $('#video_slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 30000,
        dots: false,
        infinite: true,
        adaptiveHeight: true,
        arrows: true,
        onAfterChange : function() {
            player.stopVideo();
        }
    });

    //Show Hide Toggle
    $(".click_ellipsis").click(function () {
        $(".live_action_menu").toggle(200);
    });


    //Show Login Logout
    $('#after_login_toggle').click(function (event) {
        event.stopPropagation();
        $("#after_login_dropdown").slideToggle("fast");
    });

    //MODAL
    $("#SignupForm").on('show.bs.modal', function (e) {
        $("#LoginForm").modal("hide");
    });

    $("#LoginForm").on('show.bs.modal', function (e) {
        $("#SignupForm").modal("hide");
    });


    //Show Hide Toggle
    $('#click_filter').click(function (event) {
        event.stopPropagation();
        $("#filter_drop").slideToggle("fast");
    });
    $("#filter_drop").on("click", function (event) {
        event.stopPropagation();
    });

    //Show HIde Checkbox
    $("#checkbox_address").click(function () {
        if ($(this).is(":checked")) {
            $("#billing_address_show").show();
        } else {
            $("#billing_address_show").hide();
        }
    });


    //Tab
    $('#HorizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function (event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });

    // var video = $('#video_slider .slick-active').find('iframe').get(0).play();

    // $('#video_slider').on('afterChange', function (event, slick, currentSlide, nextSlide) {
    //     $('#video_slider .slick-slide').find('video').get(0).pause();
    //     var video = $('#video_slider .slick-active').find('video').get(0).play();
    // });


    $("#video_slider").on("beforeChange", function(event, slick) {
      var currentSlide, slideType, player, command;
      
      //find the current slide element and decide which player API we need to use.
      currentSlide = $(slick.$slider).find(".slick-current");      

      slideType = currentSlide.attr("class").split(" ")[1];
      
      //get the iframe inside this slide.
      player = currentSlide.find("iframe").get(0);
      
      if (slideType == "iframe") {
        command = {
          "method": "pause",
          "value": "true"
        };
      } else {
        command = {
          "event": "command",
          "func": "pauseVideo"
        };
      }
      
      //check if the player exists.
      if (player != undefined) {
        //post our command to the iframe.
        player.contentWindow.postMessage(JSON.stringify(command), "*");
      }
    });

});

$(document).on("click", function () {
    $("#filter_drop").hide();
});

$(document).on("click", function (event) {
    var $trigger = $("#after_login_toggle");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $("#after_login_dropdown").slideUp("fast");
    }

});


autoPlayYouTubeModal();

//FUNCTION TO GET AND AUTO PLAY YOUTUBE VIDEO FROM DATATAG
function autoPlayYouTubeModal() {
    var trigger = $("body").find('[data-toggle="modal"]');
    trigger.click(function () {
        var theModal = $(this).data("target"),
            videoSRC = $(this).attr("data-theVideo"),
            videoSRCauto = videoSRC + "?autoplay=0";
        $(theModal + ' iframe').attr('src', videoSRCauto);
        $(theModal + '').click(function () {
            $(theModal + ' iframe').attr('src', videoSRC);
        });
    });
}

//PopHover
var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function (obj) {
    var self = obj instanceof this.constructor ?
        obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
    var container, timeout;

    originalLeave.call(this, obj);

    if (obj.currentTarget) {
        container = $(obj.currentTarget).siblings('.popover')
        timeout = self.timeout;
        container.one('mouseenter', function () {
            //We entered the actual popover â€“ call off the dogs
            clearTimeout(timeout);
            //Let's monitor popover content instead
            container.one('mouseleave', function () {
                $.fn.popover.Constructor.prototype.leave.call(self, self);
            });
        })
    }
};

$('body').popover({ selector: '[data-popover]', trigger: 'click hover', placement: 'auto', delay: { show: 50, hide: 50 } });

$('html').on('click', function (e) {
    if (typeof $(e.target).data('original-title') == 'undefined' &&
       !$(e.target).parents().is('.popover.in')) {
        $('[data-original-title]').popover('hide');
    }
});

//Menu
$.sidebarMenu = function (menu) {
    var animationSpeed = 300,
      subMenuSelector = '.sidebar-submenu';

    $(menu).on('click', 'li a', function (e) {
        var $this = $(this);
        var checkElement = $this.next();

        if (checkElement.is(subMenuSelector) && checkElement.is(':visible')) {
            checkElement.slideUp(animationSpeed, function () {
                checkElement.removeClass('menu-open');
            });
            checkElement.parent("li").removeClass("active");
        }

            //If the menu is not visible
        else if ((checkElement.is(subMenuSelector)) && (!checkElement.is(':visible'))) {
            //Get the parent menu
            var parent = $this.parents('ul').first();
            //Close all open menus within the parent
            var ul = parent.find('ul:visible').slideUp(animationSpeed);
            //Remove the menu-open class from the parent
            ul.removeClass('menu-open');
            //Get the parent li
            var parent_li = $this.parent("li");

            //Open the target menu and add the menu-open class
            checkElement.slideDown(animationSpeed, function () {
                //Add the class active to the parent li
                checkElement.addClass('menu-open');
                parent.find('li.active').removeClass('active');
                parent_li.addClass('active');
            });
        }
        //if this isn't a link, prevent the page from being redirected
        if (checkElement.is(subMenuSelector)) {
            e.preventDefault();
        }
    });
}

(function ($) {

});

//CHARACTAR LIMIT
$(document).ready(function (){
   $(".hover_description").each(function(i){
        var len=$(this).text().trim().length;
        if(len>150)
        {
            $(this).text($(this).text().substr(0,150)+'...');
        }
    });
 });

//Count Timer
// $('.counter_timer').timeTo({
//     timeTo: date,
//     theme: "black",
//     displayCaptions: true,
//     timeTo: new Date(new Date('Tue Apr 23 2019 09:00:00 GMT+0530 (India Standard Time)')),
//     //displayDays: 2,
//     //fontSize: 48,
//     //captionSize: 14
// });
// var date = getRelativeDate(20);
// document.getElementById('date-str').innerHTML = date.toString();
// date = getRelativeDate(7, 9);
// document.getElementById('date2-str').innerHTML = date.toString();
//End Cout Timer


