﻿using System.Web;
using System.Web.Optimization;

namespace Astaguru
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region  Login Page css & js
            bundles.Add(new StyleBundle("~/css").Include(
                            "~/Content/Admin/vendors/bootstrap/dist/css/bootstrap.min.css",
                            "~/Content/Admin/vendors/font-awesome/css/font-awesome.min.css",
                            "~/Content/Admin/vendors/animate.css/animate.min.css",
                            "~/Content/Admin/build/css/custom.min.css",
                            "~/Content/Admin/performainvoice.css"));

            bundles.Add(new StyleBundle("~/css1").Include(
                         "~/Content/Admin/Resource/Css/Login.css"));

            bundles.Add(new StyleBundle("~/css2").Include(
                         "~/Content/Admin/vendors/nprogress/nprogress.css"));

            bundles.Add(new StyleBundle("~/css3").Include(
                         "~/Content/Admin/vendors/google-code-prettify/bin/prettify.min.css"));

            bundles.Add(new StyleBundle("~/css4").Include(
                "~/Content/Admin/vendors/bootstrap/dist/css/bootstrap.min.css",
                //"~/Content/Admin/Resource/Website/Css/bootstrap.min.css",
                 "~/Content/Admin/Resource/Website/Css/font-awesome.min.css",
                 "~/Content/Admin/vendors/animate.css/animate.min.css",
                  "~/Content/Admin/Resource/Website/Css/custom.min.css",
                       "~/Content/Admin/Resource/Website/Css/css.css",
                       "~/Content/Admin/Resource/Website/Css/battabee.css"
                       ));


            bundles.Add(new ScriptBundle("~/Login/js").Include(
                           "~/Content/Admin/vendors/jquery/dist/jquery.min.js",
                           "~/Content/Admin/Resource/js/Login.js"));

            bundles.Add(new ScriptBundle("~/ClientLoginjs").Include(
                           "~/Content/Admin/vendors/bootstrap/dist/js/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/Login").Include(
                         "~/Content/Admin/Resource/js/Login.js"));

            bundles.Add(new ScriptBundle("~/Dashboard").Include(
                            "~/Content/Admin/vendors/fastclick/lib/fastclick.js",
                           "~/Content/Admin/vendors/nprogress/nprogress.js",
                            "~/Content/Admin/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js",
                         "~/Content/Admin/vendors/jquery.hotkeys/jquery.hotkeys.js",
                         "~/Content/Admin/vendors/google-code-prettify/src/prettify.js",
                         "~/Content/Admin/build/js/custom.min.js"
                         ));

            #endregion

            #region  Super Admin css & js

            bundles.Add(new StyleBundle("~/Admin/css1").Include(
                           "~/Content/Admin/vendors/bootstrap/dist/css/bootstrap.min.css",
                           "~/Content/Admin/vendors/font-awesome/css/font-awesome.min.css",
                           "~/Content/Admin/vendors/nprogress/nprogress.css",
                           "~/Content/Admin/vendors/bootstrap-daterangepicker/daterangepicker.css",
                            "~/Content/Admin/build/css/custom.min.css",
                            "~/Content/Admin/vendors/datatable_alphabet_filter/dataTables.alphabetSearch.css"
                           ));


            bundles.Add(new StyleBundle("~/Admin/css2").Include(
           "~/Content/Admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css",
           "~/Content/Admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css",
           "~/Content/Admin/vendors/iCheck/skins/flat/green.css"
                  ));


            bundles.Add(new ScriptBundle("~/Admin/js1").Include(
                          "~/Content/Admin/vendors/jquery/dist/jquery.min.js"
                          ));




            bundles.Add(new ScriptBundle("~/Admin/js2").Include(
              "~/Content/Admin/vendors/bootstrap/dist/js/bootstrap.min.js",
                           "~/Content/Admin/vendors/fastclick/lib/fastclick.js",
                           "~/Content/Admin/vendors/nprogress/nprogress.js",
                           "~/Content/Admin/vendors/Chart.js/dist/Chart.min.js",
                           "~/Content/Admin/vendors/jquery-sparkline/dist/jquery.sparkline.min.js",
                            //"~/Content/vendors/Flot/jquery.flot.js",
                            //"~/Content/vendors/Flot/jquery.flot.pie.js",
                            //"~/Content/vendors/Flot/jquery.flot.time.js",
                            //"~/Content/vendors/Flot/jquery.flot.stack.js",
                            //"~/Content/vendors/Flot/jquery.flot.resize.js",
                            //"~/Content/vendors/flot.orderbars/js/jquery.flot.orderBars.js",
                            //"~/Content/vendors/flot-spline/js/jquery.flot.spline.min.js",
                            //"~/Content/vendors/flot.curvedlines/curvedLines.js",
                            "~/Content/Admin/vendors/DateJS/build/date.js",
                            "~/Content/Admin/vendors/moment/min/moment.min.js",
                            "~/Content/Admin/vendors/bootstrap-daterangepicker/daterangepicker.js",
                            "~/Content/Admin/build/js/custom.min.js",
                            "~/Content/Admin/vendors/datatables.net/js/jquery.dataTables.min.js",
                            "~/Content/Admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js",
                            "~/Content/Admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js",
                            "~/Content/Admin/vendors/bootstrap-daterangepicker/daterangepicker.js",
                            "~/Content/Admin/vendors/iCheck/icheck.min.js",
                           //"~/Content/scripts/jquery.validate.min.js",
                           //"~/Content/scripts/jquery.validate.unobtrusive.js",
                           //"~/Content/scripts/jquery.validate.unobtrusive.min.js",
                           //"~/Content/vendors/iCheck/icheck.min.js",
                           "~/Content/Admin/vendors/datatable_alphabet_filter/dataTables.alphabetSearch.min.js"

                           ));

            #endregion


            #region Form Validation js

            bundles.Add(new ScriptBundle("~/Admin/FormValidation").Include(
                "~/Content/Admin/scripts/jquery.validate.min.js",
                "~/Content/Admin/scripts/jquery.validate.unobtrusive.js",
                "~/Content/Admin/scripts/jquery.validate.unobtrusive.min.js"
                ));

            #endregion



            // Code removed for clarity.
            BundleTable.EnableOptimizations = true; //minified all css and js

        }
    }
}
