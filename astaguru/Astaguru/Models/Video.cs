﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Models
{
    public class Video
    {
        public string Sr { get; set; }
        public int Id { get; set; }

        [Required(ErrorMessage = "Please Enter Title")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please Enter VideoUrl")]
        public string VideoUrl { get; set; }

        [Required(ErrorMessage = "Post date is required.")]
        public string PostDate { get; set; }

        public int categoryid { get; set; }

        //[Required(ErrorMessage = "Please Select Category")]
        public string category { get; set; }

        public IEnumerable<SelectListItem> CategoryList { get; set; }

        public List<Video> GridList { get; set; }
    }
}