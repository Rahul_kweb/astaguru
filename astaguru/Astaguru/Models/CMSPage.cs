﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Astaguru.Models
{
    public class CMSPage
    {

        public int CMSId { get; set; }

        public int PageId { get; set; }

        [Required(ErrorMessage = "Title is required.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Description is required.")]
        public string Description { get; set; }

        public int DisplayOrder { get; set; }

        public string PageName { get; set; }

        public List<CMSPage> CMSPageList { get; set; }

        public string Sr { get; set; }
    }
}