﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Astaguru.Models.AdminCms
{
    public class Medium
    {
        public string Sr { get; set; }
        public int mediumid { get; set; }

        [Required(ErrorMessage = "Please enter Medium")]
        public string medium { get; set; }

        public List<Medium> GridList { get; set; }
    }
}