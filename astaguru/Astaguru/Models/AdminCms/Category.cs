﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Astaguru.Models.AdminCms
{
    public class Category
    {
        public string Sr { get; set; }
        public int categoryid { get; set; }

        [Required(ErrorMessage = "Please enter Category")]
        public string category { get; set; }

        public List<Category> GridList { get; set; }
    }
}