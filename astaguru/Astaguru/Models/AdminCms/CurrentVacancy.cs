﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Astaguru.Models.AdminCms
{
    public class CurrentVacancy
    {
        public string Sr { get; set; }
        public int Id { get; set; }

        [Required(ErrorMessage = "Job Title is Required ")]
        public string Job_Title { get; set; }


        [Required(ErrorMessage = "Business Unit is Required ")]
        public string Business_Unit { get; set; }

        [Required(ErrorMessage = "Job Responsibility is Required ")]
        public string Job_Responsibility { get; set; }

        [Required(ErrorMessage = "Responsibilities is Required ")]
        public string Responsibilities { get; set; }

        [Required(ErrorMessage = "Functional Skills is Required ")]
        public string Functional_Skills { get; set; }

        [Required(ErrorMessage = "Technical skills is Required ")]
        public string Technical_skills { get; set; }

        [Required(ErrorMessage = "Qualification is Required ")]
        public string Qualification { get; set; }

        [Required(ErrorMessage = "Salary is Required ")]
        public string Salary { get; set; }

        [Required(ErrorMessage = "Experience is Required ")]
        public string Experience { get; set; }

        public List<CurrentVacancy> GridList { get; set; }
    }
}