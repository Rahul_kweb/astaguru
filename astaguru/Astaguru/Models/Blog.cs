﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Astaguru.Models
{
    public class Blog
    {
        public int BlogId { get; set; }

        [Required(ErrorMessage = "Title is required.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Image is required.")]
        public string Image { get; set; }

        [Required(ErrorMessage = "Post date is required.")]
        public string PostDate { get; set; }

        [Required(ErrorMessage = "Description is required.")]
        public string Description { get; set; }

        public string Sr { get; set; }

        public List<Blog> Gridlist { get; set; }

        public string imgPreview { get; set; }

        [Required(ErrorMessage = "CreatedBy is required.")]
        public string CreatedBy { get; set; }

        public string Slug { get; set; }
        public int categoryid { get; set; }

        //[Required(ErrorMessage = "Please Select Category")]
        public string category { get; set; }

        public IEnumerable<SelectListItem> CategoryList { get; set; }

    }
}