﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astaguru.Models
{
    public class User
    {
        public string Sr { get; set; }
        public int userid { get; set; }
            public string username { get; set; }
            public string email { get; set; }
        public string password { get; set; }

         public int confirmbid { get; set; }

         public string applyforbid { get; set; }

         public int MobileVerified { get; set; }

         public int EmailVerified { get; set; }


         public string BillingName { get; set; }
         public string BillingAddress { get; set; }

         public string billingAddress2 { get; set; }
        
         public string BillingCity { get; set; }
         public string BillingState { get; set; }
         public string BillingCountry { get; set; }
         public string BillingZip { get; set; }
         public string BillingEmail { get; set; }
         public string BillingTelephone { get; set; }

         public string nickname { get; set; }

         public int countryCode { get; set; }

         public string Mobile { get; set; }

         public string OTPNumber { get; set; }
        
          public int aboutId { get; set; }
          public string about { get; set; }

          public int interestedId { get; set; }
          public string interest { get; set; }
        
        
         public int countryid { get; set; }

         public int stateid { get; set; }

         public int cityid { get; set; }

          public string countryname { get; set; }
          public string statename { get; set; }

          public string cityname { get; set; }

          public string name { get; set; }


          public string address1 { get; set; }
          public string address2 { get; set; }

          public string zip { get; set; }

          public string lastname { get; set; }

          public string activationcode { get; set; }

          public string SmsCode { get; set; }

          public string city { get; set; }

          public string state { get; set; }
          public string country { get; set; }



          public int genderid { get; set; }

          public int bday { get; set; }

          public int bmonth { get; set; }
          public int byear { get; set; }
          public string interestedIds { get; set; }


          public string GSTIN { get; set; }
          public string panCard { get; set; }
          public string acNumber { get; set; }
          public string holderName { get; set; }
          public string ifscCode { get; set; }
          public string branchName { get; set; }
          public string swiftCode { get; set; }
          public string imagePanCard { get; set; }
          public string imageAadharCard { get; set; }
        

          public string companyName { get; set; }


          public string aadharCard { get; set; }

          public int amountlimt { get; set; }
            
        
        public int bCountryid { get; set; }
        public int bStateid { get; set; }
        public int bCityid { get; set; }

        public int isOldUser { get; set; }

        
        //created on 20_3_2020
          public string RegistrationDate { get; set; }
        
         public string LastLoggedDate { get; set; }
        public List<User> GridList{get;set;}

        public bool bconfirmbid { get; set; }

        public bool bapplyforbid { get; set; }

        public bool bEmailVerified { get; set; }

        public bool bMobileVerified { get; set; }

        public bool bisOldUser { get; set; }


        //created on 31_03_2020 for admin
        public int Visits { get; set; }
    }
}
