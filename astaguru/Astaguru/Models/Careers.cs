﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Astaguru.Models
{
    public class Careers
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email address is required.")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Invalid email address.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Job Title is required.")]
        public string JobTitle { get; set; }

        [Required(ErrorMessage = "Messages is required.")]
        public string Message { get; set; }

        [Required(ErrorMessage = "Hear About Us is required.")]
        public string aboutus { get; set; }

        [Required(ErrorMessage = "Resume is required.")]
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf)$", ErrorMessage = "Only Document files allowed.")]
        public string Resume { get; set; }

        //public string ApplyDate { get; set; }

        //public string Sr { get; set; }

        public List<Careers> ApplyForJobList { get; set; }
    }
}