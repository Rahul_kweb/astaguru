﻿using Astaguru.Models;
using Dapper;
using Repository.Lib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Astaguru.Services.UserService
{
    public class PastAuctionService
    {
        Log log = new Log();
        public List<Auction> GetPastAuctionList(int Page)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    //var Listscheme = con.Query<Auction>("Select AuctionId,auctiondate, auctiontitle, b.auctionWebImage, Sum(a.pricers) As totalrs, Sum(a.priceus) As totalus FROM acution a INNER JOIN AuctionList b ON a.Online = b.AuctionId Where a.pricelow > 1 And a.pricers > a.pricelow AND status='Past' Group By b.AuctionId, b.auctiontitle, b.auctiondate, b.auctionWebImage Order By AuctionId DESC OFFSET " + Page + " ROWS FETCH NEXT 50 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();

                    var Listscheme = con.Query<Auction>("Select AuctionId,auctiondate, auctiontitle, b.auctionWebImage, Sum(a.pricers) As totalrs, Sum(a.priceus) As totalus FROM acution a INNER JOIN AuctionList b ON a.Online = b.AuctionId Where a.pricelow > 1 And a.pricers > a.pricelow AND status='Past' Group By b.AuctionId, b.auctiontitle, b.auctiondate, b.auctionWebImage Order By AuctionId DESC", null, null, true, 0, CommandType.Text).ToList();
                    return Listscheme;
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }


            }
        }



        public List<Auction> GetPastLotList(string Subquery, int Page, int AuctionId)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    if (Subquery.Length > 0)
                    {
                        var Listscheme = con.Query<Auction>("select acution.*, artist.firstname,artist.lastname,(case when len(artist.firstname + ' ' + artist.lastname) > 13 then left(ltrim(artist.firstname + ' ' + artist.lastname),13) + '...' else (artist.firstname + ' ' + artist.lastname) end) as name, medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId,AuctionList.auctionType from acution, artist, medium, category, style,AuctionList where " + Subquery + " acution.artistid = artist.artistid and acution.mediumid = medium.mediumid and acution.categoryid=category.categoryid and AuctionList.AuctionId= acution.Online and acution.styleid=style.styleid and acution.online  = " + AuctionId + " AND AuctionList.status='Past' ORDER BY acution.productid OFFSET " + Page + " ROWS FETCH NEXT 20 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
                        return Listscheme;
                    }
                    else
                    {
                        var Listscheme = con.Query<Auction>("select acution.*, artist.firstname,artist.lastname,(case when len(artist.firstname + ' ' + artist.lastname) > 13 then left(ltrim(artist.firstname + ' ' + artist.lastname),13) + '...' else (artist.firstname + ' ' + artist.lastname) end) as name, medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId,AuctionList.auctionType from acution, artist, medium, category, style,AuctionList where acution.artistid = artist.artistid and acution.mediumid = medium.mediumid and acution.categoryid=category.categoryid and AuctionList.AuctionId= acution.Online and acution.styleid=style.styleid and acution.online  = " + AuctionId + " AND AuctionList.status='Past' ORDER BY acution.productid OFFSET " + Page + " ROWS FETCH NEXT 20 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
                        return Listscheme;
                    }
                }
                   catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }


        public double GetPastLotListCount(string Subquery, int AuctionId)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    if (Subquery.Length > 0)
                    {
                        var Listscheme = con.Query<double>("select COUNT(AuctionId)  from acution, artist, medium, category, style,AuctionList where " + Subquery + " acution.artistid = artist.artistid and acution.mediumid = medium.mediumid and acution.categoryid=category.categoryid and AuctionList.AuctionId= acution.Online and acution.styleid=style.styleid and acution.online  = " + AuctionId + "AND AuctionList.status='Past'", null, null, true, 0, CommandType.Text).SingleOrDefault();
                        return Listscheme;
                    }
                    else
                    {
                        var Listscheme = con.Query<double>("select COUNT(AuctionId) from acution, artist, medium, category, style,AuctionList where acution.artistid = artist.artistid and acution.mediumid = medium.mediumid and acution.categoryid=category.categoryid and AuctionList.AuctionId= acution.Online and acution.styleid=style.styleid and acution.online  = " + AuctionId + "AND AuctionList.status='Past'", null, null, true, 0, CommandType.Text).SingleOrDefault();
                        return Listscheme;
                    }

                }

                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }


            }
        }


        public List<Auction> GetAuctionAnalysis(int AuctionId)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetAuctionAnalysis");
                    para.Add("@AuctionId", AuctionId);
                    return con.Query<Auction>("CRUDPast", para, null, true, 0, CommandType.StoredProcedure).ToList();

                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }

            }
        }

        public int GetAuctionAnalysisTotalLots(int AuctionId)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetAuctionAnalysisTotalLots");
                    para.Add("@AuctionId", AuctionId);
                    value = con.Query<int>("CRUDPast", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();

                }

                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }
            }
            return value;
        }

        public int GetAuctionAnalysisSoldLots(int AuctionId)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetAuctionAnalysisSoldLots");
                    para.Add("@AuctionId", AuctionId);
                    value = con.Query<int>("CRUDPast", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }

            }
            return value;
        }

        public int GetAuctionAnalysisEstimate(int AuctionId)
        {
            var value = (dynamic)null;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetAuctionAnalysisEstimate");
                    para.Add("@AuctionId", AuctionId);
                    value = con.Query<int>("CRUDPast", para, null, true, 0, commandType: CommandType.StoredProcedure).SingleOrDefault();

                }

                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }

            }
            return value;
        }

        public List<Auction> GetAuctionAnalysisWinningValue(int AuctionId)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetAuctionAnalysisWinningValue");
                    para.Add("@AuctionId", AuctionId);
                    return con.Query<Auction>("CRUDPast", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }
                   catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }

    }
}