﻿using Astaguru.Models;
using Dapper;
using Repository.Lib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Astaguru.Services.UserService
{
    public class UpcomingAuctionService
    {
        Log log = new Log();
        public List<Auction> GetUpcomingAuctionList()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetUpcomingAuctionList");
                    return con.Query<Auction>("CRUDUpcoming", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }

            }
        }


        public List<Auction> GetUpcomingLotList(string Subquery, int Page, int AuctionId)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    if (Subquery.Length > 0)
                    {
                        var Listscheme = con.Query<Auction>("select acution.*, artist.firstname,artist.lastname,(case when len(artist.firstname + ' ' + artist.lastname) > 13 then left(ltrim(artist.firstname + ' ' + artist.lastname),13) + '...' else (artist.firstname + ' ' + artist.lastname) end) as name, medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId, AuctionList.auctionType,AuctionList.Slug from acution, artist, medium, category, style,AuctionList where " + Subquery + " acution.artistid = artist.artistid and acution.mediumid = medium.mediumid and acution.categoryid=category.categoryid and AuctionList.AuctionId= acution.Online and acution.styleid=style.styleid and acution.online = " + AuctionId + " AND AuctionList.status = 'Upcomming' ORDER BY acution.productid OFFSET " + Page + " ROWS FETCH NEXT 20 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
                        return Listscheme;
                    }
                    else
                    {
                        var Listscheme = con.Query<Auction>("select acution.*, artist.firstname,artist.lastname,(case when len(artist.firstname + ' ' + artist.lastname) > 13 then left(ltrim(artist.firstname + ' ' + artist.lastname),13) + '...' else (artist.firstname + ' ' + artist.lastname) end) as name, medium.medium, category.category,style.style,AuctionList.Auctionname,AuctionList.Date,AuctionList.AuctionId, AuctionList.auctionType,AuctionList.Slug from acution, artist, medium, category, style,AuctionList where acution.artistid = artist.artistid and acution.mediumid = medium.mediumid and acution.categoryid=category.categoryid and AuctionList.AuctionId= acution.Online and acution.styleid=style.styleid and acution.online = " + AuctionId + " AND AuctionList.status = 'Upcomming' ORDER BY acution.productid OFFSET " + Page + " ROWS FETCH NEXT 20 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
                        return Listscheme;
                    }
                }
                  catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public double GetUpcomingAuctionCount(string Subquery, int AuctionId)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    if (Subquery.Length > 0)
                    {
                        var Listscheme = con.Query<double>("select COUNT(acution.productid) from acution, artist, medium, category, style,AuctionList where " + Subquery + " acution.artistid = artist.artistid and acution.mediumid = medium.mediumid and acution.categoryid=category.categoryid and AuctionList.AuctionId= acution.Online and acution.styleid=style.styleid and acution.online = " + AuctionId + "AND AuctionList.status = 'Upcomming'", null, null, true, 0, CommandType.Text).SingleOrDefault();
                        return Listscheme;
                    }
                    else
                    {
                        var Listscheme = con.Query<double>("select COUNT(acution.productid) from acution, artist, medium, category, style,AuctionList where acution.artistid = artist.artistid and acution.mediumid = medium.mediumid and acution.categoryid=category.categoryid and AuctionList.AuctionId= acution.Online and acution.styleid=style.styleid and acution.online = " + AuctionId + " AND AuctionList.status = 'Upcomming'", null, null, true, 0, CommandType.Text).SingleOrDefault();
                        return Listscheme;
                    }
                }            
                      catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }

            }
        }

        public Auction GetProxyValue(int productid, int Userid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetProxyValue");
                    para.Add("@productid", productid);
                    para.Add("@userid", Userid);
                    return con.Query<Auction>("CRUDUpcoming", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
                
            }
        }


        public int getupcomingauctionid(string Slug)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString());
            try
            {
                int auctionid = 0;
                DataTable dt = new DataTable();
                SqlDataAdapter ad = new SqlDataAdapter("select AuctionId from AuctionList where status='Upcomming' and Slug='" + Slug + "'", con);
                ad.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    auctionid = int.Parse(dt.Rows[0]["AuctionId"].ToString());
                }
                return auctionid;
            }
          

             catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return 0;
            }
            finally
            {
                con.Close();
            }
        }




        public List<Auction> GetUpcomingAuctionListForHome()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetUpcomingAuctionListForHome");
                    return con.Query<Auction>("CRUDUpcoming", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }
                   catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }

    }
}