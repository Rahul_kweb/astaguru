﻿using Astaguru.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Astaguru.Services.UserService
{
    public class BlogService
    {
        public double GetBlogCount(string Subquery, string categoryid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                if (categoryid=="0")
                {
                    var Listscheme = con.Query<double>("select COUNT(BlogId) from Blog where IsActive=1", null, null, true, 0, CommandType.Text).SingleOrDefault();
                    return Listscheme;
                }
                else
                {
                    var Listscheme = con.Query<double>("select COUNT(BlogId) from Blog where IsActive=1 and Categoryid="+ categoryid + "", null, null, true, 0, CommandType.Text).SingleOrDefault();
                    return Listscheme;
                }
            }
        }


        public List<Blog> GetBlogList(string Subquery,int Page, string categoryid)
        {
   
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                if (categoryid == "0")
                {
                   
                    var Listscheme = con.Query<Blog>("select BlogId,Title,Image,CONVERT(varchar,PostDate,106) as PostDate,LEFT(Description, 250) + '...' as Description, IsActive, Slug, CreatedBy,Categoryid from Blog where IsActive=1  ORDER BY Blogid desc OFFSET " + Page + " ROWS FETCH NEXT 6 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
                    return Listscheme;
                }
                else
                {
                    var Listscheme = con.Query<Blog>("select BlogId,Title,Image,CONVERT(varchar,PostDate,106) as PostDate,LEFT(Description, 250) + '...' as Description, IsActive, Slug,CreatedBy, Categoryid from Blog where IsActive=1 and Categoryid=" + categoryid + " ORDER BY Blogid desc OFFSET " + Page + " ROWS FETCH NEXT 6 ROWS ONLY", null, null, true, 0, CommandType.Text).ToList();
                  return Listscheme;
                }
            }
        }
    }
}