﻿using Astaguru.Models.AdminModel;
using Dapper;
using Repository.Lib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Astaguru.Services.AdminService
{
    public class AdminUserService
    {
        Log log = new Log();
        public IEnumerable<AdminUser> GetLogin(string Username, string UserPassword)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetLogin");
                    para.Add("@Username", Username);
                    para.Add("@UserPassword", UserPassword);
                    return con.Query<AdminUser>("CRUDAdminUser", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }

            }
        }

        public int RefreshDotNetVersion()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "RefreshDotNetVersion");
                    return con.Query<int>("CRUDAdminUser", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                }

                      catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }
            }
        }
    }
}