﻿using Astaguru.Models;
using Dapper;
using Repository.Lib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Astaguru.Services.AdminService
{
    public class AdminUsersProfileService
    {
        Log log = new Log();
        public List<User> GetUserList()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "GetUserList");
                    return con.Query<User>("CRUDAdmin", para, null, true, 0, CommandType.StoredProcedure).ToList();
                }


                  catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }

            }
        }

        public User DisplayData(int userid)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "DisplayData");
                    para.Add("@userid", userid);
                    var value = con.Query<User>("CRUDAdmin", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                    return value;
                }
            
                  catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return null;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public int UpdateBidAmount(int userid, int amountlimt)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "UpdateBidAmount");
                    para.Add("@userid", userid);
                    para.Add("@amountlimt", amountlimt);
                    return con.Query<int>("CRUDAdmin", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                }
             
                    catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public int UpdateBidAccess(User UR)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                try
                {
                    var para = new DynamicParameters();
                    para.Add("@Mode", "UpdateBidAccess");
                    para.Add("@userid", UR.userid);
                    para.Add("@confirmbid", UR.confirmbid);
                    return con.Query<int>("CRUDAdmin", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                }        

                   catch (Exception ex)
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return 0;
                }
                finally
                {
                    con.Close();
                }
            }
        }
    }
}