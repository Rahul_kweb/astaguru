﻿using Astaguru.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Astaguru.Services
{
    public class Common
    {
        public static string baseUrlPath = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "";

        public static double getNextValidBidAmount(double price, int currenyType)
        {
            double nextValidBid = 0;
            if(currenyType == 1) //INR
            {
                if(price < 10000000) // 10%
                {
                    nextValidBid = getPercentage(price, 10);
                }
                else // 5%
                {
                    nextValidBid = getPercentage(price, 5);
                }
            }
            else if(currenyType == 2) //USD
            {
                if (price < 143750) // 10%
                {
                    nextValidBid = getPercentage(price, 10);
                }
                else // 5%
                {
                    nextValidBid = getPercentage(price, 5);
                }

            }
            else{
                if (price < 10000000) // 10%
                {
                    nextValidBid = getPercentage(price, 10);
                }
                else // 5%
                {
                    nextValidBid = getPercentage(price, 5);
                }
            }

            return Math.Round(nextValidBid);
        }

        public static double getCalculateMargin(double price, int currenyType, int percentage)
        {
            double CalculateMargin = 0;

            if (currenyType == 1) //INR
            {
                CalculateMargin = getPercentage(price, percentage);
            }
            else if (currenyType == 2) //USD
            {
                CalculateMargin = getPercentage(price, percentage);
            }
            else
            {
                CalculateMargin = getPercentage(price, percentage);
            }
            return Math.Round(CalculateMargin);
        }

        static double getPercentage(double amount, int percentage)
        {
            double percentageValue = 0, nextValidBid =0;
            percentageValue = amount * percentage;
            percentageValue = percentageValue / 100;
            nextValidBid = amount + percentageValue;

            return nextValidBid;
        }

        public static string encryption(String password)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] encrypt;
            UTF8Encoding encode = new UTF8Encoding();
            //encrypt the given password string into Encrypted data  
            encrypt = md5.ComputeHash(encode.GetBytes(password));
            StringBuilder encryptdata = new StringBuilder();
            //Create a new string by using the encrypted data  
            for (int i = 0; i < encrypt.Length; i++)
            {
                encryptdata.Append(encrypt[i].ToString());
            }
            return encryptdata.ToString();
        }

        public static String rupeeFormat(String value)
        {
            value = value.Replace(",", "");
            char lastDigit = value[(value.Length - 1)];
            String result = "";
            int len = value.Length - 1;
            int nDigits = 0;
            for (int i = len - 1; i >= 0; i--)
            {
                result = value[i] + result;
                nDigits++;
                if (((nDigits % 2) == 0) && (i > 0))
                {
                    result = "," + result;
                }
            }
            return (result + lastDigit);
        }

        public static String DollerFormat(String value)
        {
            value = value.Replace(",", "");
            char lastDigit = value[(value.Length - 1)];
            String result = "";
            int len = value.Length - 1;
            int nDigits = 1;
            for (int i = len - 1; i >= 0; i--)
            {
                result = value[i] + result;
                nDigits++;
                if (((nDigits % 3) == 0) && (i > 0))
                {
                    result = "," + result;
                }
            }
            return (result + lastDigit);
        }

        public List<Auction> GetCategory()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "GetCategory");
                return con.Query<Auction>("CRUDCommon", para, null, true, 0, CommandType.StoredProcedure).ToList();
            }
        }

        public static double getPaginationRoundupValue(double pagenationOffSet)
        {
            int value;
            if (int.TryParse(Convert.ToString(pagenationOffSet), out value))
            {
                // if value is in int
            }
            else
            {
                value = Convert.ToInt32(pagenationOffSet);
                if (pagenationOffSet > value)
                {
                    value++;
                }
                // if value in decimal
            }
            return value;

        }

        public Auction GetBannerDetails(int AuctionId)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "GetBannerDetails");
                para.Add("@AuctionId", AuctionId);
                return con.Query<Auction>("CRUDCommon", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
            }
        }

        public List<Auction> getRecordPriceArtwork()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "getRecordPriceArtwork");
                return con.Query<Auction>("CRUDCommon", para, null, true, 0, CommandType.StoredProcedure).ToList();
            }
        }


        public List<User> LoadCountryAuto()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "LoadCountryAuto");
                return con.Query<User>("CRUDCommon", para, null, true, 0, CommandType.StoredProcedure).ToList();
            }
        }


        public List<User> LoadStateAuto(int Country_id)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "LoadStateAuto");
                para.Add("@countryid", Country_id);
                return con.Query<User>("CRUDCommon", para, null, true, 0, CommandType.StoredProcedure).ToList();
            }
        }

        public List<User> LoadCityAuto(int State_id)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@Mode", "LoadCityAuto");
                para.Add("@stateid", State_id);
                return con.Query<User>("CRUDCommon", para, null, true, 0, CommandType.StoredProcedure).ToList();
            }
        }


        public List<Home> GetHomeBanner()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                return con.Query<Home>("spGetHomeBannerWeb", para, null, true, 0, CommandType.StoredProcedure).ToList();
            }
        }
        public User GetPassword(string emailId)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                para.Add("@pinEmail", emailId);
                var value = con.Query<User>("spGetPassword", para, null, true, 0, CommandType.StoredProcedure).SingleOrDefault();
                return value;
            }
        }


        public List<Auction> GetRecentAuctionList()
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Mystring"].ToString()))
            {
                var para = new DynamicParameters();
                var Listscheme = con.Query<Auction>("Select TOP 3 AuctionId,auctiondate, auctiontitle, b.recentAuctionBanner, Sum(a.pricers) As totalrs, Sum(a.priceus) As totalus FROM acution a INNER JOIN AuctionList b ON a.Online = b.AuctionId Where a.pricelow > 1 And a.pricers > a.pricelow AND status='Past' Group By b.AuctionId, b.auctiontitle, b.auctiondate, b.recentAuctionBanner Order By AuctionId DESC", null, null, true, 0, CommandType.Text).ToList();
                return Listscheme;

            }
        }


        //created on 01_07_2020  --- change next five proxy amount from 10% to 15%

        public static double getNextValidBidAmountForproxy(double price, int currenyType)
        {
            double nextValidBid = 0;
            if (currenyType == 1) //INR
            {
                if (price < 10000000) // 10%
                {
                    nextValidBid = getPercentage(price, 15);
                }
                else // 5%
                {
                    nextValidBid = getPercentage(price, 6);
                }
            }
            else if (currenyType == 2) //USD
            {
                if (price < 143750) // 10%
                {
                    nextValidBid = getPercentage(price, 15);
                }
                else // 5%
                {
                    nextValidBid = getPercentage(price, 6);
                }

            }
            else
            {
                if (price < 10000000) // 10%
                {
                    nextValidBid = getPercentage(price, 15);
                }
                else // 5%
                {
                    nextValidBid = getPercentage(price, 6);
                }
            }

            return Math.Round(nextValidBid);
        }

    }
}